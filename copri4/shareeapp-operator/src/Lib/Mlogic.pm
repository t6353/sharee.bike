package Mlogic;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Mod::DBtank;
use Tpl::CalReserv;
use Data::Dumper;

my $dbt = new DBtank;
my $calres = new CalReserv;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";
 $q->import_names('R');

 my $session="";
 my $session_and="";
 if($R::sessionid && length($R::sessionid) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }
 my $bgcolor1 = $dbt->{primary}->{$varenv->{dbname}}->{bgcolor1};

 #show it only in testmode
 if($users_sharee->{c_id} && $node_meta->{tpl_id} == 302004 && ($users_sharee->{c_id} eq $varenv->{superu_id} || $dbt->{copri_conf}->{stage} eq "test")){
  my $coo = $q->cookie('domcookie') || $q->param('sessionid') || "";
  my $api_test = "sharee_fr01"; my $bike="FR4781";# test
  print $q->div({-style=>'float:right;text-align:right;height:25px;padding:6px 15px;background-color:white'},$q->a({-style=>"background-color:#ffffff;color:#$bgcolor1;", -href=>"$varenv->{metahost}/src/scripts/tests/index.pl?sessionid=$coo\&api_test=$api_test\&bike=$bike\&user_test=$users_sharee->{txt08}", -target=>'_blank'}," [ tests --> $api_test ] "),"$users_sharee->{txt08}",$q->a({-style=>"color:#$bgcolor1;", -href=>"logout_sharee$session"},"logout")),"\n";
 }

  print "<div class='container'>\n";
    print "<div id='Contenttxt' style='margin-top:20px;padding-bottom:350px;'>\n";
      $self->tplselect($q,$node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
    print "</div>\n";
  print "</div>\n";
}

#2021-05-05 changed to Mlogic
sub tplselect(){
 my $self = shift;
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";
 $q->import_names('R');


 if($node_meta->{main_id}){
  if($node_meta->{tpl_id} == 2){
   require "Tpl/Anmelden.pm";
   &Anmelden::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }elsif($node_meta->{tpl_id} == 302 || $node_meta->{tpl_id} == 302008){
   require "Tpl/FormEdit.pm";
   &FormEdit::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }elsif($node_meta->{tpl_id} == 302004){
   require "Tpl/RentalData.pm";
   &RentalData::tpl($node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($node_meta->{tpl_id} == 308){
   require "Tpl/PayoneSelect.pm";
   &PayoneSelect::tpl($q,$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($node_meta->{tpl_id} == 314){
   #CalendarReserv CalReserv (ex. MapReserv_v2)
   $calres->tpl($q,$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
   #}elsif($node_meta->{tpl_id} == 315){
   #CalendarBikes CalReserv iframe
   #$calres->tpl($q,$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($node_meta->{tpl_id} == 197){
   require "Tpl/Contact.pm";
   &Contact::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }

print "<script src='$varenv->{metahost}/js/rentalator.js' type='text/JAVASCRIPT'></script>";

 }
  my $debug = "Mlogic --> (users_sharee->{c_id}: $users_sharee->{c_id} | ct_table: $node_meta->{ct_table} | parent_id: $node_meta->{parent_id} | main_id: $node_meta->{main_id} | tpl_id: $node_meta->{tpl_id})";
  #print $q->div({-style=>'position:fixed;bottom:0%;right:2%;z-index:10;font-size:13px;'},"$debug"),"\n" if($users_sharee->{c_id} eq $varenv->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");

}

1;


