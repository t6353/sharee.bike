package Anmelden;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $but = new Buttons;
 my $pri = new Pricing;
 my $dbh = "";
 my $script = $q->script_name();
 my $user_agent = $q->user_agent();

 my $path_info = $q->path_info();
 my $path = $path_info;
 my $clientIP = $q->remote_addr();

 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $tpl = $dbt->get_tpl($dbh,$node_meta->{tpl_id});
 my @tpl_order = split /,/,$tpl->{tpl_order};
 my $coo = $q->cookie(-name=>'domcookie') || "";
 
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
 my $bgcolor1 = "009899";#sharee
 
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

  print "<div id='ContentLogin'>\n";

  if($viewsel[1] eq "Account"){
    if($R::sharee_edit =~ /delete_account/){
	my $dummy;
    }elsif($R::sharee_edit =~ /password_forgotten/){
      $varenv->{cms}->{'iframe-email-sent'}->{txt} =~ s/::email::/$R::email/;
      print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-email-sent'}->{txt}"),"\n";  
    }
  }

  print $q->start_multipart_form(-id=>'authform', -name=>'loginscreen'),"\n";
  print $q->hidden(-name=>"merchant_id",-override=>1,-value=>"$varenv->{merchant_id}") if($varenv->{merchant_id});
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);
  print $q->hidden(-name=>"clientIP",-override=>1,-value=>"$clientIP");

  print "<div class='form-group'>\n";

  if($viewsel[1] eq "Account" && $R::sharee_edit =~ /delete_account/){
     if($R::sharee_edit eq "delete_account1" && $users_sharee->{c_id}){
	#
	my ($cttpos,$operator_hash) = $apif->user_rentals_history($q,$users_sharee);
        my $pricing = {};
        my $counting = {};
	my $cttpos_count = 0;
	my $sum = 0;
 	foreach my $id (keys(%$cttpos)){
          $cttpos_count++;
            if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
              ($pricing,$counting) = $pri->counting_rental($varenv,$cttpos->{$id});
              $sum += $pricing->{total_price};
            }else{
              my ($gesamt,$rabatt) = $pri->price2calc($cttpos->{$id});
              $sum += $gesamt;
            }
 	}
        $sum = $lb->round($sum);
        $sum = sprintf('%.2f', $sum);
        $sum =~ s/\./,/;

	if($cttpos_count > 0 && $sum > 0){
	  $varenv->{cms}->{'iframe-invoice-saldo'}->{txt} =~ s/::sum::/$sum/;
    	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-invoice-saldo'}->{txt}"),"\n";  
	  print $q->div({-style=>'margin-top:1em;text-align:center;'},$q->a({-class=>"btn btn-primary btn-lg btn-block",-href=>'javascript:history.back()', -style=>"border:1px solid #$bgcolor1;background-color:#$bgcolor1;color:#fffffe;"}, "back")),"\n";
	}else{
    	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-ask-account-delete'}->{txt}"),"\n";  
    	  print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='delete_account2' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;color:#fffffe;'>Yes</button>"),"\n";
	}
     }elsif($R::sharee_edit eq "delete_account2" && $users_sharee->{c_id}){
	my $rows = 0;
	$rows = $dbt->update_operatorsloop($varenv->{dbname},$users_sharee->{c_id},"delete");#only operator
	$rows += $dbt->delete_content("","contentadr",$users_sharee->{c_id});#final delete on primary
	if($rows >= 1){
      	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-goodby-account-delete'}->{txt}"),"\n";  
	}else{
      	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-failure-message'}->{txt}"),"\n";  
	}
     } 
  #Passwort senden
  }elsif($viewsel[1] eq "Account" && !$R::password_forgotten){
    print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-we-need-email'}->{txt}"),"\n";  
    print $q->label({-for=>'E-Mail'},""),"\n";
    print $q->textfield(-class=>'form-control', -name=>'email', -value=>'', -override=>1, -type=>'email',-class=>'form-control', -id=>'Email', -placeholder=>'E-Mail Adresse', -required=>1, -autofocus=>1),"\n";

    print $q->hidden(-name=>'password_forgotten',-value=>"1");
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='password_forgotten' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;color:#fffffe;'>$varenv->{cms}->{'iframe-send-pw-email'}->{txt}</button>"),"\n";
 
   #Login 
   }elsif(!$R::password_forgotten){
    if($R::conflict_failure){  
      print $q->div({-class=>'content2',-style=>'color:#c83434'},"$varenv->{cms}->{'iframe-uhps'}->{txt}"),"\n";
      print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-account-conflict'}->{txt}"),"\n";
      #Bitte beachten Sie: nach 60 Minuten verfallen unbestätigte Formulare und Zugangsdaten.
      print $q->div($q->a({-style=>"background-color:#ffffff;color:#$bgcolor1;font-size:1.1em;text-decoration:none;", -role=>'button', -href=>"$varenv->{wwwhost}/$varenv->{mandant}/Account?sessionid=$R::sessionid"}, "$varenv->{cms}->{'iframe-request-pw'}->{txt}")),"\n";
      print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-contact-us'}->{txt}"),"\n";
    }
    print $q->div({-class=>'content2',-style=>'font-size:1.2em;'}, "$varenv->{cms}->{'iframe-login'}->{txt}"),"\n";  
    print $q->div({-style=>'color:#c83434'},"Login verweigert. ",$q->a({-class=>"", -style=>"color:gray;", -role=>"button", -href=>"$varenv->{wwwhost}/$varenv->{mandant}/Account?sessionid=$R::sessionid"}, "$varenv->{cms}->{'iframe-request-pw'}->{txt}")),"\n" if($R::failure);
    print $q->div({-style=>'color:#c83434'},"Login verweigert. "),"\n" if($R::basicauthfailure);
    print $q->label({-for=>'Email'},""),"\n";
    print $q->textfield(-class=>'form-control', -name=>'user_id', -value=>'', -override=>1, -type=>'email',-class=>'form-control', -id=>'Email', -placeholder=>'E-Mail Adresse', -required=>1, -autofocus=>1),"\n";

    print $q->label({-for=>'PW'},""),"\n";
    print $q->password_field(-class=>'form-control', -name=>'user_pw', -value=>'', -override=>1, -type=>'password',-class=>'form-control', -id=>'txt04', -placeholder=>'Passwort', -required=>1),"\n";
    print $q->div({-style=>'text-align:left;color:grey;'}, "<input type='checkbox' onclick='show_passwd()'>", "$varenv->{cms}->{'iframe-show-pw'}->{txt}"),"\n";


   #js auth
   # print $q->div({-style=>'margin-top:1em;'},"<div type='text' onClick='javascript:request_apiauth(\"$varenv->{wwwhost}\",\"/$varenv->{mandant}/$varenv->{profile}\")' name='login_sharee' value='Login' class='btn btn-primary btn-lg btn-block'>Anmelden</div>"),"\n";
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='login_sharee' value='Login' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-login'}->{txt}</button>"),"\n";

      print $q->div({-style=>'margin-top:1em;text-align:center;'},$q->a({-class=>"btn btn-default btn-lg btn-block", -style=>"background-color:#ffffff;color:#$bgcolor1;", -role=>"button", -href=>"$varenv->{wwwhost}/$varenv->{mandant}/Account/$varenv->{accounting_1}?sessionid=$R::sessionid"}, "$varenv->{cms}->{'iframe-new-account'}->{txt}")),"\n";

      print $q->div({-style=>'margin-top:1em;text-align:center;'},$q->a({-class=>"", -style=>"background-color:#ffffff;color:#$bgcolor1;font-size:1.1em;text-decoration:underline;", -role=>"button", -href=>"$varenv->{wwwhost}/$varenv->{mandant}/Account?sessionid=$R::sessionid"}, "$varenv->{cms}->{'iframe-request-pw'}->{txt}")),"\n";

  }
  print "</div>\n";

  print $q->end_form,"\n";

  if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test"){
	my $project = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{project};

	print "<div style='position:fixed;bottom:5%;left:2%;z-index:10;font-size:13px;'>Project $project : \n";
  	print $q->a({-href=>"$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{agb_html}", -target=>'_blank'},"[AGB]"),"\n";
  	print $q->a({-href=>"$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{privacy_html}", -target=>'_blank'},"[Datenschutz]"),"\n";
  	print $q->a({-href=>"$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{impress_html}", -target=>'_blank'},"[Impressum]"),"\n";
  	print $q->a({-href=>"$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{tariff_info_html}", -target=>'_blank'},"[Tarif Info]"),"\n";
  	print $q->a({-href=>"$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{bike_info_html}", -target=>'_blank'},"[Bike Info]"),"\n";
	print "</div>\n";

     print $q->div({-style=>'position:fixed;bottom:2%;right:2%;z-index:10;font-size:13px;'},"--> $varenv->{syshost} | $varenv->{merchant_id} | $bgcolor1 | $node_meta->{tpl_name} | $users_sharee->{c_id}"),"\n";
  }
  print "</div>";
}
1;
