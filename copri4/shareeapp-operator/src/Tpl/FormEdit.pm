package FormEdit;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;

use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Tpl::AccountSubmenu;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $pri = new Pricing;
 my $but = new Buttons;
 my $submenu = new AccountSubmenu;
 my $path = $q->path_info();
 my $dbh = "";
 my $red = "red";

 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 my $session="";
 my $session_and="";
 if($R::sessionid && length($R::sessionid) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }

 my $bgcolor1 = "009899";#sharee
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

 my $project = "Freiburg";#defaults to sharee
 $project = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{project} if($varenv->{merchant_id} && $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{project});

 my $bonus_desc = "$varenv->{cms}->{'iframe-bonusnumber'}->{txt}";
 my $bonus_ak = "$varenv->{cms}->{'iframe-bonusnumber-accepted'}->{txt}";
 my $bonus_avail = "$varenv->{cms}->{'iframe-bonusnumber-not-available'}->{txt}";
 my $bonus_conflict = "$varenv->{cms}->{'iframe-bonusnumber-conflict'}->{txt}";
 my $bonus_saved = "$varenv->{cms}->{'iframe-enabled'}->{txt}:";

 #sharee.bike text
 if($project eq "Freiburg"){
   $bonus_desc = "$varenv->{cms}->{'iframe-activation-code'}->{txt}";
   $bonus_ak = "$varenv->{cms}->{'iframe-activation-code-info'}->{txt}";
   $bonus_avail = "$varenv->{cms}->{'iframe-activation-not-available'}->{txt}";
   $bonus_conflict = "$varenv->{cms}->{'iframe-activation-conflict'}->{txt}";
   $bonus_saved = "$varenv->{cms}->{'iframe-enabled'}->{txt}:";

 }

 my $ctadr = {};
 $ctadr = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});
 #
 #collect rentals on operators and get operator hash with dbname->uri_operator
 #$q->param(-name=>'month',-value=>"1"); 
 my ($cttpos,$operator_hash) = $apif->user_rentals_history($q,$ctadr);
 #print Dumper($cttpos); 

 my $cttpos_count=0;
 foreach my $id (keys(%$cttpos)){
	$cttpos_count++;
 }

 #loop operator hash to get invoices for each operator
 my $ctt_all = {};
 my $ctadrcoupon = {};
 foreach my $sharee_operator (keys (%$operator_hash)){
   my $dbh_operator = $dbt->dbconnect_extern("$sharee_operator");
   my $pref = {
        table    => "contenttrans",
        fetch    => "all",
	int10	 => "$ctadr->{c_id}",
        keyfield => "ct_name",
	ct_name  => "~::[1-9]",
        };


   if($ctadr->{c_id}){
     my $ctt = $dbt->fetch_tablerecord($dbh_operator,$pref);
     foreach my $id (keys(%$ctt)){
       $ctt->{$id}->{wwwhost} = "$operator_hash->{$sharee_operator}";
       $ctt->{$id}->{operator} = "$sharee_operator";
       $ctt->{$id}->{basedir} = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$sharee_operator}->{dir_app}";
       #print "$id|$sharee_operator|$ctt->{$id}->{wwwhost}|$ctt->{$id}->{operator}|$ctt->{$id}->{basedir}|$ctt->{$id}->{ct_name}<br>";
     }
     $ctt_all = { %$ctt_all, %$ctt };

     my $prefcoupon = {
        table   => "contentadr",
        fetch   => "one",
        txt15   => "~::\\w",
        c_id    => "$ctadr->{c_id}",
        };

     $ctadrcoupon->{$sharee_operator} = $dbt->fetch_tablerecord($dbh_operator,$prefcoupon);
     $ctadrcoupon->{$sharee_operator}->{oprefix} = "$dbt->{operator}->{$sharee_operator}->{oprefix}";
   }
 }

 my $tpl_id = $node_meta->{tpl_id};
 my $tpl = $dbt->get_tpl($dbh,$tpl_id);
 my $tpl01 = $tpl;#If nothing else, because of tpl_name Title in split_lates
 my $tpl02 = "";
 my @split_lates = ("$tpl->{tpl_order}");
 my $template01 = "$tpl_id" . "001";
 my $template02 = "$tpl_id" . "002";
 if($tpl_id < 999){
  $tpl01 = $dbt->get_tpl($dbh,$template01);
  $tpl02 = $dbt->get_tpl($dbh,$template02);
  @split_lates = ("$tpl01->{tpl_order}","$tpl02->{tpl_order}");
 }
 my $now_dt = strftime "%Y-%m-%d %H:%M", localtime;
 my $start_date = strftime "%d.%m.%Y %H:%M", localtime;

         my ($end_date,$end_time) = split(/ /,$now_dt);
         my ($e_yy,$e_mo,$e_dd) = split(/-/,$end_date);
         my ($e_hh,$e_mi) = split(/\:/,$end_time);

  my $sharee_agb_text = {};
  $sharee_agb_text->{ct_name} = $varenv->{cms}->{"iframe-form-$tpl_id.int14"}->{txt};
  $sharee_agb_text->{txt01} = "<iframe src='$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{agb_html}' style='width:100%;height:10000px;border:none;' scrolling='auto'></iframe>";
  my $sharee_privacy_text = {};
  $sharee_privacy_text->{ct_name} = "$varenv->{cms}->{'iframe-form-privacy'}->{txt}";
  $sharee_privacy_text->{txt01} = "<iframe src='$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{privacy_html}' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";

  print "<div id='Contentapp'>\n";

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_agb" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_agb_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_agb_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_privacy" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_privacy_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_privacy_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;



  #subMenue--------
  $submenu->tpl($node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  #-----------------

  my $debug=0;
  $debug=1 if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");
  my $debug_message = "";
  my $catch_failure=0;
  if(($R::failure && $R::failure =~ /\w+/) || ($ctadr->{txt31} && $ctadr->{txt31} =~ /\w/)){
	$debug_message = "($R::failure || $ctadr->{txt31})" if($debug);	
	$catch_failure=1;
	print $q->div({-class=>'content2', -style=>"clear:both;color:$red"}, "* $varenv->{cms}->{'iframe-one-error'}->{txt} $debug_message"),"\n";
  }

  if(1==1){
  
  my $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_2}";
  if($ctadr->{c_id} && $path =~ /$varenv->{accounting_2}|$varenv->{accounting_3}|$varenv->{profile}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_3}";
  }elsif($path =~ /$varenv->{accounting_1}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_1_5}";
  }elsif($ctadr->{c_id} && $path =~ /$varenv->{accounting_1}/){
    $postaction = "/$varenv->{mandant}/Account/$varenv->{accounting_2}";
  }

  print $q->start_form(-name=>'accountscreen', -action=>"$postaction$session"),"\n";
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);


  print "<div class='form-group' style='clear:both;'>\n";

  my $i=0;
  my @tpl_order;
  foreach(@split_lates){
   $i++;
   if($i==1){
	if($R::confirm_success){
   	  print $q->div({-class=>'content_title3'},"$varenv->{cms}->{'iframe-registration-confirmed'}->{txt}"),"\n";
	}else{
	  print $q->div({-class=>'content_title3'},"$varenv->{cms}->{$tpl01->{tpl_name}}->{txt}"),"\n";
	}

	if($path =~ /$varenv->{accounting_1}/){
	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-paymentdata-form'}->{txt}"),"\n";
	}
	if($path =~ /$varenv->{accounting_2}/){
	   if($tpl_id == 302008){
		print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-choose-payment'}->{txt}"),"\n";
	   }else{#old SEPA only style
		print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-sepa-mandat-prolog'}->{txt}"),"\n";
	   }
	}

   }
   if($i==2){
	print $q->div({-class=>'content_title3'},"$varenv->{cms}->{$tpl02->{tpl_name}}->{txt}"),"\n";
	print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-login-needs'}->{txt}"),"\n";

   }
   
    @tpl_order = split /,/,$_;
    my $scol = "itime";
    foreach (@tpl_order){
      my ($key,$des,$size) = split /=/,$_;
      $ctadr->{$key} = $q->unescapeHTML("$ctadr->{$key}");
      $ctadr->{$key} = $lb->newline($ctadr->{$key},"","1");

      #$des = "$tpl_id.$key" if($debug);
      $des = $varenv->{cms}->{"iframe-form-$tpl_id.$key"}->{txt} if($varenv->{cms}->{"iframe-form-$tpl_id.$key"}->{txt});
     

      my $label_des="* $des";
      my $autofocus="";
      if($key =~ /txt15|txt19/){
        $label_des="$des";
      }

      if($key =~ /txt/ && $size eq "select" && $key eq "txt10"){
	my $country = $lb->country_code();
	my $country_all = $lb->country_code_all();
	$ctadr->{$key} = "DE" if(!$ctadr->{$key});
	my @_valxx = ();
	foreach (sort { $country->{$a} cmp $country->{$b} } keys (%$country)){
   	  push @_valxx, "$_:$country->{$_}";
 	}
	foreach (sort { $country_all->{$a} cmp $country_all->{$b} } keys (%$country_all)){
   	  push @_valxx, "$_:$country_all->{$_}";
 	}

    	print $q->label({-for=>"$key",-style=>'padding-top:15px;'},"$label_des"),"\n";
	print $but->selector_class("$key","form-control","","$ctadr->{$key}",@_valxx);

      }elsif($key =~ /txt/ && $size eq "select"){
	my @_valxx = split(/,/,$varenv->{$des});
    	print $q->label({-for=>"$key",-style=>'padding-top:15px;'},"$label_des"),"\n";
	print $but->selector_class("$key","form-control","","$ctadr->{$key}",@_valxx);

      }elsif($key =~ /txt/){
        my $required="required";

	if($key eq "txt08" && $R::failure && $R::failure =~ /conflict_txt08/){
	  $autofocus = "autofocus";
	  my ($failkey,$failval) = split(/=/,$R::failure);
	  $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-attention-email-exist'}->{txt} $failval</span>";
	  $ctadr->{$key} = $R::conflict_txt08;
	}
	elsif(($R::failure && $R::failure =~ /^$key/) || ($ctadr->{txt31} && $ctadr->{txt31} =~ /$key/)){
	  $autofocus = "autofocus";
	  $varenv->{cms}->{'iframe-correct-input'}->{txt} =~ s/::input::/$des/;
	  $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-correct-input'}->{txt}</span>";
	}#end failure messages

        if($key eq "txt15"){
	  if($R::failure =~ /^txt15/){
	    $autofocus = "autofocus";
	    $bonus_desc = "<span style=color:$red>$bonus_avail</span>";
	  }
	  if($R::failure =~ /conflict_txt15/){
	   $bonus_desc = "<span style=color:$red>$bonus_conflict</span>";
	  }

	  my $saved_coupon = "";
	  if($project eq "Konstanz" && $ctadrcoupon->{sharee_kn}->{txt15}){
	    $bonus_desc = "$varenv->{cms}->{'iframe-bonusnumber-saved'}->{txt}";
	    $saved_coupon = "$ctadrcoupon->{sharee_kn}->{txt15}";
  	  }
	  if($project eq "Freiburg" && $ctadrcoupon->{sharee_bvb}->{txt15} && $ctadrcoupon->{sharee_bvb}->{txt15} !~ /-/){
	    $bonus_desc = "$varenv->{cms}->{'iframe-activation-code-saved'}->{txt}";
	    $saved_coupon = "$ctadrcoupon->{sharee_bvb}->{txt15}";
  	  }

    	  print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$bonus_desc"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' value='$saved_coupon' override $autofocus />\n";
	  print $q->div("&nbsp");
	  foreach my $opid (keys(%$ctadrcoupon)){
	   if($ctadrcoupon->{$opid}->{txt15}){
 	     print $q->div("$bonus_saved $ctadrcoupon->{$opid}->{oprefix} $ctadrcoupon->{$opid}->{txt15}"),"\n";
	   }
	  }
	
	}
	elsif($key eq "txt04"){
         if($R::failure && $R::failure =~ /confirm_txt04/){
	    $autofocus = "autofocus";
	    $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-repeat-pw-failure'}->{txt}</span>";
	 }
	 my $pw = "xxxxxxxx";
	 $pw = "" if(!$ctadr->{c_id});
    	 print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	 print "<input id='$key' type='password' class='form-control' name='$key' value='$pw' override $required $autofocus /><br />\n";
         print $q->label({-for=>"confirm_$key"},"* $varenv->{cms}->{'iframe-repeat-pw'}->{txt}"),"\n";
	 print "<input id='confirm_$key' type='password' class='form-control' name='confirm_$key' value='$pw' override $required />\n";
	 print $q->div({-style=>'text-align:left;color:grey;'}, "<input type='checkbox' onclick='show_passwd()'>", "$varenv->{cms}->{'iframe-show-pw'}->{txt}"),"\n" if($pw ne "xxxxxxxx");
	}else{
	 #all other input textfields
    	 print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	 print "<input id='$key' type='text' class='form-control' name='$key' value='$ctadr->{$key}' override $required $autofocus />\n";
	}
      }
      #end $key =~ /txt/

      #all int checkboxes disabled because of AGB downunder
      elsif($key =~ /int/ && $size eq "checkbox"){
        if(($R::failure && $R::failure =~ /^$key/) || ($ctadr->{txt31} && $ctadr->{txt31} =~ /$key/)){
	  $autofocus = "autofocus";
	  $varenv->{cms}->{'iframe-please-confirm'}->{txt} =~ s/::value::/$des/;
	  $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-please-confirm'}->{txt}</span>";
        }
	my $required="";

        #sharee AGB global new
        if($key eq "int14" && $size eq "checkbox"){
	  $required="required";
	  #bootstrap 5
	  my $sharee_agb = "<button type='button' class='btn btn-primary ' style='padding:1px 40px;border:1px solid #$bgcolor1;background-color:#$bgcolor1;' data-bs-toggle='modal' data-bs-target='#sharee_agb'>$des</button>\n";

	  print $q->label({-for=>"$key", -style=>'padding-top:20px;'},"$label_des"),"\n";
          print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctadr->{$key}","","$required","$autofocus"), "&nbsp; $sharee_agb"),"\n";
	  print $q->hidden(-name=>"$key",-override=>1,-value=>"null");

	  my $sharee_privacy = "<button type='button' class='btn btn-primary ' style='text-decoration:underline;color:#3f3f3f;border:1px solid white;background-color:white;' data-bs-toggle='modal' data-bs-target='#sharee_privacy'>$varenv->{cms}->{'iframe-form-privacy'}->{txt}</button>\n";
	  print $q->div({-style=>'padding-top:10px;'},"$sharee_privacy"),"\n";


        }elsif(1==2 && $key =~ /int02/){#newsletter
    	  	print $q->label({-for=>"$key", -style=>'padding-top:10px;'},""),"\n";
          	print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctadr->{$key}","","$required"), " $des"),"\n";
          	print $q->hidden(-name=>"$key",-override=>1,-value=>"0");
        }else{
    	  print $q->label({-for=>"$key", -style=>'padding-top:10px;'},""),"\n";
          print $q->div({-id=>"$key"},$but->checkbox("1","$key","$ctadr->{$key}","","$required"), " $des"),"\n";
          print $q->hidden(-name=>"$key",-override=>1,-value=>"0");
	}
     
      }elsif($key =~ /int/){
        if(($R::failure && $R::failure =~ /^$key/) || ($ctadr->{txt31} && $ctadr->{txt31} =~ /$key/)){
	   $autofocus = "autofocus";
	   $varenv->{cms}->{'iframe-please-confirm'}->{txt} =~ s/::value::/$des/;
	   $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-please-confirm'}->{txt}</span>";
	   print $q->label({-for=>"$key", -style=>'padding-top:10px;'},"$label_des"),"\n";
        }

	if($key =~ /int03/){

	  print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-choose-payment-method'}->{txt}"),"\n";
	
	  my $checked_sepa = "";
          my $checked_creditcard = "";
          my $checked_prepaid = "";

	  foreach my $p_id (sort keys (%{ $dbt->{copri_conf}->{payment_type} })) {
	    $checked_sepa = 1 if($ctadr->{$key} == 1);
	    $checked_creditcard = 2 if($ctadr->{$key} == 2);
	    $checked_prepaid = 3 if($ctadr->{$key} == 3);
	    if($p_id == 1){
	     print $q->div({ -class=>'radio', -style=>'padding-top:20px;' },$but->radiobox_vertical("$key","$p_id","$checked_sepa",$q->img({-id=>"pic-payment" ,-style=>'height:50px;margin:10px 0 0 10px;', -title=>"$dbt->{copri_conf}->{payment_type}->{$p_id}",-src=>"$varenv->{metahost}/img/payment-incasso2.png"}))),"\n";
	    }elsif($p_id == 2){
	     #print $q->div({ -class=>'radio', -style=>'padding-top:20px;' },$but->radiobox_vertical("$key","$p_id","$checked_creditcard",$q->img({-id=>"pic-payment" ,-style=>'height:25px;', -title=>"$dbt->{copri_conf}->{payment_type}->{$p_id}",-src=>"$varenv->{metahost}/img/payment-creditcard2.png"}))),"\n";
	     print $q->div({ -class=>'radio', -style=>'padding-top:20px;' },"<span style='text-decoration: line-through;font-size:1.5em;'>o</span>",$q->img({-id=>"pic-payment" ,-style=>'height:25px;', -title=>"$dbt->{copri_conf}->{payment_type}->{$p_id}",-src=>"$varenv->{metahost}/img/payment-creditcard2.png"}),"<br /><span style='font-size:0.91em;'>Kreditkarte ist aktuell deaktiviert</span>"),"\n";
	    }elsif($p_id == 3){
	     print $q->div({ -class=>'radio', -style=>'padding-top:25px;' },$but->radiobox_vertical("$key","$p_id","$checked_prepaid","<span style='padding-left:25px;font-size:1.2em;'>Prepaid</span>")),"\n" if($ctadr->{c_id} && $dbt->{copri_conf}->{betau_id}->{$ctadr->{c_id}});#testing by contributors
	    }
	  }
   	  print $q->hidden(-name=>"$key",-override=>1,-value=>"null");
	}
      }

      if($key eq "txt16" && $ctadr->{c_id}){
        print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_transact' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-save'}->{txt}</button>"),"\n";
      }
 
     }#end foreach (@tpl_order)
  }

   if(($R::failure && $R::failure =~ /\w+/ && $R::failure !~ /txt15|txt16/) || ($ctadr->{txt31} && $ctadr->{txt31} =~ /\w/)){
	print $q->div({-class=>'content2', -style=>"color:$red"}, "* $varenv->{cms}->{'iframe-please-check-profile'}->{txt}"),"\n";	
   } 

  if($path !~ /$varenv->{accounting_3}|$varenv->{profile}/){
   if($ctadr->{c_id}){
    my $button_name = "$varenv->{cms}->{'iframe-save'}->{txt}";
    $button_name = "$varenv->{cms}->{'iframe-next'}->{txt}" if($path =~ /$varenv->{accounting_1_5}|$varenv->{accounting_2}/);
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$button_name</button>"),"\n";
    if($path =~ /$varenv->{accounting_1}/){
    	print $q->div({-style=>'margin-top:3em;text-align:center;'},$q->a({-style=>"color:#$bgcolor1;font-size:1.2em;", -role=>"button", -href=>"$varenv->{wwwhost}/app/Account?sharee_edit=delete_account1$session_and"}, "$varenv->{cms}->{'iframe-ask-delete-account'}->{txt}")),"\n";
    }
   }else{
    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='create_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-next'}->{txt}</button>"),"\n";
   }
  }


  }#ende e-Mail iif

  print $q->div({-class=>'content2', -style=>'margin-top:2em;'}, "$varenv->{cms}->{'iframe-registration-contact-hotline'}->{txt}"),"\n" if($path =~ /$varenv->{accounting_1}|$varenv->{accounting_3}/);

  print "</div>\n";
  print $q->end_form,"\n";

  print $q->div({-style=>'position:fixed;bottom:2%;right:2%;z-index:10;font-size:13px;'},"--> $varenv->{syshost} | $varenv->{merchant_id} | $bgcolor1 | template -> $node_meta->{tpl_name},$tpl_id ($template01,$template02) | $users_sharee->{c_id}"),"\n" if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");

  print "</div>";
}
1;
