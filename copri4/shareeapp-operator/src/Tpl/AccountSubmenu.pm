package AccountSubmenu;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Mod::Basework;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $but = new Buttons;
 my $bw = new Basework;
 my $dbt = new DBtank;

 my $dbh = "";
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 #with meta_host, 
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }

 my $session="";
 if($R::sessionid && length($R::sessionid) > 20){
        $session = "?sessionid=$R::sessionid";
 }

 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);

 #my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid || "";
 my $coo = $R::authcookie || $R::sessionid || "";
 #my $merchant_id = ""; 
 #$merchant_id = $coo if($coo && length($coo) <= 10);
 #$merchant_id = $1 if($coo =~ /_(\w+)$/);

 my $bgcolor1 = "009899";#sharee
 my $hgcolor1 = "c0c0c0";#silver
 my $fgcolor1 = "ffffff";#white
 #my $bgcolor1 = "e2001a";
 
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $hgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{hgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{hgcolor1});
 $fgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{fgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{fgcolor1});

 #$bgcolor1 = $dbt->{merchant_ids}->{$merchant_id}->{bgcolor1} if($merchant_id && $dbt->{merchant_ids}->{$merchant_id}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});

 #payable check
 my $payable_check=0;
 $payable_check = $bw->isuser_rentable($users_sharee,$varenv);

 my ($node1,$rows) = $dbt->collect_node($dbh,$node_meta->{parent_id});

 my $margin_top = "0";
 if($dbt->{website}->{$varenv->{syshost}}->{layout} eq "iframe"){
  $margin_top = "40px";
 }


#bootstrap 5 with standalone bayern
if(1==1){

  #subMenue--------
  print "<style>
  ul#AccNavi {
   position:relative;
   margin-top:$margin_top;
   display:flex;
   margin-left: -32px;
  }
  ul#AccNavi li {
   list-style: none;
   display: inline;
   margin-right:20px;
  }
  ul#AccNavi li a {
   display:flex;
   text-align:center;
   align-itmes;center;
   justify-content:center;
   transition: all 0.3s ease;
   margin: 0;
   padding: 0em;
   height: 45px;
   background-color: #$bgcolor1;
   font-family:Arial,sans-serif;
   text-decoration: none;
   font-weight: normal;
   color: #fffffe;
  }
  ul#AccNavi li a img {
   width:50px;
  }
  ul#AccNavi a:hover {
   color: #fffffe;
   background-color: #$hgcolor1;
  }
  ul#AccNavi a:active {
   color: #fffffe;
   background-color: #$hgcolor1;
  }
  </style>\n";

  #print "<div style='margin-top:40px;'>\n";
  print "<ul id='AccNavi'>\n";
  foreach my $id1 (sort {$node1->{$a}->{n_sort} <=> $node1->{$b}->{n_sort}} keys (%$node1)){
    #Creditcard test switch for payone (tinkwwp)
      if($users_sharee->{c_id} && ($path =~ /$varenv->{mandant}\/$varenv->{profile}/ || $path =~ /$varenv->{mandant}\/Account/)){
	my $mstyle_1="";
     	my $mstyle_1_5="";
     	my $mstyle_2="";
     	my $mstyle_3="";
     	my $mstyle_4="";

	 if($node_meta->{main_id} == $node1->{$id1}->{main_id}){
	  $mstyle_1 .= "background-color: #$hgcolor1;" if("$node1->{$id1}->{node_name}" eq "$varenv->{accounting_1}");
	  $mstyle_1_5 .= "background-color: #$hgcolor1;" if("$node1->{$id1}->{node_name}" eq "$varenv->{accounting_1_5}");
	  $mstyle_2 .= "background-color: #$hgcolor1;" if("$node1->{$id1}->{node_name}" eq "$varenv->{accounting_2}");
	  $mstyle_3 .= "background-color: #$hgcolor1;" if("$node1->{$id1}->{node_name}" eq "$varenv->{accounting_3}");
	  $mstyle_4 .= "background-color: #$hgcolor1;" if("$node1->{$id1}->{node_name}" eq "$varenv->{accounting_4}");

	  #sharee AGB
    	  if(!$users_sharee->{int14}){
	   print $q->li($q->a({-style=>"$mstyle_1",-title=>"$varenv->{accounting_1}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kundendaten.svg"}))),"\n";
	  }
	  elsif(!$users_sharee->{int03}){
	   print $q->li($q->a({-style=>"$mstyle_1",-title=>"$varenv->{accounting_1}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kundendaten.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_1_5",-title=>"$varenv->{accounting_1_5}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1_5}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Zahlungsart.svg"}))),"\n";
	  }
	  elsif($payable_check){
	   print $q->li($q->a({-style=>"$mstyle_1",-title=>"$varenv->{accounting_1}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kundendaten.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_1_5",-title=>"$varenv->{accounting_1_5}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1_5}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Zahlungsart.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_2",-title=>"$varenv->{accounting_2}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_2}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kontoverbindung.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_3",-title=>"$varenv->{accounting_3}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_3}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Verleihdaten.svg"}))),"\n";
	   #Calendar Reservation beta
	   if($users_sharee->{c_id} && $dbt->{copri_conf}->{betau_id}->{$users_sharee->{c_id}}){
	     print $q->li($q->a({-style=>"$mstyle_4",-title=>"$varenv->{accounting_4}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_4}$session"}, $q->span({-class=>"bi bi-calendar3", -style=>"font-size: 2rem;width:3rem;padding-top:0.4rem;"}))),"\n";
   	   }
	  }
	  else{
	   print $q->li($q->a({-style=>"$mstyle_1",-title=>"$varenv->{accounting_1}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kundendaten.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_1_5",-title=>"$varenv->{accounting_1_5}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_1_5}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Zahlungsart.svg"}))),"\n";
	   print $q->li($q->a({-style=>"$mstyle_2",-title=>"$varenv->{accounting_2}", -href=>"/$viewsel[0]/Account/$varenv->{accounting_2}$session"}, $q->img({-src=>"$varenv->{metahost}/img/Account_Kontoverbindung.svg"}))),"\n";
	  }

	 }
      }

  }
  print "</ul>\n";
  #print "</div>\n";
  #-----------------
}
#print $q->div({-style=>'position:fixed;bottom:4%;right:2%;z-index:10;font-size:13px;'},"--> $varenv->{syshost} | $varenv->{merchant_id} | $bgcolor1 | template -> $node_meta->{tpl_name} | $users_sharee->{c_id}"),"\n" if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id} || $dbt->{copri_conf}->{stage} eq "test");
}
1;
