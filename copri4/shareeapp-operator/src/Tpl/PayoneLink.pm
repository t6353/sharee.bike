package PayoneLink;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Mod::Buttons;
use Mod::Basework;
use Mod::DBtank;
use Mod::Payment;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $tpl_id = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $q = new CGI;
 my $bw = new Basework;
 my $dbt = new DBtank;
 my $but = new Buttons;
 my $pay = new Payment;

 $q->import_names('R');
 my @keywords = $q->param;
 my $dbh = "";
 my $bgcolor1 = "009899";#sharee
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});
 my $aowner = "198";
 $aowner = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{id} if($varenv->{merchant_id} && $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{id});


 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 my $ctadr = { c_id => 0 };
 $ctadr = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});
 my $payable_check=0;
 $payable_check = $bw->isuser_rentable($ctadr,$varenv);
 my $prepaidhash = {};
 $prepaidhash = $pay->collect_prepaid($dbh,$ctadr) if($ctadr->{c_id});
 if(!$prepaidhash->{prepaid_id} && $ctadr->{c_id}){
   $feedb = $pay->prepaid_request($dbh,$ctadr,$aowner);
   $prepaidhash = $pay->collect_prepaid($dbh,$ctadr);
 }

  print $q->start_form(),"\n";
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid");

  print "<div class='form-group' style='clear:both;'>\n";
   print $q->div({-class=>'content_title3'},"$varenv->{cms}->{'iframe-prepaid-account'}->{txt}"),"\n";
   #print Dumper($prepaidhash);
   
   if($prepaidhash->{prepaid_id} || ($R::sharee_edit eq "generate_payonelink" && $R::prepaid_amount =~ /\d+/)){
     $prepaidhash->{prepaid_total} = sprintf('%.2f',$prepaidhash->{prepaid_total});
     $prepaidhash->{prepaid_total} =~ s/\./,/;
     $varenv->{cms}->{'iframe-prepaid-prolog'}->{txt} =~ s/\n/<br \/>/g;
     $varenv->{cms}->{'iframe-prepaid-prolog'}->{txt} =~ s/::prepaid_total::/<b>$prepaidhash->{prepaid_total}<\/b>/g;
     $varenv->{cms}->{'iframe-prepaid-prolog'}->{txt} =~ s/::app_name::/<b>$dbt->{merchant_ids}->{$varenv->{merchant_id}}->{app_name}<\/b>/g;
     $varenv->{cms}->{'iframe-prepaid-prolog'}->{txt} =~ s/::prepaid_id::/<b>$prepaidhash->{prepaid_id}<\/b>/g;
     print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-prepaid-prolog'}->{txt}"),"\n";
     
   }else{
     print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-failure-contact-hotline'}->{txt} $prepaidhash->{prepaid_id}"),"\n";
   }
      
     my $required="required";
     if($prepaidhash->{payone_link} || ($R::sharee_edit eq "generate_payonelink" && $R::prepaid_amount =~ /\d+/)){

       $varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt} =~ s/\n/<br \/>/g;
       $varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt} =~ s/::mail_datetime::/$prepaidhash->{mail_datetime}/g;
       $varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt} =~ s/::email::/$ctadr->{txt08}/g;
       $varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt} =~ s/::payone_link::/<b><a href='$prepaidhash->{payone_link}' target='_blank'>Test $prepaidhash->{payone_link}<\/a><\/b>/g if($users_sharee->{c_id} eq $dbt->{copri_conf}->{superu_id});
       $varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt} =~ s/::app_name::/<b>$dbt->{merchant_ids}->{$varenv->{merchant_id}}->{app_name}<\/b>/g;
       print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-prepaid-emailinfo'}->{txt}"),"\n";
       print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-next'}->{txt}</button>"),"\n";

     }else{
       my $label_des="$varenv->{cms}->{'iframe-prepaid-input'}->{txt}";
       print $q->label({-for=>"prepaid_amount", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
       print "<input id='prepaid_amount' type='text' class='form-control' name='prepaid_amount' override=1 value='0 EUR' $required />\n";

       print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='generate_payonelink' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-next-prepaid'}->{txt}</button>"),"\n";
     }

   print $q->div({-class=>'content2', -style=>'padding-top:50px;font-size:0.91em;'}, "$varenv->{cms}->{'iframe-prepaid-footer'}->{txt}"),"\n";

  print "</div>\n";
  print $q->end_form,"\n";

}
1;
