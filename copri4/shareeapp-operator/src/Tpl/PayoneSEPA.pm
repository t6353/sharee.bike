package PayoneSEPA;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Mod::Buttons;
use Mod::Basework;
use Mod::Shareework;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $tpl_id = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $q = new CGI;
 my $bw = new Basework;
 my $tk = new Shareework;
 my $dbt = new DBtank;
 my $but = new Buttons;

 $q->import_names('R');
 my @keywords = $q->param;
 my $dbh = "";
 my $bgcolor1 = "009899";#sharee
 $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} if($dbt->{website}->{$varenv->{syshost}}->{bgcolor1});
 $bgcolor1 = $dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1} if($dbt->{merchant_ids}->{$varenv->{merchant_id}}->{bgcolor1});
 my $red = "#c83434";

 my $coo = $q->cookie(-name=>'domcookie') || $R::sessionid;
 my $ctadr = $users_sharee if(ref($users_sharee) eq "HASH" && $users_sharee->{c_id});
 my $payable_check=0;
 $payable_check = $bw->isuser_rentable($ctadr,$varenv);

 my $tpl = $dbt->get_tpl($dbh,$tpl_id);
 my @tpl_order = split /,/,$tpl->{tpl_order};

  print $q->start_form(),"\n";
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid");
  print $q->hidden(-name=>"request",-override=>1,-value=>"managemandate");

  print "<div class='form-group' style='clear:both;'>\n";

   print $q->div({-class=>'content_title3'},"$varenv->{cms}->{'iframe-bank-account'}->{txt}"),"\n";
   print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-sepa-mandat-prolog'}->{txt}"),"\n";
   
   if($dbt->{primary}->{payment_provider} eq "payone" && $ctadr->{int03} == 1 && $ctadr->{ct_name} =~ /^\w{2}-\w+/ && !$ctadr->{int18}){
     print $q->div({-class=>'content2',-style=>"color:$red"}, "$varenv->{cms}->{'iframe-payAck-failure'}->{txt}"),"\n";
   }

   foreach (@tpl_order){
      my ($key,$des,$size) = split /=/,$_;
      $ctadr->{$key} = $q->unescapeHTML("$ctadr->{$key}");

      my $label_des="";
      $red = "#c83434";
      my $required="required";
      if($key eq "txt22"){
	  if($R::IBAN){
            $label_des = "<span style=color:$red>IBAN error</span>";
            $ctadr->{$key} = $R::IBAN;
	  }
          if($R::failure eq $key){
	    $varenv->{cms}->{'iframe-correct-input'}->{txt} =~ s/::input::/$des/;
            $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-correct-input'}->{txt}</span>";
          }
          if($R::failure eq "conflict_txt22" && $R::txt22){
	    $varenv->{cms}->{'iframe-sepa-conflict'}->{txt} =~ s/::input::/$R::txt22/;
            $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-sepa-conflict'}->{txt}</span>";
          }
          print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' override=1 value='$ctadr->{$key}' placeholder='$des' $required />\n";

      }elsif($key eq "txt23"){
          print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
	  print "<input id='$key' type='text' class='form-control' name='$key' override=1 value='$ctadr->{$key}' placeholder='$des' $required />\n";

      #------------------------
     }elsif($key =~ /int03/ && $size eq "checkbox"){
       if(!$payable_check || !-f "$varenv->{basedir}/pdfinvoice/SEPA-Lastschriftmandat-$varenv->{praefix}-$ctadr->{ct_name}.pdf"){
	 my $int03 = "";
	 #$int03 = $ctadr->{$key} if($ctadr->{$key} == 1);
	 print $q->div({-style=>'margin:10px 0;'},"$varenv->{cms}->{'iframe-sepa-mandat'}->{txt}"),"\n";

         if($ctadr->{txt27} !~ /active|pending/ && $R::failure eq $key){
	  $varenv->{cms}->{'iframe-please-confirm'}->{txt} =~ s/::value::/$des/;
          $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-please-confirm'}->{txt}</span>";
          print $q->label({-for=>"$key", -style=>'padding-top:10px;'},"$label_des"),"\n";
         }

         my $des = "$varenv->{cms}->{'iframe-sepa-mandat-accept'}->{txt}";
         print $q->div({-id=>"$key"},$but->checkbox("1","$key","$int03","","$required"), " $des"),"\n";
       }else{
	#if payone fails/error
	if($ctadr->{txt27} =~ /active|pending/){
	   if($ctadr->{ct_name} && $ctadr->{ct_name} =~ /\w{2}-\w+/ && $coo){
	     my $webtarget = "_blank";
	     my $dtext = "";
	     if($varenv->{syshost} =~ /app/){
	       $webtarget = "_self";
	       $dtext = "<br /><br />$varenv->{cms}->{'iframe-pdf-download-info'}->{txt}";
     	     }
	     if( -f "$varenv->{basedir}/pdfinvoice/SEPA-Lastschriftmandat-$varenv->{praefix}-$ctadr->{ct_name}.pdf"){
               print $q->div({-style=>'padding:10px;margin:10px 0;'},$q->a({-href=>"$varenv->{wwwhost}/FileOut?file=SEPA-Lastschriftmandat-$varenv->{praefix}-$ctadr->{ct_name}.pdf&sessionid=$coo", -target=>"$webtarget" , -type=>'application/octet-stream', -style=>'text-decoration:underline;font-size:1.1em;'}, $q->span({-class=>"bi bi-file-earmark-pdf"}), "SEPA-Lastschriftmandat.pdf"),"$dtext"),"\n";
	     }
	   }else{
	     print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;color:#c83434;'},"$varenv->{cms}->{'iframe-mandate-problem'}->{txt}"),"\n";
	   }
	}elsif($ctadr->{txt22}){ #if IBAN but no mandat_status
	   print $q->div({-style=>'padding:10px;margin:10px 0;border:1px solid silver;color:#c83434;'}, "$varenv->{cms}->{'iframe-mandate-error'}->{txt}"),"\n";

	}
       }
     }
   }

   print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block' style='border:1px solid #$bgcolor1;background-color:#$bgcolor1;'>$varenv->{cms}->{'iframe-next'}->{txt}</button>"),"\n";

  print "</div>\n";
  print $q->end_form,"\n";
}
1;
