package PayoneSelect;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use Tpl::AccountSubmenu;
use Tpl::PayoneSEPA;
use Tpl::PayoneCCclient;
use Tpl::PayoneLink;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";

 my $dbt = new DBtank;
 my $submenu = new AccountSubmenu;
 my $payDebit = new PayoneSEPA;
 my $payCC = new PayoneCCclient;
 my $payLink = new PayoneLink;

 print "<div id='Contentapp'>\n";

 $submenu->tpl($node_meta,$users_dms,$varenv,$users_sharee,$feedb);

  #Selected payment-type
 if(ref($users_sharee) eq "HASH" && $users_sharee->{int03}){
  if($users_sharee->{int03} == 1){
    $payDebit->tpl("309",$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($users_sharee->{int03} == 2){
    $payCC->tpl("310",$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($users_sharee->{int03} == 3){
    $payLink->tpl("313",$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }
 }

 print "</div>\n";

}
1;
