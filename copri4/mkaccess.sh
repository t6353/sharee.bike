#!/bin/bash

#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH

echo "YOur working directory:"
pwd

echo "Creating some empty directories:"
mkdir -vp shareedms-operator/cache
mkdir -vp shareedms-operator/csv
mkdir -vp shareedms-operator/data
mkdir -vp shareedms-operator/json
mkdir -vp shareedms-operator/pdf
mkdir -vp shareedms-operator/site
mkdir -vp shareedms-operator/xml

echo "Set up file access rights. Please also check if it meets your needs"

chgrp -R www-data *
chmod go-w *
chmod -R o-rwx *
for i in $(find . -type d -and -name csv); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name data); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name ftp); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name cache); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name pdf); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name xml); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name site); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name json); do chmod -R ug+rwx $i;done
for i in $(find . -type d -and -name shareeconf); do chmod -R go-rwx $i;done
for i in $(find . -type d -and -name shareeconf); do chmod -R ug+rx $i;done
for i in $(find . -type d -and -name sql); do chmod -R go-rwx $i;done
for i in $(find . -type d -and -name src); do chmod -R go-w $i;done
for i in $(find . -type d -and -name src); do chmod -R ug+x $i;done
for i in $(find . -type d -and -name img); do chmod -R go-w $i;done
for i in $(find . -type d -and -name apache); do chmod -R go-w $i;done
for i in $(find . -type d -and -name css); do chmod -R go-w $i;done
for i in $(find . -type d -and -name js); do chmod -R go-w $i;done
for i in $(find . -type f -and -iname README*); do chmod -R go-rwx $i;done
for i in $(find . -type d -and -name docs); do chmod -R go-rwx $i;done
for i in $(find . -type f -and -name startup.pl); do chmod -R go-w $i;done
for i in $(find . -type f -and -name robots.txt); do chmod -R go-wx $i;done
for i in $(find . -type d -and -name cronjobs); do chmod -R g-w $i/*;done
for i in $(find . -type d -and -name cronjobs); do chmod -R o+r $i/*;done
for i in $(find . -type d -and -name cronjobs); do chown -R root:root $i/*;done

echo "... done"


