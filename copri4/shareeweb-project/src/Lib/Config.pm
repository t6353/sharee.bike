package Config;

#Deprecated config file
#Please use shareeconf/*

use strict;
use warnings;
use CGI;
use Config::General;
my $q = new CGI;

sub new { 
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self; 
}


sub envonline(){
 my $self = shift;

 my $server_type = "website";
 my $server_key = "shareeweb-project";

 my $globalconf_file = "/var/www/copri4/shareeconf/global.cfg";
 my $conf = Config::General->new($globalconf_file);
 my %globalconf = $conf->getall;

   my $basedir = "$globalconf{copri_conf}->{basedir}/$globalconf{$server_type}{$server_key}->{dir_web}";
   my $metahost = $globalconf{$server_type}{$server_key}->{operatorWeb1};
   my %varenv = (    
		dbname => $globalconf{$server_type}{$server_key}{database}->{dbname},
		dbuser => $globalconf{$server_type}{$server_key}{database}->{user},
		dbpassw => $globalconf{$server_type}{$server_key}{database}->{passwd},
                dbhost => $globalconf{$server_type}{$server_key}{database}->{host},
                syshost => $globalconf{$server_type}{$server_key}->{dir_web},
                wwwhost => $globalconf{$server_type}{$server_key}->{operatorWeb},
                praefix => $globalconf{$server_type}{$server_key}{database}->{dbname},
		systype => "sharee",#(azn)
                mandant => $globalconf{$server_type}{$server_key}->{parent_node},
		start => $globalconf{shareeapp_conf}->{start},
		profile => $globalconf{shareeapp_conf}->{profile},
		accounting_1 => $globalconf{shareeapp_conf}->{accounting_1},
		accounting_1_5 => $globalconf{shareeapp_conf}->{accounting_1_5},
		accounting_2 => $globalconf{shareeapp_conf}->{accounting_2},
		accounting_3 => $globalconf{shareeapp_conf}->{accounting_3},
		accounting_4 => $globalconf{shareeapp_conf}->{accounting_4},
                superu_id => $globalconf{copri_conf}->{superu_id},
                debug => $globalconf{copri_conf}->{debug},
                logdir => $globalconf{copri_conf}->{logdir},
		metahost=>"$metahost",
		orga => "",
  		basedir => "$basedir",
                pdf => "$basedir/pdf",
                pdfinvoice => "$basedir/pdfinvoice",
                data => "$basedir/data",
		xmlfile => "$basedir/xml",
                printer => "PDF"
                );
   return %varenv;
}

1;

