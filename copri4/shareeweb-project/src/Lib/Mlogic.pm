package Mlogic;
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Mod::Buttons;
use Mod::DBtank;
use Data::Dumper;

my $dbt = new DBtank;
my $but = new Buttons;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";
 $q->import_names('R');

 my $user_agent = $q->user_agent();

 my $lang = "de";
 my $debug = 0;
 $debug = 1;
 my $dbh = "";

 my $session="";
 my $session_and="";
 if($R::sessionid && length($R::sessionid) > 20 && !$q->cookie(-name=>'domcookie')){
        $session = "?sessionid=$R::sessionid";
        $session_and = "&sessionid=$R::sessionid";
 }

 my $node = {};
 my $rows = 0;
 my $project = $dbt->{website}->{$varenv->{syshost}}->{project} || "Freiburg";#defaults to sharee

 my $bgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{bgcolor1} || "yellow";#button background
 my $hgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{hgcolor1} || "yellow";#hover background
 my $fgcolor1 = $dbt->{website}->{$varenv->{syshost}}->{fgcolor1} || "black";#font color
 my $parent_node = $dbt->{website}->{$varenv->{syshost}}->{parent_node} || "app";
 ($node,$rows) = $dbt->collect_noderel($dbh,$dbt->{website}->{$varenv->{syshost}}->{parent_id});

 #if($users_sharee->{c_id} eq $varenv->{superu_id} || $dbt->{copri_conf}->{stage} eq "test"){
 if($users_sharee->{c_id} eq $varenv->{superu_id}){
  my $coo = $q->cookie('domcookie') || $q->param('sessionid') || $dbt->{website}->{$varenv->{syshost}}->{merchant_id};
  #my $api_test = "sharee_kn"; my $bike="KN201";
  #my $api_test = "sharee_fr01"; my $bike="FR1538";
  my $api_test = "sharee_wue"; my $bike="WUE5524";
  #my $api_test = "sharee_sx"; my $bike="S3X1001";
  #my $api_test = "sharee_ren"; my $bike="REN2";
  
  print $q->div({-style=>'text-align:right;height:25px;padding:6px 15px;background-color:white'},$q->a({-style=>"background-color:#ffffff;color:#$bgcolor1;", -href=>"$varenv->{metahost}/src/scripts/tests/index.pl?sessionid=$coo\&api_test=$api_test\&bike=$bike", -target=>'_blank'}," [ tests --> $api_test ] ")),"\n";
 }

 my $site_padding = "0 0 20px 0";
 #if($dbt->{website}->{$varenv->{syshost}}->{layout} eq "iframe"){
 # $site_padding = "0";
 #}

  if(1==1){

  print "<style>
  ul#TopNavi {
   margin:25px 0;
   padding:$site_padding;
   vertical-align:top;
   border: 0px solid white;
  }
  ul#TopNavi li {
   list-style: none;
   display: inline;
   margin: 0 4px 0 0;
  }
  ul#TopNavi li a {
   transition: all 0.3s ease;
   margin: 0;
   padding: 1em 3%; 
   width:100px;
   background-color: #$bgcolor1;
   text-decoration: none;
   font-weight: normal;
   color: #fffffe;
  }
  ul#TopNavi a:hover {
   color: #fffffe;
   background-color: #$hgcolor1;
  }
  ul#TopNavi a:active {
   color: fffffe;
   background-color: #$hgcolor1;
  }

  ul#LeftNavi {
   position:fixed;
   margin:20px 0;
   width: 175px;
   height:100%;
   padding:$site_padding;
   vertical-align:top;
   border: 0px solid white;
  }
  ul#LeftNavi li {
   list-style: none;
   padding:1px;
  }
  ul#LeftNavi li a {
   display: block;
   transition: all 0.3s ease;
   margin: 0;
   padding: 1em 3%; 
   width:180px;
   background-color: #$bgcolor1;
   text-decoration: none;
   font-weight: normal;
   color: #fffffe;
  }
  ul#LeftNavi a:hover {
   color: #fffffe;
   background-color: #$hgcolor1;
  }
  ul#LeftNavi a:active {
   color: fffffe;
   background-color: #$hgcolor1;
  }

  </style>";

     my $lmenu0 = "";
     foreach my $id (sort {$node->{$a}->{n_sort} <=> $node->{$b}->{n_sort}} keys (%$node)){
       $node->{$id}->{node_path} = $node->{$id}->{node_name} if(!$node->{$id}->{node_path});
       my $mstyle = "";

       if($node->{$id}->{main_id} == $node_meta->{main_id} || $node->{$id}->{main_id} == $node_meta->{parent_id}){
        $mstyle = "background-color:#$hgcolor1;color:#fffffe;";
       }
       if($node->{$id}->{node_name} eq "Anmelden" && $node_meta->{parent_id} == 200010){
           $mstyle = "background-color:#$hgcolor1;color:#fffffe;";
       }

       if($node->{$id}->{node_name} eq "Anmelden" && $users_sharee->{c_id}){	
         $lmenu0 .= $but->lia_button("/$parent_node/$varenv->{profile}$session","Mein Profil",$node->{$id}->{main_id},"","$mstyle",$users_dms->{u_id});
       }else{
	   $lmenu0 .= $but->lia_button("/$parent_node/$node->{$id}->{node_path}",$node->{$id}->{node_name},$node->{$id}->{main_id},"","$mstyle",$users_dms->{u_id});
       }
      }

     #bootstrap 5 with standalone site
     if($dbt->{website}->{$varenv->{syshost}}->{layout} eq "standalone"){
       print "<div class='container' style='border:1px dotted silver;'>\n";
     }

     print "<div style='float:right;text-align:right;height:25px;padding:1em 0;'>\n";
     if($user_agent !~ /Android|Iphone/i && $project eq "Bayern"){
	     print "<span type='button' style='color:#3f3f3f;font-size:1em;padding-right:1em;width:8px;' onclick='change_fontsize1();'>A</span>\n";
	     print "<span type='button' style='color:#3f3f3f;font-size:1.2em;padding-right:1em;width:8px;' onclick='change_fontsize2();'>A</span>\n";
	     print "<span type='button' style='color:#3f3f3f;font-size:1.4em;padding-right:1em;width:8px;' onclick='change_fontsize3();'>A</span>\n";
	     print "<span type='button' style='color:#3f3f3f;font-size:1.3em;padding-right:1.2em;width:12px;' onclick='change_contrast();' class='bi bi-circle-half'></span>\n";
     }
	     
    if($users_sharee->{c_id} && $R::sharee_edit ne "delete_account2"){
  	print "<span style='padding-left:10px;'> | $users_sharee->{txt08}</span> <a style='background-color:#ffffff;color:#$bgcolor1;' href='logout_sharee$session'>logout</a>\n";
    }
    print "</div>\n";

    if($project eq "Bayern"){
      if($user_agent !~ /Android|Iphone/i){
        print $q->div({-style=>"text-align:left;width:100%;margin:1em 0;background-image: linear-gradient(to right, rgb(0, 156, 217) 300px, rgb(255,255,255));"},$q->img({-style=>'height:10em;', -src=>"$dbt->{website}->{$varenv->{syshost}}->{operatorWeb1}/img/Logo-LastenradBayern-01.png"})),"\n";
      }else{
        print $q->div({-style=>"text-align:left;width:100%;margin:1em 0;background-color:#$bgcolor1;"},$q->img({-style=>'height:10em;', -src=>"$dbt->{website}->{$varenv->{syshost}}->{operatorWeb1}/img/Logo-LastenradBayern-01.png"})),"\n";
      }
    }

     print "<div id='Contenttxt' style='margin:25px 0px 5px 0px;border:0px dotted #$fgcolor1;'>\n";
     print $q->div({-style=>"text-align:left;border:0px dotted #$fgcolor1;"},$q->ul({-id=>'TopNavi'},$lmenu0)),"\n";# if($project ne "Freiburg");
    
     if(($node_meta->{tpl_id} == 1 || $node_meta->{tpl_id} == 3) && $node_meta->{main_id} == 200017){
      require "Tpl/Karte_osm.pm";
      &Karte_osm::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
     }else{
      $self->tplselect($q,$node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
     }

 if($project ne "Freiburg"){

     print "<div style='margin-top:1em;border-top:1px dotted #$bgcolor1;background-color:#fff;width:100%;height:10px;text-align:right;'>\n";
     print "</div>\n";

  my $sharee_impress_text = {};
  $sharee_impress_text->{ct_name} = "Impressum";
  $sharee_impress_text->{txt01} = "<iframe src='$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{impress_html}' style='width:100%;height:1500px;border:none;' scrolling='auto'></iframe>";

  my $sharee_privacy_text = {};
  $sharee_privacy_text->{ct_name} = "Datenschutzhinweise";
  $sharee_privacy_text->{txt01} = "<iframe src='$varenv->{wwwhost}/$dbt->{project_conf}->{$project}->{privacy_html}' style='width:100%;height:8000px;border:none;' scrolling='auto'></iframe>";

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_impress" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_impress_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_impress_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;

print <<EOF
<!-- Modal -->
<div class="modal fade" id="sharee_privacy" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">$sharee_privacy_text->{ct_name}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="text-default">$sharee_privacy_text->{txt01}</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
    </div>
  </div>
</div>
EOF
;

     print $q->div({ -style=>"width:100%;text-align:right;"}, 
	"<button type='button' style='padding:0 2em;color:#$bgcolor1;border:1px solid #ffffff;background-color:#ffffff;' data-bs-toggle='modal' data-bs-target='#sharee_impress'>Impressum</button>
	<button type='button' style='padding: 0 2em;color:#$bgcolor1;border:1px solid #ffffff;background-color:#ffffff;' data-bs-toggle='modal' data-bs-target='#sharee_privacy'>Datenschutzhinweise</button>"
	),"\n";
 }

     print "</div>\n";#Contenttxt
     print "</div>\n" if($dbt->{website}->{$varenv->{syshost}}->{layout} eq "standalone");#container


  }#end if($project ne "Freiburg")

  #print "<script type='text/javascript' src='$varenv->{metahost}/js/iframeResizer.contentWindow.min.js'></script>\n";

  my $debug_footer = "Mlogic --> (syshost: $varenv->{syshost} | users_sharee->{c_id}: $users_sharee->{c_id} | ct_table: $node_meta->{ct_table} | parent_id: $node_meta->{parent_id} | main_id: $node_meta->{main_id} | tpl_id: $node_meta->{tpl_id} | $users_dms->{u_id})";
 print $q->div({-style=>'position:fixed;bottom:0%;left:2%;z-index:10;font-size:13px;'},"$debug_footer"),"\n" if($users_sharee->{c_id} eq $varenv->{superu_id});

}

#2021-05-05 changed to Mlogic
sub tplselect(){
 my $self = shift;
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift || "";
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";
 $q->import_names('R');

 my $lang = "de";
 my $tpl_id = $node_meta->{tpl_id};

 if($node_meta->{main_id}){
  if($tpl_id == 2){
   require "Tpl/Anmelden.pm";
   &Anmelden::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }elsif($tpl_id == 302 || $tpl_id == 302008){
   require "Tpl/FormEdit.pm";
   &FormEdit::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }elsif($tpl_id == 302004){
   require "Tpl/RentalData.pm";
   &RentalData::tpl($node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($tpl_id == 308){
   require "Tpl/PayoneSelect.pm";
   &PayoneSelect::tpl($q,$node_meta,$users_dms,$varenv,$users_sharee,$feedb);
  }elsif($tpl_id == 197){
   require "Tpl/Contact.pm";
   &Contact::tpl($node_meta,$users_dms,$mode,$varenv,$users_sharee,$feedb);
  }
 }

}

1;


