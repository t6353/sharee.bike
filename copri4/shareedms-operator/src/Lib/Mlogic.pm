package Mlogic;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use POSIX;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Config::General;
use Mod::Buttons;
use Mod::Basework;
use Mod::Shareework;
use Mod::Libenz;
use Mod::DBtank;
use Mod::Modalbox;
use Mod::Modalbox3;
use Data::Dumper;

my $bw = new Basework;
my $shwo = new Shareework;
my $lb = new Libenz;
my $dbt = new DBtank;
my $but = new Buttons;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $self = shift;
 my $q = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $users_sharee = shift || "";
 my $feedb = shift || "";
 $q->import_names('R');

 my %ib = $but->ibuttons();
 my $script = $q->script_name();
 my $path_info = $q->path_info();
 my $path = $path_info;
 if("$varenv->{metahost}"){
   $path = "$script" . "$path_info";
   $script="";
 }
 my $coo = $q->cookie(-name=>'domcookie');
 my $debug = "";
 my $dbh = "";
 my @viewsel = split /\//,$1 if($path =~ /^\/(.*)/);
 my $view_root = $viewsel[0] || "";
 my $parent_id = $node_meta->{parent_id};
 my $main_id = $node_meta->{main_id};
 my $red = "#c83434";

 my $coprihome = "c o p r i ";
 $coprihome .= " <span style='font-size:0.71em;'>home</span>" if($dbt->{primary}->{$varenv->{dbname}}->{title});
 my $title = "";
 $title .= $dbt->{primary}->{$varenv->{dbname}}->{title} if($dbt->{primary}->{$varenv->{dbname}}->{title});
 $title .= $dbt->{operator}->{$varenv->{dbname}}->{title} if($dbt->{operator}->{$varenv->{dbname}}->{title});
 $title .= " | " . $dbt->{operator}->{$varenv->{dbname}}->{oprefix} if($dbt->{operator}->{$varenv->{dbname}}->{oprefix});
 $title .= $dbt->{website}->{$varenv->{syshost}}->{title} if($dbt->{website}->{$varenv->{syshost}}->{title});
 $title .= " (devel $varenv->{dbname})" if($dbt->{copri_conf}->{stage} eq "test");

 #Ack codes or pw renew-prompt
 if($users_dms->{u_id} && $users_sharee->{c_id} && (!$users_sharee->{int04} || !$users_sharee->{int13} || $users_sharee->{int06})){
  print "<div style='position:fixed;z-index:10;right:0px;'>\n";
  print $q->div({-id=>'Headerlogin'},$q->a({-class=>'elinkbutton1',-href=>"$dbt->{primary}->{sharee_primary}->{primaryApp}",-target=>'_blank'},"$users_sharee->{txt08} "), $q->span({-id=>"owner", -style=>"color:silver"}, "($users_dms->{u_id})")),"\n";
  print $q->div({-id=>'Headerlogin'},$q->a({-href=>"logout",-title=>'logout'},$q->span({-class=>"bi bi-door-open", -style=>'font-size:1em;color:white;padding-right:1em;'}))),"\n";
  print "</div>\n";

  print "<div id='ContentLogin'>\n";
  print $q->start_multipart_form(-id=>'authform', -name=>'loginscreen'),"\n";
  print $q->hidden(-name=>"merchant_id",-override=>1,-value=>"$varenv->{merchant_id}") if($varenv->{merchant_id});
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);

  print "<div class='form-group'>\n";
  print $q->div({-class=>'content2', -nowrap=>1}, $q->img({ -style=>"height:25px",-src=>"$varenv->{head_logo}"}),"Operator Login – $title"),"\n";

  #pw renewe-prompt
  if($users_sharee->{int06}){
    my $key = "txt04";
    my $des = $varenv->{cms}->{"iframe-form-302.$key"}->{txt};
    my $label_des="* $des";
    print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-renew-pw'}->{txt}"),"\n";

    if($R::failure && $R::failure =~ /confirm_txt04/){
        $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-repeat-pw-failure'}->{txt}</span>";
    }
    print $q->label({-for=>"$key", -style=>'padding-top:1.5em;'},"$label_des"),"\n";
    print "<input id='$key' type='password' class='form-control' name='$key' value='' override required autofocus /><br />\n";
    print $q->label({-for=>"confirm_$key"},"* $varenv->{cms}->{'iframe-repeat-pw'}->{txt}"),"\n";
    print "<input id='confirm_$key' type='password' class='form-control' name='confirm_$key' value='' override required />\n";
    print $q->div({-style=>'text-align:left;color:grey;'}, "<input type='checkbox' onclick='show_passwd()'>", "$varenv->{cms}->{'iframe-show-pw'}->{txt}"),"\n";
    print $q->div({-style=>'margin-top:2em;text-align:center;'},"<button type='submit' name='sharee_edit' value='save_account' class='btn btn-primary btn-lg btn-block ebutton4' style='width:300px;'>Passwort speichern</button>"),"\n";

  }elsif(!$users_sharee->{int04} || !$users_sharee->{int13}){
    my $acktext = "$varenv->{cms}->{'iframe-confirmation-codes-sent'}->{txt}";
    $acktext = "$varenv->{cms}->{'iframe-sms-confirmation-code-sent'}->{txt}" if($users_sharee->{int04} && !$users_sharee->{int13});
    $acktext = "$varenv->{cms}->{'iframe-email-confirmation-code-sent'}->{txt}" if(!$users_sharee->{int04} && $users_sharee->{int13});
    print $q->div({-class=>'content2'}, "$acktext $varenv->{cms}->{'iframe-successful-confirmation'}->{txt}"),"\n";
    if(!$users_sharee->{int04}){
      my $des = "$varenv->{cms}->{'iframe-email-code'}->{txt}";
      my $key = "confirm_code";
      my $label_des="* $des";
      $varenv->{cms}->{'iframe-correct-input'}->{txt} =~ s/::input::/$des/;
      $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-correct-input'}->{txt}</span>" if($R::failure);
      print $q->label({-for=>"$key", -style=>'padding-top:1em;'},"$label_des"),"\n";
      print "<input id='$key' type='text' class='form-control' name='$key' value='' autofocus/>\n";
      print $q->div({-class=>'content2'}, " $varenv->{cms}->{'iframe-if-email-code-not-received'}->{txt}",$q->a({-class=>'elinkbutton4',-href=>"$varenv->{wwwhost}?sharee_edit=send_email"},"$varenv->{cms}->{'iframe-request-email-code'}->{txt}")),"\n";
    }else{
      print $q->div({-class=>'content2', -style=>'color:gray;'}, "* $varenv->{cms}->{'iframe-email-code-already-entered'}->{txt}"),"\n";
    }
    if(!$users_sharee->{int13}){
      my $des = "$varenv->{cms}->{'iframe-sms-code'}->{txt}";
      my $key = "confirm_smscode";
      my $label_des="* $des";
      $varenv->{cms}->{'iframe-correct-input'}->{txt} =~ s/::input::/$des/;
      $label_des = "<span style=color:$red>$varenv->{cms}->{'iframe-correct-input'}->{txt}</span>" if($R::failure);
      print $q->label({-for=>"$key", -style=>'padding-top:1em;'},"$label_des"),"\n";
      print "<input id='$key' type='text' class='form-control' name='$key' value='' autofocus />\n";
      print $q->div({-class=>'content2'}, " $varenv->{cms}->{'iframe-if-sms-code-not-received'}->{txt}",$q->a({-class=>'elinkbutton4',-href=>"$varenv->{wwwhost}?sharee_edit=send_sms"},"$varenv->{cms}->{'iframe-request-sms-code'}->{txt}")),"\n";
    }else{
      print $q->div({-class=>'content2', -style=>'color:gray;'}, "* $varenv->{cms}->{'iframe-sms-code-already-entered'}->{txt}"),"\n";
    }
    print $q->div({-style=>'margin-top:2em;text-align:center;'},"<button type='submit' name='confirm_userid' value='$users_sharee->{c_id}' class='btn btn-primary btn-lg btn-block ebutton4' style='width:300px;'>Weiter</button>"),"\n";
  }
  print "</div>\n";
  print $q->end_form,"\n";
  print "</div>";
 }
 #mainlogin
 elsif(!$users_dms->{u_id}){

  print "<div id='ContentLogin'>\n";
  print $q->start_multipart_form(-id=>'authform', -name=>'loginscreen'),"\n";
  print $q->hidden(-name=>"merchant_id",-override=>1,-value=>"$varenv->{merchant_id}") if($varenv->{merchant_id});
  print $q->hidden(-name=>"sessionid",-override=>1,-value=>"$R::sessionid") if($R::sessionid);

  print "<div class='form-group'>\n";
  print $q->div({-class=>'content2', -nowrap=>1}, $q->img({ -style=>"height:25px",-src=>"$varenv->{head_logo}"}),"Operator Login – $title"),"\n";

  #ask email
  if($R::coccessor eq "pwmail"){
    print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-we-need-email'}->{txt}"),"\n";
    print $q->label({-for=>'E-Mail'},""),"\n";
    print $q->textfield(-class=>'form-control', -name=>'email', -value=>'', -override=>1, -type=>'email',-class=>'form-control', -id=>'Email', -placeholder=>'E-Mail Adresse', -required=>1, -autofocus=>1),"\n";

    print $q->div({-style=>'margin-top:1em;text-align:center;'},"<button type='submit' name='sharee_edit' value='password_forgotten' class='btn btn-primary btn-lg btn-block ebutton4'>$varenv->{cms}->{'iframe-send-pw-email'}->{txt}</button>"),"\n";

 
  }
  #login
  else{
    print $q->div({-style=>'color:#c83434'},"Login failure"),"\n" if("$R::login_dms" eq "Login" && !$users_dms->{u_id});
    if($R::sharee_edit eq "password_forgotten"){
      $varenv->{cms}->{'iframe-email-sent'}->{txt} =~ s/::email::/$R::email/;
      print $q->div({-class=>'content2'}, "$varenv->{cms}->{'iframe-email-sent'}->{txt}"),"\n";
    }
    print $q->label({-for=>'Userid'},""),"\n";
    print $q->textfield(-class=>'form-control', -name=>'user_id', -value=>'', -override=>1, -type=>'email',-class=>'form-control', -id=>'Userid', -placeholder=>'NutzerIn', -required=>1, -autofocus=>1),"\n";
    print $q->label({-for=>'PW'},""),"\n";
    print $q->password_field(-class=>'form-control', -name=>'user_pw', -value=>'', -override=>1, -type=>'password',-class=>'form-control', -id=>'PW', -placeholder=>'Passwort', -required=>1),"\n";
    print $q->div({-style=>'margin:1em 0;'},"<button type='submit' name='login_dms' value='Login' class='btn btn-primary btn-lg btn-block ebutton4' style='width:300px;'>Login</button>"),"\n";

    print $q->div({-class=>'content2'},$q->a({-class=>'elinkbutton4', -href=>"?coccessor=pwmail"}, "Neues Passwort anfordern")),"\n";
  }
  print "</div>\n";
  print $q->end_form,"\n";
  print "</div>";
 }
 elsif($users_dms->{u_id} && $main_id){

  ###header start
  print "<div style='position:fixed;z-index:10;left:0px;width:100%;height:24px;background-color:#5c5c5c;'>\n";
  print "<div style='position:fixed;z-index:10;right:0px;'>\n";

  if($users_dms->{c_id4trans} && $users_dms->{tpl_id4trans}){
     my $table = "contenttrans";

    my $limit = $R::limit || $varenv->{limit};
    my $offset = $R::offset || "0";
    if($R::go && $R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go && $R::go eq "forward_list"){
        $offset += $limit;
    }
     
    my $pref = {
        table => "contenttrans",
        fetch => "one",
        template_id => $users_dms->{tpl_id4trans},
        c_id => $users_dms->{c_id4trans},
    };
    my $ctrel = $dbt->fetch_record($dbh,$pref);

     print $q->div({-id=>'Headerlogin',-style=>"background-color:#f7ae37;"},$q->a({-class=>'elinkbutton1',-title=>"Faktura Terminal öffnen",-href=>"$varenv->{wwwhost}/DMS/Faktura?ct_trans=open\&c_id4trans=$users_dms->{c_id4trans}\&tpl_id4trans=$users_dms->{tpl_id4trans}\&owner=$users_dms->{owner}\&offset=$offset\&limit=$limit\&relids=$R::relids\&no_redirect=1"}," Faktura \#$ctrel->{ct_name} $ctrel->{txt01}", $q->span({-id=>"c_id4trans", -style=>"color:#f7ae37"}, "$users_dms->{c_id4trans}"))) if($ctrel->{ct_name});
  }

   print $q->div({-id=>'Headerlogin'},$q->a({-class=>'elinkbutton1',-href=>"$dbt->{primary}->{sharee_primary}->{primaryApp}",-target=>'_blank'},"$users_sharee->{txt08} "), $q->span({-id=>"owner", -style=>"color:silver"}, "($users_dms->{u_id})")),"\n";
   print $q->div({-id=>'Headerlogin'},$q->a({-href=>"logout",-title=>'logout'},$q->span({-class=>"bi bi-door-open", -style=>'font-size:1em;color:white;padding-right:1em;'}))),"\n";
  print "</div>";
  print "</div>";
  ###header end

     
   if($main_id >= "100000"){
    my $mstyle=""; 

    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");
    #print Dumper($users_dms_primary);

    my $mod_active = "$dbt->{shareedms_conf}->{mod_active}";
    if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int02} >= 1){
      $mod_active .= "|Kunden";
    }else{
      $mod_active .= "|App-feedback";
    }
    if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int03} >= 1){
      $mod_active .= "|Faktura";
    }
    if($users_dms->{u_id} && ($users_dms->{int07} >= 1 || $users_dms->{int08} >= 1)){
      $mod_active .= "|Einstellung";
    }

    ###Top Menu
    #bootstrap menue
    print "<nav class='navbar navbar-expand-md navbar-dark bg-dark fixed-top' style='top:23px;opacity:0.85;'>\n";
    print "<div class='container-fluid'>\n";
    print "<button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarsDefault' aria-controls='navbarsDefault' aria-expanded='false' aria-label='Toggle navigation'>\n";
      print "<span class='navbar-toggler-icon'></span>\n";
    print "</button>\n";
    print $q->div({-id=>'Headerlogo'},$q->a({-class=>'navbar-brand',-style=>"color:black;",-href=>"$varenv->{wwwhost}"}, $q->img({ -style=>"height:25px",-src=>"$varenv->{head_logo}"}), "$coprihome" ,$q->div({-style=>'font-size:0.71em;'}, "$title"))),"\n";
    my ($node,$rows) = $dbt->collect_noderel($dbh,$dbt->{shareedms_conf}->{parent_id},"");

    print "<div class='collapse navbar-collapse' id='navbarsDefault'>\n";
    print "<ul class='navbar-nav me-auto mb-2 mb-md-0' style='padding:0 20px;text-align:left;'>\n";
    #print "<ul class='nav navbar-nav'>\n";

    foreach my $id (sort {$node->{$a}->{n_sort} <=> $node->{$b}->{n_sort}} keys (%$node)){
      $node->{$id}->{node_path} = $node->{$id}->{node_name} if(!$node->{$id}->{node_path});
      my $lmenu0 = "";
      my $url = "";
      my $mclass = "";
      my $mstyle = "";
      if("$node->{$id}->{node_name}" eq "$viewsel[1]"){
      	$mclass = "active";
	$mstyle = "color:white;";
        $parent_id = $node->{$id}->{parent_id};
        $main_id = $node->{$id}->{main_id};
      }

      if($node->{$id}->{main_id} && $node->{$id}->{node_name} =~ /$mod_active/){
        my $topath = "/$viewsel[0]/$node->{$id}->{node_path}";
	#
	#0. menue
	my $aclass = "dropdown-item";
	$aclass = "nav-link" if($node->{$id}->{node_name} =~ /journal|Karte/);
	$aclass = "nav-link" if($node->{$id}->{node_name} =~ /journal|Karte|App-feedback/ && $mod_active !~ /Kunden/);
	if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
		my $url = "$topath?node2edit=edit_relation\&main_id=$node->{$id}->{main_id}";
		$lmenu0 = $but->event_button("$topath","$node->{$id}->{node_name}","$node->{$id}->{main_id}","$aclass $mclass","","$users_dms->{u_id}","$url");
	}else{
              	$lmenu0 = $but->lo_button("$topath","$node->{$id}->{node_name}","$node->{$id}->{main_id}","$aclass $mclass","","$users_dms->{u_id}");
	}

        #1. submenu
        my $subs1=0;
        my ($node1,$rows1) = $dbt->collect_noderel($dbh,$node->{$id}->{main_id},"");
	foreach my $id1 (sort {$node1->{$a}->{n_sort} <=> $node1->{$b}->{n_sort}} keys (%$node1)){
	    $subs1=1 if($node1->{$id1}->{main_id});
	}
                
	#1. submenue
	if($subs1){
          print "<li class='nav-item dropdown'>\n";#with integrated event_button
	   print "<a class='nav-link dropdown-toggle $mclass' href='#' style='' id='navbarDropdown' role='button' data-bs-toggle='dropdown' aria-expanded='false'> $node->{$id}->{node_name} </a>\n";
          print "<ul class='dropdown-menu' aria-labelledby='navbarDropdown'>\n";
          print "<li class='nav-item'> $lmenu0 </li><li><hr class='dropdown-divider'></li>\n";
          my $lmenu1 = "";
          foreach my $id1 (sort {$node1->{$a}->{n_sort} <=> $node1->{$b}->{n_sort}} keys (%$node1)){
	   #hide Operator- and Kunden-Faktura config or be primary admin
	   if(($node1->{$id1}->{template_id} !~ /196|201/) || ($users_dms_primary->{u_id} && $users_dms_primary->{int02} >= 1)){
            my ($node2,$rows2) = $dbt->collect_noderel($dbh,$node1->{$id1}->{main_id},"");
            $topath = "/$viewsel[0]/$node->{$id}->{node_name}";
     	    $mclass = "";
            $mstyle = "";
      	    if("$node1->{$id1}->{node_name}" eq "$viewsel[2]"){
        	$mclass = "active";
        	$mstyle = "color:white;";
	    }
	    if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
		my $url = "$topath/$node1->{$id1}->{node_path}?node2edit=edit_relation\&main_id=$node1->{$id1}->{main_id}";
		$lmenu1 = $but->event_button("$topath/$node1->{$id1}->{node_path}","$node1->{$id1}->{node_name}","$node1->{$id1}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}","$url");
	    }else{
              	$lmenu1 = $but->lo_button("$topath/$node1->{$id1}->{node_path}","$node1->{$id1}->{node_name}","$node1->{$id1}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}");
	    }

	    print "<li class='nav-item'> $lmenu1 </li>\n";

	    #2. submenu (services content)
	    if(ref($node2) eq "HASH"){
	      my $lmenu2;
              foreach my $id2 (sort {$node2->{$a}->{n_sort} <=> $node2->{$b}->{n_sort}} keys (%$node2)){
		my $noderef = {
                 main_id => $node2->{$id2}->{parent_id},
		 content_id => 'null',
                 fetch  => 'one',
                };
  		my $parent_node4rel = $dbt->fetch_rel4tpl4nd($dbh,$noderef);
		if($parent_node4rel->{template_id} =~ /205|225|194/){
              	  $mclass = "";
                  $mstyle = "";
                  if("$node2->{$id2}->{node_name}" eq "$viewsel[3]"){
                    $mclass = "active";
                    $mstyle = "color:white;";
                  }
                  if($users_dms->{u_id} && ($users_dms->{int01} == 2 && $node_meta->{ct_table} =~ /content$/) || ($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id})){
                    my $url = "$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}?node2edit=edit_relation\&main_id=$node2->{$id2}->{main_id}";
                    $lmenu2 = $but->event_button("$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}","$node2->{$id2}->{node_name}","$node2->{$id2}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}","$url");
                  }else{
                    $lmenu2 = $but->lo_button("$topath/$node1->{$id1}->{node_path}/$node2->{$id2}->{node_path}","$node2->{$id2}->{node_name}","$node2->{$id2}->{main_id}","dropdown-item $mclass","","$users_dms->{u_id}");
                  }
                  print "<div class='nav-item' style='margin-left:25px;'> $lmenu2 </div>\n";
		} 
	      }
	    }
	   }
	  }
          print "</ul>\n";
          print "</li>\n";
          #end submenu
        }elsif(1==1){
          print "<li class='nav-item'> $lmenu0 </li>\n";
        }
      }
    }
   }#end foreach lmenue0
   print "</ul>\n";
   print "</div></div></nav>\n";
   ###end bootstrap menue

   #node_name=DMS
   if($varenv->{dbname} eq $dbt->{primary}->{sharee_primary}->{database}->{dbname} && $main_id == "100002"){
    my $big2menu="";
    #while (my ($key, $value) = each %{ $dbt->{operator} }) {
    foreach my $key (sort keys  (%{ $dbt->{operator} })) {
      $big2menu .= $but->lia_button("$dbt->{operator}->{$key}->{operatorDMS}","DMS $key <span style='font-size:0.81em;'>($dbt->{operator}->{$key}->{title})","","","color:#$dbt->{operator}->{$key}->{bgcolor1};","") if($dbt->{operator}->{$key}->{merchant_id});
    }
    foreach my $key (sort keys  (%{ $dbt->{website} })) {
      $big2menu .= $but->lia_button("$dbt->{website}->{$key}->{operatorWeb}","Web $key","","","color:#$dbt->{website}->{$key}->{bgcolor1};","") if($dbt->{website}->{$key}->{merchant_id});
    }
    foreach my $key (sort keys  (%{ $dbt->{appsframe} })) {
      if($key eq "shareetool"){
      	$big2menu .= $but->lia_button("$dbt->{appsframe}->{$key}->{uri}?sessionid=$dbt->{appsframe}->{$key}->{merchant_id}","App $key","","","color:#$dbt->{merchant_ids}->{$dbt->{appsframe}->{$key}->{merchant_id}}->{bgcolor1};","");
      }else{
      	$big2menu .= $but->lia_button("$dbt->{primary}->{sharee_primary}->{primaryApp}?sessionid=$dbt->{appsframe}->{$key}->{merchant_id}","App $key","","","color:#$dbt->{merchant_ids}->{$dbt->{appsframe}->{$key}->{merchant_id}}->{bgcolor1};","");
      }
    }

    print $q->div({-style=>'width:100%;margin:0;padding-top:70px;color:white;'}, ""),"\n";
    print $q->div({-style=>'width:100%;margin:0;'}, $q->ul({-id=>'BigNavi'},$big2menu)),"\n";
   }
  
  $debug = "syshost: $varenv->{syshost}, merchant_id: $varenv->{merchant_id}, (c_id4trans:$users_dms->{c_id4trans} && tpl_id4trans:$users_dms->{tpl_id4trans}) $node_meta->{tpl_name},$node_meta->{tpl_id},$node_meta->{ct_table},$parent_id,$main_id, permissions: ($users_dms->{int01},$users_dms->{int02},$users_dms->{int03},$users_dms->{int07},$users_dms->{int08},$users_dms->{int09})" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
  print $q->div({-style=>'position:fixed;bottom:0%;right:1%;z-index:10;padding:2px;font-size:13px;'},"$debug",$q->a({-style=>'color:black;text-decoration: none;',-href=>'https://sharee.bike',-target=>'_blank'},"sharee.bike &copy; TeilRad GmbH 2024")),"\n";

   print $q->div({-style=>'padding: 30px 0 0 0;'}, ""), "\n";
   $self->tplselect($node_meta,$users_dms,$mode,$varenv,$feedb);
 print "</div>\n";
 }#end logedin environment

}

#2021-05-05 changed to Mlogic
sub tplselect(){
 my $self = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $mode = shift || "";
 my $varenv = shift;
 my $feedb_req = shift || {};

 my $sort = "";
 my $dbh = "";
 my $tpl_name = $node_meta->{tpl_name};
 my $tpl_id = $node_meta->{tpl_id};
 my $ct_table = $node_meta->{ct_table};
 my $parent_id = $node_meta->{parent_id};
 my $main_id = $node_meta->{main_id};
 my $feedb = { message => '' };

 if($node_meta->{tpl_id} =~ /194|195|199/ && $node_meta->{ct_table} eq "contentuser"){
  require "Tpl/SubListe.pm";
  $feedb = &SubListe::tpl($node_meta,$users_dms,$feedb);
 }elsif($node_meta->{ct_table} eq "contentuser"){
  require "Tpl/MandantConf.pm";
  $feedb = &MandantConf::tpl($node_meta,$users_dms,$feedb);
 }elsif($node_meta->{tpl_id} == 1 && $node_meta->{main_id} == 200017){
  require "Tpl/Karte_osm.pm";
  &Karte_osm::tpl($node_meta,$users_dms,$mode,$varenv,"",$feedb);
 }elsif($node_meta->{ct_table} =~ /contentpos|contentadrpos|users$/){#contentpos or contentadrpos
  require "Tpl/SubListe.pm";
  $feedb = &SubListe::tpl($node_meta,$users_dms,$feedb);
 }elsif($node_meta->{ct_table} =~ /content$|contentadr$|contenttrans$/){
  require "Tpl/Liste3.pm";
  $feedb = &Liste3::tpl($node_meta,$users_dms,$mode,$feedb);
 }elsif($node_meta->{ct_table} =~ /contenttranspos|contenttheftpos/){
  require "Tpl/Calorin.pm";
  &Calorin::tpl($node_meta,$users_dms,$feedb);
 }
 #print Dumper($feedb);
 #exit;

 if(ref($feedb_req) eq "HASH" && $feedb_req->{message} =~  /failure/){
   require "Mod/Failure.pm";
   &Failure::tpl($varenv,$users_dms->{u_id},$feedb_req);
 }else{
  if(($R::ct_trans !~ /close/) && ($ct_table =~ /contenttrans/) && ($R::ct_trans || $R::trans2edit || $R::select_part || $R::set_main_id || $R::set_state)){
   &Modalbox::mobox($varenv,$node_meta,$users_dms,$feedb_req) if($R::ct_trans ne "delete_trans");
  }elsif(($R::ct_trans !~ /close/) && ($ct_table =~ /content$|contentadr|contentuser|contentpos|users$|contenttranspos/ && ($R::ct_trans || $R::rel_edit || $R::base_edit)) || ($R::node2edit && $R::node2edit =~ /edit/)){
   &Modalbox3::mobox3($varenv,$node_meta,$users_dms,$feedb_req) if($R::rel_edit !~ /delete|save/ && $R::ct_trans !~ /delete/ && $R::base_edit !~ /delete|save_pos/ && !$R::service_id);
  }
 }
 #prio feedback on data request
 if(ref($feedb_req) eq "HASH"){
   $bw->return_feedback($node_meta,$users_dms,$feedb_req);
 }elsif(ref($feedb) eq "HASH" && $feedb->{message}){
   $bw->return_feedback($node_meta,$users_dms,$feedb);
 }

}

1;


