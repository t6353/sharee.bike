package SMSTransport;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#SMS sender
#
#perl -cw do
#use lib "/var/www/copri-bike/shareeapp-primary/src";

use strict;
use warnings;
use POSIX;
use CGI; # only for debugging
use LWP::UserAgent;
use URI::Encode;
use JSON;
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::DBtank;
use Mod::Basework;
use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my $dbt = new DBtank;
my $bw = new Basework;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;

my $ua = LWP::UserAgent->new;
$ua->agent("sharee smsclient");

my $uri_encode = URI::Encode->new( { encode_reserved => 1 } );
my $json = JSON->new->allow_nonref;
my $response_in = {};
my $dbh = "";
my $owner = 181;

#used for sending smsAck codes to user
#Should be only used by primary. 
sub sms_ack_digest {
 my $self = shift;
 my $ctadr = shift;
 my $lang = $ctadr->{txt11} || $ctadr->{txt10};

 my %varenv = ();
 $varenv{cms} = $dbt->fetch_cms($dbh,{ lang => $lang });

 #Ack digest
 my $ack_digest = $q->unescapeHTML($ctadr->{txt34}) || "";
 my $email_ack_digest = $1 if($ack_digest =~ /^(.{5})/);
 my $sms_ack_digest = $1 if($ack_digest =~ /(.{5})$/);

 my $sms_from = "$varenv{cms}->{'sms-from-code'}->{txt}";
 my $sms_to = $ctadr->{txt07};# || "+491799xxxx72";
 my $sms_message = "";

 #goes to test if devel or joke bsp nr
 if($dbt->{copri_conf}->{stage} eq "test" || $ctadr->{txt07} =~ /17012345678/){
    $ctadr->{txt07} = "$dbt->{copri_conf}->{sms_to}";
    $sms_message = "* offline Test *";
 }
 if($ctadr->{txt07} =~ /^0/ || $ctadr->{txt07} !~ /\+[1-9]/){
   my $sms_tosub = $ctadr->{txt07};
   $sms_tosub =~ s/^\+00/\+/;
   $sms_tosub =~ s/^00/\+/;
   $sms_tosub =~ s/^0/\+49/;
   $sms_to = $sms_tosub;
 }
 $sms_message .= "$varenv{cms}->{'sms-smsack'}->{txt} $sms_ack_digest";
 my $message = Encode::encode('iso-8859-1', Encode::decode('utf-8',"$sms_message"));

 open(FILE,">>$dbt->{copri_conf}->{logdir}/sms_gtx.log");
 print FILE "\n*** $now_dt 'sharee smsclient' \n";

   my $request = {
           from => $sms_from,
           to => $sms_to,
           text => $message,
   };

   print FILE "---> request:\n" . Dumper($request);

  my $ret_json = "";
  if($sms_to =~ /^\+1/){
   #no routing fix
   my $upsms_ack = {
                table   =>      "contentadr",
                mtime   =>      "now()",
                owner   =>      "$owner",
                c_id    =>      "$ctadr->{c_id}",
   };
   $dbt->update_one($dbh,$upsms_ack,"int13=1");
   print FILE "no routing fix for $sms_to\n";
  }else{
   $ret_json = $self->get_sms_gtx($request);

   eval {
     $response_in = decode_json($ret_json);
     print FILE "<--- response_in:\n" . Dumper($response_in);
   };
   if ($@){
     print FILE "<--- failure raw response_in:\n" . Dumper($ret_json) . "\n";
     warn $@;
   }
  }

 close(FILE);

 return $ret_json;
}

#used for sending message to user
#varenv will be slurpt by syshost client
sub sms_message {
 my $self = shift;
 my $todo = shift || "";
 my $contact_hotline = shift || "";
 my $adr_id = shift || "";#phone by adr.c_id
 my $sms_to = shift || "";#direct phone (send_alarm2hotline)
 my $bike = shift || "";
 my %varenv = $cf->envonline();

 my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname},"iso-8859-1");

 my $ctadr = { c_id => 0 };
 if(looks_like_number($adr_id)){
  my $pref_adr = {
     table => "contentadr",
     fetch => "one",
     c_id => "$adr_id",
  };
  $ctadr = $dbt->fetch_tablerecord($dbh_primary,$pref_adr);
  $sms_to = $ctadr->{txt07};
  my $lang = $ctadr->{txt11} || $ctadr->{txt10};
 
  $varenv{cms} = $dbt->fetch_cms($dbh_primary,{ lang => $lang });
 }

 my $sms_from = "$varenv{cms}->{'sms-from-info'}->{txt}";
 my $sms_message = "";

 #goes to test if devel or joke bsp nr
 if($dbt->{copri_conf}->{stage} eq "test" || $sms_to =~ /17012345678/){
    $sms_to = "$dbt->{copri_conf}->{sms_to}";
    $sms_message = "* offline Test *";
 }
 if($sms_to =~ /^0/ || $sms_to !~ /\+[1-9]/){
   my $sms_tosub = $sms_to;
   $sms_tosub =~ s/^\+00/\+/;
   $sms_tosub =~ s/^00/\+/;
   $sms_tosub =~ s/^0/\+49/;
   $sms_to = $sms_tosub;
 }

 #messanger_divers longterm_occupied (delete cms sms-24h)
 if($todo eq "36"){
   my $cms_message_key = $dbt->{copri_conf}->{message_rental}->{SMS}->{$todo};
   $varenv{cms}->{$cms_message_key}->{txt} =~ s/::bike::/$bike/;
   $varenv{cms}->{$cms_message_key}->{txt} =~ s/::hour::/$todo/;
   $sms_message .= "$varenv{cms}->{$cms_message_key}->{txt} $contact_hotline";
 }

 #todo locking_progress
 if($todo eq "locking_progress"){
   $varenv{cms}->{'sms-locking-progress'}->{txt} =~ s/::bike::/$bike/;
   $sms_message .= "$varenv{cms}->{'sms-locking-progress'}->{txt} $contact_hotline";
 }

 #todo send_alarm2hotline
 if($todo eq "send_alarm2hotline"){
   $sms_from = "RadAlarm";
   $sms_message .= "Alarmmeldung zu Rad $bike. Öffne das Alarmjournal für weitere Informationen: $dbt->{operator}->{$varenv{dbname}}->{operatorDMS}/DMS/Alarmjournal";
 }

 #todo fraud_rental
 if($todo eq "fraud_rental"){
   $sms_message .= "Miete von einem Nutzer der als Betrugsfall hinterlegt ist! Mietrad $bike ";
 }


 my $message = Encode::encode('iso-8859-1', Encode::decode('utf-8',"$sms_message"));

 open(FILE,">>$dbt->{copri_conf}->{logdir}/sms_gtx.log");
 print FILE "\n*** $now_dt 'sharee smsclient' \n";

   my $request = {
           from => $sms_from,
           to => $sms_to,
           text => $message,
   };

   print FILE "---> request:\n" . Dumper($request);

   #my $ret_json = "";#$self->get_sms_gtx($request);#for testing without send
   my $ret_json = $self->get_sms_gtx($request);

   eval {
     $response_in = decode_json($ret_json);
     print FILE "<--- response_in:\n" . Dumper($response_in);
   };
   if ($@){
     print FILE "<--- failure raw response_in:\n" . Dumper($ret_json) . "\n";
     warn $@;
   }

 close(FILE);

 return $ret_json;
}

#sms gtx http request
sub get_sms_gtx {
 my $self = shift;
 my $request = shift || "";

 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;
 #print $apikeyconf{smsgtx}->{gtx_key};

 my $endpoint = "https://rest.gtx-messaging.net/smsc/sendsms/$apikeyconf{smsgtx}->{gtx_key}/json";
 my $rest;
 foreach (keys (%$request)){
   my $encoded_val = $uri_encode->encode($request->{$_});
   $rest .= "$_=$encoded_val&";
 }
 $rest =~ s/\&$//;

 my $gtx_request = "$endpoint?$rest";

 print FILE "===> GET2gtx >> " . $gtx_request . "\n";

 my $req = HTTP::Request->new(GET => "$gtx_request");
 $req->content_type('application/x-www-form-urlencoded');

 my $res = $ua->request($req);
 if ($res->is_success) {
  #print $res->content;
  return $res->content;
  print $res->status_line, "\n";
 }else {
  print $res->status_line, "\n";
 }
}
#

1;
