package Mod::ajax_json;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use warnings;
use strict;
use POSIX;
use CGI;
use CGI::Cookie ();
use Apache2::Const -compile => qw(OK );
use DBI;
use JSON;
use Mod::DBtank;
use Mod::APIfunc;
use Data::Dumper;

sub handler {
 my ($r) = @_;
 my $q = new CGI;
 $q->import_names('R');
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my @keywords = $q->param;
 my @query_output = ();
 my $debug=1;
 my $dbh = "";
 my $coo = $q->cookie('domcookie');
 my $users_sharee = { c_id => 0 };
 my $users_dms = { u_id => 0 };
 my $api_return = { authcookie => '' };
 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");
 if($users_sharee->{c_id} && $coo && length($coo) > 20){
     $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");
 }

 print $q->header(-type => "application/json", -charset => "utf-8");

 open(FILE,">>/var/log/copri-bike/ajax_json.log") if($debug);
 print FILE "$now_dt| u_id:$users_dms->{u_id}|table:$R::table|template_id:$R::template_id\n" if($debug);

 if($users_dms->{u_id} && ($users_dms->{int03} == 2 || $users_dms->{int07} == 2)){
  foreach(@keywords){
      my @val = $q->param($_);
      my $valxx = $q->escapeHTML("@val");
      print FILE "$_: $valxx\n" if($debug);
  }
  if($R::table eq "users" && $R::faksum){
    my $update_users = {
      table  => "users",
      u_id => $users_dms->{u_id},
      change => "no_time",
    };
    my $toggle = 1;
    $toggle = 0 if($users_dms->{faksum});
    $dbt->update_one($dbh,$update_users,"faksum=$toggle");
  }elsif($R::table eq "content"){
    my ($rows,$sth) = $dbt->search_json($dbh,$R::table,$R::term,$R::template_id,$R::c_id,$R::catch_equal);
    if($rows){
     while (my $row = $sth->fetchrow_hashref ){
      push @query_output, $row;
     }
    }
    print FILE Dumper(\@query_output) if($debug);
    print JSON::to_json(\@query_output);
  }elsif($R::table eq "contentadr"){
    my ($rows,$sth) = $dbt->search_json($dbh,$R::table,$R::term,$R::template_id,"",$R::catch_equal);
    if($rows){ 
     while (my $row = $sth->fetchrow_hashref ){
      push @query_output, $row;
     }
    }
    print FILE Dumper(\@query_output) if($debug);
    print JSON::to_json(\@query_output);
  }
 }
 close(FILE) if($debug);

 return Apache2::Const::OK;
}
1;

