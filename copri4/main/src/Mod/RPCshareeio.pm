package RPCshareeio;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#APIshareeio APIcall
#
#use lib "/var/www/copri-bike/shareedms-primary/src";
#
use strict;
use warnings;
use POSIX;
use CGI;
use Digest::SHA qw(hmac_sha256 hmac_sha256_base64);
use JSON;
my $json = JSON->new->allow_nonref;
use LWP::UserAgent;
use URI::Encode;
my $uri_encode = URI::Encode->new( { encode_reserved => 1 } );
use Scalar::Util qw(looks_like_number);
use Mod::DBtank;
use Mod::Basework;
use Data::Dumper;

my $q = new CGI;
my $dbt = new DBtank;
my $bw = new Basework;


sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#@all request with defined $shareeio_json
sub request_shareeio {
 my $self = shift;
 my $varenv = shift;
 my $dbh = shift;
 my $ctadr = shift || {};
 my $shareeio_json = shift || { request => ""};
 my $response_in = {};

 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 open(EMA, ">> $varenv->{logdir}/RPCshareeio.log");
 print EMA "\n*** $now_dt RPC $shareeio_json->{request}\n";

 if($shareeio_json->{request}){
        my $rest_json = encode_json(\%{ $shareeio_json });
        print EMA "rest_json:\n" . Dumper($rest_json) . "\n";

        my $ret_json = {};
        ($ret_json, my $ret_status) = $self->rpcsharee_postjson($rest_json);
        eval {
          $response_in = decode_json($ret_json);
          print EMA "<--- success shareeio response_in: $ret_status\n" . Dumper($response_in) . "\n";
          print EMA $ret_json . "\n";

        };
        if ($@){
          print EMA "<--- failure shareeio raw response_in: $ret_status\n" . Dumper($ret_json) . "\n";
          print EMA "warn:" . $@ . "\n";
	  $response_in->{shareeio}->{response_state} = "Failure: $ret_status";
        }
 }
 
 close EMA;
 return $response_in;
}


#request JSON POST to shareeio
sub rpcsharee_postjson {
 my $self = shift;
 my $rest_json = shift || "";

 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;

 my $ua = LWP::UserAgent->new();
 $ua->agent("RPCshareeio POST");
 my $bytes = 100000;
 $ua->max_size( $bytes );
 $ua->default_header( 'SHAREE-API-KEY' => "$apikeyconf{shareeio}->{sharee_api_key}" );
 #print Dumper($ua);

 my $req = HTTP::Request->new(POST => "$apikeyconf{shareeio}->{endpoint}");
 $req->content_type('application/json');
 $req->content($rest_json);
 my $res = $ua->request($req);

 if ($res->is_success) {
  #print $res->content;
  #print $res->status_line, "\n";
  return ($res->content, $res->status_line);
 }else {
  #print $res->status_line, "\n";
  return ("", $res->status_line);
 }
}


1;
