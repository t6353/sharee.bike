package Basework;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#

use strict;
use warnings;
use POSIX;
use CGI; # only for debugging
use Lib::Config;

use Data::Dumper;
use Sys::Hostname;
my $hostname = hostname;
my $cf = new Config;
my $q = new CGI;

sub new {               
 my $class = shift;
 my $self = {};         
 bless($self,$class);   
 return $self;          
}

my $time = time;
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
my $now_date = strftime "%Y-%m-%d", localtime;


#logging
sub log {
 my $self = shift;
 my ($what,$message,$stdout) = @_;
 #my ($package, $filename, $line) = caller;
 my %varenv = $cf->envonline();

 $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $logfile = "/var/log/copri4/$varenv{syshost}-process.log";
 if($varenv{debug}){
  warn "$$ $what" . "\n" . Dumper($message) . "\n";#to apache2/error.log

  #2021-07-21 disabled. error.log is enough
  if(1==2){
   open(FILE,">> $logfile");
    print FILE "\n--- $now_dt $0 ---\n";
    print FILE "$what" . "\n" . Dumper($message) . "\n";
   close FILE;
  }
  #also to stdout
  if($stdout){
    #print "\n--- $now_dt $0 ---\n";
    print "$$ $what" . "\n" . Dumper($message) . "\n";
  }

 }

}

#return headline message
sub return_feedback(){
  my $self = shift;
  my $node_meta = shift;
  my $users_dms = shift || {};
  my $feedb = shift || {};
  my $debug = "";
  #$debug = $feedb->{debug};

  if(ref($feedb) eq "HASH"){
print<<EOF
  <script>
        \$(document).ready(function(){
         \$( "#retm" ).fadeOut(10000);
  })
  </script>
EOF
;
   if($feedb->{message} && $feedb->{message} =~ /[a-z]/ && $feedb->{message} !~ /failure::/){
    print $q->div({-id=>'retm'},"$feedb->{message} $debug"),"\n";
   }else{
    print $q->div({-id=>'retm'},"Datensatz in \"$node_meta->{node_name}\" angelegt $debug"),"\n" if($feedb->{i_rows});
    print $q->div({-id=>'retm'},"Datensatz in \"$node_meta->{node_name}\" aktualisiert $debug"),"\n" if($feedb->{u_rows});
    print $q->div({-id=>'retm'},"Datensatz in \"$node_meta->{node_name}\" gelöscht $debug"),"\n" if($feedb->{d_rows});
   }
  }

 return;
}

sub battery_bars {
  my $self = shift;
  my $max_bars = shift || 0;
  my $current_percent = shift || 0;

  my $current_bars = 0;
  if($max_bars == 5){
	$current_bars = 1 if($current_percent >= 10);
	$current_bars = 2 if($current_percent >= 30);
	$current_bars = 3 if($current_percent >= 50);
	$current_bars = 4 if($current_percent >= 70);
	$current_bars = 5 if($current_percent >= 90);
  }

  return $current_bars;
}

sub battery_percent {
  my $self = shift;
  my $max_bars = shift || 0;
  my $current_bars = shift || 0;#by user input

  my $current_percent = 0;
  if($max_bars == 5){
        $current_percent = 10 if($current_bars >= 1);
        $current_percent = 30 if($current_bars >= 2);
        $current_percent = 50 if($current_bars >= 3);
        $current_percent = 70 if($current_bars >= 4);
        $current_percent = 100 if($current_bars >= 5);
  }

  return $current_percent;
}

#payable_check and rentable_check
#Disabled adr.int03==2 alias CC, 2025-02-13
#int03==1 if SEPA, 2 if CC, 3 if Prepaid
#int04==1 if emailAck
#int13==1 if smsAck
#int12==1|2|3|4 than Vde
#int14==1 if AGB
#int18>=1 if payment_provider=payone payAck (must be only set on SEPA and CC)
sub isuser_rentable {
  my $self = shift;
  my $auth = shift;
  my $varenv = shift || "";
  my $rentable_check=0;

  #if($auth->{int03} && $auth->{ct_name} && ($auth->{int18} && (($auth->{int03} == 1 && $auth->{ct_name} =~ /^\w{2}-\w+/) || ($auth->{int03} == 2 && length($auth->{ct_name}) >= 19))) || ($auth->{int03} == 3 && $auth->{ct_name} =~ /Prepaid-\d+/i)){
  if($auth->{int03} && $auth->{ct_name} && ($auth->{int03} == 1 && $auth->{ct_name} =~ /^\w{2}-\w+/) || ($auth->{int03} == 3 && $auth->{ct_name} =~ /Prepaid-\d+/i)){
    $rentable_check=1;#Account is payable
    if(!$auth->{int12} && $auth->{txt08} && $auth->{int04} == 1 && $auth->{int13} == 1 && $auth->{int14}){
      $rentable_check=2;#Account is rentalable, with all Ack's and Vde=0
    }
  }
  return $rentable_check;
}


1;
