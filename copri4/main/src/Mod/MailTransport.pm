package MailTransport;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
# new emailing module
#
#perl -cw src/Mod/MailTransport.pm
#use lib "/var/www/copri4/shareeapp-operator/src";
#
use strict;
use warnings;
use POSIX;
use CGI ':standard';
use IO::All;
use Net::SMTP;
use MIME::Base64 qw( encode_base64 );
use Try::Tiny;
use Config::General;
use Scalar::Util qw(looks_like_number);
use Mod::Basework;
use Mod::DBtank;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $q = new CGI;
my $bw = new Basework;
my $dbt = new DBtank;

#mailxcfg shareeconf/mailx.cfg
#hash data to send
sub sendrefhash {
 my $self = shift;

 my $sendref = {
        mailxcfg => "mailx_default",
        syshost => "",
        mail_from  => "",
        mail_to  => "",
        mail_bcc  => "",
        c_id => 0,
        subject => "",
        message => "",
        signature => "",
        attachment => "",
 };
 return $sendref;
}

sub mail_connect {
 my $self = shift;
 my $sendref = shift;

 my $mailx_file = "/var/www/copri4/shareeconf/mailx.cfg";
 my $conf = Config::General->new($mailx_file);
 my %mailxconf = $conf->getall;

 my $smtp = Net::SMTP->new($mailxconf{$sendref->{mailxcfg}}->{mail_gateway},
                        Port => 465,
                       Hello => 'sharee.bike',
                       Timeout => 30,
                       Debug   => 0,
                       SSL     => 1,
                      );

 $smtp->auth($mailxconf{$sendref->{mailxcfg}}->{sasl_username},$mailxconf{$sendref->{mailxcfg}}->{sasl_password});
 $smtp->mail($mailxconf{$sendref->{mailxcfg}}->{mail_from});
 
 return ($smtp,\%mailxconf);
}

sub mail_transport(){
  my $self = shift;
  my $smtp = shift;
  my $mailxconf = shift;
  my $sendref = shift;

  my $ret = 1;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  open(EMA, ">> $dbt->{copri_conf}->{logdir}/MailTransport.log");

  my $mail_from = $sendref->{mail_from} || $mailxconf->{$sendref->{mailxcfg}}->{mail_from};
  my $mail_to = $sendref->{mail_to} || $mailxconf->{$sendref->{mailxcfg}}->{mail_to};
  my $mail_bcc = $sendref->{mail_bcc} || $mailxconf->{$sendref->{mailxcfg}}->{mail_bcc} || "";
  my $subject = $sendref->{subject};

  my ($buf, $picture);
  my $boundary = 'frontier';
  my $pdfpath = "$dbt->{copri_conf}->{basedir}/$sendref->{syshost}/pdf";
  my $attachBinaryFile = "";
  my $filesize = 0;

  if(-f "$pdfpath/$sendref->{attachment}"){
   $filesize = -s "$pdfpath/$sendref->{attachment}";
   $attachBinaryFile = "$sendref->{attachment}";
  }

  if($dbt->{copri_conf}->{stage} ne "live"){
   $mail_to = $mailxconf->{$sendref->{mailxcfg}}->{mail_to};
   $subject = "* offline Test * " . $sendref->{subject};
  }
 
  print EMA "\n$now_dt, start mailing to: $mail_to | subject: $subject\n";
  print EMA "attachment: $pdfpath/$sendref->{attachment} | filesize: $filesize\n";

  my $html = "<html><head><title>$subject</title></head><body style='text-align:left;border:0px solid silver;padding:15px;margin:2%;width:90%;'>\n";
  $html .= "<div>$sendref->{message}</div>\n";
  $html .= "<div>$sendref->{signature}</div>\n" if($sendref->{signature});
  $html .= "</body></html>";


  print EMA "Trying send_mail by $0 to $mail_to\n";
  print EMA Dumper($sendref);

  if(ref($sendref) eq "HASH"){


 	if ($smtp->to($mail_to)) {
	  $smtp->mail("shareemail");
	  $smtp->to($mail_to);
	  $smtp->bcc($mail_bcc) if($mail_bcc);
  	  $smtp->data();
  	  $smtp->datasend("To: $mail_to\n");
	  $smtp->datasend("Bcc: $mail_bcc\n") if($mail_bcc);
  	  $smtp->datasend("Subject: $subject\n");
	  $smtp->datasend("\t\n") if(!$attachBinaryFile);
  	    $smtp->datasend("MIME-Version: 1.0\n");
	    $smtp->datasend("Content-type: multipart/mixed;\n\tboundary=\"$boundary\"\n");
	    $smtp->datasend("\n");

	    $smtp->datasend("--$boundary\n");
	    $smtp->datasend("Content-Type: text/html; charset=UTF-8 \n\n");
	    $smtp->datasend("$html\n\n\n\n\n\n\n\n\n\n");
	  $smtp->datasend("\n");

	  if($attachBinaryFile){
	    $smtp->datasend("--$boundary\n");
	    #$smtp->datasend("Content-Type: image/jpeg; name=\"$attachBinaryFile\"\n");
	    $smtp->datasend("Content-Type: application/pdf; name=\"$attachBinaryFile\"\n");

	    $smtp->datasend("Content-Transfer-Encoding: base64\n");
	    $smtp->datasend("Content-Disposition: attachment; filename=\"$attachBinaryFile\"\n");
	    $smtp->datasend("\n");

	   open(DAT, "$pdfpath/$attachBinaryFile") || die("Could not open $pdfpath/$attachBinaryFile");
   	    binmode(DAT);
   	    local $/=undef;
   	    while (read(DAT, $picture, 4104)) {
      	    $buf = &encode_base64( $picture );
     	     $smtp->datasend($buf);
   	    }
	    $smtp->datasend("\n");
	   close(DAT);
	   $smtp->datasend("--$boundary\r\n\r");
	  }

  	  $smtp->dataend();
	  $smtp->quit;
  	  print EMA $smtp->message();
 	}

	sleep 1;
	$ret = $?;
  }


  print EMA "done mailing with state: $?\n";
  print EMA "\n\n";
  close EMA;

  $bw->log("done mailing with state:","$ret","");
  return $ret;
}#end mail_send

#prepare email by cms markup 
sub prepare_content {
  my $self = shift;
  my $varenv = shift;
  my $sendref = shift;
  my $ctadr = shift;
  my $uadr = shift;
  my $ctt = shift || {};
  my $cms_prim = shift || "";
  my $with_pdf = shift || "";
  my $app_name = shift || "";

  if(!$app_name && $dbt->{operator}->{$varenv->{dbname}}->{app_name}){
    $app_name = $dbt->{operator}->{$varenv->{dbname}}->{app_name};
  }else{
    $app_name = "Mietrad App";
  }
  my $invoice_name = "";
  my $invoice_nr = "";
  my $total_sum = "";
  my $total_price = "";
  if($ctadr->{total_price}){
    $total_price = $ctadr->{total_price};
    $total_price =~ s/\./,/;
  }
  my $sharee_ticket = "";
  my $fibu_user = "";
  my $prepaid_id = "";
  my $payone_link = "";
  my $stagemode = $1 if($dbt->{copri_conf}->{stage} =~ /^(\w)/);
  if(ref($ctt) eq "HASH" && $ctt->{ct_name}){
    $invoice_name = "$ctt->{txt00}-$varenv->{dbname}-$ctt->{ct_name}.pdf";
    $sendref->{attachment} = "$invoice_name" if($with_pdf);
    $invoice_nr = "$varenv->{dbname}-$ctt->{ct_name}";
    $total_sum = "$ctt->{int01}";#invoice sum
    $total_sum =~ s/\./,/;
    $sharee_ticket = "[$invoice_nr]";
    $fibu_user = "\n$ctt->{fibu_user}\n" if($ctt->{fibu_user});
    $prepaid_id = $stagemode . $ctt->{ct_id} . "-" . $ctt->{c_id};
    $payone_link = $ctt->{txt30};
  }

  my $subject = "TeilRad Mietradsystem";#default
  $subject = $1 if($cms_prim =~ /--subject--(.*)--subject--/);
  $cms_prim =~ s/--subject--$subject--subject--//;
  $subject .= " $sharee_ticket" if($sharee_ticket && !$payone_link);
  $cms_prim =~ s/\n//;

my $signature = <<EOF
  --$fibu_user
  $uadr->{txt01}
  $uadr->{txt03}
  $uadr->{txt06}
  $uadr->{txt07}
  $uadr->{txt08}

  $uadr->{txt09}

  $uadr->{txt10}

EOF
;

  $sendref->{mail_to} = $ctadr->{txt08};
  $sendref->{c_id} = $ctadr->{c_id};
  $sendref->{subject} = $subject;
  $sendref->{subject} =~ s/::app_name::/$app_name/;
  $sendref->{subject} =~ s/Mein // if($sendref->{subject} =~ /^Mein/);
  $sendref->{subject} =~ s/::bike::/$ctadr->{bike}/g if($ctadr->{bike});

  $sendref->{message} = $cms_prim;
  $sendref->{message} =~ s/::user_name::/$ctadr->{txt01}/g;
  $sendref->{message} =~ s/::bike::/$ctadr->{bike}/g if($ctadr->{bike});
  $sendref->{message} =~ s/::app_name::/$app_name/g;
  $sendref->{message} =~ s/::invoice_nr::/\<b\>$invoice_nr\<\/b\>/g if($invoice_nr);
  $sendref->{message} =~ s/::total_price::/\<b\>$total_price\<\/b\>/g if($total_price);
  $sendref->{message} =~ s/::total_sum::/\<b\>$total_sum\<\/b\>/g if($total_sum);
  $sendref->{message} =~ s/::invoice_name::/$invoice_name/g if($invoice_name);
  $sendref->{message} =~ s/::txid::/$ctt->{txt16}/;
  $sendref->{message} =~ s/::email_temppassword::/\<b\>$ctadr->{txt04}\<\/b\>/g;
  $sendref->{message} =~ s/::email_ack_digest::/\<b\>$ctadr->{txt34}\<\/b\>/g;#send_emailack
  $sendref->{message} =~ s/::prepaid_id::/\<b\>$prepaid_id\<\/b\>/g;
  $sendref->{message} =~ s/::payone_link::/\<b\><a href='$payone_link'>$payone_link<\/a>\<\/b\>/g;
  $sendref->{message} =~ s/::signature::/$signature/;
  $sendref->{message} =~ s/\n/\<br \/\>/g;

  return $sendref;
}#end prepare_content


#messanger_divers longterm_occupied
sub mail_hours_occupied {
  my $self = shift;
  my $varenv = shift;
  my $hours = shift || 0;
  my $uadr = shift;
  my $contentpos = shift;
  my $sendref = $self->sendrefhash();
  $sendref->{mailxcfg} = "mailx_hotline";
  $sendref->{mailxcfg} = "mailx_konrad" if($varenv->{syshost} eq "shareedms-kn");

  my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname},"iso-8859-1");

  my $ctadr = { c_id => 0 };
  if(looks_like_number($contentpos->{ca_id})){
   my $pref_adr = {
     table => "contentadr",
     fetch => "one",
     c_id => $contentpos->{ca_id},
   };
   $ctadr = $dbt->fetch_tablerecord($dbh_primary,$pref_adr);
   $ctadr->{bike} =  $contentpos->{ct_name};
   $ctadr->{total_price} =  $contentpos->{total_price};
   my $lang = $ctadr->{txt11} || $ctadr->{txt10};
   $varenv->{cms} = $dbt->fetch_cms($dbh_primary,{ lang => $lang });
  }

  my $cms_message_key = "";
  $cms_message_key = $dbt->{copri_conf}->{message_rental}->{Email}->{$hours};
  $sendref = $self->prepare_content($varenv,$sendref,$ctadr,$uadr,"",$varenv->{cms}->{$cms_message_key}->{txt},"");

  my ($smtp,$mailxconf) = $self->mail_connect($sendref);
  my $ret = $self->mail_transport($smtp,$mailxconf,$sendref);
  return $ret;
}

#we use just db-selector hashes
sub mail_feedback2garage {
  my $self = shift;
  my $mail_to = shift;
  my $contentpos = shift;
  my $sendref = $self->sendrefhash();

  $sendref->{mail_to} = $mail_to;
  $sendref->{subject} = "sharee.bike App Meldung";
  $sendref->{message} = "Es ist folgende sharee.bike App Nachricht zu Mietrad $contentpos->{ct_name} an Station $contentpos->{int04} eingegangen:\n\n$contentpos->{txt01}\n\nFreundliche Grüße,\nIhr sharee.bike Mietradsystem";
  $sendref->{message} =~ s/\n/\<br \/\>/g;#TOD prepare_content

  my ($smtp,$mailxconf) = $self->mail_connect($sendref);
  my $ret = $self->mail_transport($smtp,$mailxconf,$sendref);
  return $ret;
}

1;

