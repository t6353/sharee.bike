package Modalbox;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#

use strict;
use warnings;

use CGI::Cookie ();
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Buttons;
use Mod::DBtank;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub mobox(){
 my $varenv = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $return = shift || "";

 my $q = new CGI;
 my $cf = new Config;
 my $but = new Buttons;
 my $dbt = new DBtank;
 $q->import_names('R');
 my @keywords = $q->param;
 my $script = $q->script_name();
 my $path = $q->path_info();
 my $lang = "de";
 my $dbh = "";

 if(looks_like_number($users_dms->{c_id4trans}) && looks_like_number($users_dms->{tpl_id4trans})){
  my $width = $node_meta->{tpl_width} || "990";
  my $bg_color = "white";
  my $bg_color2 = "#f7ae37" || "";

  my $height = "800";
  my $debug = "";
  $debug = "(c_id: $users_dms->{c_id4trans} | tpl_id: $users_dms->{tpl_id4trans})" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});

print<<EOF
  <style>
  .ui-dialog .ui-dialog-content {
    background: $bg_color;
  }
  .ui-dialog > .ui-widget-header {
    color:$varenv->{color};
    font-weight:normal;
    border:1px solid $bg_color2;
    background: $bg_color2;
  }
  .ui-widget-overlay {
    background: #666 url("$varenv->{metahost}/external/jquery-ui-1.12.1/images/ui-bg_diagonals-thick_20_666666_40x40.png") 50% 50% repeat;
    opacity: .5;
    filter: Alpha(Opacity=50);
  }
  </style>

  <script>
        \$(function() {
                \$( "#dialog-form" )
                        .css("background-color","$bg_color")
			.dialog({
				height: $height,
				width: $width,
				//show: { effect: 'drop', direction: "up" } ,
				closeOnEscape: true,
				zIndex: 1010,
				modal: true
			});
		\$('.ui-widget-overlay').click(function() {
		   \$(".ui-dialog-titlebar-close").trigger('click');
		});
		\$('.ui-dialog').css('z-index',9999);
        });
  </script>
EOF
;
  
  print "<div id='dialog-form' style='text-align:center;margin:0;padding:2px;max-width:1200px;' title='Terminal – Faktura $debug'>";

  if(looks_like_number($users_dms->{c_id4trans}) && looks_like_number($users_dms->{tpl_id4trans})){
    require "Tpl/Address3.pm";
    &Address3::tpl($varenv,$node_meta,$users_dms,$return);
  }else{
   print $q->div({-style=>"padding:0.1em;margin:0em;background-color:white;font-size:0.81em;"}, "Ein neues Formular kann im COPRI Hauptfenster geöffnet werden (Code: $users_dms->{c_id4trans} && $users_dms->{tpl_id4trans})"),"\n";
  }
 
  print "</div>\n";
 }
}
1;
