package Callib;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use DBI;
use CGI;    # only for debugging

#Deb libcalendar-simple-perl
use Calendar::Simple;
use Date::Calc qw(:all);
use Scalar::Util qw(looks_like_number);
use Mod::Buttons;
use Lib::Config;
use Mod::Basework;

my $cf  = new Config;
my $but = new Buttons;
my $bw = new Basework;

my $q   = new CGI;
$q->import_names('R');

sub new {
    my $class = shift;
    my $self  = {};
    bless( $self, $class );
    return $self;
}

my %varenv   = $cf->envonline();
my $now_time = strftime "%Y-%m-%d %H:%M", localtime;
my $day  = strftime "%d", localtime;
my $mon  = strftime "%m", localtime;
my $year = strftime "%Y", localtime;

#month map
sub monthmap(){
  my @_months = ("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
  return @_months;
}

#day map
sub daymap(){
  my @_days = ("So","Mo","Di","Mi","Do","Fr","Sa");
  return @_days;
}

#hour map
sub hourmap(){
  my @_hours = (
        "00:00", "01:00", "02:00", "03:00", "04:00", "05:00",
        "06:00", "07:00", "08:00", "09:00", "10:00", "11:00",
        "12:00", "13:00", "14:00", "15:00", "16:00", "17:00",
        "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"
  );
  return @_hours;
}

#english input date_time check
sub checkdate_time() {
    my $self = shift;
    my $date_time = shift;
    $date_time =~ s/:\d{2}\..*$//;
    my $date = $date_time;
    my $time = "00:00:00";
    ($date, $time) = split(/ /, $date_time) if($date_time =~ /\d+\s\d+/); 
    my ( $y, $m, $d ) = split( /-/, $date );
    my ( $hour, $min, $sec ) = split( /\:/, $time );

    my $check_time = 1;
    $check_time = 0 if(looks_like_number($hour) && $hour > 24);
    $check_time = 0 if(looks_like_number($min) && $min > 60);
    $check_time = 0 if(looks_like_number($sec) && $sec > 60);
    #print "$y, $m, $d && $check_time";
    if ( check_date( $y, $m, $d ) && $check_time) {
	return $date_time;
    }else{
	return 0;
    }
}

# input date check
sub checkdate() {
    my $self = shift;
    my ( $date, $time ) = @_;
    my $d_chck = 1;
    $date =~ s/,/-/g;
    $date =~ s/\./-/g;
    my ( $y, $m, $d ) = split( /-/, $date );
    if ( check_date( $y, $m, $d ) ) {
        return "$y-$m-$d";
    }
    else {
        return "$year-$mon-$day";
    }
}


#split date
sub split_date(){
 my $self = shift;
 my ($time_db) = @_;
 $time_db =~ s/:\d{2}\..*$// if($time_db);
 my ($date,$time) = split(/ /,$time_db);
 my ($yy,$mo,$dd) = split(/-/,$date) if($date =~ /-/);
 my ($hh,$mi) = split(/\:/,$time);
 return ($yy,$mo,$dd,$hh,$mi);
}

1;
