package Failure;
#
#      
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use POSIX;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#just some frontend feedback
sub tpl(){
  my $varenv = shift;
  my $u_id = shift || "";
  my $feedb = shift || {};
  my $q = new CGI;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;

  open(FILE,">>$varenv->{logdir}/feedbFailure.log");
  print FILE "\n*-->syshost $varenv->{syshost}  $now_dt| u_id: $u_id\n";
  print FILE Dumper($feedb) . "\n";

  my $failure = $feedb->{message} || "";
  $failure =~ s/failure:://g;
  my $link = "";
  my $lname = "";
  ($failure,$link,$lname) = split(/::/,$failure);
  my $width="600";
  my $height="300";
  my $bg_color1 = "white";
  my $bg_color2 = "";

  print FILE "$failure | $link | $lname\n";

if($varenv->{syshost} =~ /shareedms-/){
print<<EOF
  <style>
  .ui-dialog .ui-dialog-content {
    background: $bg_color1;
  }
  .ui-dialog > .ui-widget-header {
    color:red;
    font-weight:normal;
    border:1px solid $bg_color2;
    background: $bg_color2;
  }
  </style>

  <script>
        \$(function() {
                \$( "#dialog-failure" )
			.css("background-color","$bg_color1")
		   	.dialog({
                                height: $height,
                                width: $width,
                                closeOnEscape: true,
                                modal: true
                        });
        });
  </script>
EOF
;
}

  my $title = "Achtung";
  my $back = "zurück";
  print "<div id='dialog-failure' style='text-align:left;margin:auto;max-width:600px;' title='$title'>";


  print "<div style='padding:1em;background-color:white;'>\n";
  print $q->div("$failure");

  print "<style>.linknav2 { margin:3px; padding:1px 20px; border: 1px solid $bg_color2; background-color: $bg_color2; font-size:13px; text-decoration: none; } .linknav2:hover { color:#464433;}</style>\n";

  print $q->div({-style=>'float:left;padding:1em;'}, $q->a({-class=>"linknav2",-href=>"$link",-title=>''}, " $lname ")) if($lname);
  print $q->div({-style=>'float:left;padding:1em;'},$q->a({-class=>"linknav2",-href=>'javascript:history.back()'}, " $back "));
  print "</div>\n";

  print "</div>";

  close(FILE);
}
1;
