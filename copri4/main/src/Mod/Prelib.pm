package Prelib;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
# 
#this module holds some basic data management methodes
#
use strict;
use warnings;
use POSIX;
use File::Path qw(make_path remove_tree);
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use Scalar::Util qw(looks_like_number);
use DateTime;
use DateTime::Format::Pg;
use Text::CSV_XS;
use Lib::Config;
use Mod::Libenz;
use Mod::DBtank;
use Mod::Basework;
use Mod::Pricing;
use Mod::Shareework;
use Mod::APIsigclient;
use Mod::SMSTransport;
use Mod::MailTransport;
use Data::Dumper;

my $cf = new Config;
my $lb = new Libenz;
my $dbt = new DBtank;
my $bw = new Basework;
my $pri = new Pricing;
my $shwo = new Shareework;
my $si = new APIsigclient;
my $smstrans = new SMSTransport;
my $mailtrans = new MailTransport;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

my $i_rows=0;
my $u_rows=0;
my $d_rows=0;
my $lang = "de";

my %varenv = $cf->envonline();
my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
my $debug=1;

#select rentals for messaging user if bike > 12h ... occupied
sub longterm_occupied {
 my $self = shift;
 my $varenv = shift;
 my $dbh = "";
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $log_stamp = strftime "%d.%m.%Y %H:%M:%S", localtime;

 open(FILE,">>$dbt->{copri_conf}->{logdir}/longterm_occupied.log");
 print FILE "\n*** $now_dt $varenv->{dbname} \n";

 #operator hotline contact
 my $pref_cc = {
  	table => "contentuser",
       	fetch => "one",
       	template_id => "197",
       	c_id => 1,
 };
 my $uadr = { c_id => 0 };
 $uadr = $dbt->fetch_record($dbh,$pref_cc);

  my $search = {
        table_pos  => "contenttranspos",
        int10  => 3,
	int34  => "null",#if not staff
        start_time_interval => "(now() - interval '12 hours')",
  };
  my $update_pos = {
        table  => "contenttranspos",
	mtime => "no_time",
  };

  my $dt1 = DateTime->now(time_zone => "Europe/Berlin");

  my $cttpos = { c_id => 0 };
  $cttpos = $dbt->collect_transpos($dbh,$search,0);

  my $posting_email = $dbt->{copri_conf}->{message_rental}->{Email};
  my $posting_sms = $dbt->{copri_conf}->{message_rental}->{SMS};
  foreach my $pid (sort { $cttpos->{$b}->{end_time} cmp $cttpos->{$a}->{end_time} } keys(%$cttpos)){
    if($cttpos->{$pid}->{int10} == 3){
      if(1==1){
	print FILE "Fetched > 12 hour occupied rental with messaged int33=$cttpos->{$pid}->{int33} - pos_id:" . $cttpos->{$pid}->{c_id} . "\n";

	#Email
	foreach my $h_id (sort { $a <=> $b } keys(%$posting_email)){
	 print FILE "--> hour:$h_id\n";
         my $dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
         if($dt1 >= $dt2->add( hours => $h_id )){
	  print FILE $dt1 . ">=" . $dt2->add( hours => $h_id ) . "\n";
          if(!$cttpos->{$pid}->{int33} || $cttpos->{$pid}->{int33} < $h_id){
	    print FILE "if(!$cttpos->{$pid}->{int33} || $cttpos->{$pid}->{int33} < $h_id)\n";

	    print FILE "Sent message for > $h_id hour rental by $posting_email->{$h_id}\n";
	    my $pricing = { total_price => 0 };
	    ($pricing,my $counting) = $pri->counting_rental($varenv,$cttpos->{$pid}) if($h_id == 72);
	    $cttpos->{$pid}->{total_price} = $pricing->{total_price} if($pricing->{total_price});
	    $mailtrans->mail_hours_occupied($varenv,$h_id,$uadr,$cttpos->{$pid});
	    my $message_log = $cttpos->{$pid}->{txt20} . "\n- $log_stamp $posting_email->{$h_id}";
	    $dbt->update_one($dbh,$update_pos,"int33=$h_id,txt20='$message_log'",$cttpos->{$pid}->{c_id});
	  }
	 }
	}
	#SMS
	foreach my $h_id (sort { $a <=> $b } keys(%$posting_sms)){
	 print FILE "--> hour:$h_id\n";
         my $dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
         if($dt1 >= $dt2->add( hours => $h_id )){
	  print FILE $dt1 . ">=" . $dt2->add( hours => $h_id ) . "\n";
          if(!$cttpos->{$pid}->{int33} || $cttpos->{$pid}->{int33} < $h_id){
	    print FILE "if(!$cttpos->{$pid}->{int33} || $cttpos->{$pid}->{int33} < $h_id)\n";

	    print FILE "Sent message for > $h_id hour rental by $posting_sms->{$h_id}\n";
 	    $uadr->{txt07} =~ s/\s//g;
	    my $contact_hotline = "Hotline $uadr->{txt01} $uadr->{txt08}, $uadr->{txt07}";
	    $smstrans->sms_message($h_id,$contact_hotline,$cttpos->{$pid}->{ca_id},"",$cttpos->{$pid}->{ct_name});
	    my $message_log = $cttpos->{$pid}->{txt20} . "\n- $log_stamp $posting_sms->{$h_id}";
	    $dbt->update_one($dbh,$update_pos,"int33=$h_id,txt20='$message_log'",$cttpos->{$pid}->{c_id});

          }
	 }
        }
      }
    }
  }
  close(FILE);
  return;
}#end longterm_occupied

#insert/save/delete DMS users
sub manage_dmsusers {
 my $self = shift;
 my $q = shift;
 my $varenv = shift;
 my $base_edit = shift;
 my $u_id = shift || 0;
 my $users_dms = shift || {};
 my $owner = $users_dms->{u_id} || 0;
 my $table = "users";
 $q->import_names('R');
 my @keywords = $q->param;

 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $dbh = "";
 my $feedb = { 
	 u_id => "",
	 message => "", 
	 i_rows => 0, 
	 u_rows => 0, 
	 d_rows => 0 
 };
 $bw->log("manage_dmsusers",$q,"");

 open(FILE,">>$varenv->{logdir}/dmsusers.log");
 print FILE "\n*-->$now_dt | $varenv->{dbname} | $base_edit\n";
 print FILE Dumper($q) . "\n";

 my $auth_primary = { c_id => 0 };
 my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});

 my $ctadr = { c_id => 0 };
 my $users = { u_id => 0 };
 my $adref = {
      table           => "contentadr",
      fetch           => "one",
      template_id     => "202",
 };
 my $uref = {
      table           => "users",
      fetch           => "one",
 };

 if($u_id){
   $adref->{c_id} = "$u_id";
   $ctadr = $dbt->fetch_record($dbh,$adref);
   $uref->{u_id} = "$u_id";
   $users = $dbt->fetch_tablerecord($dbh,$uref);
 }

    if(ref($users) eq "HASH" && $users->{u_id} && (!$ctadr->{c_id} || $base_edit eq "delete_dmsusers")){
        print FILE "delete DMS user from $varenv->{dbname} | c_id: $ctadr->{c_id}\n";
        $feedb->{d_rows} = $dbt->delete_content($dbh,"users",$users->{u_id});
	if($ctadr->{c_id}){
	  $adref->{c_id} = $ctadr->{c_id};
          $dbt->update_one($dbh,$adref,"int07=null");
          $dbt->update_one($dbh,$adref,"int09=null");
	}
    }
    #insert if user search matches
    elsif($ctadr->{c_id} && !$users->{u_id} && $base_edit eq "new_dmsusers"){
         print FILE "insert DMS user to $varenv->{dbname} | c_id $ctadr->{c_id}\n";
         $feedb->{u_id} = $dbt->insert_users($dbh,$ctadr->{c_id},$owner);
	 $adref->{c_id} = $ctadr->{c_id};
         $dbt->update_one($dbh,$adref,"int07=100");
         $dbt->update_one($dbh,$adref,"int09=1");
    }
    #check user exist and insert
    elsif(!$users->{u_id} && $R::txt08 && $R::txt08 =~ /\w\@\w/ && $base_edit eq "save_dmsusers"){
	print FILE "search for insert DMS user to $varenv->{dbname} | email: $R::txt08\n";
        $adref->{txt08} = "ilike::" . $q->escapeHTML($R::txt08);
        $adref->{int05} = 1;
   	$ctadr = $dbt->fetch_record($dbh,$adref);

	#if adr local doesn't exist
	if(!$ctadr->{c_id}){

   	  $auth_primary = $dbt->fetch_record($dbh_primary,$adref);

	  #if adr primary doesn't exist, then insert
          if(!$auth_primary->{c_id}){
	   my $shareec_id = 0;
   	   $shareec_id = $shwo->create_account($dbh_primary,$owner);
	   $adref->{c_id} = $shareec_id;

 	   #delete non address object elements before saving in contentadr
      	   foreach(@keywords){
  	     if($_ =~ /^txt\d+|^int\d+/){
     	      $q->delete($_) if($_ !~ /txt0\d/);
  	     }
 	   }
   	   $shwo->save_account($dbh_primary,$q,$shareec_id,$varenv,$owner);
   	   $auth_primary = $dbt->fetch_record($dbh_primary,$adref) if($adref->{c_id});
	  }

          print FILE "INSERT adr from record_primary to operator by dbname $varenv->{dbname} | pri $auth_primary->{c_id}\n";
          if($auth_primary->{c_id}){
	    #procedure like booking_request
            my %operator_hash = ();
            if($auth_primary->{txt17} && $auth_primary->{txt17} =~ /\w\s\w/){#append DB's
          	%operator_hash = map { $_ => 1 } split(/\s+/,$auth_primary->{txt17});
            }elsif($auth_primary->{txt17}){
          	$operator_hash{$auth_primary->{txt17}} = 1;
            }
            $operator_hash{$varenv->{dbname}} = 1;
	    my @operator_array = keys %operator_hash;

            my $update_primary = {
                table => "contentadr",
                txt17 => "@operator_array",
                txt19 => "$varenv->{dbname}",
                atime => "now()",
                owner => $owner,
            };
	    my $rows = $dbt->update_record($dbh_primary,$update_primary,$auth_primary);
	    $auth_primary = $dbt->fetch_record($dbh_primary,$adref);
	    if($auth_primary->{c_id} && $auth_primary->{txt17} && $auth_primary->{txt17} =~ /$varenv->{dbname}/){
              #insert
              my $c_id = 0;
              print FILE "INSERT adr from record_primary by operator_array @operator_array | pri $auth_primary->{c_id}\n";
              my $insert = {
                %$auth_primary,
                table   =>      "contentadr",
                mtime   =>      'now()',
                owner   =>      "198",
                };
                $c_id = $dbt->insert_contentoid($dbh,$insert,"reset_adropkeys");
            	$feedb->{u_id} = $dbt->insert_users($dbh,$auth_primary->{c_id},$owner);
		$adref->{c_id} = $c_id;
           	$dbt->update_one($dbh,$adref,"int07=100");
           	$dbt->update_one($dbh,$adref,"int09=1");
            }
	  }
	 }elsif($ctadr->{c_id}){
   	   $uref->{u_id} = $ctadr->{c_id};
   	   $users = $dbt->fetch_tablerecord($dbh,$uref) if($uref->{u_id});
	   if(!$users->{u_id}){
             print FILE "insert DMS user to $varenv->{dbname} | c_id: $ctadr->{c_id}\n";
             $feedb->{u_id} = $dbt->insert_users($dbh,$ctadr->{c_id},$owner);
	     $adref->{c_id} = $ctadr->{c_id};
             $dbt->update_one($dbh,$adref,"int07=100");
             $dbt->update_one($dbh,$adref,"int09=1");
	   }else{
	     $feedb->{message} = "failure::Fehler, der DMS-Account existiert bereits.";
	   }
	 }

	if($ctadr->{c_id}){
	  $uref->{u_id} = $ctadr->{c_id};
       	  $users = $dbt->fetch_tablerecord($dbh,$uref);
	  $feedb->{u_id} = $users->{u_id};
	}
    }

    #Final save dms users data
    if(ref($users) eq "HASH" && $users->{u_id} && $ctadr->{c_id} && $ctadr->{c_id} == $users->{u_id} && $base_edit eq "save_dmsusers"){
        print FILE "update DMS user to $varenv->{dbname} | c_id: $ctadr->{c_id}\n";
	my $users_update = {
                table   =>      "users",
                mtime   =>      "now()",
                owner   =>      "$owner",
                u_id    =>      "$users->{u_id}",
    	};

        foreach(@keywords){
          my $val = $q->param($_);
          my $valxx = $q->escapeHTML("$val");
          $valxx =~ s/^\s+//; $valxx =~ s/\s+$//;
          if($_ =~ /^int\d+/){
                $valxx =~ s/,/./g;
                $valxx = 0 if(!looks_like_number($valxx));# set to 0 for using == operator
                $feedb->{u_rows} = $dbt->update_one($dbh,$users_update,"$_=$valxx");
          }elsif($_ =~ /^txt\d+/){
     		my @val = $q->param($_);
     		$valxx = $q->escapeHTML("@val");
                $feedb->{u_rows} = $dbt->update_one($dbh,$users_update,"$_='$valxx'");
  	  }
        }
          
	#delete non address object elements before saving in contentadr
        foreach(@keywords){
           if($_ =~ /^txt\d+|^int\d+/){
            $q->delete($_) if($_ !~ /txt0\d/);
           }
        }
   	$shwo->save_account($dbh_primary,$q,$ctadr->{c_id},$varenv,$owner);
    }

 close(FILE);
 return $feedb;
}#end manage_dmsusers


#insert content or contentuser
sub new_content {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $node_meta = shift;
 $q->import_names('R');
 my $dbh = "";
 my $feedb = { message => "" };

 my $main_id = $node_meta->{main_id};
 $main_id = $node_meta->{parent_id} if($R::template_id == 194 && $node_meta->{main_id} >= 400001);

 my $pref_cc = {
    	table      => "$R::ct_table",
    	fetch      => "one",
    	template_id => "$R::template_id",
    	main_id => "$main_id",
 };

 my $cc_part = { c_id => 0 };
 $cc_part = $dbt->fetch_record($dbh,$pref_cc);#get last barcode by order barcode DESC
 my $next_barcode = $cc_part->{c_id};
 $next_barcode = $cc_part->{barcode} + 1 if($cc_part->{barcode} > 0);

 my $insert_ctu = {
      table   => "$R::ct_table",
      itime   => 'now()',
      mtime   => 'now()',
      owner   => "$users_dms->{u_id}",
      barcode => "$next_barcode",
      ct_name => "---",
      template_id => "$R::template_id",
      main_id => "$main_id",
      txt12 => "$dbt->{operator}->{$varenv{dbname}}->{oprefix}",
      };
 my $c_idnew = 0;
 $c_idnew = $dbt->insert_contentoid($dbh,$insert_ctu,"") if($R::ct_table && $R::template_id && $node_meta->{main_id});

 if($c_idnew){
   $feedb->{c_id} = $c_idnew;
   $feedb->{i_rows} = $i_rows;
 }
 return $feedb;
}


#insert new_trans
sub new_contenttrans {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $node_meta = shift;
 $q->import_names('R');
 my $dbh = "";
 my $feedb = { message => "" };

 my $insert_ctt = {
      table   => "$node_meta->{ct_table}",
      itime   => 'now()',
      mtime   => 'now()',
      owner   => "$users_dms->{u_id}",
      ct_name => "---",
      template_id => "$node_meta->{template_id}",
      main_id => "$node_meta->{main_id}",
      txt00 => "$node_meta->{node_name}",
      };
 my $c_idnew = 0;
 $c_idnew = $dbt->insert_contentoid($dbh,$insert_ctt,"");

 if($c_idnew){
   $feedb->{c_id} = $c_idnew;
   $feedb->{template_id} = $node_meta->{template_id};
   $feedb->{i_rows} = $i_rows;
 }
 return $feedb;
}#end new_contenttrans


#save content or contentuser 
sub save_content {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $node_meta = shift;
 my $c_id = shift || "";
 $q->import_names('R');
 my @keywords = $q->param;
 my $dbh = "";
 my $feedb = { message => "", u_rows => 0 };

 my $ct = { c_id => $c_id };
 my $update_ct = {
    table  => $node_meta->{ct_table},
    owner => $users_dms->{u_id},
    mtime   => "now()",
    };

 foreach(@keywords){
     #my $val = $q->param($_);
     #my $valxx = $q->escapeHTML("$val");
     my @val = $q->param($_);
     my $valxx = $q->escapeHTML("@val");

     #txt24 bike_node/station_group and txt25 bike_group multiple select
     if($node_meta->{template_id} == 225 && $_ eq "txt24"){
	my %station_filter_hash = ();
	my @_valxx_filter = ();
	my $node = { 
		 template_id => 205,
                 parent_id => 200013,
                 fetch  => "all",
                 keyfield => "main_id",
                 };
	my $bike_nodes = {};
	$bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$node);

        foreach my $bike_node (@val){
                foreach my $rid ( keys (%$bike_nodes)){
                  if($bike_node == $bike_nodes->{$rid}->{main_id} && $bike_nodes->{$rid}->{type_id}){
                    $station_filter_hash{$bike_nodes->{$rid}->{type_id}} = 1;
                  }
                }
        }

        foreach my $type_id (keys (%station_filter_hash)){
                push (@_valxx_filter, "$type_id");
        }

	$update_ct->{txt25} = "@_valxx_filter";
	$update_ct->{$_} = "$valxx";
     }elsif($node_meta->{template_id} == 205 && $_ eq "txt06"){#GPS
       if($valxx =~ /^(\d{1,2}\.\d+),\s?(\d{1,2}\.\d+)$/ || !$valxx || $valxx eq "null"){
	$update_ct->{$_} = "$valxx";
       }else{
	$feedb->{message} = "failure::Eingabefehler \"$valxx\", falsches GPS Format. Bitte folgendes Format mit latitude, longitude verwenden. Bsp: 47.927738,7.973855";
       }
     }elsif($node_meta->{template_id} == 205 && $_ =~ /byte/){
       if(!$valxx){
	$update_ct->{$_} = "";
       }elsif($valxx !~ /[g-z]/ig && length($valxx) == 32){
	$update_ct->{$_} = "\\x$valxx";
       }else{
	$feedb->{message} = "failure::Eingabefehler \"$valxx\", der Token muss 32 HEX Zeichen enthalten";
       }
     }elsif($_ =~ /^time\d+/){
	$valxx = "00:00" if($valxx !~ /\d{1,2}:\d{2}/);
	$update_ct->{$_} = "$valxx";
     }elsif($_ =~ /date_time\d+/){
       my ($date_time,$chck) = $lb->checkdate($valxx);
       if(!$chck){
	$update_ct->{$_} = "$valxx";
       }else{
	$feedb->{message} = "failure::Eingabefehler \"$valxx\", falsche Datum Uhrzeit Format";
       }
     }elsif($_ =~ /barcode/){
       my $val = $q->param("$_");
       $valxx = $q->escapeHTML("$val");
       $valxx =~ s/\s//g;
       if($node_meta->{ct_table} eq "content" && (looks_like_number($valxx) || $valxx == 0) && $R::rel_id && $R::template_id){
    	my $pref_cc = {
         table        => "content",
         fetch        => "one",
         template_id  => "$R::template_id",
         barcode      => "$valxx",
	 rel_id	      => "!=::$R::rel_id",
        };

    	my $cc_part = { c_id => 0 };
    	$cc_part = $dbt->fetch_record($dbh,$pref_cc);
	if(!$cc_part->{c_id}){
	 $update_ct->{$_} = "$valxx";
	}else{
         $feedb->{message} = "failure::Fehler \"$valxx\", die Artikelnummer ist bereits vorhanden";
        }
       }else{
	 $feedb->{message} = "failure::Eingabefehler \"$valxx\", hier sind nur numerische Werte erlaubt";
       }
     }elsif($_ =~ /^int/){
       my $val = $q->param("$_");
       $valxx = $q->escapeHTML("$val");
       $valxx =~ s/,/./;
       $valxx =~ s/\s//g;
       if(looks_like_number($valxx) || $valxx == 0){
	 $update_ct->{$_} = "$valxx";
       }elsif(!$valxx || $valxx eq "null"){
	 $update_ct->{$_} = "null";
       }else{
	 $feedb->{message} = "failure::Eingabefehler \"$valxx\", hier sind nur numerische Werte erlaubt";
       }
     }elsif($node_meta->{template_id} == 194 && $_ =~ /txt/ && !$valxx){
	$update_ct->{$_} = "null";#important for coalesce select alias lang fallback
     }elsif($_ =~ /ct_name|txt|state|time/){
	$update_ct->{$_} = "$valxx";
     }
 }

 if($ct->{c_id}){
   $feedb->{u_rows} += $dbt->update_record($dbh,$update_ct,$ct);
   $dbt->operators_cms($node_meta->{ct_table},$c_id,"update") if($node_meta->{template_id} == 194);
 }

 return $feedb;
}#end save_content

#move content (mainly for bike flot)
sub move_content {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $node_meta = shift;
 my $c_id = shift || "";
 $q->import_names('R');
 my $dbh = "";
 my $feedb = { message => "", u_rows => 0 };

  my $update_relation = {
                table   => "relation",
                rel_id => "$R::rel_id",
 };

 if($R::template_id && $node_meta->{template_id} == $R::template_id && $R::rel_id && $R::main_id){
   $u_rows += $dbt->update_one($dbh,$update_relation,"main_id=$R::main_id");
   my $uri_path = $dbt->recurse_node($dbh,$R::main_id);
   print redirect("$varenv{wwwhost}/$uri_path?node2edit=editpart\&rel_id=$R::rel_id\&return=$i_rows-$u_rows-$d_rows");
   exit 0;
 }else{
    $feedb->{message} = "failure::Der Artikel konnte nicht verschoben werden.";
 }

 return $feedb;
}

#delete content
sub delete_content {
 my $self = shift;
 my $node_meta = shift || "";
 my $c_id = shift || "";
 my $users_dms = shift || "";
 my $dbh = "";
 my $feedb = { d_rows => 0,message => "" };
 
 if($node_meta->{ct_table},$c_id){
  $dbt->operators_cms($node_meta->{ct_table},$c_id,"delete") if($node_meta->{template_id} == 194);
  $feedb->{d_rows} = $dbt->delete_content($dbh,$node_meta->{ct_table},$c_id);
 }
 return $feedb;
}

#save invoice address or text
sub save_text2contenttrans {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $c_id = shift || "";

 $q->import_names('R');
 my @keywords = $q->param;
 my $dbh = "";
 my $feedb = { message => "" };

 open(FILE,">>$varenv{logdir}/save_text2contenttrans.log") if($debug);
 print FILE "\n*--> $now_dt| c_id:$c_id | u_id:$users_dms->{u_id}\n" if($debug);
 print FILE Dumper($q) . "\n" if($debug);

 my $ctt = { c_id => $c_id };
 my $update_ctt = {
    table  => "contenttrans",
    owner => $users_dms->{u_id},
    mtime   => "now()",
    };

 foreach(@keywords){
     my $val = $q->param($_);
     my $valxx = $q->escapeHTML("$val");
     my @val = $q->param($_);
     $valxx = $q->escapeHTML("@val");

     #Set formular title if Mahnung
     if($_ eq "int06"){
       my $s_hash = {};
       $s_hash = $dbt->{shareedms_conf}->{warning_state};
       if($valxx > 0){
  	foreach my $s_key (sort keys (%{ $s_hash })) {
	 if($valxx eq $s_key){
      	  $update_ctt->{txt00} = $s_hash->{$s_key} if($s_hash->{$s_key} =~ /Mahnung/);
	  $update_ctt->{$_} = "$valxx";
 	 }
  	}
       }else{
	$update_ctt->{txt00} = "Rechnung";
	$update_ctt->{$_} = "$valxx";
       }
     }
     elsif($_ =~ /int|time/){
      if(!$valxx){
	$update_ctt->{$_} = "null";
      }else{
	$update_ctt->{$_} = "$valxx";
      }
     } 
     elsif($_ =~ /txt|state/){
	$update_ctt->{$_} = "$valxx";
     }
 }

 if($ctt->{c_id}){
   $u_rows += $dbt->update_record($dbh,$update_ctt,$ctt);
   #empty warn_time if warnstate not set 
   #if(!$R::int06 || !$R::warn_time){
   #  $dbt->update_one($dbh,$update_ctt,"warn_time=null",$c_id);
   #}
   if($R::txt22 && $R::txt22 eq "Zahlungseingang"){ 
     $dbt->update_one($dbh,$update_ctt,"int14=null",$c_id);
     $dbt->update_one($dbh,$update_ctt,"pay_time=now()",$c_id);
   }
   if($R::txt22 && $R::txt22 eq "Zahlung offen"){ 
     $dbt->update_one($dbh,$update_ctt,"int14=1",$c_id);
     $dbt->update_one($dbh,$update_ctt,"pay_time=null",$c_id);
   }
 }
 close(FILE) if($debug);

 $feedb->{u_rows} = $u_rows;
 return $feedb;
}#end save_text2contenttrans

#part or fee to invoice
sub insert_contenttranspos {
 my $self = shift;
 my $q = shift;
 my $owner = shift || "";

 $q->import_names('R');
 my $dbh = "";
 my $feedb = { 	i_rows => 0,
		pos_id => 0,
	 	message => "",
       	};

 if((looks_like_number($R::c_id) || looks_like_number($R::json_select)) && looks_like_number($R::c_id4trans)){
   my $pref = {
     table  => "contenttrans",
     fetch  => "one",
     c_id   => $R::c_id4trans,
     "ct.state" => "is::null",
   };

   my $ctt = { c_id => 0 };
   $ctt = $dbt->fetch_tablerecord($dbh,$pref);

   if($ctt->{c_id}){
    my $pref_cc = {
        table => "content",
        fetch => "one",
        template_id => "IN::(224,229)",
    };

    $pref_cc->{c_id}  = $R::c_id if($R::c_id);
    $pref_cc->{barcode}  = $R::json_select if($R::json_select);
    my $cc_part = { c_id => 0 };
    $cc_part = $dbt->fetch_record($dbh,$pref_cc) if($R::c_id || $R::json_select);
      
    my $ctadr = { 
	    c_id => $ctt->{int10},
	    txt01 => $ctt->{txt01},
    };

    if($cc_part->{c_id}){
     $cc_part->{int02} = "0" if(!$cc_part->{int02});
     $feedb->{pos_id} = $dbt->insert_pos($dbh,$R::c_id4trans,$cc_part,"",$ctadr,"","",$cc_part->{barcode},"0",$owner,"");
    }else{
     $feedb->{message} = "failure::Fehler, Artikel nicht vorhanden.";
    }
    if($feedb->{pos_id} > 0){
     $feedb->{i_rows} += 1;
     $dbt->update_content4comp($dbh,$cc_part->{c_id},"-","1");
     $feedb->{message} = "Artikel Nr. $cc_part->{barcode} erfolgreich hinzugefügt.";
    }
  }else{
   $feedb->{message} = "failure::Fehler, die Rechnung ist bereits gebucht. Das hinzufügen einer weiteren Rechnungsposition wird somit abgelehnt.";
  }
 }else{
   $feedb->{message} = "failure::Fehler, es fehlt mindestens ein Key! ($R::c_id && $R::c_id4trans)";
 }
 return $feedb;
}#end insert_contenttranspos


sub save_contenttranspos {
 my $self = shift;
 my $q = shift;
 my $c_id = shift;
 my $owner = shift;

 my $dbh = "";
 my $feedb = { message => "" };
 if($c_id && $R::start_date =~ /\d{1,2}\.\d{1,2}\.\d{4}/ && $R::end_date =~ /\d{1,2}\.\d{1,2}\.\d{4}/){
   my $s_hh = $q->escapeHTML("$R::s_hh") || "0";
   my $s_mi = $q->escapeHTML("$R::s_mi") || "0";
   my $e_hh = $q->escapeHTML("$R::e_hh") || "0";
   my $e_mi = $q->escapeHTML("$R::e_mi") || "0";
   $s_hh = "24" if($s_hh > "24");
   $e_hh = "24" if($e_hh > "24");
   $s_mi = "59" if($s_mi > "59");
   $e_mi = "59" if($e_mi > "59");
   $s_hh = sprintf('%.2d',$s_hh);
   $e_hh = sprintf('%.2d',$e_hh);
   $s_mi = sprintf('%.2d',$s_mi);
   $e_mi = sprintf('%.2d',$e_mi);
   my $start_time="";
   my $end_time="";

   my $dtnow = DateTime->now( time_zone => "Europe/Berlin" );
   my $dt0 = DateTime->now( time_zone => "Europe/Berlin" );
   my $dt1 = DateTime->now( time_zone => "Europe/Berlin" );

   if($R::start_date =~ /(\d{1,2})\.(\d{1,2})\.(\d{4})/){
     $dt0 = DateTime->new(
        year       => $3,
        month      => $2,
        day        => $1,
        hour       => $s_hh,
        minute     => $s_mi,
        time_zone  => 'Europe/Berlin',
    );
    $start_time = $dt0->strftime("%Y-%m-%d %H:%M:%S");
    #print $start_time;
   }

   if($R::end_date =~ /(\d{1,2})\.(\d{1,2})\.(\d{4})/){
     $dt1 = DateTime->new(
        year       => $3,
        month      => $2,
        day        => $1,
        hour       => $e_hh,
        minute     => $e_mi,
        time_zone  => 'Europe/Berlin',
    );
    $end_time = $dt1->strftime("%Y-%m-%d %H:%M:%S");
   }

   $bw->log("save_contenttranspos time-check: $c_id && $start_time && $end_time",$c_id,"");
    if($c_id && $start_time && $end_time && $dt0 < $dtnow && $dt1 < $dtnow){

       my $pref = {
        table           => "contenttrans",
        table_pos       => "contenttranspos",
        fetch           => "one",
        template_id     => "218",#open invoice tpl_id
        c_id         => $c_id,
        "ct.state" => "is::null",
       };

       my $ctpos = { c_id => 0 };
       $ctpos =  $dbt->collect_post($dbh,$pref);

       my $pricing = {};
       my $counting = {};
       my $update_pos = {
                table   => "contenttranspos",
                start_time => "$start_time",
                end_time => "$end_time",
		int04 => "null",
                owner_end => $owner,
                mtime   =>      "now()",
       };

       #if sig
       if($ctpos->{int11} == 3 && $ctpos->{ca_id}){
        #sig booking_request
        my $sig_book = {
                bikeId => "",
                rentalId => "",
        };
	if(looks_like_number($R::int10) && $R::int10 == 1){
    	  my $authref = {
                table           => "contentadr",
                fetch           => "one",
                template_id     => "202",
                c_id            => "=::$ctpos->{ca_id}",
     	  };
	  my $authraw = {};
     	  $authraw = $dbt->fetch_record($dbh,$authref);
	  $sig_book = $si->sig_booking(\%varenv,"reserve_end",$authraw,"",$ctpos);
	}
       }else{
	if($ctpos->{c_id}){
         $u_rows += $dbt->update_record($dbh,$update_pos,$ctpos);
	}else{
	 $feedb->{message} = "failure::Fehler, Änderung abgelehnt da Rechnung bereits gebucht";
	}
       }
       #again to get setted date-times
       $ctpos =  $dbt->collect_post($dbh,$pref);

       #print "$R::start_date $s_hh:$s_mi | $R::start_date $e_hh:$e_mi xxxxxxxxx<br>";exit;
       ($pricing,$counting) = $pri->counting_rental(\%varenv,$ctpos);
       $update_pos->{int38} = "$counting->{int38}" if(looks_like_number($counting->{int38}));
       $update_pos->{int39} = "$counting->{int39}" if(looks_like_number($counting->{int39}));
       $update_pos->{int40} = "$counting->{int40}" if(looks_like_number($counting->{int40}));
       $update_pos->{int41} = "$counting->{int41}" if(looks_like_number($counting->{int41}));

        $update_pos->{int10} = $R::int10 if(looks_like_number($R::int10));
        $update_pos->{int20} = $R::int20 if(looks_like_number($R::int20));
        $update_pos->{int04} = $R::int04 if(looks_like_number($R::int04));
        $update_pos->{int07} = $R::int07 if(looks_like_number($R::int07));
        $update_pos->{int08} = $R::int08 if(looks_like_number($R::int08));
        
	$update_pos->{txt01} = $q->escapeHTML($R::txt01) if($R::txt01);
	$update_pos->{txt23} = $q->escapeHTML($R::txt23) if($R::txt23);

	#before update bike content check if realy last rental
	my $pref2ck = {
          table           => "contenttrans",
          table_pos       => "contenttranspos",
          fetch           => "one",
          template_id     => "218",#open invoice tpl_id
	  start_time      => ">::$end_time",
	  barcode         => $ctpos->{barcode},
          "ct.state" => "is::null",
        };

        my $ctpos2ck = { c_id => 0 };
        $ctpos2ck =  $dbt->collect_post($dbh,$pref2ck);

	#update bike content only if there is no later rental start
   	$bw->log("update bike content only if there is no later rental start: $ctpos->{cc_id} && !$ctpos2ck->{c_id}",$c_id,"");
        if($ctpos->{cc_id} && !$ctpos2ck->{c_id}){
          my $ctpref = {
            table  => "content",
            fetch  => "one",
            c_id   => $ctpos->{cc_id},
          };
          my $ctbike = $dbt->fetch_tablerecord($dbh,$ctpref);

	    $ctpref->{int10} = $R::int10 if(looks_like_number($R::int10));
	    $ctpref->{int20} = $R::int20 if(looks_like_number($R::int20));
	    $ctpref->{int04} = $R::int04 if(looks_like_number($R::int04));
	    
	  $ctpref->{owner} = $owner;
	  $ctpref->{mtime}   = "now()";

          $u_rows += $dbt->update_record($dbh,$ctpref,$ctbike);
	  $feedb->{message} = "Mietdaten gespeichert und Mietrad Stati in Warenstamm übernommen";
	}else{
	  $feedb->{message} = "Mietdaten gespeichert. Achtung, Mietrad Stati in Warenstamm NICHT übernommen, da es sich nicht um die letzte Miete handelt";
        }

	if($ctpos->{c_id}){
         $u_rows += $dbt->update_record($dbh,$update_pos,$ctpos);

	 #
         if($ctpos->{ct_id}){
          my $ctpref = {
            table  => "contenttrans",
            c_id   => $ctpos->{ct_id},
	    start_time => "$start_time",
	    end_time => "$end_time",
	    owner => $owner,
	    mtime   => "now()",
          };
          $u_rows += $dbt->update_record($dbh,$ctpref,$ctpref);
        }
       }else{
	$feedb->{message} = "failure::Fehler 2, Änderung abgelehnt da Rechnung bereits gebucht";
       }

     }else{
       $bw->log("save_contenttranspos time-check fails by: $c_id && $start_time && $end_time",$c_id,"");
     }
  }elsif($R::int02 && $R::int03){
    $R::int02 =~ s/,/\./ if($R::int02 =~ /\,/);
    $R::int03 =~ s/,/\./ if($R::int03 =~ /\,/);
    $R::txt01 = $q->escapeHTML($R::txt01) if($R::txt01);
    $R::txt23 = $q->escapeHTML($R::txt23) if($R::txt23);
    if($c_id && looks_like_number($R::int02) && looks_like_number($R::int03)){
      my $pref = {
        table           => "contenttrans",
        table_pos       => "contenttranspos",
        fetch           => "one",
        template_id     => "IN::(218,219)",#open invoices and prepaid tpl_id
        c_id         => $c_id,
        "ct.state" => "is::null",
       };

       my $ctpos = { c_id => 0 };
       $ctpos =  $dbt->collect_post($dbh,$pref);

      my $update_pos = {
                table   => "contenttranspos",
                int02 => "$R::int02",
                int03 => "$R::int03",
                txt01 => "$R::txt01",
                txt23 => "$R::txt23",
                owner => $owner,
                mtime => "now()",
       };
       if($ctpos->{c_id}){
         $u_rows += $dbt->update_record($dbh,$update_pos,$ctpos);
       }else{
         $feedb->{message} = "failure::Fehler 1, Änderung abgelehnt da Rechnung bereits gebucht";
       }
     }else{
       $feedb->{message} = "failure::Fehler, es sind nur numerische Werte erlaubt ($c_id && $R::int02 && $R::int03).";
     }
  }else{
     $feedb->{message} = "failure::Fehler, fehlerhafte Eingaben oder Datensatz nicht gefunden!";
  }
 $feedb->{u_rows} = $u_rows;
 return $feedb;
}#end save_contenttranspos

#delete_contenttranspos
sub delete_contenttranspos {
 my $self = shift;
 my $q = shift;
 my $c_id = shift;
 my $owner = shift;
 my $dbh = "";
 my $feedb = { 	d_rows => 0,
	 	message => "",
	};

  #prepaid pos check
  my $pref = {
        table => "contenttrans",
        table_pos => "contenttranspos",
        fetch => "one",
	catch => "content_contenttranspos",
        template_id => "219",#prepaid tpl_id
        c_id => $c_id,
	int02 => ">=::1",
        "ct.state" => "is::null",
  };

  my $ctpos = { c_id => 0 };
  $ctpos =  $dbt->collect_post($dbh,$pref) if($c_id);

 #prefent deleting prepaid positions
 if($ctpos->{c_id}){
   $feedb->{message} = "failure::Datensatz löschen ist für Prepaid-Account deaktiviert. Prepaid payments müssen erhalten bleiben.";
 }else{
   $feedb->{d_rows} = $dbt->delete_content($dbh,"contenttranspos",$c_id);
 }
 return $feedb;
}#end delete_contenttranspos


#set Faktura workflow like Rechnung to Storno
sub set_workflow {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $c_id = shift;
 my $set_main_id = shift || "";
 my %varenv = $cf->envonline();
 my $dbh = "";
 my $node = $dbt->get_node($dbh,$set_main_id);

 my $pref = {
     table  => "contenttrans",
     fetch  => "one",
     c_id   => $c_id,
  };

  my $ctt = { c_id => 0 };
  $ctt = $dbt->fetch_tablerecord($dbh,$pref);

  #barcode setting logic reset. keep barcode from orignal for backlinking
  #counter invoice subnr
  if($ctt->{ct_name} =~ /\d+-\d+/){
        my ($ct_name,$subname) = split(/-/,$ctt->{ct_name});
        $subname++;
        $ctt->{ct_name} = "$ct_name-$subname";
  }else{
        $ctt->{ct_name} = "$ctt->{ct_name}-1";
  }
  delete $ctt->{c_id}; 
  delete $ctt->{txt00}; 
  delete $ctt->{state}; 
  delete $ctt->{int01};
  delete $ctt->{int04};
  delete $ctt->{int14};
  delete $ctt->{int15};
  delete $ctt->{int16};
  delete $ctt->{int18};
  delete $ctt->{txt16};
  delete $ctt->{txt12};
  delete $ctt->{txt21};
  delete $ctt->{txt22};
  delete $ctt->{txt23};
  delete $ctt->{txt25};
  delete $ctt->{txt30};
  delete $ctt->{itime}; 
  delete $ctt->{mtime}; 
  delete $ctt->{close_time}; 
  delete $ctt->{invoice_time};
  delete $ctt->{pay_time};
  delete $ctt->{warn_time};

  my $insert_ctt = {
      %$ctt,
      table   => "contenttrans",
      itime   => 'now()',
      mtime   => 'now()',
      owner   => "$users_dms->{u_id}",
      ct_name => "$ctt->{ct_name}",
      txt00 => "$node->{node_name}",
      template_id => "218",
      main_id => "$node->{main_id}",
      };
   my $c_idnew = 0;
   $c_idnew = $dbt->insert_contentoid($dbh,$insert_ctt,"");
   $i_rows = 1 if($c_idnew);
   #print Dumper($insert_ctt);
   #exit;

   #position copy
   if($c_idnew > 0){
     my ($cttpos,$rows) = $dbt->collect_contentpos($dbh,"contenttrans",$c_id);
     foreach my $id (sort { $cttpos->{$a}->{c_id} <=> $cttpos->{$b}->{c_id} } keys(%$cttpos)){
       #reverse pos sum for example by Storno
       $cttpos->{$id}->{int01} = $cttpos->{$id}->{int01} * -1 if($cttpos->{$id}->{int01} != 0);
       $cttpos->{$id}->{int05} = $cttpos->{$id}->{c_id};# set source pos id if sub doc
       $cttpos->{$id}->{txt23} = "workflow doc" if($cttpos->{$id}->{int01} == -1);

       delete $cttpos->{$id}->{c_id};
       delete $cttpos->{$id}->{ct_id};
       delete $cttpos->{$id}->{itime};
       delete $cttpos->{$id}->{mtime};
       my $insert_pos = {
                %{$cttpos->{$id}},
                table   => "contenttranspos",
                ct_id   => $c_idnew,
                itime   => 'now()',
                mtime   => 'now()',
       };
        my $ctpos_id = $dbt->insert_contentoid($dbh,$insert_pos,"");
        $i_rows += 1 if($ctpos_id > 0);

     }
     
     my $update_users = {
        table   => "users",
        change   => "no_time",
        u_id    => $users_dms->{u_id}
     };
     $dbt->update_one($dbh,$update_users,"c_id4trans=$c_idnew,tpl_id4trans=218"); 

     print redirect("$varenv{wwwhost}/DMS/Faktura?ct_trans=open\&c_id4trans=$c_idnew\&tpl_id4trans=218\&owner=$users_dms->{u_id}\&offset=$R::offset\&limit=$R::limit\&return=$i_rows-$u_rows-$d_rows");
     exit 0;
   }
}#end set_workflow


#generate invoice from operator accounting
sub set_accounting2invoice {
 my $self = shift;
 my $q = shift;
 my $users_dms = shift;
 my $c_id = shift;
 my $set_main_id = shift || "";
 my %varenv = $cf->envonline();
 my $dbh = "";
 my $node = $dbt->get_node($dbh,$set_main_id);

 my $pref = {
     table  => "contenttrans",
     fetch  => "one",
     c_id   => $c_id,
  };

  my $ctt = { c_id => 0 };
  $ctt = $dbt->fetch_tablerecord($dbh,$pref);

  #barcode setting logic reset. keep barcode from orignal for backlinking
  #counter invoice subnr
  if($ctt->{ct_name} =~ /\d+-\d+/){
        my ($ct_name,$subname) = split(/-/,$ctt->{ct_name});
        $subname++;
        $ctt->{ct_name} = "$ct_name-$subname";
  }else{
        $ctt->{ct_name} = "$ctt->{ct_name}-1";
  }
  delete $ctt->{c_id}; 
  delete $ctt->{txt00}; 
  delete $ctt->{state}; 
  delete $ctt->{int04}; 
  delete $ctt->{itime}; 
  delete $ctt->{mtime}; 
  delete $ctt->{close_time}; 
  delete $ctt->{invoice_time};
  delete $ctt->{pay_time};

  my $insert_ctt = {
      %$ctt,
      table   => "contenttrans",
      itime   => 'now()',
      mtime   => 'now()',
      owner   => "$users_dms->{u_id}",
      ct_name => "$ctt->{ct_name}",
      txt00 => "$node->{node_name}",
      template_id => "218",
      main_id => "$node->{main_id}",
      };
   my $c_idnew = 0;
   $c_idnew = $dbt->insert_contentoid($dbh,$insert_ctt,"");
   $i_rows = 1 if($c_idnew);
   #taking just same (operator_accounting) invoices detected by int10=2

   my $update_users = {
        table   => "users",
        change   => "no_time",
        u_id    => $users_dms->{u_id}
   };
   $dbt->update_one($dbh,$update_users,"c_id4trans=$c_idnew,tpl_id4trans=218"); 

   my $uri_path = $dbt->recurse_node($dbh,$node->{main_id});
   print "$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows\n";
   print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
   exit 0;

}#end set_accounting2invoice

#new node relation with option to create subnode for Servicelog
sub new_relation {
 my $self = shift;
 my $q = shift;
 my $main_id = shift;
 my $owner = shift;
 my %varenv = $cf->envonline();
 my $ret = "";
 my $dbh = "";

 #$ret = "failure::temporarily disabled";
 #return $ret;
 open(FILE,">>$varenv{logdir}/new_relation.log") if($debug);
 print FILE "\n*--> $now_dt| main_id: $main_id | owner: $owner\n" if($debug);

   my $prefix_id = "0";
   my $working_parent_id = $R::parent_id;
   $prefix_id = $1 if($R::main_id =~ /^(\d)/ && $R::main_id >= "100000");
   $ret = $lb->checkinput($R::node_name);
   return $ret if($ret =~ /failure/);
   my $node_name = $q->escapeHTML($R::node_name);

   #check multiple node_name
   my $subrelnode = $dbt->get_subrelnode($dbh,$working_parent_id,"",$node_name);
   if($subrelnode->{node_name} eq "$R::node_name"){
     return "failure::Abbruch, der Menuename \"$subrelnode->{node_name}\" ist bereits vorhanden. Bitte eindeutige Menuenamen verwenden.";
   }

   if($R::new_submenu){
     $working_parent_id = $R::main_id;
     $prefix_id++;
   }
   my $new_main_id = $dbt->get_freenode($dbh,$prefix_id);
   my $template_id = 0,
   my $n_sort = 1;
   my $type_id = 300101;
   my $energy_id = 0;
   my $battery_hidden = 0;
   my $battery_accessible = 0;
   $template_id = $R::template_id if(looks_like_number($R::template_id));
   $n_sort = $R::n_sort if(looks_like_number($R::n_sort));
   $type_id = $R::type_id if(looks_like_number($R::type_id));
   $energy_id = $R::energy_id if(looks_like_number($R::energy_id));
   $energy_id = $R::battery_hidden if(looks_like_number($R::battery_hidden));
   $battery_accessible = $R::battery_accessible if(looks_like_number($R::battery_accessible));

    my $insert = {
	main_id		=> $new_main_id,
	parent_id	=> $working_parent_id,
        template_id   	=> $template_id,
	content_id	=> 0,
        node_name 	=> $node_name,
	n_sort		=> $n_sort,
	type_id		=> $type_id,
	energy_id	=> $energy_id,
	battery_hidden => $battery_hidden,
	battery_accessible => $battery_accessible,
        owner   	=> $owner,
        change   	=> "now()",
    };
    my $rel_id = $dbt->insert_nodeoid($dbh,$insert);
   $i_rows = 1 if($rel_id > 0);

   print FILE "new_relation with" . Dumper($insert) . "\n" if($debug);

   #sub Servicelog for rental bikes
   if($template_id == 205){
     $prefix_id++;
     my $new_submain_id = $dbt->get_freenode($dbh,$prefix_id);
     my $new_subtemplate_id = $dbt->get_freetpl($dbh,"401","499");

     my $ret_tpl_id = $dbt->copy_template($dbh,"400",$new_subtemplate_id,"$node_name Service-Config",$owner);
     my $ct_name = "$node_name Service-Config";
     $ct_name =~ s/^Flotte //;
     $dbt->copy_content($dbh,"contentuser","400",$ret_tpl_id,"$ct_name",$owner);

     my $insert_sub = {
	main_id		=> $new_submain_id,
	parent_id	=> $new_main_id,
        template_id   	=> $new_subtemplate_id,
	content_id	=> 0,
        node_name 	=> "$node_name-Servicelog",
	n_sort		=> $n_sort,
	lang		=> "de",
        owner   	=> $owner,
        change   	=> "now()",
     };
     my $subrel_id = $dbt->insert_nodeoid($dbh,$insert_sub);
     $i_rows += 1 if($subrel_id > 0);
     
     print FILE "new_subrelation with" . Dumper($insert_sub) . "\nwith template_id=$ret_tpl_id" if($debug);
   }
  
   close(FILE) if($debug);

   my $uri_path = $dbt->recurse_node($dbh,$new_main_id);
   print "$varenv{wwwhost}/$uri_path?node2edit=edit_relation\&return=$i_rows-$u_rows-$d_rows\n";
   print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
   exit 0;
}
#end new_relation


#save node relation 
sub save_relation {
 my $self = shift;
 my $q = shift;
 my $main_id = shift;
 my $owner = shift;
 my %varenv = $cf->envonline();

 my @keywords = $q->param;
 my $dbh = "";
 my $update_node = { 
                table   => "nodes",
                main_id => "$main_id",
 };
 my $update_relation = { 
                table   => "relation",
                main_id => "$main_id",
 };
 my $subrelnode = $dbt->get_subrelnode($dbh,$main_id,"","");

   foreach(@keywords){
    my $val = $q->param($_);
    my $valxx = $q->escapeHTML("$val");
    $valxx =~ s/^\s+//; $valxx =~ s/\s+$//;
    if(($_ eq "node_name") && $valxx){
        #if defined another path 
        my $node_name = $valxx;
        my $node_path = $node_name;
        #internal for splitting node_name node_path
        ($node_name,$node_path) = split(/\|/,$node_name) if($node_name =~ /\|/);
        my $return;
        $return = $lb->checkinput($node_name);
        $return = $lb->checkinput($node_path);
        return $return if($return =~ /failure/);

	$u_rows = $dbt->update_one($dbh,$update_node,"node_name='$node_name'");
	$u_rows = $dbt->update_one($dbh,$update_node,"node_path='$node_path'");

	if($subrelnode->{main_id} && $subrelnode->{template_id} >= 400 && $subrelnode->{template_id} <= 499){
	  my $update_subnode = { 
                table   => "nodes",
                main_id => "$subrelnode->{main_id}",
 	  };
	  $u_rows = $dbt->update_one($dbh,$update_subnode,"node_name='$node_name-Servicelog'");
	  $u_rows = $dbt->update_one($dbh,$update_subnode,"node_path='$node_path-Servicelog'");
	}
    }elsif($_ =~ /template_id/ && $valxx){
        $u_rows = $dbt->update_one($dbh,$update_relation,"template_id=$valxx");
    }elsif($_ =~ /int|n_sort|owner/){
        $valxx =~ s/,/./;
        $valxx = "null" if(!$valxx && $valxx ne "0");#for empty
        $valxx = "0" if($valxx eq "0");
	$u_rows = $dbt->update_one($dbh,$update_node,"$_=$valxx") if($valxx =~ /^\d+$|null|0/);
    }elsif($_ =~ /archive|type_id|energy_id|battery_/ && $valxx){
	$u_rows = $dbt->update_one($dbh,$update_node,"$_=$valxx");
	$valxx = "null" if(!$valxx || $valxx eq "null");
    }elsif($_ =~ /fleed_/){
	$u_rows = $dbt->update_one($dbh,$update_node,"$_='$valxx'");
    }elsif($_ =~ /node_public/){
        $valxx = "t" if($valxx eq "1" || $valxx eq "t");
        $valxx = "f" if(!$valxx || $valxx eq "f");
	$u_rows = $dbt->update_one($dbh,$update_node,"$_='$valxx'");
    }

    if(! -d "$varenv{data}/$main_id"){
     mkdir("$varenv{data}/$main_id",0777);
     mkdir("$varenv{data}/$main_id-thumb",0777);
     mkdir("$varenv{data}/$main_id-resize",0777);
    }
   }
   my $uri_path = $dbt->recurse_node($dbh,$main_id);
   print redirect("$varenv{wwwhost}/$uri_path?node2edit=edit_relation\&return=$i_rows-$u_rows-$d_rows");
   exit 0;
}

#delete node relation  with some ki deleting sub content
sub delete_relation {
 my $self = shift;
 my $q = shift;
 my $main_id = shift;
 my $owner = shift;
 my %varenv = $cf->envonline();
 my $dbh = "";
 my $ret = "";
 my $debug=1;
 open(FILE,">>$varenv{logdir}/delete_relation.log") if($debug);
 print FILE "\n*--> $now_dt| main_id: $main_id \n" if($debug);

 #get all node para
  my $noderef = {
                 main_id => $main_id,
                 fetch  => "one",
                 };
  my $noderel = $dbt->fetch_rel4tpl4nd($dbh,$noderef);
   my $ctref = {
       table => "$noderel->{ct_table}",
       main_id => $main_id,
       fetch => "one",
       c_id => ">::0",
       };
   my $ct_record = $dbt->fetch_record($dbh,$ctref);

 
 my $collect_rows=0;
 if($noderel->{template_id} == 205){
   (my $collect_node,$collect_rows) = $dbt->collect_noderel($dbh,$noderel->{parent_id},$noderel->{template_id});
 }
 my $subrelnode = $dbt->get_subrelnode($dbh,$main_id,"","");

 #if 1 then deleteable
 my $deleteable_subnode = 1;
 my $deleteable_node = 1;
 my $deleteable_last_node = 1;
 if($subrelnode->{template_id} > 400 && $subrelnode->{template_id} < 499){
   $deleteable_subnode = 1;
   if($collect_rows <= 1){
     $deleteable_last_node = 0;
   }
 }elsif($subrelnode->{template_id}){
   $deleteable_subnode = 0;
 }

 if($ct_record->{c_id} > 0){
   $deleteable_node = 0;
 }
 #print "$deleteable_subnode|$deleteable_node|$deleteable_last_node|$collect_rows";

 print FILE "deleteable_subnode:$deleteable_subnode | deleteable_node:$deleteable_node | deleteable_last_node:$deleteable_last_node --> collect_rows: $collect_rows|c_id: $ct_record->{c_id}\n" if($debug);

    if($deleteable_last_node == 0){
        $ret = "failure::Abbruch, es muss mindestens eine Mietrad Flotte definiert sein. ($collect_rows on $noderel->{parent_id})";
    }elsif($deleteable_subnode == 0 || $deleteable_node == 0){
        $ret = "failure::Abbruch, der Ordner enthält Daten. Für die referentielle Integrität ist es notwendig die Ordner Inhalte (content) und/oder Relationen des Ordners zu löschen. ($deleteable_subnode == 0 || $deleteable_node == 0 , $subrelnode->{template_id}, $main_id, $ct_record->{c_id}, $noderel->{ct_table})";
    }else{
      print FILE "delete_relation with $subrelnode->{main_id}, $subrelnode->{template_id}\n" if($debug);
      if($deleteable_subnode && $subrelnode->{main_id}){
	if($subrelnode->{template_id} > 400 && $subrelnode->{template_id} < 499){
          $dbt->delete_content($dbh,"contentpos","all",$subrelnode->{template_id});
          $dbt->delete_content($dbh,"contentuser",$subrelnode->{template_id});
	}
        $d_rows += $dbt->delete_noderel($dbh,$subrelnode->{main_id});
        $d_rows += $dbt->delete_template($dbh,$subrelnode->{template_id});
      }
      $d_rows += $dbt->delete_noderel($dbh,$main_id);

      remove_tree("$varenv{data}/$main_id");
      remove_tree("$varenv{data}/$main_id-thumb");
      remove_tree("$varenv{data}/$main_id-resize");
      my $uri_path = $dbt->recurse_node($dbh,$noderel->{parent_id});
      $uri_path =~ s/\/\w+$//;
      print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
      exit 0;
    }

 close(FILE) if($debug);
 return $ret;
}


#save service-config
sub save_service_desc {
  my $self = shift;
  my $q = shift;
  my $users_dms = shift;
  my $c_id = shift;
  my @keywords = $q->param;
  my $dbh = "";
  my $c_tplid = {c_id => $c_id};
  my $update_ctuser = {
          table   => "contentuser",
          template_id => "199",
          mtime => "now()",
          owner => "$users_dms->{u_id}",
  };

  my $update_template = {
          table   => "template",
          tpl_id => "$c_id",
          owner => "$users_dms->{u_id}",
  };

  open(FILE,">>$varenv{logdir}/save_service_desc.log") if($debug);
  print FILE "\n*Prelib--> $now_dt | ctpl_id $c_id | owner: $users_dms->{u_id}\n" if($debug);

  my $tpl_order_desc = "c_id=ID=4=0=0,mtime=Zeitstempel=6=0=0,owner=von=3=0=0,barcode=Rad=3=0=0,txt01=Aufgaben=area5-8=0=0";
  foreach(@keywords){
   my @val = $q->param($_);
   my $valxx = $q->escapeHTML("@val");
   $valxx =~ s/\s/=/g;
   print FILE $_ . ":" . $valxx . "<br>\n";

   my $count = 0;
   ++$count while $valxx =~ /=/g;
   return "failure::Abbruch. Die Wartungsnamen dienen als Schlüsselwerte und dürfen somit keine Leer- oder Sonderzeichen enthalten ($valxx). Das Speichern wurde abgebrochen!" if($count > 4);

   #txt01:int01=Reifen-Bremse-Lampe=checkbox=10=2
   if($_ =~ /txt\d+/ && $valxx =~ /int\d+=[a-z-]+=checkbox=\d+=\d/i){
	$update_ctuser->{$_} = "$valxx";
	$tpl_order_desc .= ",$valxx";
   }else{
	$update_ctuser->{$_} = "";
	$tpl_order_desc .= "";
   }
  }
  print FILE "update contentuser c_id:$c_id\n" . Dumper($update_ctuser) . "\n";
  print FILE "update template tpl_id:$c_id\n" . $tpl_order_desc . "\n";

  my $rows = 0;
  $rows = $dbt->update_record($dbh,$update_ctuser,$c_tplid);
  $rows = $dbt->update_one($dbh,$update_template,"tpl_order='$tpl_order_desc'");

  close(FILE) if($debug);
  return $rows;
}

#Operator Accounting
sub operator_accounting {
  my $self = shift;
  my $q = shift;
  my $users_dms = shift;
  my $users_sharee = shift;
  my $accounting_type = shift || "";
  my $ck4ex = shift || "";
  my $dbh = "";
  my %varenv = $cf->envonline();

  open(FILE,">>$varenv{logdir}/operator_accounting.log") if($debug);
  print FILE "\n*--> $now_dt| accounting_type: $accounting_type | owner: $users_dms->{u_id}\n" if($debug);

  print FILE "Invoice c_id's ck4ex: $ck4ex\n";


  my $accounting_main_id = 300029;
  my $node_faktura = $dbt->get_node($dbh,$dbt->{shareedms_conf}->{faktura});
  my $node = $dbt->get_node($dbh,$accounting_main_id);

  my $praefix = "$node->{node_name}-$varenv{praefix}";
  my $ret = "";
  my $c_idnew = "";

  if($ck4ex){

    my $opuser = {
          table => "contentuser",
          fetch => "one",
          c_id => 2,
        };
    my $ctuser = $dbt->fetch_tablerecord($dbh,$opuser);

    $c_idnew = $dbt->insert_contenttrans($dbh,$ctuser,$accounting_main_id,"208","----",$users_dms->{u_id});

    if($c_idnew){
        print FILE "c_idnew: $c_idnew\n";
	$i_rows++;
  	my $ctt = { c_id => $c_idnew };
        my $update_ctt = {
      	  table   => "contenttrans",
	  int12   => $node->{main_id},
	  txt00   => $node->{node_name},
      	  mtime   => "now()",
      	  owner   => $users_dms->{u_id},
    	};

    	my $tplop = $dbt->get_tpl($dbh,"196");#Operator-Faktura
    	my @tplop_order = split /,/,$tplop->{tpl_order};
    	foreach(@tplop_order){
     	 my ($key,$val,$size,$unit) = split /=/,$_;
     	 if($key =~ /int(\d+)/){
          #take fee values used by creating operator accounting invoice
          my $count_key = $1 + 20;#cu.int to ctt.int
          my $ctt_key = "int" . $count_key;
	  $update_ctt->{$ctt_key} = $ctuser->{$key} if($ctuser->{$key});
	 }
	}
	$dbt->update_record($dbh,$update_ctt,$ctt);

	my $pref = { 
          table => "contenttrans",
          fetch => "one",
          main_id => $accounting_main_id,
          template_id => 208,
	  c_id => $c_idnew, 
	};
        $ctt = $dbt->fetch_record($dbh,$pref);
        $bw->log("operator_accounting used invoice c_id:",$ctt->{c_id},"");
	
      	$ck4ex =~ s/\s/,/g;
  	$dbt->update_sql($dbh,"UPDATE contenttrans set int20='$ctt->{c_id}' where c_id IN ($ck4ex)");
        print FILE "UPDATE contenttrans set int20='$ctt->{c_id}' where c_id IN ($ck4ex)\n";

     	my $update_users = {
          table   => "users",
          change   => "no_time",
          u_id    => $users_dms->{u_id}
   	};
   	$dbt->update_one($dbh,$update_users,"c_id4trans=$c_idnew,tpl_id4trans=208"); 
    }
   
    my $uri_path = $dbt->recurse_node($dbh,$node->{main_id});
    print "$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows\n";
    print redirect("$varenv{wwwhost}/$uri_path?return=$i_rows-$u_rows-$d_rows");
    exit 0;

   }else{
     $ret = "failure::Abbruch, es wurden keine Belege selektiert.";
   }
   
   close FILE;
   return $ret;
}

#CSV Export
sub export_csv {
  my $self = shift;
  my $node_meta = shift;
  my $users_dms = shift;
  my $ct4rel = shift || {};
  my $bike_nodes = shift || {};
  my $coo = shift || "";

  my $time = time;
  my %varenv = $cf->envonline();
  my $table = $node_meta->{ct_table} || "content";
  my $scol = "c_id";
  if($users_dms->{"col_sort_$table"}){
    $scol = $users_dms->{"col_sort_$table"};
  }

  my $feedb = { message => "" };
  $node_meta->{tpl_order} =~ s/txt06=[\w\s=]+,/byte01=Ilockit Token,/ if($node_meta->{tpl_id} == 205);
  my @tpl_order = split(/,/,$node_meta->{tpl_order});

  my $csv_export = Text::CSV_XS->new ({ binary => 1, sep_char => ";", eol => "\r\n" });
  my $filename_csv_export = "$dbt->{operator}->{$varenv{dbname}}->{oprefix}-$node_meta->{node_name}-Export-$time.csv";
  my @header_line = ();
  open my $csv, ">", "$varenv{csv}/$filename_csv_export" or die "$filename_csv_export: $!\n";

  foreach (@tpl_order){
   my ($key,$val,$size,$title) = split /=/,$_;
   push @header_line, $val;
  }
  $csv_export->print($csv, \@header_line);#header


  foreach my $id (sort {
     if($users_dms->{"sort_updown_$table"} eq "down"){
        if ($scol =~ /barcode|int/) {
                $ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol}
        }else{
                lc($ct4rel->{$b}->{$scol}) cmp lc($ct4rel->{$a}->{$scol})
        }
     }else{
        if ($scol =~ /barcode|int/) {
                $ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol}
        }else{
                lc($ct4rel->{$a}->{$scol}) cmp lc($ct4rel->{$b}->{$scol})
        }
     }
   } keys(%$ct4rel)){
   my @line = ();
   foreach (@tpl_order){
     my ($key,$val,$size,$title) = split /=/,$_;
     if($key =~ /byte/){
        my $K_int = unpack "H*", $ct4rel->{$id}->{$key};
       	push @line, $K_int;
     }elsif($ct4rel->{$id}->{template_id} == 225 && $key eq "txt24"){
          my $flotten = "";
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
             $flotten .= "$bike_nodes->{$rid}->{node_name}\n" if($ct4rel->{$id}->{$key} =~ /$bike_nodes->{$rid}->{main_id}/);
          }
          push @line, $flotten;
     }elsif($ct4rel->{$id}->{template_id} == 225 && $key eq "txt25"){
          my %station_filter_hash = ();
          if($ct4rel->{$id}->{txt25} && $ct4rel->{$id}->{txt25} =~ /\d\s\d/){
                %station_filter_hash = map { $_ => 1 } split(/\s+/,$ct4rel->{$id}->{txt25});
          }elsif($ct4rel->{$id}->{txt25} && $ct4rel->{$id}->{txt25} =~ /(\d+)/){
                $station_filter_hash{$1} = 1;
          }
          my $station_filter = "";
          foreach my $type_id (keys (%station_filter_hash)){
                $station_filter .= "$dbt->{copri_conf}->{type_id}->{$type_id}\n";
          }

          push @line, $station_filter;
     }else{
       	push @line, $ct4rel->{$id}->{$key};
     }
   }
   $csv_export->print($csv, \@line);
  }

  if( -f "$varenv{basedir}/csv/$filename_csv_export"){
       print "<script type=\"text/javascript\">window.open('$varenv{wwwhost}/FileOut?file=$filename_csv_export\&sessionid=$coo');</script>";
  }else{
        $feedb->{message} = "failure:: Der CSV Export von \"$filename_csv_export\" ist fehlgeschlagen.";
  }

  return $feedb;
}

#prepare email standalone for extern email client posting 
#partly adapted from mailTransportcms
#attachement breaks, that's because not used
sub prepare_email {
  my $self = shift;
  my $varenv = shift;
  my $ct_id = shift || "";
  my $email_select = shift || "";
  my $dbh = "";

  my $cms_prim = $varenv->{cms}->{$email_select}->{txt};

  #mailxcfg is shareeconf/mailx.cfg <block> selection!
  #hash data to send
  my $sendref = {
        mailxcfg => "mailx_default",
        syshost => "$varenv->{syshost}",
        mail_from  => "",
        mail_to  => "",
        mail_bcc  => "",
        c_id => 0,
        subject => "",
        message => "",
        signature => "",
        attachment => "",
  };

  my $fetchctt = {
     table => "contenttrans",
     fetch => "one",
     c_id => "=::$ct_id",
  };
  my $ctt = { c_id => 0 };
  $ctt = $dbt->fetch_tablerecord($dbh,$fetchctt);

  my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname},"iso-8859-1");
  my $pref_adr = {
     table => "contentadr",
     fetch => "one",
     c_id => "$ctt->{int10}",
  };
  my $ctadr = { c_id => 0 };
  $ctadr = $dbt->fetch_tablerecord($dbh_primary,$pref_adr);

  my $app_name = "Mietrad App";
  $app_name = $dbt->{operator}->{$varenv{dbname}}->{app_name};
  my $invoice_name = "";
  my $sharee_ticket = "";
  if(ref($ctt) eq "HASH" && $ctt->{ct_name}){
    $invoice_name = "$ctt->{txt00}-$varenv{dbname}-$ctt->{ct_name}.pdf";
    $sendref->{attachment} = "$invoice_name";
    $sharee_ticket = "[$varenv{dbname}-$ctt->{ct_name}]";
  }

  my $subject = "TeilRad Mietradsystem";#default
  $subject = $1 if($cms_prim =~ /--subject--(.*)--subject--/);
  $cms_prim =~ s/--subject--$subject--subject--//;
  $subject .= " $sharee_ticket" if($sharee_ticket);
  $cms_prim =~ s/\n//;
  $cms_prim =~ s/\n//;


  $sendref->{mail_to} = $ctadr->{txt08};
  $sendref->{c_id} = $ctadr->{c_id};
  $sendref->{subject} = $subject;
  $sendref->{subject} =~ s/::app_name::/$app_name/;
  $sendref->{subject} =~ s/Mein // if($sendref->{subject} =~ /^Mein/);

  $sendref->{message} = $cms_prim;
  $sendref->{message} =~ s/::user_name::/$ctadr->{txt01}/;
  $sendref->{message} =~ s/::app_name::/$app_name/g;
  $sendref->{message} =~ s/::invoice_name::/$invoice_name/ if($invoice_name);
  $sendref->{message} =~ s/::txid::/$ctt->{txt16}/;
  $sendref->{message} =~ s/::email_temppassword::/\<b\>$ctadr->{txt04}\<\/b\>/g;
  $sendref->{message} =~ s/::email_ack_digest::/\<b\>$ctadr->{txt34}\<\/b\>/g;#send_emailack
  #$sendref->{message} =~ s/\n/\<br \/\>/g;

  return $sendref;
}#end prepare_email

1;
