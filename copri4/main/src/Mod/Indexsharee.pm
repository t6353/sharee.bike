package Mod::Indexsharee;
#      
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;

use POSIX;
use CGI::Cookie ();
use CGI ':standard';
use Time::Piece;
use DateTime;
use DateTime::Format::Pg;
use DBI;
use Apache2::RequestUtil ();
use Apache2::RequestIO ();              
use Apache2::Const -compile => qw(OK);
use Scalar::Util qw(looks_like_number);

use Lib::Config;
use Mod::Buttons;
use Lib::Mlogic;
use Mod::Basework;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Shareework;
use Mod::Prelib;
use Mod::Payment;
#use Data::Dumper;

sub handler {
 my $re = shift;
 my $q = new CGI();
 $q->import_names('R');
 my $cf = new Config;
 my $ml = new Mlogic;
 my $bw = new Basework;
 my $shwo = new Shareework;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $but = new Buttons;
 my $pl = new Prelib;
 my $pay = new Payment;

 my %varenv = $cf->envonline();
 my $netloc = $q->url(-base=>1);
 my $dbh = "";
 my $script = $q->script_name();
 my $referer = $q->referer();
 my $path = $q->path_info();
 $path = "$script" . "$path";
 $path =~ s/\.html//;
 my $refer = $ENV{HTTP_REFERER};
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $return = "";
 my $returnwww = "";
 my $html_charset = "utf-8";
 my $user_agent = $q->user_agent();

 my $debug=1;
 my $lang = "en";
 $lang = $1 if($q->http('Accept-Language') =~ /^(\w+)/);
 $lang = lc($lang) if($lang);

 my $feedb = { 
	 c_id => "",
	 message => "",
	 i_rows => 0,
	 u_rows => 0,
	 d_rows => 0,
 };


 my $modes = $dbt->{shareedms_conf}->{modes};
 my $mode = "";
 if($R::mode && length($R::mode) >= 5 && $R::mode =~ /($modes)/){
   $mode = "$R::mode";
 }elsif($path && $path =~ /\/($modes)$/){
   $mode = $1;
 }elsif($path && $path =~ /\/($modes)\?\w/){
   $mode = $1;
 }

 #main datadir is main config directive like "shareeapp-kn" and catched by syshost name
 if($netloc =~ /:\/\/(sharee\w+-\w+)\.copri/){
   #$bw->log("Indexsharee merchant select by netloc:",$netloc,"");
   $varenv{syshost} = $1 if( -d "/var/www/copri-bike/$1");
   $mode = "manager" if($varenv{syshost} =~ /shareedms-/ && !$mode);
 }elsif($netloc =~ /lastenradbayern\.net/){
   $varenv{syshost} = "shareeweb-bayern";
 }
 exit 0 if(!$varenv{syshost});


 if($varenv{orga} ne "dms" && $path =~ /DMS|Waren|Kunden\/|Einstellung|journal|Faktur/i){
   print redirect("$varenv{wwwhost}");
   exit 0;   
 }

 my $dyn_js = "";
 my $users_dms = { u_id => 0 };
 my $users_sharee = { c_id => 0 };
 my $api_return = {};

 print $q->header(-charset=>"$html_charset");

 my $coo = $q->cookie('domcookie') || $R::sessionid || "";
 #Prio sessionid if also domcookie is set ... and s.u.
 if($R::sessionid  && $R::sessionid ne $q->cookie('domcookie')){
 	$coo = $q->param('sessionid');
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);
 }

 my $aowner = "";
 my $return_merchant = { 
	 merchant_id => "",
	 project_id => "",
 };
 ($aowner,$return_merchant) = $apif->fetch_merchant($q,\%varenv,$coo,$R::merchant_id);
 $varenv{merchant_id} = $return_merchant->{merchant_id};
 $varenv{project_id} = $return_merchant->{project_id};
 $bw->log("Indexsharee merchant select used with merchant_id $varenv{merchant_id}, project_id $varenv{project_id}, access_owner $aowner",$varenv{merchant_id},"");

 ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");

  if($R::email && $R::sharee_edit =~ /password_forgotten/){
    	my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));
	$shwo->send_password(\%varenv,$R::email,$hw_id,$aowner);
  }

 #login-screen should only be available if auth_verify fails 
 if($R::login_sharee || $R::login_dms){

    #1. logout
    $apif->authout($q,$coo);

    my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));#old $co
    #3. authorize
    my $author = { authcookie => ""};
    $author = $apif->authorization($q,$varenv{merchant_id},$hw_id,$lang,$aowner);#$co like browser hw_id
    #4. verify and get user values
    ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"");
    #print "$author->{authcookie},$users_sharee->{c_id}"; exit; 

    if($author->{authcookie} && length($author->{authcookie}) > 30){
	# take last 21 chars
    	$coo = substr $author->{authcookie}, 15;
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);

    	if($users_sharee->{c_id} && $varenv{orga} eq "dms" && $R::login_dms eq "Login" && $coo && length($coo) > 20){
	  $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id});
	  if($users_dms->{u_id}){
	    my $update_users = {
                table   => "users",
                change   => "no_time",
                u_id    => $users_dms->{u_id}
            };
	    $dbt->update_one($dbh,$update_users,"cookie='$coo',atime='now()'");

     	    print redirect("$varenv{wwwhost}");
     	    exit 0;
	  }
    	}
     }
 }

 my $session="";
 my $session_and="";
 if(length($coo) > 20 && !$q->cookie(-name=>'domcookie')){
 #if(length($coo) > 20){#breaks login
        $session = "?sessionid=$coo";
        $session_and = "&sessionid=$coo";
	$bw->log("Indexsharee authcookie userid=$users_sharee->{c_id} by using sessionid=$coo with merchant_id $varenv{merchant_id}, project_id $varenv{project_id}, referer=$referer, path=$path, access_owner $aowner",$varenv{merchant_id},"");
 }else{
	$bw->log("Indexsharee authcookie userid=$users_sharee->{c_id} by using cookie=$coo with merchant_id $varenv{merchant_id}, project_id $varenv{project_id}, referer=$referer, path=$path, access_owner $aowner",$varenv{merchant_id},"");
 }

 #DMS
 if($users_sharee->{c_id} && $varenv{orga} eq "dms" && $coo && length($coo) > 20){
    $users_dms = { u_id => 0 };
    $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo' and u_id = $users_sharee->{c_id}");
    my $update_users = {
        table => "users",
        change => "no_time",
        u_id  => $users_dms->{u_id}
    };

    my $dt1 = DateTime->now;
    my $dt2 = DateTime->now;
    $dt2 = DateTime::Format::Pg->parse_datetime($users_sharee->{atime}) if($users_sharee->{atime});
    my $logout_hours = 2;
    my $dt2_over = $dt2->add( hours => $logout_hours );
    if($dt1 > $dt2_over){
     $apif->authout($q,$coo);
     $dbt->update_one($dbh,$update_users,"cookie=''");
     print redirect("$varenv{wwwhost}?autologout=$logout_hours");
     exit 0;
    }
    $dbt->update_one($dbh,$update_users,"atime='now()'");
 }

 $varenv{cms} = $dbt->fetch_cms($dbh,{ lang => $q->escapeHTML($lang) });

 #Save anyway on create ... and hopefully delete it later
 if($R::sharee_edit && $R::sharee_edit =~ /create_account/ && $R::txt04 && $R::txt04 =~ /\w+/ && $R::txt08 && $R::txt08 =~ /\w+\@\w+/){

    #1. logout
    $apif->authout($q,$coo);

   #create_account. 2. inserts contentadr
   my $shareec_id = $shwo->create_account($dbh,$aowner);
   ($returnwww,$feedb) = $shwo->save_account($dbh,$q,$shareec_id,\%varenv,$aowner);

    #Like login_sharee, redundant
    my $hw_id = unpack ('H*', pack('Nc', time, $$ % 0xff));#old $co
    #3. authorize
    my $author = $apif->authorization($q,$varenv{merchant_id},$hw_id,$lang,$aowner);#$co like browser hw_id
    #print "3. authorize: " . $author->{authcookie} . " --  " . $q->param('authcookie') . " ++ " . $coo . "<br>"; 

    #4. verify and get user values
    ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"",$author->{new_authcoo});
    #print "4. verifyize: " . $author->{authcookie} . " -- c_id: " . $users_sharee->{c_id} . " ++ " . $coo  . "<br>"; 

    #5. domcookie by authcookie substr (cut first 15 chars), AND also sessionid 
    if($author->{authcookie} && length($author->{authcookie}) > 30){
	# take last 21 chars
    	$coo = substr $author->{authcookie}, 15;
    	my $cookie = CGI::Cookie->new(-name  => 'domcookie',-value => $coo);
    	print $q->header(-charset=>"$html_charset", -cookie=>$cookie);
	#print "5. set cookie: " . $author->{authcookie} . " --  " . $q->param('authcookie') . " ++ " . $coo . "<br>"; 

        #if user-pw authorized, then ignore conflict_ because it matches exist user-data
	#should be result to Uhps page
        if($shareec_id && $returnwww && $returnwww =~ /conflict_txt07|conflict_txt08/){
          $return = $shwo->delete_account($shareec_id,$aowner);
          $apif->authout($q,$coo) if($coo);
          print redirect("$varenv{wwwhost}/$varenv{mandant}/Anmelden?conflict_failure=1");
          exit 0;
        }
        elsif(length($coo) > 20){
    	  #we need this to get $R::sessionid to FormEdit
    	  ($api_return,$users_sharee) = $apif->auth_verify($q,$author->{authcookie},"");
      	  if(($returnwww && $returnwww =~ /failure::(.*)/) || ($users_sharee->{txt31} && $users_sharee->{txt31} =~ /\w/)){
   		$returnwww =~ s/::/=/g;
      	  	print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?sessionid=$coo\&$returnwww");
      	  	exit 0;
      	  }else{
      	  	print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?sessionid=$coo");
      	  	exit 0;
      	 }
	}
      }
 }

 #$payable_check==1 if payment data OK
 #$payable_check==2 if payment data AND sms-,email-Ack,:!Vde,AGB OK
 my $payable_check=0;
 $payable_check = $bw->isuser_rentable($users_sharee,\%varenv);
  
 # Logout
 if($mode =~ /logout/){
   if($mode =~ /logout_sharee/){
     $apif->authout($q,$coo);
   }elsif($users_dms->{u_id}){
     my $update_users = {
        table   => "users",
	change   => "no_time",
        u_id    => $users_dms->{u_id}
     };
     $dbt->update_one($dbh,$update_users,"cookie=''");
   }
   print redirect("$varenv{wwwhost}");
   exit 0;
 }
 
 my $tpl = $dbt->get_tpl($dbh,"302001");#Kundendaten template
 $tpl->{tpl_order} .= ",txt04,txt08,int14";


 if($R::login_sharee){
  if($users_sharee->{c_id} && (!$payable_check || ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/))){
      if(!$payable_check){
        my $update_adr = {
                table   => "contentadr",
                mtime   => "now()",
                owner   => $aowner,
                c_id    => $users_sharee->{c_id}
        };
        $dbt->update_one($dbh,$update_adr,"int12=1");
      }
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}$session");
      exit 0;
  }elsif($users_sharee->{c_id} && !$payable_check){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}$session");
      exit 0;
  }elsif(!$users_sharee->{c_id} || ($users_sharee->{c_id} && $users_sharee->{c_id} !~ /^\d+$/)){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Anmelden?failure=1$session_and");
      exit 0;
  }else{
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}$session");
     exit 0;
  }
 }
 
 my $oncontextmenu="";
 $oncontextmenu="return false;";# if($mode =~ /maintainer/);
 $oncontextmenu="" if(!$users_dms->{u_id});
 

 #Menu & node_path handling
 my $view = "";
 my @viewsel = ("app","shareestart");
 if($path && $path =~ /^\/\w/ && $path =~ /^\/(.*)/){
  @viewsel = split /\//,$1;
  if("$path" =~ /\/$modes$/){
    pop @viewsel;
  }
  $view = $viewsel[0];
 }
 my $node_meta = $dbt->get_node_meta($dbh,\@viewsel);

 #DMS & Tools
 if($varenv{orga} eq "dms" && $users_dms->{u_id}){
   #just for pw renew
   if($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /save_account/){
    ($returnwww,$feedb) = $shwo->save_account($dbh,$q,$users_sharee->{c_id},\%varenv,$aowner);
    ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");
   }

   my $update_users = {
       table   => "users",
       change   => "no_time",
       u_id    => $users_dms->{u_id}
   };

   if($R::cal_delta_start){
     use Date::Calc qw(Add_Delta_YMD);
     my $day = strftime "%d", localtime;
     my $mon = strftime "%m", localtime;
     my $year = strftime "%Y", localtime;
     $users_dms->{cal_start} =~ s/\s.*//;
     ($year,$mon,$day) = split(/-/,$users_dms->{cal_start}) if($users_dms->{cal_start});
     my ($year1,$mon1,$day1) = split(/:/,$R::cal_delta_start);
     my ($Dy,$Dm,$Dd) = Add_Delta_YMD($year,$mon,$day, $year1,$mon1,$day1);
     $dbt->update_one($dbh,$update_users,"cal_start='$Dy-$Dm-$Dd'");
   }
   if($R::cal_today){
     my $today4db = strftime("%Y-%m-%d %H:%M",localtime(time));
     $dbt->update_one($dbh,$update_users,"cal_start='$today4db'");
   }
   if($R::cal_sort_updown){
     $dbt->update_one($dbh,$update_users,"cal_sort_updown='$R::cal_sort_updown'");
   }

   if($R::col_sort && $node_meta->{ct_table}){
     $dbt->update_one($dbh,$update_users,"col_sort_$node_meta->{ct_table}='$R::col_sort'");
   }
   if($R::sort_updown){
     $dbt->update_one($dbh,$update_users,"sort_updown_$node_meta->{ct_table}='$R::sort_updown'");
   }

   #base_edit implements new DMS methodes without Pre* things
   #permissions
   #DMS Kunden rw
   if($R::base_edit && $node_meta->{ct_table} eq "contentadr"){
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}",$q,"");
    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");
    if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int02} == 2){
     if($R::c_id && $R::base_edit eq "remove_chk4rel"){
   	my $delete_key = "delete_adr";
	$feedb->{message} = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&c_id=$R::c_id ::löschen";
     }elsif($R::c_id && $R::base_edit eq "delete_adr"){
	$return = $shwo->delete_account($R::c_id,$users_dms->{u_id});
     }elsif($R::c_id && $R::base_edit eq "save_adr"){
      ($returnwww,$feedb) = $shwo->save_account($dbh,$q,$R::c_id,\%varenv,$users_dms->{u_id});
     }
    }else{
	$feedb->{message} = "failure::Abbruch. Schreibender Zugriff \"Kunden Stammdaten\" verweigert.";
    }
   }

   #DMS users accounts
   if($R::base_edit && $node_meta->{ct_table} eq "users"){#DMS-Account rw
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}",$q,"");
    if($users_dms->{u_id} && $users_dms->{int07} == 2){
     if($R::u_id && $R::base_edit eq "remove_chk4rel"){#users
	my $delete_key = "delete_dmsusers";
	$feedb->{message} = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&u_id=$R::u_id ::löschen";
     }elsif($R::base_edit =~ /new_dmsusers|save_dmsusers|delete_dmsusers/){
	my $u_id = 0;
	$u_id = $1 if($R::u_id && $R::u_id =~ /(\d+)/);
	$u_id = $1 if($R::c_idadr && $R::c_idadr =~ /(\d+)/ && $R::base_edit eq "new_dmsusers");
	$feedb = $pl->manage_dmsusers($q,\%varenv,$R::base_edit,$u_id,$users_dms);
     }
    }else{
	$feedb->{message} = "failure::Abbruch. Schreibender Zugriff \"DMS-Account\" verweigert.";
    }
   }

  #DMS Waren
  if(($R::rel_edit || $R::base_edit) && $node_meta->{ct_table} eq "content"){
   $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}",$q,"");
   if($users_dms->{u_id} && $users_dms->{int01} == 2){
    if($R::rel_edit eq "save_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->save_relation($q,$R::main_id,$users_dms->{u_id});
    }elsif($R::rel_edit eq "delete_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->delete_relation($q,$R::main_id,$users_dms->{u_id});
    }elsif($R::rel_edit eq "new_relation" && $R::main_id && $R::main_id >= 200000){
     $return = $pl->new_relation($q,$R::main_id,$users_dms->{u_id});
    }elsif($R::base_edit eq "new_content" && $node_meta->{main_id} && $node_meta->{main_id} >= 200000){
     $feedb = $pl->new_content($q,$users_dms,$node_meta);
    }elsif($R::base_edit eq "save_content"){
     $feedb = $pl->save_content($q,$users_dms,$node_meta,$R::c_id) if(looks_like_number($R::c_id));
     $apif->stations_caching($q,\%varenv,$users_sharee) if($node_meta->{template_id} =~ /210|205|225/);
    }elsif($R::base_edit eq "move_content"){
     $feedb = $pl->move_content($q,$users_dms,$node_meta,$R::c_id) if(looks_like_number($R::c_id));
     $apif->stations_caching($q,\%varenv,$users_sharee) if($node_meta->{template_id} == 205);
    }elsif($R::c_id && $R::base_edit eq "remove_chk4rel"){
   	my $delete_key = "delete_content";
	$feedb->{message} = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&c_id=$R::c_id ::löschen";
    }elsif($R::c_id && $R::base_edit eq "delete_content"){
	$feedb = $pl->delete_content($node_meta,$R::c_id);
        $apif->stations_caching($q,\%varenv,$users_sharee) if($node_meta->{template_id} =~ /210|205|225/);
    }
   }else{
     $feedb->{message} = "failure::Abbruch. Schreibender Zugriff \"Waren Stammdaten\" verweigert.";
   }
  }

  #Einstellung
  if(($R::rel_edit || $R::base_edit) && $node_meta->{ct_table} eq "contentuser"){
   $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}",$q,"");
   if($users_dms->{u_id} && $users_dms->{int08} == 2){
    if($R::rel_edit eq "save_ctuser" && $node_meta->{main_id} && $node_meta->{main_id} >= 200000){
     $return = $pl->save_service_desc($q,$users_dms,$R::c_id);
    }elsif($R::rel_edit eq "save_user"){
     $feedb = $pl->save_content($q,$users_dms,$node_meta,$R::c_id) if(looks_like_number($R::c_id));
    }elsif($R::base_edit eq "new_contentcms" && $node_meta->{main_id} && $node_meta->{main_id} >= 200000){
     $feedb = $pl->new_content($q,$users_dms,$node_meta);
    }elsif($R::base_edit eq "save_contentcms" && $node_meta->{main_id} && $node_meta->{main_id} >= 200000){
     $feedb = $pl->save_content($q,$users_dms,$node_meta,$R::c_id) if(looks_like_number($R::c_id));
    }elsif($R::c_id && $R::base_edit eq "remove_chk4rel"){
   	my $delete_key = "delete_contentuser";
	$feedb->{message} = "failure::Datensatz wirklich löschen. ::?base_edit=$delete_key\&exit_box2=1\&c_id=$R::c_id ::löschen";
    }elsif($R::c_id && $R::base_edit eq "delete_contentuser"){
	$feedb = $pl->delete_content($node_meta,$R::c_id);
    }
   }else{
     $feedb->{message} = "failure::Abbruch. Schreibender Zugriff \"Einstellung\" verweigert.";
   }
  }

  #DMS Faktura
  if($node_meta->{ct_table} eq "contenttrans"){
    $bw->log("DMS action from $users_dms->{u_id} on $node_meta->{ct_table}",$q,"");
    my $update_users = {
        table   => "users",
        change   => "no_time",
        u_id    => $users_dms->{u_id}
    };
    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");

    if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int03} >= 1 && $R::ct_trans eq "open"){#DMS Faktura read
      $dbt->update_one($dbh,$update_users,"c_id4trans=$R::c_id4trans,tpl_id4trans=$R::tpl_id4trans") if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans));
      $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");
    }
    #DMS Faktura rw
    elsif($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int03} == 2){
      $dbt->update_one($dbh,$update_users,"c_id4trans=$R::c_id4trans,tpl_id4trans=$R::tpl_id4trans") if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans));
      $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");

     if($R::ct_trans eq "new_trans" && $node_meta->{main_id} && $node_meta->{main_id} >= 200000){
       $feedb = $pl->new_contenttrans($q,$users_dms,$node_meta);
       $dbt->update_one($dbh,$update_users,"c_id4trans=$feedb->{c_id},tpl_id4trans=$feedb->{template_id}") if(looks_like_number($feedb->{c_id}) && looks_like_number($feedb->{template_id}));
       $users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");
     }
     elsif($R::ct_trans =~ /set_workflow2invoice|set_workflow2storno/){
       $return = $pl->set_workflow($q,$users_dms,$R::c_id4trans,$R::set_main_id4workflow) if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans) && looks_like_number($R::set_main_id4workflow));
     }
     elsif($R::ct_trans =~ /set_accounting2invoice/){
       $return = $pl->set_accounting2invoice($q,$users_dms,$R::c_id4trans,$R::set_main_id4workflow) if(looks_like_number($R::c_id4trans) && looks_like_number($R::tpl_id4trans) && looks_like_number($R::set_main_id4workflow));
     }
     elsif($R::ct_trans =~ /save_text|save_adr/ && $R::c_id4trans){
       $feedb = $pl->save_text2contenttrans($q,$users_dms,$R::c_id4trans);
     }
     elsif($R::c_id && $R::ct_trans eq "remove_chk4rel"){
        my $delete_key = "delete_trans";
	if(looks_like_number($R::tpl_id4trans) && $R::tpl_id4trans == 219){
          $feedb->{message} = "failure::Datensatz löschen ist für Prepaid-Account deaktiviert. Prepaid requests oder payments müssen erhalten bleiben.";
	}else{
          $feedb->{message} = "failure::Datensatz wirklich löschen. ::?ct_trans=$delete_key\&exit_box2=1\&c_id=$R::c_id ::löschen";
	}
     }
     elsif($R::c_id && $R::ct_trans eq "delete_trans"){
        $feedb = $pl->delete_content($node_meta,$R::c_id);
	$dbt->update_one($dbh,$update_users,"c_id4trans=0,tpl_id4trans=0");
      	$users_dms = $dbt->select_users($dbh,$users_sharee->{c_id},"and cookie='$coo'");
     }
     #not used because of attachement
     elsif($R::ct_trans =~ /email_generate/ && $R::c_id4trans && $R::email_select){
	#my $sendref = $pl->prepare_email(\%varenv,$R::c_id4trans,$R::email_select);
	#print redirect("mailto:$sendref->{mail_to}?subject=$sendref->{subject}\&body=$sendref->{message}");
	#exit 0;
	
     }
     elsif($R::detail_search && $R::detail_search eq "operator_accounting"){
	my $ck4ex = "@R::ck4ex" || "";
       	$return = $pl->operator_accounting($q,$users_dms,$users_sharee,$R::accounting_type,$ck4ex);
     }
     #position management in invoices 
     elsif($R::trans2edit eq "transpos" && $R::select_part){
    	$feedb = $pl->insert_contenttranspos($q,$users_dms->{u_id});
     }
     elsif($R::ct_trans eq "save_pos" && $R::tpl_id4trans =~ /218|219/ && $R::c_idpos){
	$feedb = $pl->save_contenttranspos($q,$R::c_idpos,$users_dms->{u_id});
     }
     elsif($R::ct_trans eq "delete_pos" && $R::c_idpos){
	$feedb = $pl->delete_contenttranspos($q,$R::c_idpos,$users_dms->{u_id});
     }
     elsif($R::set_state && $R::c_id4trans){
	$feedb = $pl->save_text2contenttrans($q,$users_dms,$R::c_id4trans);
	$feedb = $pay->book_payment($q,\%varenv,$node_meta,$users_dms);
     }
     elsif($R::print_pdfview && $R::c_id4trans){
	$feedb = $pay->print_pdf($q,\%varenv,$node_meta,$users_dms,$feedb);
     }
     elsif($R::send_invoice_cms && $R::cms_message_key && $R::c_id4trans){
	$feedb = $pay->send_invoice_cms($q,\%varenv,$node_meta,$users_dms,$feedb);
     }
     elsif($R::close_trans){
	$feedb = $pay->close_transactions($q,\%varenv,$node_meta,$users_dms);
	if($feedb->{u_rows}){
	  print redirect("$varenv{wwwhost}/DMS/Faktura/Verkaufsjournal?redirected=1");
	  exit 0;
	}
     }
    }elsif($R::ct_trans){
     $feedb->{message} = "failure::Abbruch. Schreibender Zugriff \"Faktura\" verweigert.";
    }
   }

   #DMS Mietjournal
   if($node_meta->{ct_table} eq "contenttranspos" && $R::base_edit eq "save_pos"){
     $feedb = $pl->save_contenttranspos($q,$R::c_id,$users_dms->{u_id});
     $apif->stations_caching($q,\%varenv,$users_sharee);
   }

   #DMS insert part or fee
   if($node_meta->{ct_table} eq "contenttranspos" && $R::insert_contenttranspos){
     $feedb = $pl->insert_contenttranspos($q,$users_dms->{u_id});
   }

  $users_dms = $dbt->select_users($dbh,$users_dms->{u_id},"");#just to get update after save

 }
 #web frame
 elsif($varenv{orga} ne "dms"){

  #save_account. 3. updates contentadr 
  if($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /save_account/){
    ($returnwww,$feedb) = $shwo->save_account($dbh,$q,$users_sharee->{c_id},\%varenv,$aowner);
  }
  elsif($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /save_transact/){
    $returnwww  = $shwo->save_transact($q,$users_sharee->{c_id},$coo,$aowner);
  }
  elsif($users_sharee->{c_id} && $R::sharee_edit && $R::sharee_edit =~ /generate_payonelink/ && $R::prepaid_amount && $R::prepaid_amount =~ /\d+/){
    system("$dbt->{copri_conf}->{basedir}/$varenv{syshost}/src/scripts/payonelink.pl '$varenv{syshost}' '$users_sharee->{c_id}' '$R::prepaid_amount' '$aowner' &");
  }

  if($returnwww && $returnwww =~ /failure::(.*)/){
   $returnwww =~ s/::/=/g;
   if($returnwww =~ /txt22|txt23/){
     #should be result to Uhps page
     if($returnwww =~ /conflict_txt22/){
        print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}?cum=1\&conflict_failure=1$session_and\&$returnwww");
        exit 0;
     }
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /int03/){
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /txt09|txt16/){
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=1$session_and\&$returnwww");
   }elsif($returnwww =~ /int01/ && $R::radID){
     print redirect("$varenv{wwwhost}/?ask_radID=$R::radID\&failure=not-synced$session_and");
   }else{
     print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?cum=1$session_and\&$returnwww");
   }
   exit 0;
  }else{
   ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");


   #Payone Response POST (TransactionStatus)
   #payone response ($R::pseudocardpan || $R::status)
   if($users_sharee->{c_id} && ($R::pseudocardpan || $R::status)){
	my $payone_return = "";
   	 open(FILE,">>$varenv{logdir}/payone-return-post.log");
         print FILE "<--- $now_dt from Indexsharee.pm \nPayone return-way by ajaxCall: $R::status\n";
 	 my @keywords = $q->param;
   	 foreach(@keywords){
          my $val = $q->param($_);
          print FILE "$_=$val\n";
	  $payone_return .= "$_=$val\n";# if($_ =~ /error|message/i);
   	 }

	my $update_adr = {
      		table   => "contentadr",
      		mtime   => "now()",
      		owner   => $aowner,
		c_id 	=> $users_sharee->{c_id}
 	};
	my $vde_on_fail = $users_sharee->{int12} || 1;#keep last or set 1

	#SEPA, done by Shareework managemandate
	#CC
 	if($R::pseudocardpan && length($R::pseudocardpan) >= 19){#done by payone AJAX return 
	  $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
	  $update_adr->{txt22} = "";
	  $update_adr->{txt23} = "";
	  $update_adr->{ct_name} = $q->escapeHTML($R::pseudocardpan);
	  $update_adr->{txt27} = $q->escapeHTML($R::status);
          $update_adr->{txt28} = $now_dt . "\n" . $q->escapeHTML($payone_return);
	  $update_adr->{int12} = 0 if($users_sharee->{int12} != 2);#Vde hard set by fibu
	  $update_adr->{int03} = 2;
	  $update_adr->{int24} = "null";#cpdate reset
	  $dbt->update_record($dbh,$update_adr,$users_sharee) if($users_sharee->{c_id} > 0);
	  ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");
	   
	  #payment_ack payAck
	  system("$dbt->{copri_conf}->{basedir}/$varenv{syshost}/src/scripts/payment_ack.pl '$varenv{syshost}' 'payment_ackCC' '$users_sharee->{c_id}' '$aowner' &");

	   if(1==1){
   	     open(FILE,">>$varenv{logdir}/confirm.log") if($debug);
   	     print FILE "\n\n*--> $now_dt done by $0\n" if($debug);
   	     print FILE "trigger confirm-code request by pseudocardpan\n" if($debug);

   	     if(($users_sharee->{int04} != 1) && ($users_sharee->{txt08} =~ /\w\@\w/)){
    		$shwo->emailack(\%varenv,$users_sharee->{c_id});
   	     }
   	     if(($users_sharee->{int13} != 1) && ($users_sharee->{txt07} =~ /\d{9}/ && length($users_sharee->{txt07}) <= 16)){
    		$shwo->smsack($users_sharee);
   	     }
	     close(FILE) if($debug);
     	   }
          $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");

     	  print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?$returnwww$session_and");
     	  exit 0;
	}else{
          $update_adr->{txt28} = $now_dt . " else pseudocardpan\n" . $q->escapeHTML($payone_return);
          $update_adr->{int12} = $vde_on_fail;
	  $dbt->update_record($dbh,$update_adr,$users_sharee) if($users_sharee->{c_id} > 0);
          $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");
	}

	#print FILE Dumper($update_adr) . "\n" if($debug);
        close(FILE);
     }#end payone response with pseudocardpan
  }

  #redirections 
  if($users_sharee->{c_id} && ($path =~ /$varenv{mandant}\/$varenv{profile}/ || $path =~ /$varenv{mandant}\/Account/)){

    if($R::sharee_edit && $R::sharee_edit =~ /save_account/){
     $returnwww =~ s/::/=/g if($returnwww && $returnwww =~ /success::\w+/);
     ($api_return,$users_sharee) = $apif->auth_verify($q,$coo,"");
     $payable_check = $bw->isuser_rentable($users_sharee);

     if((!$users_sharee->{int14}) || ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/)){
     #failure redirect should do the delete job
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?failure=$users_sharee->{txt31}$session_and#top");
      exit 0;
     }
     elsif(!$users_sharee->{int03}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}$session");
      exit 0;
     }
     elsif(!$payable_check && $users_sharee->{int03}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}$session");
      exit 0;
     }
     elsif($payable_check && (!$users_sharee->{int04} || !$users_sharee->{int13})){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2\&sharee_edit=send_email_send_sms$session_and\&$returnwww");#send both
      exit 0;
     }
     elsif($payable_check && $payable_check == 1){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}?cum=2-$payable_check$session_and\&$returnwww");
      exit 0;
     }
     elsif($payable_check && $payable_check == 2){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=2-$payable_check$session_and\&$returnwww");
      #print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_2}?cum=2-$payable_check$session_and\&$returnwww");
      exit 0;
     }
    }elsif($path =~ /$varenv{mandant}\/$varenv{profile}/ && $referer !~ /failure=\w/){
     if((!$users_sharee->{int14}) || ($users_sharee->{txt31} && $tpl->{tpl_order} =~ /$users_sharee->{txt31}/)){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?cum=3\&failure=$users_sharee->{txt31}$session_and#top");
      exit 0;
     }elsif(!$payable_check){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?cum=3$session_and\&$returnwww");
      exit 0;
     }else{
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=3$session_and\&$returnwww");
      exit 0;
     }  
    }
  }
  #redirect used by App user profile button 
  elsif(1==1 && $users_sharee->{c_id} && ($path =~ /$varenv{mandant}\/Anmelden|$varenv{mandant}\/$varenv{profile}/)){
    if(!$users_sharee->{int14}){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1}?cum=4$session_and\&$returnwww");
      exit 0;
    }elsif(!$payable_check){
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_1_5}?cum=4$session_and\&$returnwww");
      exit 0;
    }else{
      print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=4$session_and\&$returnwww");
      exit 0;
    }
  }
  elsif(!$users_sharee->{c_id} && !$R::logedout && ($path !~ /$varenv{mandant}\/Karte|$varenv{mandant}\/Anmelden|$varenv{mandant}\/Account|$varenv{mandant}\/Kontakt|PDFGenerator/)){
    print redirect("$varenv{wwwhost}?logedout=1$session_and");
    exit 0;
  }
 }#end web frame


 #@all
 ($api_return,$users_sharee) = $apif->auth_verify($q,"",$users_sharee->{c_id});
 #send confirm codes
 if($users_sharee->{c_id} && $users_sharee->{txt34} && length($users_sharee->{txt34}) > 10 && ($users_sharee->{int04} != 1 || $users_sharee->{int13} != 1) && ($R::sharee_edit && $R::sharee_edit =~ /save_account|send_email|send_sms/ || $R::base_edit && $R::base_edit eq "save_dmsusers")){
   open(FILE,">>$varenv{logdir}/confirm.log") if($debug);
   print FILE "\n\n*--> $now_dt done by default\n" if($debug);
   print FILE "confirm-code request by sharee_edit:$R::sharee_edit or base_edit:$R::base_edit\n" if($debug);
   print FILE "$users_sharee->{txt08}:$users_sharee->{int04} || $users_sharee->{txt07}:$users_sharee->{int13}\n" if($debug);

   if($users_sharee->{int04} != 1 && ($users_sharee->{txt08} =~ /\w\@\w/)){
    $shwo->emailack(\%varenv,$users_sharee->{c_id});
   }
   if($users_sharee->{int13} != 1 && ($users_sharee->{txt07} =~ /\d{9}/ && length($users_sharee->{txt07}) <= 16)){
    $shwo->smsack($users_sharee);
   }
   close(FILE) if($debug);
 }#send confirm code

 #email and sms acknowledgments, check and save confirm code states
 if($users_sharee->{c_id} == $R::confirm_userid && ($R::confirm_code && length($R::confirm_code) >= 5 || $R::confirm_smscode && length($R::confirm_smscode) >= 5)){

    $shwo->code_confirmation($q,\%varenv,$aowner);
    $dbt->update_operatorsloop($varenv{dbname},$users_sharee->{c_id},"update");
    ($api_return,$users_sharee) = $apif->auth_verify($q,"",$users_sharee->{c_id});

    if(!$users_sharee->{int04} || !$users_sharee->{int13}){
      my $field = "int13";
      $field = "int04" if(!$users_sharee->{int04});
      if($varenv{orga} eq "dms"){
 	print redirect("$varenv{wwwhost}/$dbt->{shareedms_conf}->{parent_node}/$dbt->{shareedms_conf}->{start}/?failure=$field#top");
      }else{
        print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?cum=5$session_and\&failure=$field#top");
      }
      exit 0;
    }

    if($users_sharee->{c_id} && $users_sharee->{int04} && $users_sharee->{int13}){
      if($varenv{orga} eq "dms"){
        print redirect("$varenv{wwwhost}/$dbt->{shareedms_conf}->{parent_node}/$dbt->{shareedms_conf}->{start}/?confirm_success=1");
      }else{
        print redirect("$varenv{wwwhost}/$varenv{mandant}/Account/$varenv{accounting_3}?confirm_success=1$session_and");
      }
      exit 0;
    }
 }#end confirm


 #Printpreview
 if($view =~ /Printpreview/){
  require "Mod/Printpreview.pm";
  &Printpreview::printpre();
  exit 0;
 }
 elsif($view =~ /PDFGenerator/){
  require "Mod/PDFGenerator.pm";
  &PDFGenerator::printpre();
  exit 0;
 }

 #CSVout
 if($R::rel_edit && $R::rel_edit =~ /XLSout/){
  $users_dms = $dbt->select_users($dbh,$users_dms->{u_id});
  if(-f "$dbt->{copri_conf}->{basedir}/pdf/$users_dms->{owner}-$users_dms->{time4csv}.xls"){
   print $q->redirect(-uri=>"$varenv{metahost}/pdf/$users_dms->{owner}-$users_dms->{time4csv}.xls", -type=>"application/octet-stream", -target=>'_blank');
   exit 0;
  }
 }

 my $dyn_css = "";
 my $url = "$varenv{wwwhost}/$path";
 my $onload="";

 my $local_style = "$varenv{metahost}/$dbt->{shareeapp_conf}->{local_style}";
 #my $jquery = "$varenv{metahost}/$dbt->{copri_conf}->{jsscript}";
 my $jquery = "$varenv{metahost}/$dbt->{shareeapp_conf}->{jquery}";#used by CalReserv / rentalator.js
 my $jquery_ui = "$varenv{metahost}/$dbt->{copri_conf}->{jsscript}";

 my $style_jquery_ui = "";
 my $js_bootstrap = "$varenv{metahost}/$dbt->{shareeapp_conf}->{js_bootstrap}";
 my $style_bootstrap = "$varenv{metahost}/$dbt->{shareeapp_conf}->{style_bootstrap}";
 my $style_bootstrap_icons = "$varenv{metahost}/$dbt->{copri_conf}->{style_bootstrap_icons}";

 if($varenv{wwwhost} =~ /shareedms/){
    $local_style = "$varenv{metahost}/$dbt->{shareedms_conf}->{local_style}";
    $jquery = "$varenv{metahost}/$dbt->{shareedms_conf}->{jquery}";
    $jquery_ui = "$varenv{metahost}/$dbt->{shareedms_conf}->{jquery_ui}";
    $style_jquery_ui = "$varenv{metahost}/$dbt->{shareedms_conf}->{style_jquery_ui}";
    $js_bootstrap = "$varenv{metahost}/$dbt->{shareedms_conf}->{js_bootstrap}";
    $style_bootstrap = "$varenv{metahost}/$dbt->{shareedms_conf}->{style_bootstrap}";
    $dyn_js = "\$(function() {
         \$( '#datepicker1' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
         \$( '#datepicker2' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
         \$( '#datepicker3' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
         \$( '#datepicker4' ).datepicker({ dateFormat: 'dd.mm.yy', dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'], monthNames: ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember']});
        });";
 }

 my $favicon = "css/favicon.ico";
 my $base_uri = "true";
 my $title = "";
 $title = $dbt->{primary}->{$varenv{dbname}}->{title} if($dbt->{primary}->{$varenv{dbname}}->{title});
 $title = $dbt->{operator}->{$varenv{dbname}}->{title} if($dbt->{operator}->{$varenv{dbname}}->{title});
 if($dbt->{website}->{$varenv{syshost}}->{title}){
   $title = $dbt->{website}->{$varenv{syshost}}->{title};
   $favicon = "data/favicon.ico";
 }
 $title .= " devel" if($dbt->{copri_conf}->{stage} eq "test");

 my $html5 = $q->start_html(-title=>"$title",
			-lang=>"$lang",
                        -onload=>"$onload",
			-oncontextmenu=>"$oncontextmenu",
			-encoding=>"$html_charset",
			-base=>"$base_uri",
			-target=>"",
			-head=>[
				Link({
				-rel=>'shortcut icon', 
				-type=>'image/x-icon', 
				-href=>"$varenv{metahost}/$favicon"
				})
				],
			-meta=>{
				'viewport'=>"width=device-width,initial-scale=1,user-scalable=yes",
 			        'author'=>"Rainer Gümpelein",
				'publisher'=>"TeilRad GmbH",
				'copyright'=>"TeilRad GmbH",
				'keywords'=>"",
				'description'=>"sharee.bike Mietradmanagementsystem"
				},
			-script=>[
			 	{-language=>'JAVASCRIPT',
                                -src=>"$jquery"},
			 	{-language=>'JAVASCRIPT',
                                -src=>"$jquery_ui"},
				{-language=>'JAVASCRIPT',
				-src=>"$varenv{metahost}/$dbt->{copri_conf}->{jsscript}"},
				{-language=>'JAVASCRIPT',
                                -code=>"$dyn_js"}
				],
			-STYLE=>{
				-code=>"$dyn_css",
				-src=>[
				"$local_style",
				"$style_bootstrap",
				"$style_bootstrap_icons",
				"$style_jquery_ui"
				],
				-verbatim=>"\@import url(\"$local_style\");",
				-media=>'screen'
				}
			);
 # CGI.pm doesn't support HTML5 DTD; replace the one it puts in.
 $html5 =~ s{<!DOCTYPE.*?>}{<!DOCTYPE html>}s;
 $html5 =~ s{<html.*?>}{<html lang='$lang'>}s;
 print $html5;

 print $q->div({-style=>'background-color:black;color:white;'},"<noscript>JavaScript is off. Please enable to view full site.</noscript>"),"\n";

 #feedb will be the new hashed return. todo refactoring
 if(ref($return) eq "HASH"){
   $feedb = $return;
 }elsif($return =~ /failure::/){
   $feedb->{message} = $return;
 }
 $ml->tpl($q,$node_meta,$users_dms,$mode,\%varenv,$users_sharee,$feedb);

if(ref($api_return) eq "HASH" && $api_return->{response_text}){
print<<EOF
 <style>div#retm_konrad {position:fixed;right:40%;top:0;padding:5px;text-align:center;color:black;background-color:white;z-index:110;}</style>
  <script>
        \$(document).ready(function(){
         \$( "#retm_konrad" ).fadeOut(7000);
  })
  </script>
EOF
;
 print $q->div({-id=>'retm_konrad'},"$api_return->{response_text}"),"\n";
}

 print "<script src='$js_bootstrap'></script>\n";

 print $q->end_html;
 return Apache2::Const::OK;
}
1;
