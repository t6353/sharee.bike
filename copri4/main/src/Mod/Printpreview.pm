package Printpreview;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;

use POSIX;
use CGI::Cookie ();
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Libenz;
use Mod::DBtank;
use Mod::Pricing;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

sub printpre(){
 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $dbt = new DBtank;
 my $pri = new Pricing;

 my %varenv = $cf->envonline();
 my $path = $q->path_info();
 my $script = $q->script_name();
 my $user_agent = $q->user_agent();
 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my $lang="de";
 my $dbh = "";

 my $printer_id=$R::printer_id;
 my $u_id=$R::u_id;
 my $c_id=$R::c_id4trans;
 my $mandant_main_id=$R::mandant_main_id;
 my $main_id=$R::main_id;
 my $ct_name2print=$R::ct_name2print;
 my $pkey = $R::pkey || "";
 my $debug = 0;#print line to doc if debug

 $varenv{cms} = $dbt->fetch_cms($dbh,{ lang => $lang });
 my $api_file = "/var/www/copri4/shareeconf/apikeys.cfg";
 my $aconf = Config::General->new($api_file);
 my %apikeyconf = $aconf->getall;

 print $q->start_html(-title=>'',
                        -base=>'true',
                        -target=>'Printpre',
			-title=>"$R::ct_name2print"
                      );

 my $width="1400px";
if(1==1){
 print "<style type='text/css'><!--
body,html {
   background-color: white;
   width: $width;
   margin: 0px; padding: 0;
   border: 0px solid black;
   text-align: left;
   font-family: Arial,sans-serif;
   font-size:22px;
}
.Oline {
  border-bottom-width:1px;
  border-bottom-style:solid;
  border-color:black;
  margin:0.5em 0;
}
th {
  font-size:0.91em;
  border-bottom: solid thin gray;
  border-right: solid thin gray;
}
td {
 border:0px;
}

.tdtxt {
  font-size:0.91em;
  padding:0.1em 0.5em;
  vertical-align: top;
  text-align: left;
  min-width:5em;
}
.tdtxt2 {
  font-size:1em;
  padding:0.1em 0.5em;
  vertical-align: top;
  text-align: left;
  width:50%;
}

.tdsum {
  font-size:1em;
  padding:0.1em 0.5em;
  vertical-align: top;
  text-align: right;
  font-weight:bold;
}

.tdint {
  font-size:0.91em;
  padding:0.1em 0.5em;
  vertical-align: top;
  text-align: right;
  min-width:20px;
}

.tdpdf1 {
  font-size:1.2em;
  padding:0.1em 0;
  vertical-align: top;
  text-align: left;
}
.tdpdf2 {
  background-color:white;
  padding:0.1em 1.2em 0 0;
  vertical-align: top;
  text-align: left;
  font-size:0.85em;
}
.tdpdf3 {
  padding:0.1em 0 0.3em 0;
  vertical-align: top;
  text-align: center;
  font-size:1em;
}
.tdpdf4 {
  padding:0.2em 0;
  vertical-align: top;
  text-align: left;
  font-size:1em;
}
.tdpdf5 {
  padding:0.2em 0;
  vertical-align: top;
  text-align: right;
  font-size:1em;
}
//--></style>";
}

 my $tplf = $dbt->get_tpl($dbh,201);#Kunden-Faktura alias Firma tpl
 my @tplf_order = split /,/,$tplf->{tpl_order};

 #logging siteformat
 open(PMA, ">> $varenv{logdir}/Printpreview.log");
 print PMA "\n-->$now_dt --> $varenv{dbname}-$ct_name2print\n";

 if(!$apikeyconf{pdfprinter}->{pkey} || !$pkey || $apikeyconf{pdfprinter}->{pkey} ne $pkey){
     print "Failure: access denied, api-key doesn't match";
     return Apache2::Const::OK;
     exit 0;
 }

 my $site=1;
 my $site_all=1;

 my $header_top = 0;
 my $footer_px = 1800;
 my $footer_top = $footer_px;#end 1.site

 my $sum_paid = 0;
 my $exit_table_wc = 0;
 my $nullcount = 0;
 my $cttpos = { c_id => 0 };
 my $max_table_wc = "10";
 my $break_table_wc = 0;
    
 my $ctf = { c_id => 0 };
 my $pref_cu = {
    table => "contentuser",
    fetch => "one",
    c_id  => $mandant_main_id,
 };
 $ctf = $dbt->fetch_tablerecord($dbh,$pref_cu) if($mandant_main_id);

 my $ctt = { c_id => 0 };
 my $pref_ctt = {
    table => "contenttrans",
    fetch => "one",
    c_id  => $c_id,
 };
 $ctt = $dbt->fetch_record($dbh,$pref_ctt) if($c_id);

 #pre-count position rows
 if($ctt->{int10} && $ctt->{int10} == 2){
   $max_table_wc = "41";
   #take original operator accounting c_id to collect related invoices
   if($ctt->{template_id} != 208){#not equal Abrechnung
    my $pref = {
        table           => "contenttrans",
        fetch           => "one",
        barcode         => $ctt->{barcode},
        txt00           => "Abrechnung",
    };
    my $ctt_accounting = $dbt->fetch_record($dbh,$pref);
    ($cttpos,$break_table_wc) = $dbt->collect_contenttrans($dbh,$ctt_accounting->{content_id});
   }else{
    ($cttpos,$break_table_wc) = $dbt->collect_contenttrans($dbh,$ctt->{c_id});
   }
 }
 #client rental position
 else{
   $max_table_wc = "12";
   ($cttpos,my $rows) = $dbt->collect_contentpos($dbh,"contenttrans",$ctt->{c_id});

   #pre counting rentals with gesamt > 0 to get line count
   foreach my $id (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

   my $gesamt = 0;
   my $pricing = {};
   my $counting = {};

   if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
     $pri->count_freedrental("Printpreview",\%varenv,$ctt->{int10},$cttpos->{$id},1);
     ($pricing,$counting) = $pri->counting_rental(\%varenv,$cttpos->{$id});
     $gesamt = $pri->round($pricing->{total_price});
     $gesamt = sprintf('%.2f', $gesamt);
   }else{
     ($gesamt,my $rabatt) = $pri->price2calc($cttpos->{$id});
     $gesamt = $pri->round($gesamt);
     $gesamt = sprintf('%.2f', $gesamt);
   }
   if($gesamt != 0){
    $break_table_wc++;
   }
  }
 }
 print PMA "ctt.c_id: $ctt->{c_id} | ctt.int10: $ctt->{int10} | break_table_wc: $break_table_wc > max_table_wc:$max_table_wc\n";

 if($break_table_wc > $max_table_wc){
   $site_all = $break_table_wc / $max_table_wc;
   $site_all += 1 if($site_all =~ /\d\.\d/);
   $site_all = sprintf('%.1d',$site_all);
   print PMA "2) site_all: $site_all if($break_table_wc > $max_table_wc)\n";
 }

 if(1==1){
  for($site=1; $site <= $site_all; $site++){
   #1.site
   if($site == 1){
    print PMA "1.$site) header_big header_top: $header_top | $site\n";
    &header_big(\%varenv,$ctf,$ctt,$header_top);
    &data_invoice(\%varenv,$ctt,$ct_name2print,$header_top);

    #operator invoices int02=2 Operator-Faktura else position accounting
    if($ctt->{int10} && $ctt->{int10} == 2){
   	($break_table_wc,$nullcount) = &accounting_data_table(\%varenv,$ctf,$ctt,$cttpos,$break_table_wc,$max_table_wc,$header_top);
    }else{
   	($break_table_wc,$nullcount) = &data_table(\%varenv,$ctf,$ctt,$cttpos,$break_table_wc,$max_table_wc,$header_top);
    }

    print PMA "1.$site) text_footer: footer_top: $footer_top | $site/$site_all\n";
    &text_footer(\%varenv,$ctf,$ctt,$footer_top,$site,$site_all);
   }
   # > 2.site
   else{
    my $header_px = 2000;
    $header_top = $header_px;#2. site

    $header_top = $header_px * ($site -1);
    $footer_top = $footer_px * $site + ($header_px - $footer_px -10);
    $footer_top = $footer_px * $site + ($site * (0.75 * ($header_px - $footer_px))) if($site > 2);

    print PMA "2.$site) text_footer footer_top: $footer_top | $site/$site_all\n";
    &text_footer(\%varenv,$ctf,$ctt,$footer_top,$site,$site_all);
   
    print PMA "2.$site) header_small header_top: $header_top | $site/$site_all\n";
    &header_small(\%varenv,$ctf,$header_top);

    $header_top = $header_top + 250;
    &data_invoice(\%varenv,$ctt,$ct_name2print,$header_top);

    #table_break
    $break_table_wc -= $max_table_wc;
    print PMA "2.$site) break_table_wc: $break_table_wc > max_table_wc:$max_table_wc\n";
     $header_top +=60;
     #operator invoices int02=2 Operator-Faktura else position accounting
     if($ctt->{int10} && $ctt->{int10} == 2){
       ($exit_table_wc,$nullcount) = &accounting_data_table(\%varenv,$ctf,$ctt,$cttpos,$break_table_wc,$max_table_wc,$header_top);
     }else{
       ($exit_table_wc,$nullcount) = &data_table(\%varenv,$ctf,$ctt,$cttpos,$break_table_wc,$max_table_wc,$header_top);
     }
   }#end > 2.site
  }
  &text_description(\%varenv,$ctf,$ctt,$nullcount,$footer_top);
 }
 
 close PMA;

 #header_small
 sub header_small(){
  my $varenv = shift;
  my $ctf = shift;
  my $header_top = shift || 0;

  my $position = "";
  my $header_toppx = "$header_top" . "px";
  $position = "position:absolute; top: $header_toppx;" if($header_top);
  my $line = "";
  $line = $header_top if($debug);

  my $height = "120px;";
  my $margin = "0 0 100px 0;";
  if($dbt->{operator}->{$varenv->{dbname}}->{project} =~ /Bayern|Freiburg/){
	$height = "200px;";
	$margin = "0;";
  }

  my $logo = $ctf->{img01};
  $logo = "Logo-sharee_right.png" if($ctt->{int10} == 2);
  print $q->div({-style=>"$position width:$width;text-align:right;border:0px solid black;margin:$margin"},"$line",$q->img({-src=>"$varenv->{metahost}/img/$logo",-height=>"$height"})),"\n";
 }#end header_small

 #header_big
 sub header_big(){
  my $varenv = shift;
  my $ctf = shift;
  my $ctt = shift;
  my $header_top = shift || 0;

  $ctt->{txt01} = $lb->newline($ctt->{txt01},"","0");
  my $line = "";
  $line = $header_top if($debug);

  my $height = "120px;";
  my $margin = "0 0 100px 0;";
  if($dbt->{operator}->{$varenv->{dbname}}->{project} =~ /Bayern|Freiburg/){
        $height = "200px;";
        $margin = "0;";
  }

  my $logo = $ctf->{img01};
  $logo = "Logo-sharee_right.png" if($ctt->{int10} == 2);
  print $q->div({-style=>"width:$width;text-align:right;border:0px solid black;margin:$margin"},$q->img({-src=>"$varenv->{metahost}/img/$logo",-height=>"$height"})),"\n";

  print $q->start_table({-width=>'100%',-border=>'0',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
  print "<tr>\n";

  print "<td style='border:0px solid black;vertical-align:top;width:60%;height:250px;padding:0 0 0 2em;'>\n";
  print $q->start_table({-width=>'100%',-border=>'0',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf4', -style=>'padding-bottom:0.8em;text-decoration:underline;color:black;'}, "$ctf->{txt01} | $ctf->{txt03} | $ctf->{txt06}"),"\n";
  #print $q->Tr();
  #print $q->td({-class=>'tdpdf1'}, "$ctt->{txt02} &nbsp;"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf1'}, "$ctt->{txt01} &nbsp;"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf1'}, "$ctt->{txt03} &nbsp;"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf1'}, "$ctt->{txt06} &nbsp;"),"\n";
  print $q->Tr();
  print $q->td({-style=>'padding:0.8em 0;'}),"\n";
  print $q->end_table;  
  print "</td>";


  my $txid = "";
  $txid = "Verwendungszweck TXID: $ctt->{txt16}" if($ctt->{txt16} && $ctt->{state} =~ /payone/i);
  my $mailaccount = "";
  $mailaccount = "Account: $ctt->{txt08}" if($ctt->{txt08});
  my $invoice_period = "";
  $invoice_period = "Leistungszeitraum: $ctt->{txt20}" if($ctt->{txt20});

  print "<td style='width:40%;vertical-align:right;border:0px solid black;'>";
  print $q->start_table({-border=>'0',-align=>'right', -cellpadding=>'0', -cellspacing=>'0'});
  print $q->Tr();
  print $q->td({-style=>'padding:2em 0;'},"&nbsp;"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf5'}, "$txid"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf5'}, "$mailaccount"),"\n";
  print $q->Tr();
  print $q->td({-class=>'tdpdf5'}, "$invoice_period"),"\n";
  print $q->end_table;  
  print "</td>";

  print "</tr>";
  print $q->end_table;
 }#end header_big

 #head line
 sub data_invoice(){
  my $varenv = shift;
  my $ctt = shift;
  my $ct_name2print = shift;
  my $header_top = shift || 0;
  my $line = "";
  $line = $header_top if($debug);

  my $kd_nr = " Kunden Nr. $ctt->{int10}" if($ctt->{int10} != 2);
  my $mandat_nr = "";
  $mandat_nr = " Mandatsreferenz: $ctt->{txt26}" if($ctt->{txt26} =~ /\w{2}-/);

  my $invoice_time = $ctt->{invoice_time} || $ctt->{mtime};
  my $update_ctt = {
           table   => "contenttrans",
           invoice_time   => "$invoice_time",
         };
  $dbt->update_record($dbh,$update_ctt,$ctt);
  my $mtime = $lb->time4de("$invoice_time","");

  my $position = "";
  my $header_toppx = "$header_top" . "px";
  $position = "position:absolute; top: $header_toppx;" if($header_top);

  print $q->div({-style=>"width:$width;$position border: 0px solid black;"},
  $q->span({-style=>'font-size:1.5em;margin:0.5em 0 0.5em 0.5em;'},"$line","$ctt->{txt00}"),
  $q->span({-style=>'font-size:1em;margin:0.5em 0.5em 0.5em 0;'},"Nr. $varenv->{dbname}-$ct_name2print"),
  $q->span({-style=>'float:right;text-align:right;font-size:1em;margin:0.5em 0 0.5em 0;'},"Datum: $mtime&nbsp;"),
  $q->span({-style=>'font-size:1em;margin:0.5em;'}," $kd_nr"),
  $q->span({-style=>'font-size:1em;margin:0.5em;'}," $mandat_nr"),
  ); 
 }

 #operator accounting invoice
 sub accounting_data_table {
  my $varenv = shift;
  my $ctf = shift;
  my $ctt = shift;
  my $cttpos = shift;
  my $break_table_wc = shift || 0;
  my $max_table_wc = shift || 0;
  my $header_top = shift || 0;
  my @tpl_order = ();

  #int9x are not in db
  #operator invoice
  if($ctt->{txt00} eq "Rechnung"){
    @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int93=Entgelt TeilRad GmbH");
  }
  #operator accounting
  else{
   if($varenv->{dbname} eq "sharee_sx"){
      @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int94=Disagio","int02=Gutschrift");
    }else{
      @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int94=Disagio","int95=Transaktion","int96=Zahlungsmeldung","int97=Kreditkarte Zuordnung","int02=Gutschrift");
    }
  }

   my $sum_operatorcredit=0;
   my $diff19 = 100 + 19;
   my $sum_umst19=0;
   my $i=0;
   my $j=0;
   my $rows = 0;

   my $header_toppx = "$header_top" . "px";
   my $position = "";;
   $position = "position:absolute; top: $header_toppx;" if($header_top);
   my $line = "";
   $line = $header_top if($debug);
   print "\n<div style='width:$width;$position border:0px solid black;'>\n";
   print $q->start_table({ -border=>'0', -width=>"$width",-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

   my $tc=0;
   print $q->Tr();
   #print $q->th("$line Pos."),"\n";
   foreach (@tpl_order){
    my ($key,$val) = split /=/,$_;
    $tc++;
    $val = "$line $val" if($key eq "c_id");
    print $q->th({-class=>'tdint'},"$val");
   }

   my $ctf_operator = { c_id => 0 };
   my $pref_cuop = {
    	  table => "contentuser",
    	  fetch => "one",
    	  c_id  => 2,
   };
   $ctf_operator = $dbt->fetch_tablerecord($dbh,$pref_cuop);

   foreach my $id (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){
     #print Dumper($cttpos->{$id}); exit;
     my $oac = { c_id => 0 };
     $oac = $pri->operator_accounting2calc($varenv,$cttpos->{$id},$ctf_operator);
     $oac->{int01} = sprintf('%.2f', $oac->{int01});

    $j++;
    #if($j<=$max_table_wc || $break_table_wc)
    if($j<=$max_table_wc){
      $now_dt =~ s/[-:\s]//g;
      open(CSV, ">> $varenv->{pdf}/$ctt->{txt00}-$ctt->{c_id}-$now_dt.csv") or die "Can't opencsv\n";

     $i++;
     print $q->Tr(),"\n";
     #print $q->td({-class=>'tdint'},"$i"),"\n";

     foreach (@tpl_order){
      my ($key,$val) = split /=/,$_;
      $cttpos->{$id}->{$key} = $q->unescapeHTML($cttpos->{$id}->{$key});

      if(1==1){
        if($key =~ /ct_name/){
	  print CSV "$cttpos->{$id}->{$key};";
          #print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key} $cttpos->{$id}->{txt01}"),"\n";
          print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key}"),"\n";
        }
        elsif($key =~ /c_id/){
	  print CSV "$cttpos->{$id}->{$key};";
	  if($debug){
	   print $q->td({-class=>'tdint'},"$i) $cttpos->{$id}->{$key}"),"\n";
	  }else{
           print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
	  }
        }
        elsif($key =~ /int/){
          $oac->{$key} =~ s/\./,/;
	  if($key =~ /int02|int93/){
	    print CSV "$oac->{$key}\n";
	  }else{
	    print CSV "$oac->{$key};";
	  }
          print $q->td({-class=>'tdint',-nowrap=>1},"$oac->{$key} €"),"\n";
        }
        elsif($key =~ /state/){
	  $cttpos->{$id}->{$key} =~ s/ \(payone\)//;
	  print CSV "$cttpos->{$id}->{$key};";
          print $q->td({-class=>'tdint',-nowrap=>1},"$cttpos->{$id}->{$key}"),"\n";
        }
        elsif($key =~ /txt/){
	  print CSV "$cttpos->{$id}->{$key};";
          print $q->td({-class=>'tdtxt'},"$cttpos->{$id}->{$key}"),"\n";
        }
      }
     }
     delete $cttpos->{$id};
     close CSV;
    }
   }

   #Sum Operator invoice
   if($break_table_wc > 0 && $break_table_wc <= $max_table_wc){

    #print "$ctt->{int01}|$ctt->{int02}|";
    my $sum_brutto = $ctt->{int01} || 0;
    $sum_brutto =~ s/,/\./;#hm
    my $sum_operatorcredit = $ctt->{int02} || 0;
    my $total_operatorcredit = $sum_operatorcredit - $sum_brutto;
    $total_operatorcredit =~ s/\./,/;

    if($sum_brutto && $sum_brutto != 0){
     $sum_umst19 = $sum_brutto / $diff19 * 19;
     $sum_umst19 = $pri->round($sum_umst19);
    }

    my $sum_netto19 = $sum_brutto - $sum_umst19;
    $sum_netto19 = sprintf('%.2f', $sum_netto19);
    $sum_netto19 =~ s/\./,/;
    $sum_brutto =~ s/\./,/;

    $sum_umst19 = sprintf('%.2f', $sum_umst19);
    $sum_umst19 =~ s/\./,/;

    $sum_operatorcredit = sprintf('%.2f', $sum_operatorcredit);
    $sum_operatorcredit =~ s/\./,/;

    print $q->Tr(),"\n";
    print $q->td({-class=>'Oline',-colspan=>$tc},"&nbsp;"),"\n";

    my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
        $ctt->{state} =~ s/\(.*//;#delete like (payone)
        if($des =~ /$ctt->{state}/){
          if($sum_brutto < 0){
            $payment_text = "$ctf->{txt58}";
          }else{
            $payment_text = "$ctf->{$key}";
          }
        }
      }
    }

    my $sc = 2;
    $sc = 1 if($ctt->{template_id} != 208);
    my $pt = $tc - $sc;
    print "<tr>\n";
    print "<td colspan='$pt' style='border:0px solid silver;vertical-align:top;padding:0.2em 0.5em;font-size:0.91em;'>$payment_text</td>\n";

    ###print sum
    print "<td colspan='$sc' style='font-size:1em;max-width:200px;'>\n";
    print $q->start_table({-style=>'border:1px solid silver;', -border=>'0', -width=>'100%',-align=>'right', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

   #Sum operator invoive
   if($ctt->{template_id} != 208){#not equal Abrechnung
     print $q->Tr("\n");
     print $q->td({-class=>'tdint',-colspan=>1},"Nettobetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto19 €");

     if($sum_netto19 != 0){
      print $q->Tr("\n");
      print $q->td({-class=>'tdint',-colspan=>1,-nowrap=>"1"},"19% UmSt auf $sum_netto19 €");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");
     }

     print $q->Tr("\n");
     print $q->td({-class=>'tdsum',-colspan=>1},"Entgelt TeilRad GmbH");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_brutto €");
   }
   #Summe Betreiber Abrechnung (Gutschrift)
   else{
     print $q->Tr("\n");
     print $q->td({-class=>'tdsum',-colspan=>1},"Summe Gutschrift");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_operatorcredit €");

     print $q->Tr("\n");
     print $q->td({-class=>'tdsum',-colspan=>1},"Entgelt TeilRad GmbH");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"- $sum_brutto €");

     print $q->Tr("\n");
     print $q->td({-class=>'tdsum',-colspan=>1},"Auszahlungsbetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$total_operatorcredit €");
   }

    print $q->end_table,"\n";
    print "</td>\n";
    ###print sum end

    print "</tr>\n";
   }

   print $q->end_table,"\n";
   print "</div>\n";
   return ($j,$nullcount);
 }#end sub accounting_data_table

 #client invoice
 sub data_table(){
  my $varenv = shift;
  my $ctf = shift;
  my $ctt = shift;
  my $cttpos = shift;
  my $break_table_wc = shift || 0;
  my $max_table_wc = shift || 0;
  my $header_top = shift || 0;
 
  #again to keep time format#TODO
  #($cttpos,my $rows) = $dbt->collect_contentpos($dbh,"contenttrans",$ctt->{c_id});

  my  @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=Mietzeit","int03=Menge (Std:Min)","int02=Preis","int07=Rabatt","int04=Gesamt");
  foreach my $id (keys(%$cttpos)){
    if($cttpos->{$id}->{int26}){
      @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=Mietzeit","int26=Einsparung","int03=Menge (Std:Min)","int02=Preis","int07=Rabatt","int04=Gesamt");
    }
  }

  my $sum_parts19=0;
  my $diff19 = 100 + 19;
  my $sum_umst19=0;
  my $j=0;
  my $nullcount=0;

  my $header_toppx = "$header_top" . "px";
  my $position;
  $position = "position:absolute; top: $header_toppx;" if($header_top);  
  my $line = "";
  $line = $header_top if($debug);

  print "\n<div style='width:$width;$position border:0px solid black;'>\n";
  print $q->start_table({ -border=>'0', -width=>"$width",-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

  #Tableheader
  my $tc=0;
  print $q->Tr(),"\n";
  foreach (@tpl_order){
   my ($key,$val) = split /=/,$_;
   $tc++ if($val && $key !~ /int07/);
   print $q->th({-class=>'tdint'},"$line $val"),"\n" if("$key" eq "ct_name");
   print $q->th({-class=>'tdtxt2'},"$val"),"\n" if($key =~ /txt01/);
   print $q->th({-class=>'tdint'},"$val"),"\n" if($key =~ /int02|int03|int04|int26/);
  }

  foreach my $id (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

   my $gesamt = 0;
   my $rabatt = "";
   my $pricing = {};
   my $counting = {};
   my $rental_feed = {};

   if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
     $pri->count_freedrental("Printpreview",\%varenv,$ctt->{int10},$cttpos->{$id},1);
     ($pricing,$counting) = $pri->counting_rental($varenv,$cttpos->{$id});
     $rental_feed = $pri->fetch_rentalfeed($varenv,$varenv,$cttpos->{$id},$counting);
     $rabatt = $pricing->{discount};
     $sum_parts19 += $pricing->{total_price};
     $gesamt = $pri->round($pricing->{total_price});
     $gesamt = sprintf('%.2f', $gesamt);
   }else{
     ($gesamt,$rabatt) = $pri->price2calc($cttpos->{$id});
     $sum_parts19 += $gesamt;
     $gesamt = $pri->round($gesamt);
     $gesamt = sprintf('%.2f', $gesamt);
   }

   if($gesamt == 0){
	$nullcount++;
   }

   if($gesamt != 0){

    $j++;
    if($j<=$max_table_wc){

      print $q->Tr(),"\n";
      foreach (@tpl_order){
        my ($key,$val) = split /=/,$_;
        $cttpos->{$id}->{$key} = $q->unescapeHTML("$cttpos->{$id}->{$key}");
        $cttpos->{$id}->{$key} = $lb->newline($cttpos->{$id}->{$key},"");
        if("$key" eq "ct_name"){
          my $ct_pos = "";
 	  if($cttpos->{$id}->{int09}){#if Tarif defined
	    $ct_pos = "Mietrad: $cttpos->{$id}->{ct_name}";
	    $ct_pos .= "<br />Endstation: $cttpos->{$id}->{txt13}$cttpos->{$id}->{int04}" if($cttpos->{$id}->{int04});
	  }else{
            $ct_pos = "$cttpos->{$id}->{ct_name}";
	  }
	  print $q->td({-class=>'tdint',-nowrap=>'1'},"$ct_pos&nbsp;"),"\n";
        }
        if($key =~ /txt01/){
         print "<td class='tdtxt'>\n";
          if($cttpos->{$id}->{$key}){
	    $cttpos->{$id}->{$key} =~ s/\<br \/\>/, &nbsp; /g;
	    my $tariff = "";
	    $tariff = "<br />Tarif: $cttpos->{$id}->{int09} $cttpos->{$id}->{txt04}" if($cttpos->{$id}->{txt04});
            print "$cttpos->{$id}->{$key} $tariff<br />\n";
          }
          if($cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
            #print "<br />\n";
            $cttpos->{$id}->{start_time} = $lb->time4de($cttpos->{$id}->{start_time},"1");
            $cttpos->{$id}->{end_time} = $lb->time4de($cttpos->{$id}->{end_time},"1");

            print $q->span("$cttpos->{$id}->{start_time} – $cttpos->{$id}->{end_time}"),"\n";
          }
          print "</td>\n";
        }
	if($key eq "int26"){
            my $co2saving = "";
            if($cttpos->{$id}->{int26}){
              $co2saving = "Einsparung</br>";
              my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />" if($sprit_price !~ /-/);
              $cttpos->{$id}->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{$id}->{int26} KM";
            }
            print $q->td({-class=>'tdint',-nowrap=>'1'},"$co2saving");
	}
        if($key =~ /int03/){
         if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
            print $q->td({-class=>'tdint',-nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
          }else{
            $cttpos->{$id}->{$key} =~ s/\./,/;
            print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
          }
	}

        if($key =~ /int02/){
          if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
            print "<td class='tdint' nowrap>\n";
            foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
              if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
                print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
              }
            }
            print "</td>\n";
          }else{
            $cttpos->{$id}->{$key} =~ s/\./,/;
            print $q->td({-class=>'tdint',-nowrap=>1},"$cttpos->{$id}->{$key} €"),"\n";
          }
        }

        if($key =~ /int04/){
	  $gesamt =~ s/\./,/;
	  if($rabatt != 0){
	    $rabatt =~ s/\./,/;
      	    print $q->td({-nowrap=>'1',-class=>'tdint'},"$rabatt $gesamt €"),"\n";
	  }else{
      	    print $q->td({-nowrap=>'1',-class=>'tdint'},"$gesamt €"),"\n";
	  }
         }
       }
       delete $cttpos->{$id};
      }
     }
    }#end foreach

  #Sum Endrechnung
  if($break_table_wc > 0 && $break_table_wc <= $max_table_wc){

   my $sum_parts19 = $ctt->{int01} || 0;
   if($sum_parts19 && $sum_parts19 != 0){
     $sum_umst19 = $sum_parts19 / $diff19 * 19;
     $sum_umst19 = $pri->round($sum_umst19);
   }
   my $sum_netto19 = $sum_parts19 - $sum_umst19;
   $sum_netto19 = sprintf('%.2f', $sum_netto19);
   $sum_netto19 =~ s/\./,/;

   my $sum_paid = $sum_parts19;
   $sum_paid = $pri->round($sum_paid);
   my $sum_preauth = $sum_paid || 0;
   $sum_paid = sprintf('%.2f', $sum_paid);
   $sum_paid =~ s/\./,/;

   $sum_parts19 = sprintf('%.2f', $sum_parts19);
   $sum_umst19 = sprintf('%.2f', $sum_umst19);
   $sum_umst19 =~ s/\./,/;

    my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
	$ctt->{state} =~ s/\(.*//;
        if($des =~ /$ctt->{state}/ && ($j <= $max_table_wc || $break_table_wc)){
          if($sum_parts19 < 0){
            $payment_text = "$ctf->{txt58}";
          }else{
            $payment_text = "$ctf->{$key}";
          }
        }
      }
   }


    print $q->Tr(),"\n";
    print $q->td({-class=>'Oline',-colspan=>6},"&nbsp;"),"\n";

    print "<tr>\n";
    print "<td colspan='3' style='border:0px solid silver;vertical-align:top;padding:0.2em 0.5em;font-size:0.91em;'>$payment_text</td>\n";

    ###print sum
    print "<td colspan='3' style='font-size:1em;'>\n";
    print $q->start_table({-style=>'border:1px solid silver;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
     print $q->Tr("\n");
     print $q->td({-class=>'tdint',-colspan=>2},"Nettobetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto19 €");

     if($sum_netto19 != 0){
      print $q->Tr("\n");
      print $q->td({-class=>'tdint',-colspan=>2,-nowrap=>"1"},"19% UmSt auf $sum_netto19 €");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");
     }

     print $q->Tr("\n");
     print $q->td({-class=>'tdsum',-colspan=>2},"Summe");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_paid €");
 
    }#end break_table_wc

    print $q->end_table,"\n";
    print "</td>\n";
    ###print sum end

    print "</tr>\n";

   print $q->end_table,"\n";
   print "</div>\n";
   return ($j,$nullcount);
  }#end sub data_table

  #Beschreibungs-Text
  sub text_description(){
   my $varenv = shift;
   my $ctf = shift;
   my $ctt = shift;
   my $nullcount = shift || 0;
   my $footer_top = shift || 0;

   my $position = "";
   $footer_top -= 170;
   my $footer_toppx = "$footer_top" . "px";
   $position = "position:absolute; top: $footer_toppx;" if($footer_top);
   my $line = "";
   $line = $footer_top if($debug);

   print "\n<div style='width:$width;$position border:0px solid black;'>$line\n";
   print $q->start_table({-style=>"margin-top:0.5em;", -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'0', -cellspacing=>'0'});

    #Frei-Text
    my $style = "padding:0.4em;text-align:left;font-size:0.91em;";
    if($nullcount > 0){
      print $q->Tr();
      print $q->td({-style=>"$style"},"In diesem Zeitraum gab es $nullcount kostenfreie Buchungsvorgänge.");
    }

    if($ctt->{txt12}){
      $ctt->{txt12} = $q->unescapeHTML("$ctt->{txt12}");
      $ctt->{txt12} = $lb->newline($ctt->{txt12},"","");
      print $q->Tr();
      print $q->td({-style=>"$style"},"$ctt->{txt12}");
    }
    #Text & Vorbelegungen
    print $q->Tr();
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;

      if($key =~ /txt6/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
	my @rechnungstext = split(/\<br \/\>/,$ctf->{$key});
        if($ctt->{txt21} =~ /$key/){
          foreach(@rechnungstext){
                print $q->Tr();
                print $q->td({-style=>"$style"},"$_");
          }
        }
      }
    }

   print $q->end_table;
   print "</div>\n";
  }#end text_description
  

  #site footer
  sub text_footer(){
   my $varenv = shift;
   my $ctf = shift;
   my $ctt = shift;
   my $footer_top = shift || 0;
   my $site = shift || 1;
   my $site_all = shift || 1;

   my @txt11 = split (/;/,"$ctf->{txt11}");#Tel usw.
   my @txt12 = split (/;/,"$ctf->{txt12}");#Geschäftsführer
   my @txt19 = split (/;/,"$ctf->{txt19}");#Bank usw. 

   my $top2 = $footer_top + 150;
   $footer_top .= "px";
   $top2 .= "px";
   print PMA "sub text_footer --> footer_top: $footer_top | top2: $top2\n";
   my $line = "";
   $line = $footer_top if($debug);

    print "<div style='border:0px solid black;background-color:white;height:150px;width:$width;position:absolute;top:$footer_top;'>$line\n";
    if($ctt->{int10} == 2){
     print $q->span({-style=>'padding:0.2em;text-align:center;font-size:0.81em;'},"Gebühren der Zahlungsdienstleistung:"),"\n" if($ctt->{template_id} == 208);
     my $tplop = $dbt->get_tpl($dbh,"196");#Operator-Faktura
     my @tplop_order = split /,/,$tplop->{tpl_order};
     foreach(@tplop_order){
      my ($key,$val,$size,$unit) = split /=/,$_;
      if($ctt->{template_id} != 208){
	if($key eq "int01"){
         $ctt->{$key} =~ s/\./,/;
         print $q->span({-style=>'padding:0.2em;text-align:center;font-size:0.81em;'},"$val $ctt->{int21} $unit "),"\n";
	}
      }else{
       	if($key ne "int01" && $key =~ /int(\d+)/){
         #take fee values used by creating operator accounting invoice
         my $count_key = $1 + 20;#cu.int to ctt.int
         my $ctt_key = "int" . $count_key;
	 if($ctt->{$ctt_key}){
          $ctt->{$ctt_key} =~ s/\./,/;
          print $q->span({-style=>'padding:0.2em;text-align:center;font-size:0.81em;'},"$val $ctt->{$ctt_key} $unit | "),"\n";
	 }
        }
      }
     }
     print "<br />\n";
     print "<br />\n";
    }

    print $q->start_table({-width=>'100%',-border=>'0',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'});
    print "<tr>";

    print "<td>";
    print $q->start_table({-border=>'0',-align=>'left'});
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt12[0]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt12[1]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt12[2]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt12[3]"),"\n";
    print $q->end_table;  
    print "</td>";

    print "<td>";
    print $q->start_table({-border=>'0',-align=>'left'});
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "TEL: $ctf->{txt07}"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$ctf->{txt08}"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt11[0]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt11[1]"),"\n";
    print $q->end_table;  
    print "</td>";

    print "<td>";
    print $q->start_table({-border=>'0',-align=>'left'});
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[0]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[1]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[2]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[3]"),"\n";
    print $q->end_table;  
    print "</td>";

    print "<td>";
    print $q->start_table({-border=>'0',-align=>'left'});
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[4]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[5]"),"\n";
    print $q->Tr();
    print $q->td({-class=>'tdpdf2'}, "$txt19[6]"),"\n";
    print $q->end_table;  
    print "</td>";

    print "</tr>";
    print $q->end_table;
    print "</div>";
    #end Adresse
  #}
   my $debug = "";
   print $q->div({-style=>"text-align:right;width:$width;position:absolute;top:$top2;font-size:0.61em;"},"$debug Seite: $site/$site_all");
 }

 print $q->end_html;
 return Apache2::Const::OK;
}
1;

