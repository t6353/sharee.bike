#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#Examples
#./src/scripts/messanger_divers shareedms-operator

use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || die 'syshost not defined';
}

use lib "/var/www/copri-bike/$syshost/src";

use strict;
use warnings;
use Lib::Config;
use Mod::Prelib;

my $cf = new Config;
my %varenv = $cf->envonline();
my $pl = new Prelib;
 
my $longterm_ctadr = $pl->longterm_occupied(\%varenv);
