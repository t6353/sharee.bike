#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#Examples
#SMS message if 24h,48h,72h occupied
#./src/scripts/sms_message.pl shareedms-operator '24h_occupied' '' ''
#
#SMS message locking_progress after 60sec
#./src/scripts/sms_message.pl shareeapp-operator locking_progress $adr_id $pos_id
#
#SMS theftalarm
#./src/scripts/sms_message.pl shareedms-operator 'send_alarm2hotline' '' '18498'
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || die 'syshost not defined';
}

use lib "/var/www/copri-bike/$syshost/src";

use strict;
use warnings;
use POSIX;
use Scalar::Util qw(looks_like_number);
use DateTime;
use DateTime::Format::Pg;
use Lib::Config;
use Mod::DBtank;
use Mod::SMSTransport;
use Data::Dumper;

my $cf = new Config;
my %varenv = $cf->envonline();
my $dbt = new DBtank;
my $smstrans = new SMSTransport;
 
my $todo = $ARGV[1] || die 'todo not defined';
my $adr_id = $ARGV[2] || "";
my $pos_id = $ARGV[3] || "";
my $dbh = "";
my $oprefix = $dbt->{operator}->{$varenv{dbname}}->{oprefix} || "";

#operator contact
   my $pref_cc = {
        table           => "contentuser",
        fetch           => "one",
        template_id     => "197",
        c_id    => 1,
   };
my $record_cc = { c_id => 0 };
$record_cc = $dbt->fetch_record($dbh,$pref_cc);
my $contact_hotline = "";
$record_cc->{txt07} =~ s/[\s\-\/]//g;
$contact_hotline = "Hotline $record_cc->{txt01} $record_cc->{txt08}, $record_cc->{txt07}";

#2024-02-22 substitued by messanger_divers longterm_occupied
#SMS message if 24h,48h,72h occupied
if($todo eq "24h_occupied"){
  #select booking pos if state=occupied and start_time > 24h
  my $search = {
        table_pos  => "contenttranspos",
        int10  => 3,
	int34  => "null",#if not staff
	start_time_interval => "(now() - interval '1 day')",
  };
  my $update_pos = {
        table  => "contenttranspos",
  };

  my $dt1 = DateTime->now(time_zone => "Europe/Berlin");

  my $cttpos = { c_id => 0 };
  $cttpos = $dbt->collect_transpos($dbh,$search,1);

  foreach my $pid (sort { $cttpos->{$b}->{end_time} cmp $cttpos->{$a}->{end_time} } keys(%$cttpos)){
    if($cttpos->{$pid}->{int10} == 3){
	my $dt2 = "";
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_24h_occupied = $dt2->add( days => 1 );
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_48h_occupied = $dt2->add( days => 2 );
	$dt2 = DateTime::Format::Pg->parse_datetime($cttpos->{$pid}->{start_time});
	my $dt2_72h_occupied = $dt2->add( days => 3 );
	#print $dt1 . ">=" . $dt2_72h_occupied . "|" .  $dt1 . ">=" . $dt2_48h_occupied . "|" . $dt1 . ">=" . $dt2_24h_occupied . "\n";
	if($dt2){
	 if($dt1 >= $dt2_72h_occupied){
	  if($cttpos->{$pid}->{int33} != 3){ 
	    $todo = "72h_occupied";
	    print $dt1 . ">=" . $dt2_72h_occupied . "|$todo,$contact_hotline,$cttpos->{$pid}->{ca_id},$cttpos->{$pid}->{ct_name}\n";
	    $smstrans->sms_message($todo,$contact_hotline,$cttpos->{$pid}->{ca_id},"",$cttpos->{$pid}->{ct_name});
	    $dbt->update_one($dbh,$update_pos,"int33=3",$cttpos->{$pid}->{c_id});
	  }
	 }
	 #2023-01-17 disabled, 24h and 72h are enough
	 #elsif($dt1 >= $dt2_48h_occupied){
	 # if($cttpos->{$pid}->{int33} != 2){ 
	 #   $todo = "48h_occupied";
	 #   print $dt1 . ">=" . $dt2_48h_occupied . "|$todo,$contact_hotline,$cttpos->{$pid}->{ca_id},$cttpos->{$pid}->{ct_name}\n";
	 #   $smstrans->sms_message($todo,$contact_hotline,$cttpos->{$pid}->{ca_id},"",$cttpos->{$pid}->{ct_name});
	 #   $dbt->update_one($dbh,$update_pos,"int33=2",$cttpos->{$pid}->{c_id});
	 # }
	 #}
	 elsif($dt1 >= $dt2_24h_occupied){
	  if($cttpos->{$pid}->{int33} != 1){ 
	    $todo = "24h_occupied";
	    print $dt1 . ">=" . $dt2_24h_occupied . "|$todo,$contact_hotline,$cttpos->{$pid}->{ca_id},$cttpos->{$pid}->{ct_name}\n";
            $smstrans->sms_message($todo,$contact_hotline,$cttpos->{$pid}->{ca_id},"",$cttpos->{$pid}->{ct_name});
	    $dbt->update_one($dbh,$update_pos,"int33=1",$cttpos->{$pid}->{c_id});
	  }
	 }
	}
	sleep 1;
    }
  }
}#end

#SMS message locking_progress after 60sec
if($todo eq "locking_progress" && $adr_id && $pos_id){
  sleep 60;
  #select booking pos if lock_state=locking still set
  my $booking_pos = {
        table           => "contenttranspos",
        fetch           => "one",
        c_id         => "$pos_id",
  };

  my $booking = { c_id => 0 };
  $booking = $dbt->fetch_tablerecord($dbh,$booking_pos);
  if($booking->{int20} == 3){
	$smstrans->sms_message($todo,$contact_hotline,$adr_id,"",$booking->{ct_name});
  }
}#end


#SMS message theftalarm
if($todo eq "send_alarm2hotline" && $pos_id){
  my $sms_to = "$dbt->{copri_conf}->{sms_to}";

  #will be executed by Ilockit cron, that's because extern
  my $client_encoding = "iso-8859-1";
  my $dbh_operator = $dbt->dbconnect_extern($dbt->{operator}->{$varenv{dbname}}->{database}->{dbname},$client_encoding);

  my $pref_ctu = {
     table => "contentuser",
     fetch => "one",
     c_id => "1",#Kontakt-hotline
  };
  my $uadr = { c_id => 0 };
  $uadr = $dbt->fetch_tablerecord($dbh_operator,$pref_ctu);
  my $phone = $uadr->{txt07} || $sms_to;

  my $pref_ct = {
     table => "contenttheftpos",
     fetch => "one",
     c_id => $pos_id,
  };

  my $ct = { c_id => 0 };
  $ct = $dbt->fetch_tablerecord($dbh_operator,$pref_ct);

  if($dbt->{copri_conf}->{sms_to_alarm}){
    my @alarm_phone = ("$dbt->{copri_conf}->{sms_to_alarm}");
    @alarm_phone = split(/\|/,$dbt->{copri_conf}->{sms_to_alarm}) if($dbt->{copri_conf}->{sms_to_alarm} =~ /\|/);
    foreach(@alarm_phone){
      $smstrans->sms_message($todo,"","",$_,"$oprefix$ct->{barcode}");
    }
  }else{
    $smstrans->sms_message($todo,"","",$phone,"$oprefix$ct->{barcode}");
  }

}#end


#SMS message fraud to me
if($todo eq "fraud_rental" && $pos_id){
  my $sms_to = "$dbt->{copri_conf}->{sms_to}";

  my $booking_pos = {
        table           => "contenttranspos",
        fetch           => "one",
        c_id         => "$pos_id",
  };

  my $booking = { c_id => 0 };
  $booking = $dbt->fetch_tablerecord($dbh,$booking_pos);

  $smstrans->sms_message($todo,"","",$sms_to,$booking->{ct_name});

}#end


