#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#with date range
#./src/scripts/statistik_userevaluation.pl shareedms-operator '2022-01-01' '2022-02-01'
#
#Without date defaults to duration of last 1 month up to now
#./src/scripts/statistik_userevaluation.pl shareedms-operator '' ''
#
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || exit 1;
}
use lib "/var/www/copri-bike/$syshost/src";

use strict;
#use warnings;
use feature qw(say);
use POSIX;
use CGI ':standard';
use Lib::Config;
use Scalar::Util qw(looks_like_number);
use DateTime;
use DateTime::Format::Pg;
use Text::CSV_XS;
use Mod::Libenz;
use Mod::DBtank;

use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my $lb = new Libenz;
my $dbt = new DBtank;
my %varenv = $cf->envonline();

my $dbh = $dbt->dbconnect();

my $start_itime = "";
my $end_itime = "";
my $filename = "";

#If NO date on command then now
my $dt1 = DateTime->now(time_zone  => 'Europe/Berlin');
if($ARGV[2] && $ARGV[2] =~ /(\d{4})-(\d{2})-(\d{2})/){
  $end_itime = DateTime->new(
    year       => $1,
    month      => $2,
    day        => $3,
    time_zone  => 'Europe/Berlin',
  );
}else{
  $end_itime = $dt1;
}

if($ARGV[1] && $ARGV[1] =~ /(\d{4})-(\d{2})-(\d{2})/){
  $start_itime = DateTime->new(
    year       => $1,
    month      => $2,
    day        => $3,
    time_zone  => 'Europe/Berlin',
  );
}else{
  $start_itime = $end_itime->clone->add(months => -1, end_of_month => 'preserve');
}

my $month = $start_itime->month;
$month = sprintf('%.2d',$month);
my $year = $start_itime->year;

my $month_days = DateTime->last_day_of_month(  
    year  => $year,
    month => $month,
);
my $days = $1 if($month_days =~  /\d{4}-\d{2}-(\d{2})/);

 #fetch contentadrpos
 my $pref_adr = {
        table           => "contentadrpos",
        fetch           => "all",
        keyfield        => "c_id",
	template_id	=> 602,#evaluation template_id
	txt10		=> "IN::('MAK','SX','CAD','LEC')",#operators
	start_mtime 	=> ">=::$start_itime",
	end_mtime 	=> "<=::$end_itime",
        };
 my $adr_rec = $dbt->fetch_tablerecord($dbh,$pref_adr);

my $csv_minianswer = Text::CSV_XS->new ({ binary => 1, sep_char => ";", eol => "\r\n" });
my $filename_minianswer = "Statistik-LastenradBayern-evaluation_$year-$month.csv";
open my $auser, ">", "$varenv{csv}/$filename_minianswer" or die "$filename_minianswer: $!\n";
my @minianswer = ("$year-$month ID","Zeitstempel","Operator","Antwort zu 1.","Antwort zu 2.","Antwort zu 3.","KM");
$csv_minianswer->print($auser, \@minianswer);#CSV header


if(1==1){

  foreach my $a_id (sort { $adr_rec->{$a}->{c_id} <=> $adr_rec->{$b}->{c_id} } keys(%$adr_rec)){
    #if($adr_rec->{$a_id}->{txt01} || $adr_rec->{$a_id}->{txt02} || $adr_rec->{$a_id}->{txt03}){
	my $mtime = $lb->time4de($adr_rec->{$a_id}->{mtime},"","");
        my @minianswer_line = ("$adr_rec->{$a_id}->{c_id}","$mtime","$adr_rec->{$a_id}->{txt10}","$adr_rec->{$a_id}->{txt01}","$adr_rec->{$a_id}->{txt02}","$adr_rec->{$a_id}->{txt03}","$adr_rec->{$a_id}->{int02}");
	print "\n";
	foreach (@minianswer_line){
	  print $_ . ";";
	}
	print "\n";
   	$csv_minianswer->print($auser, \@minianswer_line);#foreach one line

   #}
  }
}


