#!/usr/bin/perl 
#
#      
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
##forking payone request to generate an mail payone-link

use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || exit 1;
}

use lib "/var/www/copri-bike/$syshost/src";
use strict;
use warnings;
use POSIX;
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::DBtank;
use Mod::Payment;
use Data::Dumper;

my $cf = new Config;
my %varenv = $cf->envonline();
my $dbt = new DBtank;
my $pay = new Payment;
my $now_dt = strftime("%Y-%m-%d %H:%M:%S",localtime(time));

my $adrc_id = $ARGV[1] || "";
my $prepaid_amount = $ARGV[2] || "";
my $aowner = $ARGV[3] || "196";
my $dbh = "";

my $dbh = $dbt->dbconnect();
my $authref = {
   table           => "contentadr",
   fetch           => "one",
   template_id     => "202",
   c_id            => "$adrc_id",
};
my $users_sharee = { c_id => 0 };
$users_sharee = $dbt->fetch_record($dbh,$authref) if($adrc_id);

my $prepaidhash = {};
$prepaidhash = $pay->collect_prepaid($dbh,$users_sharee);
my $ret_json = $pay->generate_payonelink(\%varenv,$users_sharee,$prepaidhash,$prepaid_amount,$aowner);

