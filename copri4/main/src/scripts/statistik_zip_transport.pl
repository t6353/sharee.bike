#!/usr/bin/perl 
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#Just CSV attachment zipping
#

#with start_time and end_time
#./src/scripts/statistik_zip_transport.pl shareedms-operator '2022-01-01' '2022-02-01'
#
#defaults now()
#./src/scripts/statistik_zip_transport.pl shareedms-operator
#
use vars qw($syshost);

BEGIN {
  $syshost = $ARGV[0] || exit 1;
}

use lib "/var/www/copri-bike/$syshost/src";

my $start_itime = $ARGV[1] || "";
my $end_itime = $ARGV[2] || "";

use strict;
use warnings;
use utf8;
use POSIX;
use CGI ':standard';
use Lib::Config;
use Scalar::Util qw(looks_like_number);
use DateTime;
use DateTime::Format::Pg;
use Sys::Hostname;
my $hostname = hostname;

use Data::Dumper;

my $q = new CGI;
my $cf = new Config;
my %varenv = $cf->envonline();

my $dt1 = DateTime->now(time_zone  => 'Europe/Berlin');
if(!$end_itime){
  $end_itime = $dt1;
}

if(!$start_itime){
  $start_itime = $dt1->clone->add(months => -1, end_of_month => 'preserve');
}

my $month = $1 if($start_itime =~ /\d{4}-(\d{2})-\d{2}/);
$month = sprintf('%.2d',$month);
my $year = $1 if($start_itime =~ /(\d{4})-\d{2}-\d{2}/);
my $filename = "Statistik_$year-$month.zip";

#zip it
`cd $varenv{csv} && rm -f $filename && /usr/bin/zip $filename *$year-$month*.csv && sync`;

#`$varenv{basedir}/src/scripts/ftpSWKstatistik.pl $filename`;


