/*
SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (c) haveltec GmbH, TeilRad GmbH

Created by Bjoern Kinberger on 04.10.18.

import android.util.Log;
*/
import java.io.InputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

public class Ilockkeygen {
    //private static final String LOG_TAG = Ilockkeygen.class.getName();
    public static byte[] sha1Value;

    public static void generateKey(byte[] randomNum, byte[] internKey) {

	System.out.println("K_int = " + Arrays.toString(internKey));

        byte[] filler = {0, 0, 0, 0};
        Random random = new Random();

        for (int i = 0; i < 16; i++) {
            randomNum[i] = (byte) random.nextInt(255);
        }

	System.out.println("K_seed = " + Arrays.toString(randomNum));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            outputStream = new ByteArrayOutputStream();
            outputStream.write(randomNum);
            outputStream.write(internKey);
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte tempoKey[] = outputStream.toByteArray();
	System.out.println("K_temp = " + Arrays.toString(tempoKey));
/*
        Log.d(LOG_TAG, "onCreate: temp key " + Arrays.toString(tempoKey));
*/
        try {
            sha1Value = SHA1(tempoKey);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            outputStream = new ByteArrayOutputStream();
            outputStream.write(sha1Value);
            outputStream.write(filler);
            sha1Value = outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

	System.out.println("K_a = " + Arrays.toString(sha1Value));
/*
        Log.d(LOG_TAG, "onCreate: 1. " + Arrays.toString(sha1Value));
*/

        try {
            sha1Value = SHA1(sha1Value);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            outputStream = new ByteArrayOutputStream();
            outputStream.write(sha1Value);
            outputStream.write(filler);
            sha1Value = outputStream.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
	System.out.println("K_u = " + Arrays.toString(sha1Value));
/*
        Log.d(LOG_TAG, "onCreate: 2. " + Arrays.toString(sha1Value));
*/
    }

    private static byte[] SHA1(byte[] tempoKey) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(tempoKey);
        return md.digest();
    }
}

