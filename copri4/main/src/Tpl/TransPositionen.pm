package TransPositionen;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use Date::Calc::Object qw(:ALL);
use Scalar::Util qw(looks_like_number);
use Data::Dumper;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Mod::RPCshareeio;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $self = shift;
  my $varenv = shift;
  my $node_meta = shift;
  my $users_dms = shift;
  my $ctf = shift;
  my $ctadr = shift;
  my $ctt = shift;

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;
  my $pri = new Pricing;
  my $rpcs = new RPCshareeio;
  my %ib = $but->ibuttons();
  my $today = strftime "%d.%m.%Y",localtime;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $dbh = "";

  my $channel_map = $dbt->channel_map();
  my $ct_users = $dbt->users_map($dbh);
  my $tc=0; 
  my $txt20 = $R::txt20 || $ctt->{txt20} || "";#Leistungsdatum
  my $int05 = $R::int05 || $ctt->{int05} || "";#manuell
  my $rows = 0;
  my $spart_ct_name = $R::spart_ct_name || "";
  my $c_idpos = $R::c_idpos || $R::pos_id || 0;
  my $scol = "itime";

  my $vibuchen_mtime = "(nicht verfügbar, siehe Internas und log)";
  my @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int03=Menge (Miet - Gratis Zeit)","int02=Preis","int07=Rabatt","int04=Gesamt");
  my $tplf = $dbt->get_tpl($dbh,201);#Kunden-Faktura Firma
  my @tplf_order = split /,/,$tplf->{tpl_order};

  my $cttpos = { c_id => 0 };
  ($cttpos,$rows) = $dbt->collect_contentpos($dbh,"contenttrans",$ctt->{content_id});

  #if operator invoice with prepaid type exist, then check whether balance positive
  my $sum_prepaid = 0;
  if($ctadr->{ct_name} =~ /Prepaid-\d+/ && $varenv->{dbname} ne $dbt->{primary}->{sharee_primary}->{database}->{dbname}){
      my $shareeio_json = {
          request => "collect_prepaid_invoices",
          userID => "$ctadr->{c_id}",
      };

      my $prepaid_invoices = $rpcs->request_shareeio($varenv,$dbh,$ctadr,$shareeio_json);
      $sum_prepaid = $prepaid_invoices->{shareeio}->{users_prepaid_invoices}->{$ctadr->{c_id}}->{sum_prepaid_available};
  }

  my $station_all = {};
  my $pref_st = {
         table => "content",
         fetch  => "all",
         keyfield => "c_id",
         template_id => 225,
         };
  $station_all = $dbt->fetch_record($dbh,$pref_st);

  my @_stationvalxx = ("null:");
  foreach my $rid (sort { $station_all->{$a}->{int04} <=> $station_all->{$b}->{int04} } keys (%$station_all)){
     push (@_stationvalxx, "$station_all->{$rid}->{int04}:$station_all->{$rid}->{int04} - $station_all->{$rid}->{txt01}");
  }
  my @_rentalstate_valxx = ();
  foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{bike_state} })) {
     push @_rentalstate_valxx, "$s_key:$dbt->{copri_conf}->{bike_state}->{$s_key}" if($s_key =~ /1|2|3/);
  }
  my @_lock_valxx = ();
  foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{lock_state} })) {
     push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{lock_state}->{$s_key}";
  }

  foreach my $id (keys(%$cttpos)){
   if(ref($cttpos->{$id}) eq "HASH"){ 
    if($cttpos->{$id}->{int26}){
      @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int26=Einsparung","int03=Menge (Miet - Gratis Zeit)","int02=Preis","int07=Rabatt","int04=Gesamt");
    }
    #enhanced tariff
    #if($cttpos->{$id}->{int35}){
    #  @tpl_order = ("txt01=Beschreibung","ct_name=Nummer","date_time=timerange","int26=Einsparung","int35=Mietzeit Menge","int03=Artikel Menge","int02=Preis","int07=Rabatt","int04=Gesamt");
    #}
   }else{
      $lb->failure3("Der selekt einer Verkaufsposition ist fehlgeschlagen, errocode ($ctt->{content_id} | $ctt->{template_id} | $users_dms->{c_id4trans}). Bitte admin kontaktieren");
      exit;
   }
  }


print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     font-size:14px;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_select').autocomplete({
            source: '/ajax_json?table=content&template_id=224,229&catch_equal=0',
            minLength: 2,
	    select: function(event, ui) {
                    \$('#spart_ct_name').val(ui.item.spart_ct_name);
                    \$('#c_id').val(ui.item.c_id);
            }
       });
  });
  </script>
EOF
;


  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'});

  #Parts Header
  print $q->start_form(-name=>'spartform');

  print $q->hidden(-name=>'trans2edit', -value=>"transpos", -override=>'1'),"\n";
  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
  print $q->hidden(-id=>'c_id', -name=>"c_id", -override=>'1'),"\n";
  print $q->hidden(-id=>'spart_ct_name', -name=>"spart_ct_name", -override=>'1'),"\n";

  print $q->Tr(); 
  print $q->th($but->singlesubmit("select_part","*")),"\n";
  foreach (@tpl_order){
   my ($key,$val) = split /=/,$_;
   $tc++ if($val);
   if($key eq "ct_name"){
     print $q->th($q->textfield(-class=>'etxt',-style=>'height:19px;width:140px;font-size:1.3em;text-align:right;',-id=>"json_select",-name=>"json_select",-value=>"", -override=>'1',-size=>"25",-maxlength=>50, -placeholder=>'Nummer',-autofocus=>1),""),"\n";
   }elsif($key =~ /int|txt|time\d+/){
     print $q->th("$val"),"\n";
   }
  }

  print $q->end_form,"\n";

  print $q->start_form(-name=>'transposform'),"\n";

  my $sum_parts19=0;
  my $sumgeb_teil=0;
  my $sumgeb_bank=0;
  my $diff19 = 100 + 19;
  my $sum_umst19=0;
  my $i=0;
  my $accounting_start = "";
  my $accounting_end = "";
  my $xjournal = $dbt->get_node($dbh,$dbt->{shareedms_conf}->{xjournal});

  #foreach my $id (sort { $cttpos->{$b}->{$scol} <=> $cttpos->{$a}->{$scol} } keys(%$cttpos)){
  foreach my $id (sort { $cttpos->{$b}->{$scol} cmp $cttpos->{$a}->{$scol} } keys(%$cttpos)){

   my $gesamt = 0;
   my $pricing = {};
   my $counting = {};
   my $rental_feed = {};

   if($cttpos->{$id}->{int35} && $cttpos->{$id}->{start_time} && $cttpos->{$id}->{end_time}){
     $pri->count_freedrental($q,$varenv,$ctt->{int10},$cttpos->{$id},1);
     ($pricing,$counting) = $pri->counting_rental($varenv,$cttpos->{$id});

     $rental_feed = $pri->fetch_rentalfeed($varenv,$varenv,$cttpos->{$id},$counting);
     $sum_parts19 += $pricing->{total_price};
     $gesamt = $pri->round($pricing->{total_price});
     $gesamt = sprintf('%.2f', $gesamt);
   }else{
     ($gesamt,my $rabatt) = $pri->price2calc($cttpos->{$id});
     $sum_parts19 += $gesamt;
     if($cttpos->{$id}->{int16} && $cttpos->{$id}->{int16} == 2){
       $sumgeb_bank += $gesamt;
     }elsif($cttpos->{$id}->{int16} && $cttpos->{$id}->{int16} == 1){
       $sumgeb_teil += $gesamt;
     }
     $gesamt = $pri->round($gesamt);
     $gesamt = sprintf('%.2f', $gesamt);
   }

   my $set_style="";
   my $occupied_style = "";
   my $time_style = "";
   $occupied_style = "color:#ff1493" if($cttpos->{$id}->{int10} == 2 ||$cttpos->{$id}->{int10} == 3 || $cttpos->{$id}->{int10} == 6);

   #max. Rechnungspositionen
   if(1==1){
    $i++;
    if($ctt->{ct_name} =~ /^\d+$/){
      if($i==1){
    	$accounting_end = "$3.$2.$1" if($cttpos->{$id}->{itime} =~ /(\d+)\-(\d+)\-(\d+)/);
    	$accounting_end = "$3.$2.$1" if($cttpos->{$id}->{end_time} =~ /(\d+)\-(\d+)\-(\d+)/);
	$accounting_start = $accounting_end;
      }else{
    	$accounting_start = "$3.$2.$1" if($cttpos->{$id}->{itime} =~ /(\d+)\-(\d+)\-(\d+)/);
      }
    }

    #1. Spalte
    print $q->Tr(),"\n";
    print "<td class='tdtxt' nowrap>","\n";
    if(($c_idpos == $cttpos->{$id}->{c_id}) && ($R::trans2edit && $R::trans2edit =~ /transpos/)){
        print $q->hidden(-name=>'c_idpos', -value=>"$cttpos->{$id}->{c_id}", -override=>'1'),"\n";
        print $q->hidden(-name=>'cc_id', -value=>"$cttpos->{$id}->{cc_id}", -override=>'1'),"\n";
        print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
        print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
	print $but->singlesubmit2glyph("ct_trans","save_pos","$ib{save_pos}","background-color:white;padding:5px 5px 0 0;","")," ", 
	$but->singlesubmit2glyph("ct_trans","delete_pos","$ib{delete_pos}","background-color:white;padding:5px 0 0 5px;",""),"\n";
     }elsif(!$ctt->{close_time}){
        print $q->a({-class=>"editnav3",-href=>"/DMS/Faktura?trans2edit=transpos\&c_idpos=$cttpos->{$id}->{c_id}\&c_id4trans=$ctt->{content_id}\&tpl_id4trans=$ctt->{template_id}\&owner=$users_dms->{u_id}",-title=>"Datensatz bearbeiten"}, $q->span({-class=>"bi bi-file-earmark-text", -style=>'font-size:1.7em;'})),"\n";
    }
    print $q->a({-class=>"linknav3",-href=>"/DMS/$xjournal->{node_name}/?cttpos_id=$cttpos->{$id}->{c_id}",-title=>"Im $xjournal->{node_name} anzeigen"},"<br /><br />&rarr; ID $cttpos->{$id}->{c_id}"),"\n";
    print "</td>\n";

    #Tablecontent (parameter)
    foreach (@tpl_order){
      my ($key,$val,$inputsize) = split /=/,$_;     
      $cttpos->{$id}->{$key} = $q->unescapeHTML($cttpos->{$id}->{$key});
      $cttpos->{$id}->{$key} = $lb->newline($cttpos->{$id}->{$key},"",$R::trans2edit) if($R::trans2edit);
      my $ct_pos = "$cttpos->{$id}->{ct_name}";
      my $txtstyle = "text-align:left;min-width:130px;";
      my $isize = "30";
      $isize = $inputsize if($inputsize);
      if($key =~ /int\d+/){
       $txtstyle = "text-align:right;min-width:50px;";
       $isize = "5";
      }

      #edit position
      if(($c_idpos == $cttpos->{$id}->{c_id}) && ($R::trans2edit && $R::trans2edit =~ /transpos/)){
	if($key =~ /ct_name/){
   	 print $q->td({-class=>'element',-style=>"$set_style text-align:right;"}, $q->textfield(-class=>'etxt',-style=>"text-align:right;min-width:120px;",-name=>"ct_name",-default=>"$ct_pos", -override=>'1',-size=>10,-readonly=>'1')),"\n";
       }elsif($key eq "int26"){
            my $co2saving = "";
            if($cttpos->{$id}->{int26}){
              $co2saving = "Einsparung</br>";
              my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />" if($sprit_price !~ /-/);
              $cttpos->{$id}->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{$id}->{int26} KM";
            }
            print $q->td({-class=>'tdint'},"$co2saving"),"\n";

	}elsif($key =~ /int03/){
          if($cttpos->{$id}->{int35} && $pricing->{start_time} && $pricing->{end_time}){
            print $q->td({-class=>'tdint', -nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
          }else{
	    print $q->td({-colspan=>'1',-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>10)),"\n";
	  }
	}elsif($key =~ /int02/){
	  if($cttpos->{$id}->{int35} && $pricing->{start_time} && $pricing->{end_time}){
	    print "<td class='tdint' nowrap>\n";
	    foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
	      if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
		print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
	      }
	    }
	    print "</td>\n";
	  }else{
	    print $q->td({-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100)),"\n";
	  }
	}elsif($key =~ /int07/){
	  print $q->td({-class=>'element',-style=>"$txtstyle $set_style",-nowrap=>'1'},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100),$but->selector("int08","40px","$cttpos->{$id}->{int08}",("0:%","1:€"))),"\n";
	}elsif($key =~ /int04/){
         print $q->td({-class=>'tdint',-nowrap=>"1"},"$gesamt €"),"\n";
 	}elsif($key =~ /txt01/){
 	 if($cttpos->{$id}->{int09} && $cttpos->{$id}->{$key} !~ /Manuell/){
	   $cttpos->{$id}->{$key} .= "\nManuell bearbeitet\n";
	 }
         
	 my ($s_yy,$s_mo,$s_dd,$s_hh,$s_mi) = $lb->split_date($pricing->{start_time});
         my ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi) = $lb->split_date($pricing->{end_time});

	 print "<td class='tdtxt3' style='$txtstyle $set_style;$occupied_style;'>\n";
	 print $q->textarea(-class=>'autos',-style=>"border: 1px solid #ededed;",-name=>"$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-rows=>"2",-cols=>60, -autofocus=>1),"<br />\n";

	 #if Traiff Nr.
	 if($cttpos->{$id}->{int09}){

	   print $q->span({-style=>"$txtstyle $set_style"},"Mietzeit: ", 
	   $q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"start_date",-default=>"$s_dd.$s_mo.$s_yy",-size=>"8",-maxlength=>10),
	   $q->textfield(-class=>'etxt',-name=>"s_hh",-default=>"$s_hh",-size=>"1",-maxlength=>2),":",
	   $q->textfield(-class=>'etxt',-name=>"s_mi",-default=>"$s_mi",-size=>"1",-maxlength=>2)," &rarr; ",
	   $q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"end_date",-default=>"$e_dd.$e_mo.$e_yy",-size=>"8",-maxlength=>10),
	   $q->textfield(-class=>'etxt',-name=>"e_hh",-default=>"$e_hh",-size=>"1",-maxlength=>2),":",
	   $q->textfield(-class=>'etxt',-name=>"e_mi",-default=>"$e_mi",-size=>"1",-maxlength=>2)),"<br />\n";
        
   	   my $end_station = "null";
           $end_station = $cttpos->{$id}->{int04} if(looks_like_number($cttpos->{$id}->{int04}));
	   print $q->span({-style=>"$txtstyle $set_style"},"Station Start: $cttpos->{$id}->{int06}, End: ",$but->selector_class("int04","eselect","width:250px;","$end_station",@_stationvalxx)),"<br />\n";
	   #print $q->span({-style=>"$txtstyle $set_style"},"GPS: $cttpos->{$id}->{txt06}"),"<br />\n";
 
	   print $q->span({-style=>"$txtstyle $set_style"},"Rental State:", $but->selector_class("int10","eselect","",$cttpos->{$id}->{int10},@_rentalstate_valxx));
           print $q->span({-style=>"$txtstyle $set_style"},"Lock State:",$but->selector_class("int20","eselect","",$cttpos->{$id}->{int20},@_lock_valxx)),"<br />\n";
	   print $q->span({-style=>"$txtstyle $set_style"}, "Tarif: $cttpos->{$id}->{int09} $cttpos->{$id}->{txt04}"), "<br />\n";
	 }

	 print "<br /><b>Bemerkung (Mietjournal)</b> $cttpos->{$id}->{txt23}\n" if($cttpos->{$id}->{txt23});
	 print "</td>\n"; 
 	}elsif($key =~ /txt/){
	  print $q->td({-class=>'element',-style=>"$txtstyle $set_style"},$q->textfield(-class=>'etxt',-style=>"$txtstyle",-name=>"$key",-default=>"$cttpos->{$id}->{$key}", -override=>'1',-size=>"$isize",-maxlength=>100)),"\n";
	}elsif($key =~ /int/){
	  print $q->td({-class=>'tdint',-nowrap=>"1"},"&nbsp;"),"\n";
	}
      }
      #end edit position 
      #
      #start view position
      else{
        if($key =~ /ct_name/){
	  #print $q->td({-class=>'tdint',-style=>"min-width:60px;"},"$ct_pos");
	  my $stamm_style = "background-color:#98c13b;padding:2px;";
	  my $article = $cttpos->{$id}->{ct_name};
	  if($cttpos->{$id}->{int09}){
	    print $q->td({-class=>'tdint',-style=>"min-width:60px;padding-top:5px;"}, $q->a({-class=>"linknav3",-style=>"$stamm_style",-href=>"/DMS/Waren/?detail_search=1&s_barcode=$cttpos->{$id}->{barcode}",-title=>"Im Warenstamm"},"$article")),"\n";
	  }else{
	    print $q->td({-class=>'tdint'},"$article"),"\n";
	  }
       }elsif($key eq "int26"){
            my $co2saving = "";
            if($cttpos->{$id}->{int26}){
              $co2saving = "Einsparung</br>";
              my $co2diff = $pri->co2calc($cttpos->{$id});
	      #my $sprit_price = $pri->sprit2calc($cttpos->{$id});
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />";
              $cttpos->{$id}->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{$id}->{int26} KM";
            }
            print $q->td({-class=>'tdint'},"$co2saving"),"\n";

        }
	#yes, int03=Menge on parts, int35=unit_price1 on rental
	elsif($key =~ /int03/){
	  if($cttpos->{$id}->{int35} && $pricing->{start_time} && $pricing->{end_time}){
	    print $q->td({-class=>'tdint',-nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
  	  }else{
	    $cttpos->{$id}->{$key} =~ s/\./,/;
	    print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
	  }
	}elsif($key =~ /int02/){
	  if($cttpos->{$id}->{int35} && $pricing->{start_time} && $pricing->{end_time}){
	    print "<td class='tdint'>\n";
	    foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
	      if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
		print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
	      }
	    }
	    print "</td>\n";
	  }else{
	    $cttpos->{$id}->{$key} =~ s/\./,/;
	    print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key} €"),"\n";
	  }
	}elsif($key =~ /int04/){
	  $gesamt =~ s/\./,/;
	  print $q->td({-class=>'tdint',-nowrap=>1},"$gesamt €"),"\n";
        }elsif($key =~ /int07/){
	  if($cttpos->{$id}->{$key}){
  	    my $proz="";
	    $proz = "%" if($cttpos->{$id}->{$key} != 0);
	    $proz = "€" if($cttpos->{$id}->{$key} != 0 && $cttpos->{$id}->{int08} && $cttpos->{$id}->{int08} == 1);
	    $cttpos->{$id}->{$key} =~ s/\./,/;
	    print $q->td({-class=>'tdint',-nowrap=>"1"},"$cttpos->{$id}->{$key} $proz"),"\n";
	  }else{
	    print $q->td({-class=>'tdint',-nowrap=>"1"},""),"\n";
	  }
	}elsif($key =~ /txt01/){
	 $cttpos->{$id}->{$key} = $q->unescapeHTML("$cttpos->{$id}->{$key}");
         $cttpos->{$id}->{$key} = $lb->newline($cttpos->{$id}->{$key},"","");
          print "<td class='tdtxt3', style='$occupied_style;'>\n";
	  if($cttpos->{$id}->{barcode} && $cttpos->{$id}->{int09}){#bike with tariff-nr
                my $u_name = $cttpos->{$id}->{owner};
                my $u_name_end = $cttpos->{$id}->{owner_end};
                foreach my $ctu_id (keys (%$ct_users)){
                  if($channel_map->{$u_name}){
                        $u_name = $channel_map->{$u_name};
                  }elsif($cttpos->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
                        $u_name = $ct_users->{$ctu_id}->{txt01};
                  }
                  if($channel_map->{$u_name_end}){
                        $u_name_end = $channel_map->{$u_name_end};
                  }elsif($cttpos->{$id}->{owner_end} eq $ct_users->{$ctu_id}->{c_id}){
                        $u_name_end = $ct_users->{$ctu_id}->{txt01};
                  }
                 }
      	    if($cttpos->{$id}->{itime} =~ /(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2})/){
              print $q->span("$dbt->{copri_conf}->{bike_state}->{$cttpos->{$id}->{int10}} &rarr; $dbt->{copri_conf}->{lock_state}->{$cttpos->{$id}->{int20}} &rarr; $u_name / $u_name_end"),"\n";
	      my $pos_raw = "";
	      if($users_dms->{u_id} && $users_dms->{u_id} =~ /1842/){
               my $pos_details = "";
	       foreach my $did (sort keys (%{$pricing->{rentalog}})){
	        $pos_details .= $did . " = " . $pricing->{rentalog}->{$did} . "</br>" if($pricing->{rentalog}->{$did});
	       }
	       #$pos_details = Dumper($pricing->{rentalog});
	       $pos_raw = $q->div({-class=>"popup",-onclick=>"toggle_box('$id')"},"$cttpos->{$id}->{c_id}", $q->span({-class=>"popuptext",-id=>"$id"},"$pos_details"));
	       print "(raw $pos_raw)<br />\n";
    	      }
	    }
	    print "<br />\n";
	  }
	  $cttpos->{$id}->{txt01} =~ s/fixed/\<span style='color:red'\>fixed\<\/span\>/;
	  $cttpos->{$id}->{txt01} =~ s/defect/\<span style='color:red'\>defect\<\/span\>/;
	  print $q->span("$cttpos->{$id}->{$key}"),"<br />\n";
	  if($pricing->{start_time} && $pricing->{end_time}){
    	    my $start_time = $lb->time4de($pricing->{start_time},"1");
    	    my $end_time = $lb->time4de($pricing->{end_time},"1");
	    print $q->span({-style=>"$time_style"}, "Mietzeit: $start_time &rarr; $end_time"),"<br />\n";
	  }

	  if($cttpos->{$id}->{int06} || $cttpos->{$id}->{int04}){
	    print $q->span("Start/End Station: $cttpos->{$id}->{int06} / $cttpos->{$id}->{int04}"),"<br />\n";
	  }
	  if($cttpos->{$id}->{int09}){
	    my $tariff = "";
	    $tariff = "Tarif: $cttpos->{$id}->{int09} $cttpos->{$id}->{txt04}" if($cttpos->{$id}->{txt04});
	    print $q->span("$tariff"),"<br />\n";
	  }

	  print "<br /><b>Bemerkung (Mietjournal)</b> $cttpos->{$id}->{txt23}\n" if($cttpos->{$id}->{txt23});
	  print "</td>\n";
	}elsif($key =~ /txt/){
          print $q->td({-class=>'tdtxt'},"$cttpos->{$id}->{$key}"),"\n";
	}elsif($key =~ /int/){
          print $q->td({-class=>'tdint',-nowrap=>"1"},"&nbsp;"),"\n";
	}
      }#end view position

     }
    }
   }#foreach end

   print $q->end_form,"\n";#en transposform

   print $q->start_form(-name=>'set_stateform'),"\n";

   if($sum_parts19 && $sum_parts19 != 0){
     $sum_umst19 = $sum_parts19 / $diff19 * 19;
     $sum_umst19 = $pri->round($sum_umst19);
   }
   my $sum_netto19 = $sum_parts19 - $sum_umst19;
   $sum_netto19 = sprintf('%.2f', $sum_netto19);
   $sum_netto19 =~ s/\./,/;

   my $sum_paid = $sum_parts19;
   $sum_paid = $pri->round($sum_paid);
   my $sum_preauth = $sum_paid || 0;
   $sum_paid = sprintf('%.2f', $sum_paid);
   my $sum_paid_view = $sum_paid;
   $sum_paid_view =~ s/\./,/;

   $sum_parts19 = sprintf('%.2f', $sum_parts19);
   $sum_umst19 = sprintf('%.2f', $sum_umst19);
   $sum_umst19 =~ s/\./,/;

   my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
	if($ctt->{state}){
         $ctt->{state} =~ s/\(.*//;#delete like (payone)
         if($des =~ /$ctt->{state}/){
          if($sum_parts19 < 0){
            $payment_text = "$ctf->{txt58}";
          }else{
            $payment_text = "$ctf->{$key}";
          }
         }
	}
      }
    }

   my $cs = $tc - 3;
   print "<tr>\n";
   print "<td colspan='$cs'><div  style='font-size:0.81em;padding:0.3em 0em;border:0px;'>$payment_text</div></td>\n";

   print "<td style='font-size:1em;' colspan='3'>\n";
   print $q->start_table({-class=>'list',-style=>'border-top:1px;border-style:solid;border-color:black;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
   print $q->Tr("\n");
   print $q->td("&nbsp;");

   print $q->Tr("\n");
   print $q->td({-class=>'tdint'},"Nettobetrag:");
   print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto19 €");

   print $q->Tr("\n");
   print $q->td({-class=>'tdint',-nowrap=>"1"},"19% UmSt auf $sum_netto19 €:");
   print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");

   print $q->Tr("\n");
   print $q->td({-class=>'tdsum'},"<b>Summe:</b>");
   print $q->td({-class=>'tdint',-nowrap=>"1"},"<b>$sum_paid_view €<b/>");
   print $q->hidden(-name=>'sum_paid', -override=>'1',-value=>"$sum_paid");
   print $q->hidden(-name=>'sumgeb_teil', -override=>'1',-value=>"$sumgeb_teil") if($sumgeb_teil);
   print $q->hidden(-name=>'sumgeb_bank', -override=>'1',-value=>"$sumgeb_bank") if($sumgeb_bank);
   #print $q->hidden(-name=>'sum_prepaid', -override=>'1',-value=>"$sum_prepaid");

   print $q->end_table;
   print "</td><td>&nbsp;</td>";
   print "</tr>";
   print $q->end_table;

   print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{u_id}"),"\n";
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
   print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
   print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
   print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
 

   if($users_dms->{int03} == 2){
    #only if user is also a primary DMS user with invoice rw
    print $q->hidden(-name=>'printer_id', -value=>"PDF", -override=>'1');
    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");

    if($users_dms_primary->{u_id}){
     my $kind_of_payment = "";#old
     my @_paymentstate = ("");#old
     my @_payment_valxx = ("");#new
     my $setdefault_payment = "";#new
     my $p_hash = $dbt->{shareedms_conf}->{payment_state2};
     foreach my $s_key (sort keys (%{ $p_hash })) {
      if($s_key <= 7 || $s_key >= 10){
        push @_paymentstate, "$p_hash->{$s_key}";
        push @_payment_valxx, "$s_key:($s_key) $p_hash->{$s_key}";
      }
      if($ctt->{state} || $ctt->{int04}){
	$kind_of_payment = $ctt->{state} if($ctt->{state}); 
	$setdefault_payment = $ctt->{int04} if($ctt->{int04});
      }
      elsif($ctt->{txt00} eq "Storno"){
        $kind_of_payment = $p_hash->{6};
	$setdefault_payment = 6;
      }
      elsif($ctt->{int06} && $ctt->{int06} >= 3 || $ctt->{ct_name} =~ /\d-\d/){
        #default to Überw.
        $kind_of_payment = $p_hash->{4};
	$setdefault_payment = 4;
      }
      elsif($ctadr->{int03}){
	if($ctadr->{ct_name} =~ /(PO|TM)-\d+/){
          $kind_of_payment = $p_hash->{1};
	  $setdefault_payment = 1;
	}
	if($ctadr->{ct_name} =~ /MA-\d+/){
          $kind_of_payment = $p_hash->{10};
	  $setdefault_payment = 10;
	}
       	if(length($ctadr->{ct_name}) >= 19){
          $kind_of_payment = $p_hash->{2};
	  $setdefault_payment = 2;
	}
       	if($ctadr->{ct_name} =~ /Prepaid-\d+/){
          $kind_of_payment = $p_hash->{3};
	  $setdefault_payment = 3;
	}
      }
     }
     #only prepaid on primary
     if($varenv->{dbname} eq $dbt->{primary}->{sharee_primary}->{database}->{dbname}){
       @_paymentstate = ("Prepaid");
       @_payment_valxx = ("3:(3) Prepaid");
       $setdefault_payment = 3;
     }
     if($ctt->{int04} && $ctt->{int01}){
     	$ctt->{int01} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int01} ne $sum_paid);
  	$vibuchen_mtime =  $lb->time4de($ctt->{pay_time},1) if($ctt->{pay_time}); 
        my $opos = "";
        $opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
	if($ctt->{int16} && ($ctt->{int16} == 1 || $ctt->{int16} == 2)){
          print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Summe $ctt->{int01} € gebucht per \"$p_hash->{$ctt->{int04}}\" | Payone Saldo $ctt->{int16} € | $vibuchen_mtime "),"\n";
	}else{
	  my $fibutext = "";
	  $fibutext .= "<span style='color:silver;'>(Bank Gebühr $ctt->{int07})</span>" if($ctt->{int07});
	  $fibutext .= "<span style='color:silver;'>(TeilRad Gebühr $ctt->{int08})</span>" if($ctt->{int08});
	  #$fibutext .= "<span style='color:silver;'>(Prepaid $ctt->{int09})</span>" if($ctt->{int09});
          print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Summe $ctt->{int01} € $fibutext gebucht per \"$p_hash->{$ctt->{int04}}\" | $vibuchen_mtime "),"\n";
	}
     }else{
        print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:silver;'>Summe ist nicht gebucht!</span>"),"\n";
     }

     if($ctadr->{ct_name} =~ /Prepaid-\d+/ && $varenv->{dbname} ne $dbt->{primary}->{sharee_primary}->{database}->{dbname} && (!$ctt->{int04} || $ctt->{int14})){
	my $presaldo = $sum_prepaid - $sum_paid;
	$presaldo = sprintf('%.2f', $presaldo);
	my $precolor = "red";
	$precolor = "green" if($presaldo >= 0);
        print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:$precolor;'>Es können max. $sum_prepaid € per Prepaid abgebucht werden</span>"),"\n";
	#$sum_paid = $sum_prepaid if($sum_paid > $sum_prepaid);#It can only paid max prepaid sum
     }

     if(!$ctt->{close_time} && $ctt->{template_id} != 219){
       my $send_invoice_checkbox = 1;
       $send_invoice_checkbox = 0 if($ctt->{txt30} || $ctt->{ct_name} =~ /\d-\d/);
       print $q->div({-class=>'element6',-style=>'float:right;background-color:silver;'}, $q->b("$ctt->{txt00}"),
       ": PDF drucken ",$but->checkbox("1","print_pdf","1","PDF drucken",""),
       " – eMail senden ",$but->checkbox("1","send_invoice","$send_invoice_checkbox","eMail $ctt->{txt00}",""),
       " – Zahlungsart/Status ",
       #$but->selector("state","250px",$kind_of_payment,@_paymentstate),
       $but->selector_class("int04","","",$setdefault_payment,@_payment_valxx),
       $but->singlesubmit1("set_state","buchen")),
	"\n";
  
      if($ctt->{txt00} ne "Storno" && $ctt->{ct_name} =~ /\d+-\d/){
       my @s_valxx = ("");
       my $s_hash = {};
       $s_hash = $dbt->{shareedms_conf}->{warning_state};
       foreach my $s_key (sort keys (%{ $s_hash })) {
        push @s_valxx, "$s_key:($s_key) $s_hash->{$s_key}";
       }
       my $warn_time = $ctt->{warn_time} || "";
       $warn_time = $lb->time4de($warn_time,0) if($warn_time);

       if($ctt->{ct_name} =~ /\d+-\d/){
        undef @s_valxx[1];
        undef @s_valxx[2];
       }
       print $q->div({-class=>'element6',-style=>'float:right;background-color:silver;'},"Mahnstufe",$but->selector_class("int06","","",$ctt->{int06},@s_valxx),
    "Fälligkeit",$q->textfield(-id=>'datepicker3',-class=>'etxt',-name=>"warn_time",-default=>"$warn_time",-override=>1,-size=>"8",-maxlength=>10)),"\n";
       print $q->hidden(-name=>'int14', -override=>'1', -value=>"1"),"\n";#set OPOS
      }
     }
    }
   }
  print $q->end_form;
  #end set_stateform

  my $praefix = "$ctt->{txt00}-$varenv->{praefix}";
  print $q->div({-style=>"clear:both;height:0.1px;"},""),"\n";
  if($ctt->{txt30}){
    $ctt->{txt30} = $lb->newline($ctt->{txt30},"","");
    print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "$ctt->{txt30}"),"\n";
  }elsif( -f "$dbt->{copri_conf}->{basedir}/$varenv->{syshost}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf" ){
     print $q->start_form(),"\n";
     print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
     print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
     print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
     print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
     #print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "eMail wurde nicht versandt! $ctt->{txt00} eMail ", $but->singlesubmit1("send_invoice_again","eMail nochmals senden","")),"\n" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
     print $q->end_form;
  }
 
  if( -f "$varenv->{basedir}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf"){
    print $q->div({-style=>'padding:0.5em;font-size:0.91em;width:98%;text-align:right;'}, "PDF download: ", $q->a({-href=>"$varenv->{wwwhost}/FileOut?file=$praefix-$ctt->{ct_name}.pdf&sessionid=$coo", -target=>"_blank" , -type=>'application/octet-stream', -style=>'text-decoration:underline;'}, "$praefix-$ctt->{ct_name}.pdf")),"\n";
  }

  #copri-Ticket-mailing, eMail send by select cms_message_key
  if("$varenv->{praefix}-$ctt->{ct_name}" =~ /\d+/){
     my $email_invoice_cms = $dbt->fetch_cms($dbh,{ lang => 'de', ct_name => 'email-invoice%' });
     my @_valxx = ("email CMS-Text select");
     foreach my $rid (sort { $email_invoice_cms->{$a}->{ct_name} cmp $email_invoice_cms->{$b}->{ct_name} } keys (%{$email_invoice_cms})){
            push (@_valxx, "$email_invoice_cms->{$rid}->{ct_name}:$email_invoice_cms->{$rid}->{ct_name}");
     }

     print $q->start_form(),"\n";
     print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
     print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";
     print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
     print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
     print $q->hidden(-name=>'sum_paid', -override=>'1',-value=>"$sum_paid");

     print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "Copri-Ticket-mailing: ", $but->selector_class("cms_message_key","eselect","width:250px;","",@_valxx), "mit PDF", $but->checkbox("1","print_pdf","1","mit PDF",""), $but->singlesubmit1("send_invoice_cms","eMail senden","")),"\n";
     print $q->end_form;
  }

  if(!$int05 && $accounting_start && $accounting_end){
    my $update_ctt = {
        table   => "contenttrans",
        mtime   => "no_time",
        c_id    => $ctt->{content_id},
    };
    $dbt->update_one($dbh,$update_ctt,"txt20='$accounting_start - $accounting_end'");
  }
  return;
}
1;
