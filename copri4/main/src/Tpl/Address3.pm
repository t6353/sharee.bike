package Address3;
#
##
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Tpl::TransPositionen;
use Tpl::TransInvoices;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $varenv = shift;
  my $node_meta = shift;
  my $users_dms = shift;

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $dbt = new DBtank;
  my $but = new Buttons;
  my $transp = new TransPositionen;
  my $trin = new TransInvoices;
  my $lang = "de";
  my %ib = $but->ibuttons();
  my $dbh = "";

  #selects Kunden-Faktura
  my $ctf = { c_id => 0 };
  my $pref_cu = {
    table => "contentuser",
    fetch => "one",
    c_id  => $dbt->{shareedms_conf}->{parent_id},
  };
  $ctf = $dbt->fetch_tablerecord($dbh,$pref_cu) if($pref_cu->{c_id});

  my $ctt = { c_id => 0 };
  my $pref = {
        table           => "contenttrans",
        fetch           => "one",
	template_id 	=> "IN::(208,209,218,219)",
	c_id		=> $users_dms->{c_id4trans},
  };
  $ctt = $dbt->fetch_record($dbh,$pref) if($pref->{c_id});
  my $praefix = "$ctt->{txt00}-$varenv->{praefix}";
  my $warn_time = $ctt->{warn_time} || "";
  $warn_time = $lb->time4de($warn_time,0) if($warn_time);
  my $set_main_id = $dbt->{shareedms_conf}->{invoice};
  $set_main_id = $ctt->{main_id} if($ctt->{main_id} > "300000");

  my $ctadr = { c_id => 0 };
  my $c_idadr = $ctt->{int10} || "";#c_id orig from contentadr
  my $pref_adr = {
        table           => "contentadr",
        fetch           => "one",
        template_id 	=> 202,
        c_id            => $c_idadr,
  };

  #selects Operator-Faktura
  if($node_meta->{tpl_id} == 208){
    $pref_cu->{c_id} = 2;
    $ctadr = $dbt->fetch_tablerecord($dbh,$pref_cu) if($pref_cu->{c_id});
  }
  #selects end-customer
  else{
    $ctadr = $dbt->fetch_record($dbh,$pref_adr) if($pref_adr->{c_id});
  }

  if($R::trans2edit eq "client" && looks_like_number($R::c_idadr)){
        $pref_adr->{c_id} = $R::c_idadr;
    	$ctadr = $dbt->fetch_record($dbh,$pref_adr) if($pref_adr->{c_id});
	$c_idadr = $ctadr->{c_id};
	$ctt->{txt01} = $ctadr->{txt01};
	$ctt->{txt03} = $ctadr->{txt03};
	$ctt->{txt06} = $ctadr->{txt06};
	$ctt->{txt07} = $ctadr->{txt07};
	$ctt->{txt08} = $ctadr->{txt08};
	$ctt->{txt10} = $ctadr->{txt10};
	$ctt->{txt11} = $ctadr->{txt11};
  }

print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_selectadr').autocomplete({
            source: '/ajax_json?table=contentadr&template_id=202&catch_equal=0',
            minLength: 2,
            select: function(event, ui) {
                    \$('#vorname_name').val(ui.item.vorname_name);
                    \$('#c_idadr').val(ui.item.c_id);
            }
       });
     

  });
  </script>
EOF
;
 
  print "\n<div style='position:absolute;text-align:left;background-color:white;width:100%;'>\n";
  if(!$ctt->{c_id}){
    $lb->failure3("Das Formular wurde gelöscht bzw. ist nicht vorhanden");   
  }
  
  #######Verkauf Header
  print $q->start_form(),"\n";
  print $q->hidden(-name=>'printer_id', -override=>'1', -value=>"PDF");
  print "\n<div style='background-color:#a7a18f;height:25px;text-align:left;width:100%;' nowrap=1>\n";
    #submit Header

    my $invoice_time = "";
    if($ctt->{invoice_time}){#since 2020-03-16 will be set on Printpreview
      $invoice_time = $lb->time4de($ctt->{invoice_time},1);
    }else{
      $invoice_time = $lb->time4de($ctt->{mtime},1);
    }

    my $channel_map = $dbt->channel_map();
    my $buchen_users = { txt01 => "" };
    if($channel_map->{$ctt->{owner}}){
      $buchen_users = { txt01 => "$channel_map->{$ctt->{owner}}" };
    }else{
      $pref_adr->{c_id} = $ctt->{owner};
      $buchen_users = $dbt->fetch_record($dbh,$pref_adr) if($pref_adr->{c_id});
    }

    my $journalhead = "";
    $journalhead = "<br /><span style='color:silver;font-size:0.81em;'>Journal</span>" if($ctt->{template_id} == 209);
    print $q->hidden(-name=>'set_main_id', -override=>'1', -value=>"$set_main_id");
    print $q->span({-style=>'margin:0em 0.5em 0 0.5em;padding:0.2em 1em;background-color:white;border:solid thin gray;position:absolute;left:3px;'}, "$ctt->{txt00} $varenv->{praefix}-$ctt->{ct_name} $journalhead");
    
    print "<span style='margin:0 0.2em;position:absolute;left:280px;'>\n";
    #print $q->b({-style=>'padding:0 1em;'}, "\# $ctt->{ct_name}"), "\n";
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} eq "Storno"){
      print $but->singlesubmit3("ct_trans","set_workflow2invoice","","",""),"\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300008"), "\n";
    }
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} =~ /Rechnung|Mahnung/ && $ctt->{int10} != 2){#only for client invoice
      print $but->singlesubmit3("ct_trans","set_workflow2storno","","",""), "\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300009"), "\n";
    }
    if($ctt->{ct_name} =~ /\d+/ && $ctt->{txt00} eq "Abrechnung"){
      print $but->singlesubmit3("ct_trans","set_accounting2invoice","","",""), "\n";
      print $q->hidden(-name=>'set_main_id4workflow', -override=>'1', -value=>"300008"), "\n";
    }

    print "</span>\n";

    if(!$journalhead && (!looks_like_number($ctt->{int01}) || $ctt->{ct_name} =~ /-/)){
      print $q->span({-style=>'margin:0 0.2em;position:absolute;right:3px;'}, $but->singlesubmit7("ct_trans","remove_chk4rel","$ib{remove_chk4rel}","")),"\n";
      print $q->hidden(-name=>'c_id', -override=>'1', -value=>"$ctt->{content_id}");
      print $q->hidden(-name=>'template_id', -override=>'1', -value=>"$ctt->{template_id}");
      print $q->hidden(-name=>'main_id', -override=>'1', -value=>"$ctt->{main_id}");
      print $q->hidden(-name=>'rel_id', -override=>'1', -value=>"$ctt->{rel_id}");
    }
    if(looks_like_number($ctt->{int01}) && ! -f "$varenv->{basedir}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf"){
       print $q->span({-style=>'margin:0 0.2em;position:absolute;right:3px;'}, $but->singlesubmit1("print_pdfview","Print PDF","")),"\n";
    } 
    print $q->span({-style=>'margin:0 0.2em;padding:0.3em;font-size:0.81em;position:absolute;right:100px;'},$q->a({-class=>"ebutton3",-target=>'_blank', -href=>"$dbt->{shareedms_conf}->{copri_wiki}"}, "copri wiki"), "$buchen_users->{txt01} / $invoice_time "),"\n";
   print "</div>\n"; 

  print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{owner}");
  print $q->hidden(-name=>'c_idadr', -override=>'1', -value=>"$c_idadr");
  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset");
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit");
  print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids");
  print $q->hidden(-name=>'ct_name4workflow', -override=>1, -value=>"$ctt->{ct_name}");
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}");
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
  print $q->hidden(-name=>'close_time', -override=>'1', -value=>"$ctt->{close_time}");

  print $q->end_form,"\n";
  print $q->div({-style=>'position:fixed;bottom:2%;right:1%;z-index:10;font-size:13px;'}," (c_id: $ctt->{c_id} | rel_id: $ctt->{rel_id} | tpl_id: $ctt->{tpl_id})"),"\n" if($users_dms->{u_id} eq $varenv->{superu_id});
  ##########
 
  #Relationen
  if($ctt->{barcode}){
   my $pref_rel = {
    table     => "contenttrans",
    fetch     => "all",
    keyfield  => "c_id",
    template_id => "IN::(209,218)",
    barcode   => $ctt->{barcode},
   };
   my $ctt_rel = "";
   $ctt_rel = $dbt->fetch_record($dbh,$pref_rel);

   if(ref($ctt_rel) eq "HASH"){
    my $top = 30;
    foreach my $id (sort { $ctt_rel->{$b}->{c_id} <=> $ctt_rel->{$a}->{c_id} } keys (%$ctt_rel)){
	my $toppx = $top . "px";
	my $rel_color = "silver";
	$rel_color = "#f7ae37" if($ctt->{c_id} == $ctt_rel->{$id}->{c_id});
	my $journal = "";
	$journal = "<span style='color:silver;background-color:white;'>Journal </span>" if($ctt_rel->{$id}->{template_id} == 209);
        my $rel_opos = "";
        $rel_opos = "<span class='bi bi-record2' style='color:Maroon;'></span>" if($ctt_rel->{$id}->{int14});
        $rel_opos = "<span class='bi bi-record2' style='color:Olive;'></span>" if($ctt_rel->{$id}->{int04} == 7);
        print $q->div({-style=>"position:absolute;top:$toppx;right:10px;font-size:0.91em;background-color:$rel_color"}, "$journal ", $q->a({-class=>"linknav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Faktura/$ctt_rel->{$id}->{node_name}?ct_trans=open\&c_id4trans=$ctt_rel->{$id}->{c_id}\&tpl_id4trans=$ctt_rel->{$id}->{template_id}\&owner=$users_dms->{u_id}",-title=>"Faktura Terminal öffnen"},"[ $rel_opos $ctt_rel->{$id}->{txt00} $varenv->{praefix}-$ctt_rel->{$id}->{ct_name} ]")),"\n";
	$top += 22;
    }
   }
  }


 

  #Start form addressdata
  print $q->start_form(-name=>'addressform'),"\n";
  #Big table
  print $q->start_table({-border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  print "<tr><td width='50%' style='font-size:1em;'>\n"; 
  print $q->start_table({-class=>'list', -height=>'10em',-border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
  $ctt->{txt01} = $q->unescapeHTML("$ctt->{txt01}");
  my $int05 = "";
  $int05 = "(manuell)" if($ctt->{int05});

  if($users_dms->{u_id} && $R::trans2edit eq "client"){
    print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}"),"\n";
    print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
    print $q->hidden(-name=>'rel_id', -override=>'1', -value=>"$ctt->{rel_id}"),"\n";
    print $q->hidden(-name=>'int10', -override=>'1', -value=>"$c_idadr"),"\n";
    print $q->hidden(-name=>'txt10', -override=>'1', -value=>"$ctt->{txt10}"),"\n";
    print $q->hidden(-name=>'txt11', -override=>'1', -value=>"$ctt->{txt11}"),"\n";

    print $q->Tr(),"\n";  
    print "<td class='tdescr4' nowrap>";
    print $but->singlesubmit7("ct_trans","save_adr","$ib{save_adr}","","","autofocus"),"\n";
    #print $q->span($q->a({-class=>"ebutton3",-href=>'javascript:history.back()'}, " back "));
    print "</td>\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'},"Kunden ID $c_idadr"),"\n"; 
    $ctt->{txt01} = $lb->newline($ctt->{txt01},"","1");
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"Vorname Name<br />Zusatz"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'},$q->textarea(-class=>'autos',-style=>'border:1px solid #ededed;background-color: #ededed;', -name=>'txt01', -default=>"$ctt->{txt01}", -rows=>1, -columns=>38)),"\n";
    print $q->Tr(),"\n";   
    print $q->td({-class=>'tdescr4'},"Straße Nr."),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2',-nowrap=>1}, $q->textfield(-class=>'etxt',-name=>'txt03', -default=>"$ctt->{txt03}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"PLZ Ort"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt06', -default=>"$ctt->{txt06}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n"; 
    print $q->td({-class=>'tdescr4'},"eMail"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt08', -default=>"$ctt->{txt08}", -size=>'34', maxlength=>'45')),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Telefon"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt07', -default=>"$ctt->{txt07}", -size=>'34', maxlength=>'40')),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Leistungsdatum"),"\n";
    print $q->td({-class=>'tdval4',-colspan=>'2'}, $q->textfield(-class=>'etxt',-name=>'txt20', -default=>"$ctt->{txt20}", -size=>'34', maxlength=>'60'),$but->checkbox("1","int05","$ctt->{int05}"),"manuell"),"\n";
    print $q->hidden(-name=>"int05",-override=>1,-value=>"");

  }elsif($users_dms->{u_id}){
    print $q->Tr(),"\n";  
    if(!$ctt->{close_time} && (!$ctt->{int10} || $ctt->{int10} != 2)){
      print "<td class='tdescr4' style='width:8em;' nowrap>"; 
      print $but->singlesubmit2glyph("trans2edit","client","Kunden bearbeiten","background-color:white;"),"\n";
      print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}"),"\n";
      print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
      #from json_selectadr
      print $q->hidden(-id=>'c_idadr', -name=>"c_idadr", -override=>'1'),"\n";
      print $q->hidden(-id=>'vorname_name', -name=>"vorname_name", -override=>'1'),"\n";
      print $q->textfield(-style=>'border:1px solid silver;vertical-align: top;',-id=>"json_selectadr",-name=>"json_selectadr", -value=>""),"\n";
      print "</td>\n";
    }else{
      print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
    }

     #print $q->td({-class=>'tdval4'},"$ctt->{txt02}"),"\n";
     if($c_idadr && $ctadr->{rel_id}){
      my $vde = "";
      $vde = " Vde $ctadr->{int12}" if($ctadr->{int12});
      print $q->td({-class=>'tdval4',-colspan=>'2'},$q->span({-style=>"background-color:#dcd77f;"},$q->a({-class=>"linknav",-href=>"/DMS/Kunden?node2edit=editpart\&mode=manager\&rel_id=$ctadr->{rel_id}\&tpl_id=202",-title=>"Kunden Stammdaten öffnen"}," Kunden ID $c_idadr ")), $q->span({-style=>"color:red;padding-left:10px;"}," $vde")),"\n"; 
     }
     $ctt->{txt01} = $lb->newline($ctt->{txt01},"","");
     print $q->Tr(),"\n"; 
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt01}"),"\n"; 
     print $q->Tr(),"\n";  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt03}"),"\n"; 
     print $q->Tr(),"\n";  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt06}"),"\n"; 
     print $q->Tr(),"\n";  
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt08}"),"\n"; 
     print $q->Tr(),"\n";
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt07}"),"\n";
     print $q->Tr(),"\n";
     print $q->td({-class=>'tdescr4'},"&nbsp;"),"\n";
     print $q->td({-class=>'tdval4',-colspan=>2},"$ctt->{txt20} $int05"),"\n";
  }
  print $q->end_table;
  print "</td>\n";

  print "<td width='50%' style='font-size:1em;'>\n"; 
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

  #if operator accounting/invoice
  if($node_meta->{tpl_id} == 208 || $ctt->{int10} == 2){
    my $tplop = $dbt->get_tpl($dbh,"196");#Operator-Faktura
    my @tplop_order = split /,/,$tplop->{tpl_order};
    foreach(@tplop_order){
     my ($key,$val,$size,$unit) = split /=/,$_;
     if($key =~ /int(\d+)/){
	#take fee values used by creating operator accounting invoice
	my $count_key = $1 + 20;#cu.int to ctt.int
	my $ctt_key = "int" . $count_key;
	$ctt->{$ctt_key} =~ s/\./,/;
       	print $q->Tr(),"\n";
       	print $q->td({-class=>'tdescr4', -style=>'width:50%;'},"$val"),"\n";
	if($ctt->{$ctt_key}){
	  $ctt->{$ctt_key} =~ s/\./,/;
       	  print $q->td({-class=>'tdval4'},"$ctt->{$ctt_key} $unit"),"\n";
	}else{
       	  print $q->td({-class=>'tdval4'},""),"\n";
	}
     }
    }
  }else{
    my $pay_state = "";
    #if txid && balance==0 && sequence==0 && invoice-sum!=receivable
    $pay_state = " | receivable: $ctt->{int19}. Einzug im PMI prüfen!" if($ctt->{txt16} && $ctt->{int16} == 0 && $ctt->{int18} == 0 && looks_like_number($ctt->{int19}) && $ctt->{int01} != $ctt->{int19});
    my $pay_sequence = "";
    $pay_sequence = " | sequencenr: $ctt->{int18}" if($ctt->{int18});
     
    my $kind_of_payment = "fehlt";
    my $p_hash = $dbt->{copri_conf}->{payment_type};
    foreach my $s_key (sort keys (%{ $p_hash })) {
      if($ctadr->{int03}){
	$kind_of_payment = "Kunde " . $p_hash->{$s_key} if($ctadr->{int03} == $s_key);
      }
      if($ctt->{int03}){
	$kind_of_payment .= " | Faktura " . $p_hash->{$s_key} if($ctt->{int03} == $s_key);
      }
    }
    my $payteaser = "";
    if($ctt->{int03} && $ctt->{int03} != 3 && $ctadr->{txt28}){
       $payteaser = substr($ctadr->{txt28},0,50) . " ...";
    }
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Zahlungsart"),"\n";
    print $q->td({-class=>'tdval4'},"$kind_of_payment"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone log Kunde"),"\n";
    print $q->td({-class=>'tdval4'},"$payteaser"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone TXID"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{txt16}"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone Saldo"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{int16} $pay_sequence $pay_state"),"\n";
    print $q->Tr(),"\n";
    print $q->td({-class=>'tdescr4'},"Payone Referenz"),"\n";
    print $q->td({-class=>'tdval4'},"$ctt->{txt25}"),"\n";

  }
  print $q->end_table;
  print "</td></tr>\n"; 
  print $q->end_form,"\n";
  #end form addressdata

  ###Content #Edit Parts
  print "<tr><td colspan='5' style='font-size:1em;'>\n"; 

   #operator invoices else position accounting
   if($ctt->{template_id} == 208 || $ctt->{int10} == 2){
     $trin->tpl($varenv,$node_meta,$users_dms,$ctf,$ctadr,$ctt);
   }else{
     $transp->tpl($varenv,$node_meta,$users_dms,$ctf,$ctadr,$ctt);
   }

  print "</td></tr>\n"; 
  ###end Edit Parts 

 #Text&State Save
 if($ctt->{template_id} != 219){

  my $tplf = $dbt->get_tpl($dbh,201);#tpl Kunden-Faktura
  my @tplf_order = split /,/,$tplf->{tpl_order};

  print $q->start_form(),"\n";
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{c_id}");

  #Text save area
  #Internas und Bearbeitungstatus
  print "<tr><td colspan='1'>\n"; 
  print $q->start_table({-border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #1.
  print $q->Tr(),"\n"; 
  print $q->td({-class=>'tdval5',-style=>'height:40px;',-colspan=>"1"},$q->span({-style=>'font-weight:bold;'},"Internas und Bearbeitungstatus")),"\n";
  if($ctt->{txt00} ne "Storno"){
    my @_orderstate = split(/\|/,$dbt->{shareedms_conf}->{order_state});
    print $q->td({-class=>'tdval5',-colspan=>1},$but->selector("txt22","180px",$ctt->{txt22},@_orderstate)),"\n";
  }else{
    print $q->td({-class=>'tdval5',-colspan=>"1"},"&nbsp;"),"\n";
  }

  #2.
  print $q->Tr(),"\n";  
  if($ctt->{txt00} eq "Storno" || $ctt->{ct_name} =~ /\d+-\d/){
    print $q->td({-class=>'tdval5',-colspan=>"2"},"&nbsp;"),"\n";
  }else{
    my @s_valxx = ("");
    my $s_hash = {};
    $s_hash = $dbt->{shareedms_conf}->{warning_state};
    foreach my $s_key (sort keys (%{ $s_hash })) {
      push @s_valxx, "$s_key:$s_key";
    }

    if($ctt->{ct_name} !~ /\d+-\d/){
	undef @s_valxx[3];
	undef @s_valxx[4];
	undef @s_valxx[5];
    }
    print $q->td({-class=>'tdval5',-colspan=>"1"},"Zahlungserinnerung",$but->selector_class("int06","","",$ctt->{int06},@s_valxx)),"\n";
    print $q->td({-class=>'tdval5',-colspan=>1},"Fälligkeit",$q->textfield(-id=>'datepicker3',-class=>'etxt',-name=>"warn_time",-default=>"$warn_time",-override=>1,-size=>"8",-maxlength=>10)),"\n";
  }

  #3.
  $ctt->{txt23} = $q->unescapeHTML("$ctt->{txt23}") if($ctt->{txt23});
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdval5',-colspan=>'2'},$q->textarea(-class=>'etxt',-name=>'txt23', -default=>"$ctt->{txt23}", -rows=>8, -columns=>55)),"\n";

  print $q->end_table,"\n";
  print "</td>\n"; 
  #------------------------------

  #PDF Formular Text
  print "<td colspan='1'>\n"; 
  print $q->start_table({-border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #1.
  print $q->Tr(),"\n";  
  print $q->td({-class=>'tdval5',-style=>'height:40px;',-colspan=>"2"},$q->span({-style=>'font-weight:bold;'},"PDF Formular Text "),$q->span({-style=>'color:silver;font-weight:normal;'}," ( für den Kunden sichtbar )")),"\n";

  #2.
  print $q->Tr(),"\n";
  print $q->td({-class=>'tdval5',-style=>'',-colspan=>"2"},"&nbsp;"),"\n";
  #print $q->td({-class=>'tdval5',-style=>'height:45px;',-colspan=>"2"},"Zahlungseingang",$but->rev_checkbox("4","int14","$ctt->{int14}")),"\n";

  #3.
  $ctt->{txt12} = $q->unescapeHTML($ctt->{txt12}) || "";
  print $q->Tr(),"\n";  
  print $q->td({-class=>'tdval5',-colspan=>'2'},$q->textarea(-class=>'etxt',-name=>'txt12', -default=>"$ctt->{txt12}", -rows=>8, -columns=>55)),"\n";

  print $q->end_table,"\n";
  print "</td></tr>\n"; 

  print $q->Tr(),"\n";
  if($ctt->{close_time}){
    print $q->td({-class=>'tdval5',-style=>'width:90%;',-colspan=>2}, $but->singlesubmit7("ct_trans","save_text_internas","$ib{save_text}","","","autofocus")),"\n";
  }else{
    print $q->td({-class=>'tdval5',-style=>'width:90%;',-colspan=>2}, $but->singlesubmit7("ct_trans","save_text","$ib{save_text}","","","autofocus")),"\n";
  }

  print $q->end_form,"\n";
 }#end Text & State Save

 print $q->end_table,"\n";
 ###end Big
 
 #payone-return log
 $ctt->{txt28} = $lb->newline($ctt->{txt28},"","");
 print $q->div({-id=>'Oline'},""),"\n";
 print $q->div({-style=>'padding: 0 15px 20px 15px;font-size:0.91em;'},"<b>payment-response log</b><br />$ctt->{txt28}"),"\n";

 print "</div>\n";
 return;
}
1;
