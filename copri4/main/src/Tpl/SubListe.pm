package SubListe;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
#uncomment for perl -cw src/Tpl/Liste3.pm
#use lib qw(/var/www/copri4/shareedms-primary/src);
#
use strict;
use warnings;
use POSIX;
use CGI;
use URI::Escape;
use Encode;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Storable;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $feedb = shift || {};

  my $q = new CGI;
  my @keywords = $q->param;
  my $keycount = scalar(@keywords);
  my $referer = $q->referer();
  $referer =~ s/\?.*//;
  my $full_url = $q->url( -path_info => 1 );
  my $time = time;
  my $now_db = strftime("%d.%m.%Y %H:%M:%S",localtime(time));
  my $cf = new Config;
  my $lb = new Libenz;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;

  my $keylength = scalar(@keywords);
  my %varenv = $cf->envonline();
  my $path = $q->path_info();
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $dbh = "";

  my %ib = $but->ibuttons();
  my $opdir_dms = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$varenv{dbname}}->{dir_dms}" || "";   
  my $s_owner_id = "";
  my $offset = $R::offset || "0";
  my $limit = $R::limit || 250;

  my $searchref = {
        time => $time,
	scol => "mtime",
        offset => $offset,
        limit => $limit,
        export => "",
	opos => "",
  };

  my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
  my $users_dms_primary = { u_id => 0 };
  $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");

  my $ct_users_primary = {};
  my @_users_primary = ();
  $ct_users_primary = $dbt->users_map($dbh_primary);
  @_users_primary = join(",", keys %{$ct_users_primary});
  my $channel_map = $dbt->channel_map();
  #my $ct_users = $dbt->users_map($dbh,{ u_id => "NOT IN::(@_users_primary)" });
  my $ct_users = $dbt->users_map($dbh,{ });


  my @_users = ("");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($R::s_owner && $channel_map->{$id} eq $R::s_owner){
        $searchref->{owner} = $id;
	#$s_u_name = $channel_map->{$id};
     }
  }

   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i) || ($ct_users->{$ctu_id}->{c_id} eq $searchref->{owner})){
        $searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
	#$s_u_name = $ct_users->{$ctu_id}->{txt01};
     }
   }

  #backward | forward
    if($R::go eq "backward_list"){
        $searchref->{offset} -= $searchref->{limit} if($searchref->{offset} >= $searchref->{limit});
    }elsif($R::go eq "forward_list"){
        $searchref->{offset} += $searchref->{limit};
    }

  my $date = "";
  my $start_chck=0;
  my $end_chck=0;
  my $last_year;
  if($R::s_start_mtime){
    ($date,$start_chck) = $lb->checkdate($R::s_start_mtime) if($R::s_start_mtime !~ "%"); 
    $feedb->{message} .= ">>> Datum Eingabefehler: $date <<<" if($start_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
    $last_year = $c_yy if("$c_yy" eq "2011");
  }
  if($R::s_end_mtime){
    ($date,$end_chck) = $lb->checkdate($R::s_end_mtime) if($R::s_end_mtime !~ "%");
    $feedb->{message} .= ">>> Datum Eingabefehler: $date <<<" if($end_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
  }

  my $main_ids = $node_meta->{parent_id};
  my $tpl_ids = $node_meta->{tpl_id};
  my $table = "content";
  $searchref->{table_pos} = "contentpos";
  if($node_meta->{tpl_id} == 194 || $node_meta->{tpl_id} == 199){
    $table = "contentuser";
    $searchref->{table} = "$table";
    $searchref->{table_pos} = "";
    $searchref->{template_id_pos} = "";
    $searchref->{c_id} = ">::400" if($node_meta->{tpl_id} == 199);
    if($node_meta->{main_id} < 400000){
      $main_ids = $node_meta->{main_id};
    }else{
      $searchref->{ct_name} = "$node_meta->{node_name}";
    }
    $tpl_ids = $node_meta->{tpl_id};
  }
  elsif($node_meta->{tpl_id} > 400 && $node_meta->{tpl_id} <= 499){
    $table = "content";
    $searchref->{table_pos} = "contentpos";
    $searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $tpl_ids = "205";
    $tpl_ids = "225" if($node_meta->{tpl_id} == 499);
    $node_meta->{tpl_order} .= ",int04=Station on insert,txt10=Redistribution,txt11=smartlock_charge,txt12=bike_charge" if($node_meta->{tpl_id} != 499);
  }
  elsif($node_meta->{tpl_id} > 600 && $node_meta->{tpl_id} < 700){
    $table = "contentadr";
    $searchref->{table_pos} = "contentadrpos";
    $searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $main_ids = $node_meta->{main_id};
    $tpl_ids = $node_meta->{tpl_id};
  }
  elsif($node_meta->{tpl_id} == 198){
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

    #only if user is also a primary DMS user with invoice rw
    if(!$users_dms_primary->{u_id} || $users_dms_primary->{int03} != 2){	
      $node_meta->{tpl_order} =~ s/int02=[\w\s\+\&=]+,//;
      $node_meta->{tpl_order} =~ s/int03=[\w\s\+\&=]+,//;
    }

    $table = "contentadr";
    $searchref->{table_pos} = "users";
    #$searchref->{template_id_pos} = "$node_meta->{tpl_id}";
    $tpl_ids = 202;
print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       
      \$('#json_selectadr').autocomplete({
            source: '/ajax_json?table=contentadr&template_id=202&catch_equal=0',
            minLength: 2,
            select: function(event, ui) {
                    \$('#vorname_name').val(ui.item.vorname_name);
                    \$('#c_idadr').val(ui.item.c_id);
            }
       });
     

  });
  </script>
EOF
;

  }
  my @tpl_order = split /,/,$node_meta->{tpl_order};
  my $rows = 0;
  #table columne check for col_sort
  if($users_dms->{"col_sort_$searchref->{table_pos}"}){
    $searchref->{scol} = $users_dms->{"col_sort_$searchref->{table_pos}"};
  }
  if(!$searchref->{table_pos} && $users_dms->{"col_sort_$searchref->{table}"}){
    $searchref->{scol} = $users_dms->{"col_sort_$searchref->{table}"};
  }
  if($node_meta->{template_id} == 198){
    $searchref->{scol} = "u_id";
    if(!$users_dms_primary->{u_id} && scalar(@_users_primary) > 0){
      $searchref->{u_id} = "NOT IN::(@_users_primary)";
    }
  }
  $users_dms->{"sort_updown_$searchref->{table_pos}"} = "down" if(!$users_dms->{"sort_updown_$searchref->{table_pos}"});

  #print Dumper($node_meta);
  my $hashfile = "$varenv{logdir}/$users_dms->{u_id}-$varenv{dbname}-$searchref->{table_pos}-searchhash";
  $hashfile = "$varenv{logdir}/$users_dms->{u_id}-$varenv{dbname}-$searchref->{table}-searchhash" if(!$searchref->{table_pos});

  if($node_meta->{tpl_id} =~ /194/ && !$R::detail_search && -f $hashfile && ($R::node2edit || $R::base_edit)){
      $searchref = {};
      $searchref = retrieve($hashfile);
      $feedb->{message} .= "Es wurden die letzten Suchparameter oder Filter geladen" if(keys %$searchref > 7);
  }

  my $ct4rel = {};
  if(!$start_chck && !$end_chck && $main_ids && $tpl_ids){

        #collect search keys. 
        foreach my $postkey (@keywords){
          foreach(@tpl_order){
            my ($key,$val,$size) = split /=/,$_;
            if($postkey =~ /s_$key|s_start_$key|s_end_$key/){
                my $val = $q->param($postkey);
                $postkey =~ s/^s_//; 
                $searchref->{$postkey} = $val; 
            }
          }
        }

	my $ck4ex = "";
	#only if permission read
	if(($node_meta->{ct_table} eq "users" && $users_dms->{int07} >= 1) || ($node_meta->{ct_table} eq "contentadrpos" && $users_dms->{int01} >= 1) || ($node_meta->{ct_table} eq "contentpos" && $users_dms->{int01} >= 1) || ($node_meta->{ct_table} eq "contentuser" && $users_dms->{int08} >= 1)){
  	  
	  #trying to save hashref
  	  if($node_meta->{tpl_id} =~ /194/ && ref($searchref) eq "HASH" && !$R::node2edit && !$R::base_edit){
	   if($keylength > 0){
      	     store $searchref, $hashfile;
	   }else{
	     unlink $hashfile;
	   }
  	  }

          $ct4rel = $dbt->search_content($dbh,$searchref,$node_meta,$users_dms,"$main_ids","$tpl_ids","",$ck4ex);
	}else{
	  $feedb->{message} = "failure::Abbruch. Keine Zugriffsberechtigung";
	}
  }

  print "<div id='Content4sublist'>\n";
  my $header_style = "";
  print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path");
  print $q->div({-style=>'background-color:silver;height:10px;'},""),"\n";

  print $q->start_form(-name=>'searchform'),"\n";
  print $q->hidden(-name=>'offset', -value=>"$searchref->{offset}"),"\n";
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}"),"\n";
  print $q->hidden(-name=>'mode', -value=>"manager"),"\n";
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}"),"\n";
  print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}"),"\n";
  print $q->hidden(-name=>'ct_table', -value=>"$node_meta->{ct_table}"),"\n";

  my $hstyle = "width:20px;background-color:$node_meta->{bg_color};border-right: solid thin gray;border-bottom: solid thin gray;";
  print $q->start_table({ -style=>'clear:both;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #new_edit and search (disabled for Statistik, Service-Config and DMS-Account view)
  if($node_meta->{tpl_id} !~ /195|198|199/){
   print $q->Tr(),"\n";
   print $q->td({-style=>"background-color:silver;$hstyle"},$but->singlesubmit1("detail_search","search","","")),"\n";

   my $a_color = $node_meta->{bg_color} || "yellow";
   if($node_meta->{tpl_id} =~ /194/){
     my $edit="base_edit";
     my $new_key="new_contentcms";#Text-CMS
     print $q->td({-style=>"width:25px;background-color:$a_color;$hstyle"}, $but->singlesubmit2glyph("$edit","$new_key","$ib{$new_key}","background-color:$a_color;"));
   }else{
     print $q->td({-style=>"$hstyle"},"&nbsp;"),"\n";
   }

   #1. Search-fields
   foreach(@tpl_order){
     my ($key,$val,$size) = split /=/,$_;
     $size = 15 if($size =~ /area/);
     $size = 10 if($size =~ /time/);
     $size = 2 if($size =~ /checkbox/);

     my $s_val = "";
     $s_val = $searchref->{$key} if($searchref->{$key});
     #print "$key=$searchref->{$key}|";
     if($key =~ /node|txt|int|ct_name|_id|barcode|sort|public/){
      if($key =~ /_id|barcode|int04/){
      	print $q->td({-class=>"search_line", -style=>'width:100px;'},$q->textfield(-class=>'stxt',-name=>"s_$key",-size=>"4",-default=>"$s_val",-maxlength=>40, -autofocus=>1)),"\n";
      }else{
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-size=>"$size",-default=>"$s_val",-maxlength=>40)),"\n";
      }
     }elsif($key =~ /owner/){
        print $q->td({-class=>'search_line', -style=>'width:10px;'},$but->selector("s_$key","","$s_val",@_users)),"\n";
     }
     my $s_mtime; my $e_mtime;
     if($key eq "mtime"){
      $s_mtime = $searchref->{start_mtime};
      $e_mtime = $searchref->{end_mtime};
     }
     if($key eq "date_time"){
      $s_mtime = $searchref->{start_date_time};
      $e_mtime = $searchref->{end_date_time};
     }

     print $q->td({-nowrap=>1,-class=>"search_line_date"},$q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"s_start_$key",-default=>"$s_mtime",-size=>"$size",-maxlength=>10),"-",$q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"s_end_$key",-default=>"$e_mtime",-size=>"$size", -maxlength=>10)),"\n" if($key =~ /time/);
   }#end Search-fields
  }

  #2. Tableheader
  print $q->Tr();
  my $i=0;
    if($node_meta->{tpl_id} == 198){
      my $edit="base_edit";
      my $new_key="new_dmsusers";

      print "<th style='$hstyle;width:250px;'>\n"; 
      print $but->singlesubmit2glyph("$edit","$new_key","$ib{$new_key}","background-color:$node_meta->{bg_color};"),"\n";
      print $q->hidden(-id=>'c_idadr', -name=>"c_idadr", -override=>'1'),"\n";
      print $q->hidden(-id=>'vorname_name', -name=>"vorname_name", -override=>'1'),"\n";
      print $q->textfield(-style=>'border:1px solid grey;height:25px;width:80%;',-id=>"json_selectadr",-name=>"json_selectadr", -placeholder=>'Vorname Name (aus Kunden)', -value=>""),"\n";

      print "</th>\n";
    }elsif($node_meta->{tpl_id} !~ /195|199/){
      #print $q->th({-style=>""},"&nbsp;"),"\n";
      my $sort_up = "up";
      my $sort_down = "down";
      $sort_up = "<b>$sort_up</b>" if($users_dms->{"sort_updown_$searchref->{table_pos}"} eq "up");
      $sort_down = "<b>$sort_down</b>" if($users_dms->{"sort_updown_$searchref->{table_pos}"} eq "down");
      print $q->th($q->a({-class=>"sortnav",-href=>"?sort_updown=up\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?sort_updown=down\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>'Absteigend sortieren'},"$sort_down")),"\n";
      print $q->th({-style=>""},"&nbsp;"),"\n";
    }

  my $j = 0;
  foreach (@tpl_order){
   my ($key,$val,$size,$interval) = split /=/,$_;
   $val .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});

   my $divstyle = "";
   if($node_meta->{tpl_id} == 199 && $val =~ /int\d+/){
     $j++;
     my $val_head = $val;
     $val = "Wartung $j";
     $val .= " ($val_head)" if($users_dms->{u_id} eq $varenv{superu_id});
     $divstyle = "width:100px;white-space:nowrap;";
   }
   if($size =~ /checkbox/){
     	$size = 2;
	$divstyle = "width:20px;white-space:nowrap;overflow:hidden;";
   }
   my $sort_title="| $val";

   if($node_meta->{tpl_id} !~ /198|199/){
     $val = "<b>$val</b>" if($key eq $users_dms->{"col_sort_$searchref->{table_pos}"});
     $val = "<b>$val</b>" if($key eq $users_dms->{"col_sort_$searchref->{table}"} && !$searchref->{table_pos});
     print $q->th({-style=>'padding:5px 0'},$q->div({-style=>"$divstyle"},$q->a({-class=>"sortnav",-href=>"?col_sort=$key\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"$val"},"$val"))),"\n" if($key ne "u_id");
   }else{
     print $q->th({-style=>'padding:5px 0'},$q->div({-style=>"$divstyle"},"$val")),"\n" if($key ne "u_id");
   }
  }#end Tableheader

  my $nr=0;
  my $tdclass = "tdtxt";
  my $tdstyle = "text-align:left;";

  #Statistic file view
  if($node_meta->{tpl_id} == 195){
   if( -d "$opdir_dms/csv" && $users_dms->{int08} >= 1){
    my @pdfs = $lb->read_dirfiles("$opdir_dms/csv","\.ZIP","file","");
    @pdfs = reverse(@pdfs);
    foreach (@tpl_order){
      my ($key,$val,$size) = split /=/,$_;
      if($key eq "pdf01"){
       	  foreach(@pdfs){
	 	print $q->Tr(),"\n";
        	print $q->td({-class=>"$tdclass"},$q->a({-class=>'linknav3',-href=>"$varenv{wwwhost}/FileOut?file=$_&sessionid=$coo", -target=>'_default', -title=>'Download',-type=>'application/octet-stream'},$q->span({-class=>"bi bi-download", -style=>'font-size:1.2em;'}), "$_")),"\n";
       	  }
	}
      }
    }else{
	$feedb->{message} = "failure::Abbruch. Keine Zugriffsberechtigung";
    }
  }
  #BIG LOOP loop content table
  else{
   my $scol = $searchref->{scol};
   foreach my $id (sort {
    if($users_dms->{"sort_updown_$searchref->{table_pos}"} eq "down"){
        if ($scol =~ /barcode|int|_id/) {
                $ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol}
        }else{
                $ct4rel->{$b}->{$scol} cmp $ct4rel->{$a}->{$scol}
        }
    }else{
        if ($scol =~ /barcode|int|_id/) {
                $ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol}
        }else{
                $ct4rel->{$a}->{$scol} cmp $ct4rel->{$b}->{$scol}
        }
    }
  } keys(%$ct4rel)){

   my $set_style = "";
    $nr++;
    #Tablecontent (parameter)
    print $q->Tr(),"\n";

    if($node_meta->{tpl_id} != 198 && $node_meta->{tpl_id} != 199){
      print $q->td({-class=>'tdtxt',-style=>"$set_style"},""),"\n";
      print $q->td({-class=>'tdint',-style=>"$set_style"},""),"\n";
    }
    my $k=0;
    foreach (@tpl_order){
      my ($key,$val,$size) = split /=/,$_;
      $size = 15 if($size =~ /area/);
      $size = 2 if($size =~ /checkbox/);
      $tdclass = "tdtxt";
      $tdstyle = "text-align:left;";
      if($size =~ /\w\+\w/){
	$size = 5;
      }elsif($key =~ /barcode|c_id|ct_name|int|state|sort|public/){
        $tdclass = "tdint";
        $tdstyle = "text-align:right;max-width:8em;$size px;";
      }
      $ct4rel->{$id}->{$key} = $lb->time4de($ct4rel->{$id}->{$key},"1") if($key =~ /time|change/);
      $ct4rel->{$id}->{$key} = $q->unescapeHTML($ct4rel->{$id}->{$key}) if($ct4rel->{$id}->{$key});
      $ct4rel->{$id}->{$key} = $lb->newline($ct4rel->{$id}->{$key},"","") if($ct4rel->{$id}->{$key});
      if($key eq "owner"){
   	my $u_name = $ct4rel->{$id}->{owner};
     	foreach my $ctu_id (keys (%$ct_users)){
          if($channel_map->{$u_name}){
           $u_name = $channel_map->{$u_name};
          }elsif($ct4rel->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
           $u_name = $ct_users->{$ctu_id}->{txt01};
          }
       }
        #$u_name = $ct_users->{$ct4rel->{$id}->{$key}}->{txt01} || $ct4rel->{$id}->{$key};
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},"$u_name"),"\n";

      }elsif($key eq "ct_name" && $node_meta->{ct_table} eq "contentuser" && $node_meta->{tpl_id} =~ /194|199/){
	my $spec_style = "";
	$spec_style = "min-width:140px;";
	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $spec_style"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&c_id=$ct4rel->{$id}->{c_id}",-title=>"edit"},"$ct4rel->{$id}->{$key}")),"\n";

      }elsif($key eq "barcode" && $node_meta->{ct_table} eq "contentadrpos"){
	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},$q->a({-class=>"linknav3",-href=>"$varenv{wwwhost}/DMS/Waren/?detail_search=1\&s_barcode=$ct4rel->{$id}->{barcode}",-title=>"zum Rad "},"$ct4rel->{$id}->{barcode}")),"\n";
 
      }elsif($key eq "u_id" && $node_meta->{ct_table} eq "users"){
	my $adref = {
          table           => "contentadr",
          fetch           => "one",
          template_id     => "202",
          c_id          => "$ct4rel->{$id}->{$key}",
        };
        my $ctadr = $dbt->fetch_record($dbh,$adref);

	print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style width:300px;"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&u_id=$ct4rel->{$id}->{u_id}",-title=>"edit"},"$ctadr->{txt01} ($ct4rel->{$id}->{$key})")),"\n";
      }elsif($key eq "txt08" && $node_meta->{ct_table} eq "contentadrpos"){
	my $subject = "sharee.bike feedback";
	my $body = "Hallo $ct4rel->{$id}->{txt01},\%0A\%0Avielen Dank für Ihre Nachricht \"$ct4rel->{$id}->{txt02}\"";
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},$q->a({-class=>"editnav",-href=>"mailto:$ct4rel->{$id}->{$key}?subject=$subject&body=$body"},$q->span({-class=>"bi bi-envelope"}," $ct4rel->{$id}->{$key}"))),"\n";
      }elsif($key =~ /int0[1-8]/ && $node_meta->{ct_table} eq "users"){
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style",-nowrap=>1},"$dbt->{copri_conf}->{permission}->{$ct4rel->{$id}->{$key}}"),"\n";
      }elsif($key =~ /int09/ && $node_meta->{ct_table} eq "users"){
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style",-nowrap=>1},"$dbt->{copri_conf}->{access}->{$ct4rel->{$id}->{$key}}"),"\n";
      }elsif($key =~ /txt\d+/ && $node_meta->{tpl_id} == 199){
	my $tpl_desc = $ct4rel->{$id}->{$key};
	$tpl_desc =~ s/^int\d+=//;
	$tpl_desc =~ s/=checkbox=(\d+)/\<br \/\>$1 Tage Intervall/;
	$tpl_desc =~ s/=1/\<br \/\>Typ weich/;
	$tpl_desc =~ s/=2/\<br \/\>Typ hart/;
	my $spec_style = "";
	$spec_style = "min-width:140px;" if($tpl_desc);
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $spec_style"},"$tpl_desc"),"\n";
      }else{
        print $q->td({-class=>"$tdclass",-style=>"$tdstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
      }
     }
    }
   }

   print $q->end_table;

   my $offset_nr = $searchref->{offset} + $nr;
   print $q->div({-style=>'float:left;padding:0.5em;'},"Zeile: $searchref->{offset} - $offset_nr");

   #backward | forward
   print "<div style='padding:0.5em;'>\n";
   print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$searchref->{offset};limit=$searchref->{limit}",-title=>'backward'}," &larr; ") if($searchref->{offset} >= $searchref->{limit});
   print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$searchref->{offset};limit=$searchref->{limit}",-title=>'forward'}," &rarr; ") if($nr >= $limit-10); #if($rows > $limit && $nr > 0);
   print "</div>\n";

  print $q->end_form,"\n";
  print "</div>\n";
  my $debug = "(ct_table: $node_meta->{ct_table} | main_id: $node_meta->{main_id}  | template_id: $node_meta->{template_id} | table_pos: $searchref->{table_pos})";
  print $q->div({-style=>'position:fixed;bottom:1%;font-size:13px;'},"$debug"),"\n" if($users_dms->{u_id} eq $varenv{superu_id});
  
  return $feedb;
}

1;
