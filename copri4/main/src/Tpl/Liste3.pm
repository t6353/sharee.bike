package Liste3;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
#uncomment for perl -cw src/Tpl/Liste3.pm
#use lib qw(/var/www/copri4/main/src);
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use URI::Escape;
use Encode;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Prelib;
use Mod::DBtank;
use Date::Calc qw(:all);
use Scalar::Util qw(looks_like_number);
use Storable;
use Mod::APIfunc;
use Mod::Pricing;
use Mod::Shareework;
use Mod::APIsigclient;
use Mod::Basework;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $feedb = shift || {};

  my $q = new CGI;
  my @keywords = $q->param;
  my $cf = new Config;
  my $lb = new Libenz;
  my $pl = new Prelib;
  my $but = new Buttons;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $pri = new Pricing;
  my $tk = new Shareework;
  my $si = new APIsigclient;
  my $bw = new Basework;

  my %varenv = $cf->envonline();
  my $keycount = scalar(@keywords);
  my $referer = $q->referer();
  $referer =~ s/\?.*//;
  my $full_url = $q->url( -path_info => 1 );
  my $path = $q->path_info();
  my $now_db = strftime("%d.%m.%Y %H:%M:%S",localtime(time));
  my $dbh = "";
  my $debug = 0;
  my $coo = $q->cookie(-name=>'domcookie') || "";
  my $opdir_dms = "$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$varenv{dbname}}->{dir_dms}" || "";
  my $xjournal = $dbt->get_node($dbh,$dbt->{shareedms_conf}->{xjournal});
  my $p_hash = $dbt->{shareedms_conf}->{payment_state2};

  $path =~ s/\/login|\/user|\/manager|\/admin|\/$//;
  my $user_agent = $q->user_agent();
  my %ib = $but->ibuttons();
  my $main_id = $node_meta->{main_id};
  my $tpl_id = $node_meta->{template_id};
  my $time = time;
  my $today = strftime("%d.%m.%Y",localtime(time));
  my $now_date = strftime("%Y-%m-%d",localtime(time));
  my @months = ("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
  my $hh=0;
  my $mm=0;
  my $day = strftime "%d", localtime;
  my $mon = strftime "%m", localtime;
  my $year = strftime "%Y", localtime;
  ($year,$mon,$day,$hh,$mm) = $lb->split_date($users_dms->{cal_start}) if($users_dms->{cal_start});

  my $start_date_time = $R::start_date_time || "";
  $start_date_time = "01.$mon.$year" if(!$start_date_time);
  #$start_date_time = "01.01.2024";#sharee_tr workaround for SubOp yearly Abrechnung

  my $end_date_time = $R::end_date_time || "";
  if(!$end_date_time){
    #my $days4month = Days_in_Month($year,$mon);
    #$end_date_time = "$days4month.$mon.$year";
    my ($nyear,$nmon,$nday) = Add_Delta_YMD($year,$mon,1, 0,1,0);
    $end_date_time = "$nday.$nmon.$nyear";
  }
  my $c_date; my $start_chck=0;my $end_chck=0;
  if($start_date_time){
    ($start_date_time,$start_chck) = $lb->checkdate($start_date_time) if($start_date_time ne "%");
    $feedb->{message} .= ">>> Datum Eingabefehler: $start_date_time <<<" if($start_chck);
  }
  if($end_date_time){
    ($end_date_time,$end_chck) = $lb->checkdate($end_date_time) if($end_date_time ne "%");
    $feedb->{message} .= ">>> Datum Eingabefehler: $end_date_time <<<" if($end_chck);
  }

  my $search_startdt = "\'$start_date_time\'";
  my $search_enddt = "\'$end_date_time\'";

  my $cttpos = {};
  my $cttpos_trans = {};
  my $cttpos_theft = {};
  if(($node_meta->{ct_table} eq "content") && ($node_meta->{tpl_id} == 205) && (!$start_chck && !$end_chck)){
    my $cttpos1 = $dbt->collect_postime($dbh,"contenttranspos","$search_startdt","$search_enddt","$start_date_time","$end_date_time");
    foreach my $key (keys %$cttpos1){
          $cttpos_trans->{"trans_" . $key} = $cttpos1->{$key};
    }
    #my $cttpos2 = $dbt->collect_postime($dbh,"contenttheftpos","$search_startdt","$search_enddt","$start_date_time","$end_date_time");
    #foreach my $key (keys %$cttpos2){
    #      $cttpos_theft->{"theft_" . $key} = $cttpos2->{$key};
    #}
  }

  #$cttpos = { %$cttpos_trans, %$cttpos_theft };
  $cttpos = { %$cttpos_trans };


  my $table = $node_meta->{ct_table} || "content";

  my $hashfile = "$varenv{logdir}/$users_dms->{u_id}-$varenv{dbname}-$table-searchhash";

  if($table eq "content"){
	$node_meta->{tpl_order} =~ s/txt17=[\w\s=]+,//;
	$node_meta->{tpl_order} =~ s/byte01=[\w\s=]+,//;
  }
  if($table eq "contentadr" && $varenv{dbname} ne $dbt->{primary}->{sharee_primary}->{database}->{dbname}){
	$node_meta->{tpl_order} =~ s/txt17=[\w\s=]+,//;
	$node_meta->{tpl_order} =~ s/txt19=[\w\s=]+,//;
  }
  if($table eq "contentadr" && $varenv{dbname} eq $dbt->{primary}->{sharee_primary}->{database}->{dbname}){
	$node_meta->{tpl_order} =~ s/Tarif/privat-Tarif/;
  }
  $node_meta->{tpl_order} =~ s/c_id=raw=[\w\s=]+// if($users_dms->{u_id} != $dbt->{copri_conf}->{superu_id});

  my @tpl_order = split /,/,$node_meta->{tpl_order};

  my $scol = "mtime";  
  if($users_dms->{"col_sort_$table"}){
    $scol = $users_dms->{"col_sort_$table"};
  }

  my $limit = $R::limit || 250;
  my $offset = $R::offset || "0";

  #backward | forward
    if($R::go && $R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go && $R::go eq "forward_list"){
        $offset += $limit;
    }

 my ($daymarker,$raster_mmpx,$day4month) = $lb->month_line($users_dms);
 my $rows = 0;
 
 my $c_id4csv = "";
 my $ct4rel = {};
 my $ct4rel_parts = {};
 my $k=0;

  my $framewidth = "min-width: 1200px;";
  #to keep calendar line
  $framewidth = "min-width: 2000px;" if($node_meta->{ct_table} eq "content");
  print "<div id='Content4list' style='$framewidth'>\n";
  
  my $v_journal = $R::v_journal || "";
  if($node_meta->{main_id} == $dbt->{shareedms_conf}->{invoicejournal}){
    $v_journal = $node_meta->{node_name};
  }

  my $date = ""; 
  my $start_chck = 0;
  my $end_chck = 0;
  my $last_year = "";
  if($R::s_start_mtime){
    ($date,$start_chck) = $lb->checkdate($R::s_start_mtime) if($R::s_start_mtime !~ "%"); 
    $feedb->{message} .= ">>> Datum Eingabefehler: $date <<<" if($start_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
    $last_year = $c_yy if("$c_yy" eq "2011");
  }
  if($R::s_end_mtime){
    ($date,$end_chck) = $lb->checkdate($R::s_end_mtime) if($R::s_end_mtime !~ "%"); 
    $feedb->{message} .= ">>> Datum Eingabefehler: $date <<<" if($end_chck);
    my ($c_dd,$c_mm,$c_yy) = split(/\./,$date);
  }
   
  my $update_users = {
       table   => "users",
       change   => "no_time",
       u_id    => $users_dms->{u_id}
  };

  my $export = "";
  my $ck4ex = "@R::ck4ex" || "";
  $export = "check4export" if($R::ck4ex);
  if($export || $ck4ex){
   $dbt->update_one($dbh,$update_users,"time4csv='$time'");
   $users_dms = $dbt->select_users($dbh,$users_dms->{u_id});
  }

  my $searchref = {
  	time => $time,
	scol => $scol,
	offset => $offset,
	limit => $limit,
	export => $export,
	opos => "",
  };
  my $tplids = "$tpl_id," || 0;

  #Faktura actions
  if($node_meta->{template_id} =~ /208|209|218|219/){ 
   $main_id = $dbt->{shareedms_conf}->{faktura};
   $tplids = "218,219";

   if($node_meta->{node_name} =~ /OPOS/){
    $R::detail_search="search";
    $searchref->{int14} = ">=1";
    $feedb->{message} .= ">>> Offene Payone Posten (Fehlgeschlagene Geldeinzüge) <<<";
    $searchref->{offset} = 0;
    $searchref->{limit} = 1000;
    $tplids = "209,218";
    my $tpl = $dbt->get_tpl($dbh,209);
    @tpl_order = split /,/,$tpl->{tpl_order};
   }

   elsif($node_meta->{node_name} =~ /Mahnung/){
    $R::detail_search="search";
    $searchref->{int06} = ">=1";
    $feedb->{message} .= "";
    $searchref->{offset} = 0;
    $searchref->{limit} = 1000;
    $tplids = "209,218";
    my $tpl = $dbt->get_tpl($dbh,209);
    @tpl_order = split /,/,$tpl->{tpl_order};
   }

   elsif($node_meta->{node_name} eq "Tagesbericht"){
    $R::detail_search="search";
    $searchref->{offset} = 0;
    $searchref->{limit} = 10000;
    $v_journal = $node_meta->{node_name};
    $searchref->{opos} = "null";
    $tplids = 218;
   }
   
   elsif($node_meta->{node_name} eq "Verkaufsjournal" || $R::tpl_id4trans eq "209"){
    $main_id = $dbt->{shareedms_conf}->{invoicejournal};
    $v_journal = $node_meta->{node_name};
    $tplids = 209;
    $searchref->{offset} = 0;
    $searchref->{limit} = 1000;
   }
   elsif($node_meta->{node_name} eq "Rechnung"){
    $main_id = $dbt->{shareedms_conf}->{invoice};
    $tplids = 218;
   }
   elsif($node_meta->{node_name} eq "Storno"){
    $main_id = 300009;
    $tplids = 218;
   }
   elsif($node_meta->{node_name} eq "Abrechnung" || $R::tpl_id4trans eq "208"){
    $main_id = 300029;
    $tplids = 208;
   }

   #search at all
   if($R::detail_search && $R::detail_search eq "search"){
    $tplids = "209,218";
   }
   $searchref->{tplids} = "$tplids";
  }
  if($R::detail_search && $R::detail_search eq "search"){
    $searchref->{offset} = 0;
    $searchref->{limit} = 1500;
  }

  my $main_ids .= $dbt->collect_noderec($dbh,$main_id);

  #path-line
  my $redirect = "";
  $redirect = "(redirected)" if($R::redirected);
  #top of bootstrap
  my $header_style = "";

  if($node_meta->{tpl_id} == 205 || $node_meta->{tpl_id} == 209){
    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path", $q->span({-style=>"padding:5px 10px;background-color:#86cbd7;color:white;"},
  " $months[$mon -1] $year",
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat zurück",-href=>"?cal_delta_start=0:-1:0"}," &larr; "),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat aktuell",-href=>"?cal_today=1"},"&bull;"),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat vorwärts",-href=>"?cal_delta_start=0:1:0"}," &rarr; ")
    )),"\n";
  }elsif($node_meta->{template_id} =~ /209|218/){
print<<EOF
<script>
    function postave(val) {
        jQuery.ajax({
            url: "/ajax_json",
            data: { 'table':'users','faksum':val },
            type: 'POST',
        });
        //console.log('faksum '+ val);
    }
</script>
EOF
;
   my $checked_sum = 0;
   $checked_sum = 1 if($users_dms->{faksum});
    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path $redirect | ",$q->checkbox(-name=>" ∑ sum ", -title=>'Faktura Positionen summieren ∑ — kostet Zeit', -onclick=>"postave(1)", -checked=>"$checked_sum")),"\n";
  }else{
    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path $redirect"),"\n";
  }

  print $q->start_form(-name=>'searchform'),"\n";
  print $q->hidden(-name=>'offset', -value=>"$searchref->{offset}"),"\n";
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}"),"\n";
  print $q->hidden(-name=>'mode', -value=>"manager"),"\n";
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}"),"\n";
  print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}"),"\n";
  print $q->hidden(-name=>'ct_table', -value=>"$node_meta->{ct_table}"),"\n";
 
  my $record_st = { c_id => 0 };
  if($table eq "content"){
      $tplids = "205,224,225,210,226,227,228,229";
      my $pref_st = {
        table           => "content",
        fetch           => "all",
        keyfield        => "int04",
        template_id     => "225",
        };
     $record_st = $dbt->fetch_record($dbh,$pref_st);
  }
  #defaults to rental bikes
  if($path =~ /Waren$/){
    $tplids = 205;
  }
  my $s_ct_name = $q->escapeHTML($R::s_ct_name) || "";
  my $s_barcode = $q->escapeHTML($R::s_barcode) || "";

  my $relnod = $dbt->collect_rel4nodes($dbh,$main_ids);
  my $channel_map = $dbt->channel_map();
  my $ct_users = $dbt->users_map($dbh,"");

  my @_users = ("");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($R::s_owner && $channel_map->{$id} eq $R::s_owner){
	$searchref->{owner} = $id;
     }
  }
  if(!$searchref->{owner}){
   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($R::s_owner && $ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i) || ($ct_users->{$ctu_id}->{c_id} eq $searchref->{owner})){
  	$searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
     }
   }
  }

  my $node = { 
 	template_id => 205,
        parent_id => $dbt->{shareedms_conf}->{waren},
        fetch  => "all",
        keyfield => "main_id",
       };
  my $bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$node);
  my $tariff_all = {};
  my $tariff = {
        table           => "content",
        fetch           => "all",
        keyfield        => "barcode",
        template_id     => "210",#Tariff tpl_id
        };
  $tariff_all = $dbt->fetch_record($dbh,$tariff);

  my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
  my $users_dms_primary = { u_id => 0 };
  $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");

  my $ctrel = {};
  #only if permission read
  if(($node_meta->{ct_table} eq "content" && $users_dms->{int01} >= 1) || ($node_meta->{ct_table} eq "contentadr" && $users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int02} >= 1) || ($node_meta->{ct_table} eq "contenttrans" && $users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int03} >= 1)){

 
  my $c_id4trans = $R::c_id4trans || "";
  #Take only c_id from users if 
  if($users_dms->{c_id4trans} && $R::ct_trans && (($R::ct_trans eq "add_transadr" && $R::c_idadr) || ($R::ct_trans eq "add_transpos" && $R::c_id) || $R::ct_trans eq "new_trans")){
    $c_id4trans = $users_dms->{c_id4trans};
  }
	
  #Verkaufsjournal default view
  if($node_meta->{tpl_id} == 209 && $v_journal && !$R::detail_search && $full_url eq $referer){
	$searchref->{start_pay_time} = $start_date_time;
	$searchref->{end_pay_time} = $end_date_time;
  }

  if(!$R::detail_search && -f $hashfile && $full_url eq $referer && ($R::select_part || $R::ct_trans || $R::trans2edit || $R::node2edit || $R::base_edit || $R::rel_edit || $R::set_state)){
	  $searchref = {};
 	  $searchref = retrieve($hashfile);
	  $tplids = $searchref->{tplids} if($searchref->{tplids});
	  $feedb->{message} .= "Es wurden die letzten Suchparameter oder Filter geladen" if(keys %$searchref > 7);
  }

  #it will takes only one dataset
  if($node_meta->{ct_table} eq "contenttrans" && $c_id4trans && $c_id4trans =~ /^\d+$/ && (($users_dms->{faksum}) || ($full_url ne $referer) || ($node_meta->{tpl_id} == 209 && $v_journal && !$R::detail_search))){
    my $pref_ct = {
        table => "contenttrans",
        fetch     => "all",
        keyfield  => "c_id",
        c_id   => $c_id4trans,
    };
    $ct4rel = $dbt->fetch_record($dbh,$pref_ct);

  }elsif(!$start_chck && !$end_chck){

	#collect search keys
 	foreach my $postkey (@keywords){
	  foreach(@tpl_order){
    	    my ($key,$val,$size) = split /=/,$_;
	    if($postkey =~ /s_$key|s_start_$key|s_end_$key/){
     	  	my $val = $q->param($postkey);
		$postkey =~ s/^s_//; 
		$searchref->{$postkey} = $val; 
	    }
	  }
	}
	#$searchref->{txt29} = "Oberried";#sharee_tr workaround for SubOp yearly Abrechnung

	#trying to save hashref
	if(ref($searchref) eq "HASH"){
	  if($keycount > 0){
	    store $searchref, $hashfile;
	  }else{
	    unlink $hashfile;
	  }
	}
      $rows = $dbt->count_content($dbh,$table,$main_ids,$tplids);

      #reload sig bikes and stations states
      #$bw->log("Liste3 condition",$node_meta,"");
      if($varenv{syshost} eq "shareedms-sx"){
	my $ctadr = { c_id => $users_dms->{u_id} };
	if($node_meta->{tpl_id} == 205 || $node_meta->{node_name} eq "Waren"){
	  $q->param(-name=>'request',-value=>"bikes_available");
          (my $response->{bikes},my $return2copri->{bikes}) = $si->sig_available($q,\%varenv,$ctadr);
	  #$bw->log("return2copri",$return2copri->{bikes},"");
          $tk->sigbike_cupdate($return2copri->{bikes});
	}elsif($node_meta->{tpl_id} == 225){
          $q->param(-name=>'request',-value=>"stations_available");
          (my $response->{stations},my $return2copri->{stations}) = $si->sig_available($q,\%varenv,$ctadr);
	  #$bw->log("return2copri",$return2copri->{stations},"");
          $tk->sigstation_cupdate($return2copri->{stations});
	}
      }
      $ct4rel = $dbt->search_content($dbh,$searchref,$node_meta,$users_dms,"$main_ids","$tplids",$v_journal,$ck4ex);
      $feedb = $pl->export_csv($node_meta,$users_dms,$ct4rel,$bike_nodes,$coo) if($ck4ex);

      if($node_meta->{node_name} eq "Faktura" || $tplids =~ /218/){
	my $v_journalparts = "";
       	$v_journalparts = $v_journal . "_parts";
       	$ct4rel_parts = $dbt->search_content($dbh,$searchref,$node_meta,$users_dms,"$main_ids","$tplids",$v_journalparts,$ck4ex);
      }

   }#end $ct4rel collect
  }else{
    $feedb->{message} = "failure::Abbruch. Keine Zugriffsberechtigung";
  }#end permission read

   my $max_sum_pos = -10000;
   my $max_op = ">=";
   if($searchref->{int01} =~ /\d/){
     $max_sum_pos = $searchref->{int01};
     $max_sum_pos =~ s/\s//g;
     $max_sum_pos =~ s/,/./g;
     #$max_op = "==";
     if($max_sum_pos =~ /(\<\=|\>\=|\<|\>|\=\=)(\d+)/){
	$max_op = $1;
	$max_sum_pos  = $2;
     }
   }   
  print $q->div({-style=>'background-color:silver;height:10px;'},"&nbsp;"),"\n";

  my $hstyle = "border-right: solid thin gray;border-bottom: solid thin gray;";

  #accounting workflow
  if($node_meta->{node_name} eq "Verkaufsjournal"){
   print "<div style='padding-bottom:5px;background-color:silver;'>\n";
   if($R::accounting_select){
     print $but->singlesubmit1("detail_search","operator_accounting","","margin:0 0 0px 20px;"),"\n";
   }else{
      print $q->a({-class=>'elinkbutton',-style=>'margin:0 0 2px 20px;', -href=>'?accounting_select=1'},"1. Einzüge selektieren"),"\n";
   }
   print "<b>$months[$mon -1] $year</b>\n";
   print $q->span({-style=>'font-size:0.81em;'}," (1. selektiert Rechnungen mit Payone Einzug & kein OPOS & p-Saldo=0 & kein Storno & keinen internen Bearbeitungsstatus & payone-sequence=1, alle anderen bitte manuell überprüfen und falls i.O. selektieren)"),"\n";
   print "</div>\n";
  }
  print $q->hidden(-name=>'mandant_main_id', -value=>"$dbt->{shareedms_conf}->{parent_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -value=>"$node_meta->{tpl_id}"),"\n";

  print $q->start_table({ -style=>'clear:both;', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'0', -cellspacing=>'0'}),"\n";

  #1. search line
  print $q->Tr();
  print $q->td({-style=>"background-color:silver;"},""),"\n";
  print $q->td({-style=>"background-color:silver;$hstyle"},$but->singlesubmit1("detail_search","search","","width:100%;")),"\n";

   my $edit="base_edit"; 
   my $new_key="new_content";
   my $a_color = $node_meta->{bg_color} || "yellow";
   if($table eq "contenttrans"){
     $edit="ct_trans"; 
     $new_key="new_trans";
   }
   if($node_meta->{parent_id} >= 200000){
     print $q->td({-style=>"width:25px;background-color:$a_color;$hstyle"}, $but->singlesubmit2glyph("$edit","$new_key","$ib{$new_key}","background-color:$a_color;"));
     print $q->hidden(-name=>'open_set_main_id', -value=>"$node_meta->{main_id}") if($table =~ /contenttrans/);
   }else{
     print $q->td({-class=>'ebutton',-style=>"width:25px;background-color:$a_color;$hstyle"},"&nbsp;"),"\n";
   }

  #1. Search-fields
  my $h=0;
  my $s_val = "";
  foreach(@tpl_order){
    $h++;
    my ($key,$val,$size) = split /=/,$_;
   if($size !~ /select|readonly/){
    if($size =~ /area/){
     $size = "5em";
    }elsif($key =~ /int0|c_id|ct_name/){
     $size = "1em";# if(!$size); 
    }else{
     $size = "2em";
    }
   }
    $s_val = $searchref->{$key};
    #print "$key=$searchref->{$key}|";
    if($key =~ /byte|node|txt|int|time\d+|ct_name|c_id|barcode|sort|public/){
      if($table eq "content" && $tpl_id == 225 && $key eq "int04"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($table eq "content" && $key eq "barcode"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($table =~ /contentadr|contenttrans/ && $key eq "txt08"){
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40, -autofocus=>1),"\n");
      }elsif($size =~ /select/ && $key =~ /int12/ && $tpl_id =~ /210/){#Tarif for Flot
	my @s_valxx = ("");
	foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
          push (@s_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $dbt->{copri_conf}->{type_id}->{$bike_nodes->{$rid}->{type_id}}");
        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";
      }
      #txt24=bike_group
      elsif($size =~ /select/ && $key =~ /txt24/ && $tpl_id =~ /225/){
	#my %station_filter_hash = ();
	my @s_valxx = ("");
	foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
	  push (@s_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name}");

        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";

      }elsif($size =~ /select/ && $key =~ /int21|int22/ && $tpl_id == 228){#Bonusnummern
	my @s_valxx = ("");
        foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
          push (@s_valxx, "$tariff_all->{$rid}->{barcode}:$tariff_all->{$rid}->{barcode} - $tariff_all->{$rid}->{ct_name}");
        }
        print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";

      }elsif($size =~ /select|readonly/ && $key !~ /txt23|txt24|int04/){#txt23=color-code or txt24=Flotten ID select
	my @s_valxx = ("");
	my $s_hash = {};
	$s_hash = $dbt->{copri_conf}->{lock_state} if($tpl_id == 205 && $key eq "int20");
	$s_hash = $dbt->{copri_conf}->{bike_state} if($tpl_id == 205 && $key eq "int10");
	$s_hash = $dbt->{copri_conf}->{station_state} if($tpl_id == 225 && $key eq "int10");
	$s_hash = $dbt->{copri_conf}->{lock_system} if($tpl_id == 205 && $key eq "int11");
	$s_hash = $dbt->{copri_conf}->{fee_type} if($tpl_id == 229 && $key eq "int16");
	$s_hash = $dbt->{copri_conf}->{sharing_type} if($tpl_id == 210 && $key eq "int18");
	$s_hash = $dbt->{copri_conf}->{type_id} if($tpl_id == 225 && $key eq "txt25");
	$s_hash = { 1 => 1, 2 => 2, 3 => 3, 4 => 4 } if($tpl_id == 225 && $key eq "txt07");
        foreach my $s_key (sort keys (%{ $s_hash })) {
          push @s_valxx, "$s_key:$s_hash->{$s_key}";#[2:unlocked]
        }
	if(scalar @s_valxx < 2){
          print $q->td({-class=>'search_line',readonly=>1},""),"\n";
	}else{
          print $q->td({-class=>'search_line'},$but->selector_class("s_$key","","",$s_val,@s_valxx)),"\n";
	}
      }else{
      	print $q->td({-class=>"search_line"},$q->textfield(-class=>'stxt',-name=>"s_$key",-default=>"$s_val",-size=>"$size",-maxlength=>40)),"\n";
      }
    }elsif($key =~ /owner/){
        print $q->td({-class=>'search_line'},$but->selector("s_$key","120px","$s_val",@_users)),"\n";
    }elsif($key =~ /state/){
        my @_states = ("");
        foreach my $s_key (sort keys (%{ $p_hash })) {
          push @_states, "$p_hash->{$s_key}";
	}
	my @_orderstates = split(/\|/,$dbt->{shareedms_conf}->{order_state});
	push (@_states,@_orderstates);
        print $q->td({-class=>'search_line'},$but->selector("s_$key","120px","$s_val",@_states)),"\n";
    }
    my $s_mtime=""; 
    my $e_mtime="";
    if($key eq "mtime"){
      $s_mtime = $searchref->{start_mtime};
      $e_mtime = $searchref->{end_mtime};
    }
    if($key eq "atime"){
      $s_mtime = $searchref->{start_atime};
      $e_mtime = $searchref->{end_atime};
    }
    if($key eq "pay_time"){
      $s_mtime = $searchref->{start_pay_time};
      $e_mtime = $searchref->{end_pay_time};
    }
    if($key eq "warn_time"){
      $s_mtime = $searchref->{start_warn_time};
      $e_mtime = $searchref->{end_warn_time};
    }
    if($key eq "date_time"){
      $s_mtime = $searchref->{start_date_time};
      $e_mtime = $searchref->{end_date_time};
    }

    print $q->td({-nowrap=>1,-class=>"search_line_date"},$q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"s_start_$key",-default=>"$s_mtime",-size=>"8",-maxlength=>10),"<",$q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"s_end_$key",-default=>"$e_mtime",-size=>"8",-maxlength=>10)),"\n" if($key =~ /time$/);
  }


  #2. Tableheader
  print $q->Tr();
  #permissions
  if($users_dms_primary->{int03} == 2 && $users_dms->{int03} == 2){
    print $q->td({-style=>"background-color:silver;"},$q->checkbox(-name=>'toggle_all', -checked=>'', -value=>'off', -label=>'', -title=>'Alle auswählen', -onclick=>'CheckAll();')),"\n";
  }else{
    print $q->td({-style=>"background-color:silver;"},"&nbsp;"),"\n";
  }
  my $i=0;
  $i += 2;
    my $sort_up = "up";
    my $sort_down = "down";
    $sort_up = "<b>$sort_up</b>" if($users_dms->{"sort_updown_$table"} eq "up");
    $sort_down = "<b>$sort_down</b>" if($users_dms->{"sort_updown_$table"} eq "down");

    print $q->th($q->a({-class=>"sortnav",-href=>"?sort_updown=up\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?sort_updown=down\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>'Absteigend sortieren'},"$sort_down")),"\n";
    print $q->th(""),"\n";

  foreach (@tpl_order){
   my ($key,$val,$size,$title) = split /=/,$_;
   my $sort_title=" $title";
   $val = "<b>$val</b>" if($key eq $users_dms->{"col_sort_$table"});
   if($key =~ /byte|txt|time|node|int|time\d+|save|state|owner|c_id|ct_name|barcode|sort|public/){
     $i++;
     print $q->th({-nowrap=>1},$q->a({-class=>"sortnav",-href=>"?col_sort=$key\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"$sort_title"},"$val")),"\n";
   }
  }

  my $tdcal = scalar @tpl_order;
  if($tpl_id == "205"){
   print $q->Tr();
   print $q->td({-style=>'background-color:silver;'},""),"\n";
   print $q->td({-class=>'tdtxt',-style=>'background-color:silver;',-colspan=>2},"$months[$mon -1] $year"),"\n";
   print $q->td({-style=>"font-size:0.71em;padding:0;border:0px solid green;",-colspan=>"$tdcal",-nowrap=>"1"},"$day4month"),"\n";
  }

  my $sum_saldo=0;my $sum_opos=0;my $sum_prepaid=0;my $sum_ueb=0;my $sum_success=0;my $sum_SEPApayone=0;my $sum_SEPAmanually=0;my $sum_CCpayone=0;my $sum_ausfall=0;my $sum_gutschrift=0;my $sum_einzugfail=0;my $sum_entgelt=0;my $sum_abr=0;my $close_time="";my $payment_time=""; 
  my $nr=0;my $nx=0; 
  
  #BIG LOOP loop content table
  foreach my $id (sort { 
    if($users_dms->{"sort_updown_$table"} eq "down"){
	if ($scol =~ /barcode|int/) { 
		$ct4rel->{$b}->{$scol} <=> $ct4rel->{$a}->{$scol} 
	}else{ 
		lc($ct4rel->{$b}->{$scol}) cmp lc($ct4rel->{$a}->{$scol}) 
	} 
    }else{
	if ($scol =~ /barcode|int/) { 
		$ct4rel->{$a}->{$scol} <=> $ct4rel->{$b}->{$scol} 
	}else{ 
		lc($ct4rel->{$a}->{$scol}) cmp lc($ct4rel->{$b}->{$scol}) 
	} 
    }
  } keys(%$ct4rel)){

   my $set_style = "";
   my $set_style4nr = "";
   my $sum_error = 0;
   my $ecol=0;
   my $lock_system = 2;#defaults to Ilockit
   $lock_system = $ct4rel->{$id}->{int11} if($ct4rel->{$id}->{int11});

   # check sum_pos and sum_buchen
   my $sum_pos = 0;
   my $pricing = {};
   my $counting = {};
   #pre-sum only if client invoice
   if($ct4rel->{$id}->{template_id} == 218 && $ct4rel->{$id}->{int10} != 2 && $users_dms->{faksum} == 1){
     foreach my $cpid (keys (%$ct4rel_parts)){
	if($ct4rel->{$id}->{c_id} == $ct4rel_parts->{$cpid}->{ct_id}){
   
	  if($ct4rel_parts->{$cpid}->{int35} && $ct4rel_parts->{$cpid}->{start_time} && $ct4rel_parts->{$cpid}->{end_time}){
    	    ($pricing,$counting) = $pri->counting_rental(\%varenv,$ct4rel_parts->{$cpid});
	    $sum_pos += $pricing->{total_price}
	  }elsif((looks_like_number($ct4rel_parts->{$cpid}->{int02}) && $ct4rel_parts->{$cpid}->{int02} != 0) && (looks_like_number($ct4rel_parts->{$cpid}->{int03}) && $ct4rel_parts->{$cpid}->{int03} != 0)){
	   my ($gesamt,$rabatt) = $pri->price2calc($ct4rel_parts->{$cpid});
	   $sum_pos += $gesamt;
	  }
	}
     }
     my $sum_chk = $ct4rel->{$id}->{int01};
     $sum_pos = $lb->round($sum_pos);
     $sum_pos = sprintf('%.2f',$sum_pos);
     my $sum_chkdiff = $sum_chk - $sum_pos;
     if($ct4rel->{$id}->{int01} && ($sum_pos ne $sum_chk) && ($sum_chkdiff > 0.02 || $sum_chkdiff < -0.02)){
     	$sum_error = "1";
     }
     $sum_error = "0" if($R::s_kontext && $R::s_kontext eq "Waren" || $R::node2edit || $R::ct_trans);
   }


   my $u_name = $ct4rel->{$id}->{owner};
     foreach my $ctu_id (keys (%$ct_users)){
       if($channel_map->{$u_name}){
	$u_name = $channel_map->{$u_name};
       }elsif($ct4rel->{$id}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
	$u_name = $ct_users->{$ctu_id}->{txt01};
       }
     }
 
   #print $q->start_form(-name=>'listeform');

   if(1==1){    
   if(1==1){
    $nr++;
    $nx=$nr;
    $close_time = $ct4rel->{$id}->{close_time} if($ct4rel->{$id}->{close_time});
    $close_time = $lb->time4de($close_time,"1") if($close_time);
    $payment_time = "";
    if($ct4rel->{$id}->{payment_time}){
      $payment_time = $ct4rel->{$id}->{payment_time};
      $payment_time = $lb->time4de($payment_time,"1");
    }

    #summarize
    #alias state
    if($ct4rel->{$id}->{int04}){
     $sum_saldo += $ct4rel->{$id}->{int16};

     if($ct4rel->{$id}->{int04} == 7){
      $sum_ausfall += $ct4rel->{$id}->{int01};
     }elsif($ct4rel->{$id}->{int04} == 6){
      $sum_einzugfail += $ct4rel->{$id}->{int01};
     }elsif($ct4rel->{$id}->{int14} >= 1){
      $sum_opos += $ct4rel->{$id}->{int01};
     }else{
      $sum_SEPApayone += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 1);
      $sum_SEPAmanually += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 10);
      $sum_CCpayone += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 2);
      $sum_prepaid += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 3);
      $sum_ueb += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 4);
     }
     $sum_gutschrift += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 5);
     $sum_entgelt += $ct4rel->{$id}->{int01} if($ct4rel->{$id}->{int04} == 9);
     $sum_abr -= $ct4rel->{$id}->{int02} if($ct4rel->{$id}->{int04} == 8);
    }

    $c_id4csv .= "$ct4rel->{$id}->{c_id},";
    $ct4rel->{$id}->{ct_name} = $q->unescapeHTML($ct4rel->{$id}->{ct_name}) || "";
    $ct4rel->{$id}->{c_id} = $q->unescapeHTML($ct4rel->{$id}->{c_id}) || "";
    $set_style = "background-color:#fcfdfb;";
    $set_style = "background-color:#f4f1ee;" if($nx %= 2);
    #$set_style = "background-color:#e4f1ee;" if($ct4rel->{$id}->{ct_name} eq $ct4rel->{$id}->{barcode});
    my $ccc_id = $R::c_id || $R::c_id4trans || $users_dms->{c_id4trans};
    $set_style = "background-color:$node_meta->{bg_color};" if(($R::rel_id && $R::rel_id eq $ct4rel->{$id}->{rel_id}) && !$R::empty_rel_id);
    $set_style = "background-color:$node_meta->{bg_color};" if(($ccc_id && $ccc_id eq $ct4rel->{$id}->{content_id}) && ("$table" =~ /contenttrans/));
    $set_style = "background-color:$node_meta->{bg_color};" if(($ccc_id && $ccc_id eq $ct4rel->{$id}->{content_id}) && ("$table" =~ /contenttver/));

    #red
    $set_style = "background-color:#d39c9c;" if($sum_error && $sum_error == 1 && $node_meta->{node_name} eq "Faktura");
    $set_style4nr = $set_style;
    my $m_id;

    #Instanz Buttons-Logic
    print $q->Tr(),"\n";
    #checkboxes
    #permissions
    if($users_dms_primary->{int03} == 2 && $users_dms->{int03} == 2){
      my $checked = 0;
    
      #autoselect succesfull payone payments
      if($R::accounting_select && $node_meta->{template_id} == 209 && $ct4rel->{$id}->{int01} > 0 && $ct4rel->{$id}->{int04} && $ct4rel->{$id}->{int04} <= 2 && !$ct4rel->{$id}->{int14} && $ct4rel->{$id}->{int16} == 0 && !$ct4rel->{$id}->{txt22} && $ct4rel->{$id}->{int18} eq "1"){
        $checked = 1; 
      }

      print $q->td({-style=>"background-color:silver;"}, $q->checkbox(-name=>"ck4ex", -checked=>"$checked", -value=>"$ct4rel->{$id}->{c_id}", -label=>'')),"\n";
    }else{
      print $q->td({-style=>"background-color:silver;"},"&nbsp;"),"\n";
    }
    #1.Spalte
    if("$ct4rel->{$id}->{main_id}" eq "$node_meta->{main_id}" || "$ct4rel->{$id}->{main_id}" eq "$node_meta->{parent_id}"){
     if($table =~ /content$|contentadr/){        
	$ecol++;
	print "<td class='tb_inst' style='$set_style' nowrap>\n";

	#fee2pos
 	if($table eq "content" && ($ct4rel->{$id}->{template_id} =~ /229/) && $R::c_id4trans){
	  print $q->a({-class=>"editnav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/$xjournal->{node_name}?insert_contenttranspos=1\&c_id=$ct4rel->{$id}->{c_id}\&c_id4trans=$R::c_id4trans\&owner=$users_dms->{u_id}",-title=>"Gebühr hinzufügen"}, $q->span({-class=>"bi bi-clipboard2-plus", -style=>'font-size:1.5em;'}));
	}

	if($table eq "content" && ($ct4rel->{$id}->{template_id} =~ /205|225/)){
	    my $rnid = $ct4rel->{$id}->{rel_id};
      	    my $node_names = "failure $rnid | $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}";
	    if($relnod->{$rnid}->{content_id} && $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}){
		$node_names = $relnod->{$rnid}->{node_name};
	    }else{
	      	my $subnode = $dbt->get_subrelnode($dbh,$node_meta->{main_id});
		$node_names = $subnode->{node_name};
	    }
	    my $search_key = "s_barcode=$ct4rel->{$id}->{barcode}";#bike
	    $search_key = "s_int04=$ct4rel->{$id}->{int04}" if($ct4rel->{$id}->{template_id} == 225);#station 
	    print $q->a({-class=>"editnav",-href=>"/$dbt->{shareedms_conf}->{parent_node}$path/$node_names?detail_search=1\&$search_key",-title=>"zur Liste der Rad $ct4rel->{$id}->{barcode} Servicearbeiten"}, $q->span({-class=>"bi bi-wrench"}));
	}
	##
	print "</td>\n";
      }elsif($users_dms->{u_id} > 0 && $table eq "contenttrans"){        
	my $accounting_id = "";
	$accounting_id = "AbrID $ct4rel->{$id}->{int20}" if($ct4rel->{$id}->{int20});
	print $q->td({-class=>'tdtxt', -style=>"$set_style; color:grey;", -nowrap=>1}, "$accounting_id"),"\n";
	
      }else{
	$ecol++;
	print $q->td({-style=>"$set_style"}),"\n";
      }
    }elsif($ct4rel->{$id}->{content_id} && !$v_journal){
      #2017-11-10 simple multi node_name directory view
      $ecol++;
      my $rnid = $ct4rel->{$id}->{rel_id};
      my $node_names = "failure $rnid | $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}";
      if($relnod->{$rnid}->{content_id} && $relnod->{$rnid}->{content_id} == $ct4rel->{$id}->{c_id}){
	$node_names = $relnod->{$rnid}->{node_name};
      }else{
	my $subnode = $dbt->get_subrelnode($dbh,$node_meta->{main_id});
	$node_names = $subnode->{node_name};
      }

      #node_names fails TODO
      if(1==1 && $table eq "content" && ($ct4rel->{$id}->{template_id} =~ /205|225/)){
	    my $search_key = "s_barcode=$ct4rel->{$id}->{barcode}";#bike 
	    $search_key = "s_int04=$ct4rel->{$id}->{int04}" if($ct4rel->{$id}->{template_id} == 225);#station
      	    print $q->td({-class=>'tdtxt',-colspan=>1,-style=>"$set_style", -nowrap=>1}, $q->a({-class=>"editnav",-href=>"/$dbt->{shareedms_conf}->{parent_node}$path/$node_names?detail_search=1\&$search_key",-title=>"zur Liste der Rad $ct4rel->{$id}->{barcode} Servicearbeiten $ct4rel->{$id}->{main_id}"},"$node_names")), "\n";
      }else{
      	    print $q->td({-class=>'tdtxt',-colspan=>1,-style=>"$set_style", -nowrap=>1},"$ct4rel->{$id}->{txt00}"),"\n";
      }
    }else{
      $ecol++;
      print $q->td({-class=>'element',-colspan=>1,-style=>"$set_style"},""),"\n";
    }
    $ecol++;

    #2.Spalte
    my $ny="";
    if($table eq "contentadr" && ($ct4rel->{$id}->{int12} || !$ct4rel->{$id}->{int04} || !$ct4rel->{$id}->{int13})){
      print $q->td({-class=>'tdicon',-style=>"$set_style color:red;"},"&bull;","\n") if($ecol <= "2");
    }elsif($table eq "contenttrans" && ($ct4rel->{$id}->{int14} && $ct4rel->{$id}->{int14} >= 1)){
      print $q->td({-class=>'tdicon bi bi-record2', -style=>"$set_style;color:Maroon;"},""),"\n" if($ecol <= "2");
    }elsif($table eq "content" && $ct4rel->{$id}->{template_id} == 227){
     my $dir_thumb = "$varenv{data}/$node_meta->{main_id}-thumb/$ct4rel->{$id}->{c_id}"; 
     my @pics = $lb->read_dirfiles($dir_thumb,"\.JPG|\.PNG","file","");
     my $dir_main = "$varenv{data}/$node_meta->{main_id}/$ct4rel->{$id}->{c_id}"; 
     my @docs = $lb->read_dirfiles($dir_main,"\.HTML","file","");

     print "<td class='tdtxt' style='$set_style' nowrap>\n";

     if( -d "$dir_thumb" && scalar(@pics) > 0){
      foreach(@pics){
       print $q->span($q->img({-style=>'max-width:75px;',-src=>"$varenv{metahost}/data/$node_meta->{main_id}-thumb/$ct4rel->{$id}->{c_id}/$_"},"$_")),"\n";
       print $q->br(),"\n" if(scalar(@pics) > 1);
      }
     }
     print "</td>\n";

    }else{
      print $q->td({-class=>'tdint',-style=>"$set_style color:grey;"},"$ny"),"\n" if($ecol <= "2");
    }

    #Tablecontent (parameter)
    $k=0;
    foreach (@tpl_order){
      $k++;
      my ($key,$val,$size,$colorize) = split /=/,$_;     
      my $txtstyle = "text-align:left;";
      if($key =~ /barcode|c_id|ct_name|int|time\d+|state|sort|public/ && $size ne "select"){
        $txtstyle = "text-align:right;max-width:8em;";
      }
      $ct4rel->{$id}->{$key} = $lb->time4de($ct4rel->{$id}->{$key},"1") if($key =~ /time$/);
      #$ct4rel->{$id}->{$key} = $q->unescapeHTML($ct4rel->{$id}->{$key});# if($key !~ /byte/);
      #my $br4text = $R::node2edit || "";
      #$ct4rel->{$id}->{$key} = $lb->newline($ct4rel->{$id}->{$key},"","$br4text");
      $set_style4nr = $set_style;
      $set_style4nr="background-color:#e3dbc9;" if(($key eq "barcode") || ($key eq "int04"));
      $ct4rel->{$id}->{$key} =~ s/\./,/ if($key =~ /int/ && $ct4rel->{$id}->{$key});

	if($key eq "c_id"){

            my $pos_hash = $ct4rel->{$id};
            my $pos_details = "";
            foreach my $did (sort keys (%{$pos_hash})){
          	my $teaser = "";
          	if($pos_hash->{$did}){
                  $teaser = substr($pos_hash->{$did},0,30);
          	}
                $pos_details .= $did . " = " . $teaser . "</br>" if($pos_hash->{$did});
            }
	    my $pos_id = "$ct4rel->{$id}->{c_id}";
	    if($users_dms_primary->{u_id} && $dbt->{copri_conf}->{betau_id}->{$users_dms_primary->{u_id}}){
              $pos_id = $q->div({-class=>"popup",-onclick=>"toggle_box('$id')"},"$ct4rel->{$id}->{c_id}", $q->span({-class=>"popuptext",-id=>"$id"},"$pos_details"));
	    }
	    print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},"$pos_id"),"\n";	
	}
	elsif($key eq "ct_name" && $ct4rel->{$id}->{$key}){
	  if($table =~ /contenttrans|contenttver/){
	    my $opos = "";
	    $opos = "<span class='bi bi-record2' style='color:Maroon;'></span>" if($ct4rel->{$id}->{int14});
	    print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr", -nowrap=>1},$q->a({-class=>"linknav3",-href=>"?ct_trans=open\&mode=manager\&c_id4trans=$ct4rel->{$id}->{c_id}\&tpl_id4trans=$ct4rel->{$id}->{template_id}\&owner=$users_dms->{u_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$opos $ct4rel->{$id}->{$key}"));
	  }elsif($table =~ /content$|contentadr/){
	      print $q->td({-class=>"tdint",-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	  }elsif(($node_meta->{tpl_order} !~ /barcode/) || ("$ct4rel->{$id}->{$key}" ne "$ct4rel->{$id}->{barcode}")){
            print $q->td({-class=>'tdint',-style=>"font-weight:bold;$set_style4nr"},"$ct4rel->{$id}->{$key}"),"\n"; 
	  }else{
            print $q->td({-class=>'tdint',-style=>"font-weight:bold;$set_style4nr"},""),"\n"; 
	  }
	}elsif($key eq "int13" && $table eq "contenttrans"){
   	    my $pref_rel = {
    		table     => "contenttrans",
    		fetch     => "all",
    		keyfield  => "c_id",
		template_id => "IN::(209,218)",
    		barcode   => $ct4rel->{$id}->{barcode},
   	    };
   	    my $ctt_rel = "";
   	    $ctt_rel = $dbt->fetch_record($dbh,$pref_rel) if($ct4rel->{$id}->{barcode});
	    print "<td class='tdint' style='$set_style4nr' nowrap>\n";
	    if(ref($ctt_rel) eq "HASH"){
    	      foreach my $irid (sort { $ctt_rel->{$b}->{c_id} <=> $ctt_rel->{$a}->{c_id} } keys (%$ctt_rel)){
		if($ct4rel->{$id}->{c_id} != $ctt_rel->{$irid}->{c_id}){
		  my $rel_opos = "";
		  $rel_opos = "<span class='bi bi-record2' style='color:Maroon;'></span>" if($ctt_rel->{$irid}->{int14});
	    	  print $q->a({-class=>"linknav3",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Faktura/$ctt_rel->{$irid}->{node_name}?ct_trans=open\&c_id4trans=$ctt_rel->{$irid}->{c_id}\&tpl_id4trans=$ctt_rel->{$irid}->{template_id}\&owner=$users_dms->{u_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$rel_opos $ctt_rel->{$irid}->{ct_name}<br />");
		}
    	      }
    	    }
	    print "</td>\n";
	}elsif($key =~ /txt01/ && $table =~ /contenttrans|contenttver/){
	    print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?ct_trans=open\&mode=manager\&c_id4trans=$ct4rel->{$id}->{c_id}\&tpl_id4trans=$ct4rel->{$id}->{template_id}\&owner=$users_dms->{u_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	
	}elsif($key =~ /txt01/ && $table =~ /content$|contentadr/){# && $lock_system != 3){
	  my $txt01 = "---";
	  $txt01 = "$ct4rel->{$id}->{$key}" if($ct4rel->{$id}->{$key});
	  print $q->td({-class=>'tdtxt',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$txt01")),"\n";
	}elsif($key =~ /barcode/ && $table =~ /content$|contentadr/ && $ct4rel->{$id}->{template_id} != 225){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	}elsif($key =~ /int04/ && $table eq "content" && $ct4rel->{$id}->{template_id} == 225){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;$set_style4nr"},$q->a({-class=>"linknav3",-href=>"?node2edit=editpart\&mode=manager\&rel_id=$ct4rel->{$id}->{rel_id}\&offset=$searchref->{offset}\&limit=$searchref->{limit}",-title=>"Terminal öffnen"},"$ct4rel->{$id}->{$key}")),"\n";
	}elsif($key =~ /int04/ && $table eq "content" && $ct4rel->{$id}->{template_id} == 205){# && $lock_system != 3){
	  print $q->td({-class=>'tdint',-style=>"font-weight:normal;"},$q->a({-class=>"linknav3",-style=>"",-href=>"?detail_search=1&s_int04=$ct4rel->{$id}->{$key}",-title=>"$record_st->{$ct4rel->{$id}->{int04}}->{txt01} | Station Filter"},"$ct4rel->{$id}->{$key}"));
        }elsif($key =~ /date_time/){
	  $ct4rel->{$id}->{start_time} = $lb->time4de($ct4rel->{$id}->{start_time},"1") if($ct4rel->{$id}->{start_time});
	  $ct4rel->{$id}->{end_time} = $lb->time4de($ct4rel->{$id}->{end_time},"1") if($ct4rel->{$id}->{end_time});
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style", -nowrap=>1},"$ct4rel->{$id}->{start_time} - $ct4rel->{$id}->{end_time}"),"\n";
        }elsif($key =~ /int01/ && ($node_meta->{parent_id} == $dbt->{shareedms_conf}->{faktura} || $node_meta->{main_id} == $dbt->{shareedms_conf}->{faktura})){
	    my $betrag = "";
	    if($ct4rel->{$id}->{$key}){
	      $betrag = $ct4rel->{$id}->{$key};
	    }elsif($users_dms->{faksum}){
	      $betrag = "&sum; $sum_pos";
	    }
	    $betrag = $ct4rel->{$id}->{$key} if($v_journal);
	    $betrag =~ s/\./,/;
            print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style",-nowrap=>1},"$betrag"),"\n";
        }elsif($key =~ /int02/ && $ct4rel->{$id}->{state} eq "Tagesabschluss"){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},""),"\n";
	}elsif($key =~ /int03/ && "$table" eq "contenttrans"){ 
	  print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},""),"\n";
        }elsif($key =~ /mtime/ && $ct4rel->{$id}->{close_time}){
	  my $close = "<br />$close_time TaAb.";
	  my $payment = "";
	  $payment = "<br />$payment_time ZaEi." if($payment_time);
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key} $close $payment"),"\n";
        }elsif($key =~ /owner/){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$u_name"),"\n";
        }elsif($key =~ /state/){#int04 state key
	  my $order_state = "";
	  $order_state = "$ct4rel->{$id}->{txt22}" if($ct4rel->{$id}->{txt22});
	  $order_state = "$ct4rel->{$id}->{txt22},<br />" if($ct4rel->{$id}->{txt22} && $p_hash->{$ct4rel->{$id}->{int04}});
	  my $capture_state = "<span>$p_hash->{$ct4rel->{$id}->{int04}}</span>";
	  $capture_state = "<span style='color:Maroon;'>$p_hash->{$ct4rel->{$id}->{int04}}</span>" if($ct4rel->{$id}->{int14} >= 1);
	  $capture_state = "<span style='color:Olive;'>$p_hash->{$ct4rel->{$id}->{int04}}</span>" if($ct4rel->{$id}->{int04} == 7);
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$order_state $capture_state"),"\n";
        }elsif($ct4rel->{$id}->{template_id} =~ /205|225/ && $key eq "int10"){#bike_state
	  my $colorize = "";
	  $colorize = "color:#c63e3e;" if($ct4rel->{$id}->{$key} == 2 || $ct4rel->{$id}->{$key} == 3);
	  $colorize = "color:#bd2424;" if($ct4rel->{$id}->{$key} == 4 || $ct4rel->{$id}->{$key} == 5 || $ct4rel->{$id}->{$key} == 6);
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style $colorize"},"$dbt->{copri_conf}->{bike_state}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "int20"){#lock_state
	  my $colorize = "";
	  $colorize = "color:#c63e3e;" if($ct4rel->{$id}->{$key} == 2 || $ct4rel->{$id}->{$key} == 3);
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style $colorize"},"$dbt->{copri_conf}->{lock_state}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 229 && $key eq "int16"){
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$dbt->{copri_conf}->{fee_type}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "int11"){#lock_system
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$dbt->{copri_conf}->{lock_system}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 210 && $key eq "int18"){#sharing_type
 	  print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$dbt->{copri_conf}->{sharing_type}->{$ct4rel->{$id}->{$key}}"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 210 && $key eq "int12"){
          my $flotten = "";
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
             $flotten .= "$bike_nodes->{$rid}->{node_name} - $dbt->{copri_conf}->{type_id}->{$bike_nodes->{$rid}->{type_id}}<br />" if($ct4rel->{$id}->{$key} =~ /$bike_nodes->{$rid}->{main_id}/);
          }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$flotten"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 225 && $key eq "txt24"){
          my $flotten = "";
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
             $flotten .= "$bike_nodes->{$rid}->{node_name}<br />" if($ct4rel->{$id}->{$key} =~ /$bike_nodes->{$rid}->{main_id}/);
          }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$flotten"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 225 && $key eq "txt25"){
          my %station_filter_hash = ();
          if($ct4rel->{$id}->{txt25} && $ct4rel->{$id}->{txt25} =~ /\d\s\d/){
                %station_filter_hash = map { $_ => 1 } split(/\s+/,$ct4rel->{$id}->{txt25});
          }elsif($ct4rel->{$id}->{txt25} && $ct4rel->{$id}->{txt25} =~ /(\d+)/){
                $station_filter_hash{$1} = 1;
          }
          my $station_filter = "";
          foreach my $type_id (keys (%station_filter_hash)){
                $station_filter .= "$dbt->{copri_conf}->{type_id}->{$type_id}<br />";
          }

          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$station_filter"),"\n";
        }elsif($ct4rel->{$id}->{template_id} == 228 && $key =~ /int21|int22/){
          my $bonustarif = "";
          foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
            $bonustarif .= "$tariff_all->{$rid}->{barcode} - $tariff_all->{$rid}->{ct_name}<br />" if($ct4rel->{$id}->{$key} == $tariff_all->{$rid}->{barcode});
          }

          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$bonustarif"),"\n";
 
        }elsif($key =~ /int|barcode/){
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
        }elsif($key =~ /time$/){
	  my $timeview = "";
	  $timeview = $ct4rel->{$id}->{$key} if($ct4rel->{$id}->{$key} && $ct4rel->{$id}->{$key} =~ /\d/); 
          print $q->td({-class=>'tdint',-style=>"$txtstyle $set_style"},"$timeview"),"\n";
        }elsif($key =~ /txt03/ && $ct4rel->{$id}->{template_id} eq "227"){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},$q->a({-style=>"color:black;",-href=>"$varenv{metahost}/site/$ct4rel->{$id}->{$key}", -target=>'_blank',-title=>'anzeigen'},"$ct4rel->{$id}->{$key}")),"\n";
        }elsif($key =~ /txt00/ && $v_journal){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
        }elsif($key =~ /txt08/ && $table eq "contentadr"){
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
	 #color code
        }elsif($ct4rel->{$id}->{template_id} == 205 && $key eq "txt23"){
	  my @service_code = split(/\s/,$ct4rel->{$id}->{$key});
	  my $color_code = "";
	  foreach(@service_code){
	    $color_code .= "<span class='coloricon' style='background-color:$_;'></span>";
	  }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$color_code"),"\n";
	 #teaser shorts longtext
        }elsif(($table eq "contentadr" && $key =~ /txt29/) || ($table eq "content" && $key =~ /txt04/) || ($table eq "contenttrans" && $key =~ /txt23|txt28/)){
	  my $teaser = "";
	  if($ct4rel->{$id}->{$key}){
	  	$teaser = substr($ct4rel->{$id}->{$key},0,50) . " ..."; 
	  }
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style; min-width:200px;"},"$teaser"),"\n";
        }elsif($key eq "txt30" && $node_meta->{template_id} == 202){
	  $txtstyle = "text-align:left;";
	   my @user_tarif = ("$ct4rel->{$id}->{$key}");
	   @user_tarif = split(/\s/,$ct4rel->{$id}->{$key}) if($ct4rel->{$id}->{$key} =~ /\w\s+\w/);
	   my $registered_tarif = "";
	   foreach(@user_tarif){
		$registered_tarif .= "$_ - $dbt->{copri_conf}->{sharing_type}->{$tariff_all->{$_}->{int18}} - $tariff_all->{$_}->{ct_name} - $bike_nodes->{$tariff_all->{$_}->{int12}}->{node_name} | ";
	   }
	   #print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$registered_tarif"),"\n";
	   print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style", -title=>"$registered_tarif"},"$ct4rel->{$id}->{$key}"),"\n";
	 #default to anything else
        }elsif($key =~ /ct_name|node|txt|uri/){
	  $txtstyle = "text-align:left;";
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
        }elsif($key =~ /byte/){
	  #$ct4rel->{$id}->{$key} =~ s/\\x//;
	  my $K_int = $ct4rel->{$id}->{$key};
	  #$K_int =~ s/(.)/sprintf( "%x", ord($1))/eg;
	  my $K_int = unpack "H*", $ct4rel->{$id}->{$key};
	  $txtstyle = "text-align:left;";
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$K_int"),"\n";
        }elsif($key =~ /time\d+/){
	  $txtstyle = "text-align:right;";
	  $ct4rel->{$id}->{$key} =~ s/:00$//;
          print $q->td({-class=>'tdtxt',-style=>"$txtstyle $set_style"},"$ct4rel->{$id}->{$key}"),"\n";
 
        }
     }
    }

    #sub-table-row for rent Calendar
    my $cal_count = 0;
    foreach my $ctid (sort { $cttpos->{$b}->{end_time} cmp $cttpos->{$a}->{end_time} } keys (%$cttpos)){
     if($ct4rel->{$id}->{c_id} == $cttpos->{$ctid}->{cc_id}){	
	$cal_count++;
	if($cal_count <= 1){
    	 my $scale_color = "#86cbd7;";
	 my $calement = "calement_86cbd7";
	 if($ct4rel->{$id}->{int13} == $cttpos->{$ctid}->{int13} && $cttpos->{$ctid}->{int05} == 7){
	   $scale_color = "#f0536e;";
	   $calement = "calement_f0536e";
 	 }
	
      	my ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$ctid}->{start_time}) if($cttpos->{$ctid}->{start_time});
      	my ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$ctid}->{end_time}) if($cttpos->{$ctid}->{end_time});

      	if($year_st && $mon_st && $day_st && $hh_st && $mm_st && $year_en && $mon_en && $day_en && $hh_en && $mm_en){
          #generate px for rent scale

          my $start_nr = $year_st . $mon_st . $day_st . $hh_st . $mm_st;
          my $end_nr = $year_en . $mon_en . $day_en . $hh_en . $mm_en;
          my $day_stpx = 0;
          my $rent_day_px = 0;
          my $time_style = "";
 	  if($start_nr <= $end_nr){
          	($day_stpx,$rent_day_px) = $lb->rent_scale($users_dms,$year_st,$mon_st,$day_st,$hh_st,$mm_st,$year_en,$mon_en,$day_en,$hh_en,$mm_en);
          }else{
  	        $time_style="color:red;";
          }
	  print $q->Tr();
	  print $q->td({-style=>'background-color:silver;'},""),"\n";
	  print $q->td({-class=>"$calement",-colspan=>2,-style=>"$set_style;"},""),"\n";
	  print "<td class='$calement' colspan='$tdcal' style='$set_style;'>\n";
	  #print $q->div({-style=>"position:absolute;margin-left:$daymarker;border-right: solid thin #86cb00;height:1.7em;"},"&nbsp;"),"\n";# if("$mon" eq "$mon_today");
	  my $calpath = $xjournal->{node_name};
	  if(1==2 && $ct4rel->{$id}->{int13} == $cttpos->{$ctid}->{int13} && $cttpos->{$ctid}->{int10} =~ /7|8/){
	    $calpath = "Alarmjournal";
  	    $time_style="color:red;";
  	  }
	  #print $q->div({-style=>"position:static;margin-left:$day_stpx;width:$rent_day_px;background-color:$scale_color"},$q->a({-class=>"linknav3",-style=>"$time_style",-href=>"$varenv{wwwhost}/DMS/$calpath/?cttpos_id=$cttpos->{$ctid}->{c_id}",-title=>"Im $calpath öffnen"},"$hh_en:$mm_en")),"\n";
	  print $q->div({-style=>"position:static;margin-left:$day_stpx;width:$rent_day_px;background-color:$scale_color"},$q->a({-class=>"linknav3",-style=>"$time_style",-href=>"$varenv{wwwhost}/DMS/$calpath/?barcode=$cttpos->{$ctid}->{barcode}",-title=>"Im $calpath öffnen"},"$hh_en:$mm_en")),"\n";
	  print "</td>\n";
	  print $q->Tr();
	  print $q->td({-style=>'padding-top:1px;'},""),"\n";
       }
      }
     }
    }
  }
 }#journal offen ende

 
 if(($nr > 0) && ($v_journal || $R::close_trans) && !$R::rel_id){
   my $m = $k - 5;
   print $q->Tr();
   print $q->td({-style=>"background-color:silver;"},""),"\n";
   print $q->td({-class=>'tdtxt',-style=>'text-align:center;background-color:silver;',-colspan=>"$k"},"Gesamt Umsätze");
   print $q->td({-class=>'tdsum',-colspan=>"4",-style=>'background-color:silver;'},"");


    if($sum_saldo != 0){
     $sum_saldo = sprintf('%.2f',$sum_saldo);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Saldo Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_saldo €");
    }
    if($sum_opos != 0){
     $sum_opos *= -1;
     $sum_opos = sprintf('%.2f',$sum_opos);
     $sum_success += $sum_opos;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"OPOS Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_opos €");
    }
    if($sum_ueb != 0){
     $sum_ueb = sprintf('%.2f',$sum_ueb);
     $sum_success += $sum_ueb;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Überweisung Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_ueb €");
    }
    if($sum_prepaid != 0){
     $sum_prepaid = sprintf('%.2f',$sum_prepaid);
     $sum_success += $sum_prepaid;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Prepaid Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_prepaid €");
    }
    if($sum_SEPApayone != 0){
     $sum_SEPApayone = sprintf('%.2f',$sum_SEPApayone);
     $sum_success += $sum_SEPApayone;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"SEPA-Lastschrift (payone) Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_SEPApayone €");
    }
    if($sum_SEPAmanually != 0){
     $sum_SEPAmanually = sprintf('%.2f',$sum_SEPAmanually);
     $sum_success += $sum_SEPAmanually;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"SEPA-Lastschrift (manuell) Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_SEPAmanually €");
    }
    if($sum_CCpayone != 0){
     $sum_CCpayone = sprintf('%.2f',$sum_CCpayone);
     $sum_success += $sum_CCpayone;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Kreditkarte (payone) Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_CCpayone €");
    }
    if($sum_gutschrift != 0){
     #$sum_gutschrift *= -1;
     $sum_gutschrift = sprintf('%.2f',$sum_gutschrift);
     $sum_success += $sum_gutschrift;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Gutschrift Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_gutschrift €");
    }
    if($sum_einzugfail != 0){
     #$sum_einzugfail *= -1;
     $sum_einzugfail = sprintf('%.2f',$sum_einzugfail);
     $sum_success += $sum_einzugfail;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"fehlgeschlagener Einzug Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_einzugfail €");
    }
    if($sum_ausfall != 0){
     $sum_ausfall *= -1;
     $sum_ausfall = sprintf('%.2f',$sum_ausfall);
     $sum_success += $sum_ausfall;
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Zahlungsausfall Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_ausfall €");
    }
    if($sum_success != 0){
     $sum_success = sprintf('%.2f',$sum_success);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdsum',-colspan=>"$m"},"Gesamtsumme");
     print $q->td({-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},"$sum_success €");
    }
    #disabled, only useful by timerange
    if(1==2 && $sum_entgelt != 0){
     $sum_entgelt = sprintf('%.2f',$sum_entgelt);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Entgelt Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_entgelt €");
    }
    if(1==2 && $sum_abr != 0){
     $sum_abr = sprintf('%.2f',$sum_abr);
     print $q->Tr();
     print $q->td({-style=>"background-color:silver;"},""),"\n";
     print $q->td({-class=>'tdint',-colspan=>"$m"},"Abrechnung Summe");
     print $q->td({-class=>'tdint',-colspan=>"1",-nowrap=>"1"},"$sum_abr €");
    }

     if($users_dms->{u_id}){
      if($v_journal eq "Tagesbericht"){
      	print $q->Tr();
      	print $q->td({-style=>"background-color:silver;"},""),"\n";
      	print $q->td({-style=>"background-color:silver;",-class=>'tdsum',-colspan=>"$m"},"Tagesbericht abschließen, Übertrag in das Verkaufsjournal"),"\n";
      	print $q->td({-style=>"background-color:silver;",-class=>'tdsum',-colspan=>"1",-nowrap=>"1"},$but->singlesubmit("close_trans","Speichern")),"\n";
      }
     }

 }
 
 if($varenv{dbname} eq "sharee_kn" && $node_meta->{tpl_id} == 228){
   if( -d "$opdir_dms/ftp/SWK_codes"){
    my @pdfs = $lb->read_dirfiles("$opdir_dms/ftp/SWK_codes","got_last\.csv","file","");
    foreach(@pdfs){
       print $q->Tr(),"\n";
       print $q->td({-class=>"tdtxt",-colspan=>10},"SWK Bonunsnummern download ",$q->a({-class=>'linknav3',-href=>"$varenv{wwwhost}/FileOut?file=$_&sessionid=$coo", -target=>'_default', -title=>'Download',-type=>'application/octet-stream'}, $q->span({-class=>"bi bi-download"}), "\"$_\"")),"\n";
    }
   }
 }

 print $q->end_table;
 
 print $q->hidden(-name=>'tpl_id',-override=>'1', -value=>"$tpl_id");
 print $q->end_form;

 my $offset_nr = $searchref->{offset} + $nr;
 my $counter = $rows;
 print $q->div({-style=>'float:left;padding:6px 0 0 10px;'},"Zeile: $searchref->{offset} - $offset_nr / $counter");

 #backward | forward
 print "<div style='padding:6px 0 0 10px;'>\n";
 print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$searchref->{offset};limit=$searchref->{limit}",-title=>'backward'},"&larr; ") if($searchref->{offset} >= $searchref->{limit});
 print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$searchref->{offset};limit=$searchref->{limit}",-title=>'forward'}," &rarr;") if($counter >= $limit-10); #if($rows > $limit && $nr > 0);
 print $q->span({-style=>'color:silver;'}," (offset:$searchref->{offset}, limit:$searchref->{limit})");
 print "</div>\n";

 print $q->div({-style=>''}, "&nbsp;"),"\n";
 print $q->div({-style=>'padding:6px 0 6px 20px;text-decoration:underline;'}, "Symbol Legende"),"\n";
 if($table eq "contentadr"){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({-style=>'padding:0.1em 0.8em;',-style=>'color:red;'},"&bull;"), "Der Verleih ist nicht freigeschaltet"),"\n";
 }elsif($table eq "contenttrans"){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({ -class=>'bi bi-record2', -style=>'padding:0.1em 0.8em;color:Maroon;'},""), "OPOS - Offener Posten"),"\n";
 }elsif($table eq "content" && $node_meta->{tpl_id} =~ /205|225/){
   print $q->div({-style=>'padding:0 20px;font-style:italic;'}, $q->span({-class=>"bi bi-wrench"}), "Service-Wartung"),"\n";
 }
 print $q->div({-style=>'padding:0.5em;'}, "&nbsp;"),"\n";

 print "</div>";

 return $feedb;
}
1;
