package BaseEdit;
#
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#use lib "/var/www/copri4/shareedms-primary/src";
#
use strict;
use warnings;
use POSIX;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Lib::Config;
use Mod::Buttons;
use Date::Calc qw(:all);
use Mod::Libenz;
use Mod::Basework;
use Scalar::Util qw(looks_like_number);
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $varenv = shift;
 my $node_meta = shift;
 my $users_dms = shift;
 my $feedb = shift || {};

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $bw = new Basework;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $pri = new Pricing;
 my $but = new Buttons;
 my %ib = $but->ibuttons();

 my $lang = "de";
 my $dbh = "";

 my $c_id = "";
 #helper to get c_id by rel_id
 if($R::rel_id && $R::rel_id =~ /(\d+)/){
   my $rel_id = $1;
   my $ref_helper = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        template_id     => "$node_meta->{tpl_id}",
        rel_id          => "$rel_id",
        };
   my $ctrel_helper = $dbt->fetch_record($dbh,$ref_helper);
   $c_id = $ctrel_helper->{c_id} if($ctrel_helper->{c_id});
 }
 $c_id = $1 if($R::c_id && $R::c_id =~ /(\d+)/);
 $c_id = $feedb->{c_id} if($feedb->{c_id});

 my $u_id = "";
 $u_id = $1 if($R::u_id && $R::u_id =~ /(\d+)/);
 $u_id = $1 if($R::c_idadr && $R::c_idadr =~ /(\d+)/ && $R::base_edit eq "new_dmsusers");
 $u_id = $feedb->{u_id} if($feedb->{u_id});
 my $edit = "";
 my $save_key = "";
 my $move_key = "";
 my $delete_key = "";
 my $tpl = {};
 my @tpl_order = ();
 my $edit_template = "";
 my $bg_color = $node_meta->{bg_color} || "grey";
 my $ctrel = {};
 my $cttpos = {};
 my $gesamt = 0;
 my $pricing = {};
 my $counting = {};
 my $rental_feed = {};
 my $occupied_style = "";

 if($node_meta->{ct_table} eq "content" && $node_meta->{tpl_id} && $c_id){
   #check rental state
   if($node_meta->{tpl_id} == 205){
     my $pref = {
        table => "contenttranspos",
        fetch => "one",
	int10 => "IN::(2,3)",
        cc_id  => $c_id,
        };
     $cttpos = $dbt->fetch_tablerecord($dbh,$pref);
     $occupied_style = "color:#ff1493" if($cttpos->{int10} == 2 ||$cttpos->{int10} == 3);
     $move_key = "move_content";
   }
   my $ref = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        template_id     => "$node_meta->{tpl_id}",
        c_id          => "$c_id",
        };
   $ctrel = $dbt->fetch_record($dbh,$ref);
   print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$ctrel->{c_id}");
   print $q->hidden(-name=>'rel_id',-override=>'1', -value=>"$ctrel->{rel_id}");

   $edit = "base_edit";
   $save_key = "save_content";
   $edit_template = "$ctrel->{template_id}";
   $tpl = $dbt->get_tpl($dbh,$edit_template);
 }
 elsif($node_meta->{ct_table} eq "contentadr" && $node_meta->{tpl_id} && $c_id){
   my $ref = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        template_id     => "$node_meta->{tpl_id}",
        c_id          => "$c_id",
        };
   $ctrel = $dbt->fetch_record($dbh,$ref);
   print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$ctrel->{c_id}");
   print $q->hidden(-name=>'rel_id',-override=>'1', -value=>"$ctrel->{rel_id}");

   $edit = "base_edit";
   $save_key = "save_adr";
   $edit_template = "$ctrel->{template_id}" . "000";
   $tpl = $dbt->get_tpl($dbh,$edit_template);
 }
 elsif($node_meta->{ct_table} eq "users" && $node_meta->{tpl_id} == 198){
    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");
    $edit_template = $node_meta->{tpl_id} . "000";
    $tpl = $dbt->get_tpl($dbh,$edit_template);
    
    #only if user is also a primary DMS user with invoice rw
    if(!$users_dms_primary->{u_id} || $users_dms_primary->{int03} != 2){        
      $tpl->{tpl_order} =~ s/int02=[\w\s\+\&=]+,//;
      $tpl->{tpl_order} =~ s/int03=[\w\s\+\&=]+,//;
    }
    $edit = "base_edit";
    $save_key = "save_dmsusers";

    if($u_id){
      my $ref = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        u_id          => "$u_id",
        };
      $ctrel = $dbt->fetch_tablerecord($dbh,$ref);

      print $q->hidden(-name=>'u_id',-override=>'1', -value=>"$ctrel->{u_id}");
    }
 }
 elsif($node_meta->{ct_table} eq "contentuser" && $node_meta->{tpl_id} == 199){
   my $ref = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        template_id     => "$node_meta->{tpl_id}",
        c_id          => "$c_id",
        };
   $ctrel = $dbt->fetch_record($dbh,$ref);
   print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$ctrel->{c_id}");
   print $q->hidden(-name=>'rel_id',-override=>'1', -value=>"$ctrel->{rel_id}");

   $edit = "rel_edit";
   $save_key = "save_ctuser";
   $tpl = $dbt->get_tpl($dbh,$node_meta->{tpl_id});

 }
 elsif($node_meta->{ct_table} eq "contentuser" && $node_meta->{tpl_id} == 194 && $c_id){
   my $ref = {
        table           => "$node_meta->{ct_table}",
        fetch           => "one",
        template_id     => "$node_meta->{tpl_id}",
        c_id          => "$c_id",
        };
   $ctrel = $dbt->fetch_record($dbh,$ref);
   print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$ctrel->{c_id}");
   print $q->hidden(-name=>'rel_id',-override=>'1', -value=>"$ctrel->{rel_id}");

   $edit = "base_edit";
   $save_key = "save_contentcms";
   $tpl = $dbt->get_tpl($dbh,$node_meta->{tpl_id});

 }
 elsif($node_meta->{ct_table} eq "contenttranspos" && $node_meta->{tpl_id} == 221){
   my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
   my $pref = {
        table           => "contenttrans",
        table_pos       => "contenttranspos",
        fetch           => "one",
        c_id            => $c_id,
        };

   print $q->hidden(-name=>'c_id',-override=>'1', -value=>"$c_id");

   $cttpos =  $dbt->collect_post($dbh,$pref);
   $occupied_style = "color:#ff1493" if($cttpos->{int10} == 2 ||$cttpos->{int10} == 3 || $cttpos->{int10} == 6);

   if($cttpos->{int35} && $cttpos->{start_time} && $cttpos->{end_time}){
     ($pricing,$counting) = $pri->counting_rental($varenv,$cttpos);

     my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
     my %varenv_prim = ();
     $varenv_prim{cms} = $dbt->fetch_cms($dbh_primary,{ lang => $q->escapeHTML($lang) });

     $rental_feed = $pri->fetch_rentalfeed(\%varenv_prim,$varenv,$cttpos,$counting);
     $gesamt = $pri->round($pricing->{total_price});
     $gesamt = sprintf('%.2f', $gesamt);
   }else{
     ($gesamt,my $rabatt) = $pri->price2calc($cttpos);
     $gesamt = $pri->round($gesamt);
     $gesamt = sprintf('%.2f', $gesamt);
   }

   $edit = "base_edit";
   $save_key = "save_pos";
   my $tpl_id = $node_meta->{tpl_id};
   $tpl_id = 223 if($cttpos->{template_id} && $cttpos->{template_id} =~ /224|229|219/);
   $tpl = $dbt->get_tpl($dbh,$tpl_id);
 
 }
 else{
   print $q->div("Es konnten keine Daten gefunden werden"),"\n";
   return "failure::Error, no table selected";
 }


 #if(ref($ctrel) ne "HASH" && !$ctrel->{c_id} && !$ctrel->{u_id}){
 #   print $q->div("error: no content available");
 #}

 my $u_name = "";
 my $dbmtime = "";
 my $dbowner = $ctrel->{owner} || $cttpos->{owner_end} || "";
 my $dbmtime = $ctrel->{mtime} || $cttpos->{mtime} || "";
 $dbmtime = $ctrel->{change} if($node_meta->{tpl_id} == 198);
 $u_name = $dbt->sys_username($dbh,$dbowner);
 $dbmtime = $lb->time4de($dbmtime,"1") if($dbmtime);
 my $selsize="200px";

  print $q->hidden(-name=>'parent_id',-override=>'1', -value=>"$node_meta->{parent_id}");
  print $q->hidden(-name=>'template_id',-override=>'1', -value=>"$node_meta->{template_id}");
  print $q->hidden(-name=>'offset',-override=>'1', -value=>"$R::offset");
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit");
  print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids");

  ###

  my $bike_nodes = {};
  my $node = { 
	 template_id => 205,
	 parent_id => 200013,
   	 fetch	=> "all",
	 keyfield => "main_id",
  	 };
  $bike_nodes = $dbt->fetch_rel4tpl4nd($dbh,$node);

  my $station_all = {};
  my $pref_st = { 
	 table => "content",
   	 fetch	=> "all",
	 keyfield => "c_id",
	 template_id => 225,
  	 };
  $station_all = $dbt->fetch_record($dbh,$pref_st);

  my $tariff_all = {};
  my $tariff = {
        table           => "content",
        fetch           => "all",
        keyfield        => "barcode",
        template_id     => "210",#Tariff tpl_id
        };
  $tariff_all = $dbt->fetch_record($dbh,$tariff);

  print "<div id='Container_cms'>\n";
  #1. table submit
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
     print $q->Tr();
     print $q->td({-style=>"background-color:#a7a18f;padding-right:0px;border-bottom: 1px solid silver;"},$but->singlesubmit7("$edit","$save_key","$ib{$save_key}","margin:0 5px;"));
     if($move_key){
       	my @_main_valxx = ();
	foreach my $rid (sort { $bike_nodes->{$a}->{n_sort} <=> $bike_nodes->{$b}->{n_sort} } keys (%$bike_nodes)){
	    push (@_main_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $dbt->{copri_conf}->{type_id}->{$bike_nodes->{$rid}->{type_id}}");
	}
	if(scalar(@_main_valxx) > 1){
       	  print $q->td({-style=>"background-color:#a7a18f;padding-right:10px;border-bottom: 1px solid silver;"},
	       $but->singlesubmit7("$edit","$move_key","$ib{$move_key}","margin:0 5px;"),
	       $but->selector_class("main_id","eselect","",$ctrel->{main_id},@_main_valxx)
       	  );
	}
     }
     print $q->td({-style=>"background-color:#a7a18f;border-bottom: 1px solid silver;text-align:right;font-size:11px;"}, "$u_name / $dbmtime");

     #if($node_meta->{tpl_id} !~ /199|221/){
     if(($node_meta->{tpl_id} =~ /198|205|210|219|224|225|228|229/) || ($node_meta->{tpl_id} == 202 && $varenv->{dbname} eq $dbt->{primary}->{sharee_primary}->{database}->{dbname}) || ($node_meta->{tpl_id} == 194 && !$ctrel->{int11})){
       print $q->td({-style=>"background-color:#a7a18f;padding-right:10px;border-bottom: 1px solid silver;text-align:right;"},$but->singlesubmit7("$edit","remove_chk4rel","$ib{remove_chk4rel}","margin:0 5px;"));
     }
  print $q->end_table;


  ###Terminal target
  my $c_key = "c_id";
  my $add_trans = "add_transpos";
  if("$node_meta->{ct_table}" =~ /contentadr/){
    $add_trans = "add_transadr";
    $c_key = "c_idadr";
  }

  my $tpath = "";
  my $top = 70;
  if($node_meta->{tpl_id} == 202){
   my $pref = {
        table           => "contenttrans",
        fetch           => "all",
	keyfield	=> "c_id",
        template_id     => "IN::(209,218)",
        int10           => "$ctrel->{c_id}",
        };

    my $cttrans = $dbt->fetch_record($dbh,$pref);
    foreach my $id (sort { $cttrans->{$b}->{c_id} <=> $cttrans->{$a}->{c_id} } keys (%$cttrans)){
	my $toppx = $top . "px";
	my $rel_opos = "";
        $rel_opos = "<span class='bi bi-record2' style='color:Maroon;'></span>" if($cttrans->{$id}->{int14});
        $rel_opos = "<span class='bi bi-record2' style='color:Olive;'></span>" if($cttrans->{$id}->{int04} == 7);
  	print $q->div({-style=>"position:absolute;top:$toppx;right:20px;"}, " TXID $cttrans->{$id}->{txt16} ",$q->span({-style=>"background-color:#f7ae37"},$q->a({-class=>"linknav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Faktura/$cttrans->{$id}->{node_name}?ct_trans=open\&c_id4trans=$cttrans->{$id}->{c_id}\&tpl_id4trans=$cttrans->{$id}->{template_id}\&owner=$users_dms->{u_id}",-title=>"Faktura Terminal öffnen"},"[ $rel_opos $cttrans->{$id}->{node_name}  #$cttrans->{$id}->{ct_name} ]"))),"\n";
	$top += 22;
    }
  }elsif($node_meta->{tpl_id} == 205){
	 my $day  = strftime "%d", localtime;
 	my $month  = strftime "%m", localtime;
 	my $year = strftime "%Y", localtime;
 	my ($nyear,$nmonth,$nday) = Add_Delta_YMD($year,$month,$day, 0,0,-28);
	my $toppx = $top . "px";
	print $q->div({-style=>"position:absolute;top:$toppx;right:20px;"}, $q->span({-style=>"background-color:#86cbd7"},$q->a({-class=>"linknav",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Mietjournal/?detail_search=1&barcode=$ctrel->{barcode}&start_date_time=$nday.$nmonth.$nyear",-title=>"Mietjournal der letzten 4 Wochen"},"[ Mietjournal zu Rad $dbt->{operator}->{$varenv->{dbname}}->{oprefix}$ctrel->{barcode} ]"))),"\n";
	$top += 22;
	#}else{
	#print $q->div({-style=>"position:absolute;top:$top;right:20px;"},"no valid users path definition: $tpath"),"\n";
   }
  ###

#just for checking if ct_name or barcode still available
if($varenv->{orga} eq "dms"){
print<<EOF
    <style>
     .ui-autocomplete {
     text-align:left;
     font-size:14px;
     background:#eeeeee;
     border:1px solid silver;
  }
  </style>

  <script>
  \$(function() {       

      \$('#json_select').autocomplete({
            source: '/ajax_json?table=content&template_id=$node_meta->{template_id}&c_id=$ctrel->{c_id}&catch_equal=1',
            minLength: 1,
            response: function(event, ui) {
		if (ui.content.length === 0) {
                    \$('#log').text('neu');
		}else{
                    \$('#log').text('bereits angelegt!');
		}
            }

       });
  });
  </script>
EOF
;
}

  @tpl_order = split /,/,$tpl->{tpl_order};
  #2. table content
  print $q->start_table({-border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
  print "<tr>\n";

   my $j=0;

   #2.1 sub-table
   print "<td style='border-bottom:1px solid silver;border-right:1px solid silver;vertical-align:top;padding:15px;'>\n"; 
   print $q->start_table({-border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
   my @_service_valxx = ("","1","2","3","4");

   foreach (@tpl_order){

    #Mietjournal edit
    if($node_meta->{ct_table} eq "contenttranspos" && $node_meta->{tpl_id} == 221){
       	my ($key,$des,$size,$postdes) = split /=/,$_;
       	$des .= " ($key)" if($users_dms->{u_id} eq $varenv->{superu_id});
	 my $prefix = $cttpos->{txt12};
	 $prefix = "S3X" if($prefix eq "SX");

       	if($key =~ /barcode/){
	 print $q->hidden(-name=>"$key",-override=>1,-value=>"$cttpos->{$key}"),"\n";
	 print $q->hidden(-name=>"ct_id",-override=>1,-value=>"$cttpos->{ct_id}"),"\n";
       	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
         print $q->td({-class=>'content1_cms'}, $q->b("$prefix$cttpos->{$key}")),"\n";
       	}elsif($key =~ /c_id|ct_name|txt08/){
       	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
         print $q->td({-class=>'content1_cms'}, $q->b("$cttpos->{$key}")),"\n";
        }elsif($key =~ /txt/ && $size eq "area"){
	 $cttpos->{$key} = $q->unescapeHTML("$cttpos->{$key}");
         $cttpos->{$key} = $lb->newline($cttpos->{$key},"",1);
       	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
         if($key eq "txt01" && $cttpos->{int09} && $cttpos->{$key} !~ /Manuell/){
           $cttpos->{$key} .= "\nManuell bearbeitet\n";
         }

         print "<td class='content1_cms' style='$occupied_style;'\n>";
	 my $class = "etxt";
	 $class = "etxt2" if($key eq "txt23");
         print $q->textarea(-class=>"$class",-name=>"$key",-default=>"$cttpos->{$key}", -override=>'1',-rows=>"5",-cols=>50),"\n";


         print "</td>\n";
       #if Traiff Nr.
       }elsif($key eq "int09"){
       	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	 #print $q->td({-class=>'content1_cms'}, $q->textfield(-class=>'etxt',-name=>"int09",-default=>"$cttpos->{int09}",-size=>"5",-maxlength=>5), "Text", $q->textfield(-class=>'etxt',-name=>"txt04",-default=>"$cttpos->{txt04}",-size=>"30",-maxlength=>50)),"\n";
         print $q->td({-class=>'content1_cms'}, "$cttpos->{int09} ($cttpos->{txt04})"),"\n";
       }
       #pos start station
       elsif($key eq "int06" && $node_meta->{tpl_id} == 221){
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},"$prefix $cttpos->{int06}");
       }
       #pos end station
       elsif($key eq "int04" && "$size" eq "select" && $node_meta->{tpl_id} == 221){
          my @_valxx = ("null:");
	  my $end_station = "null";
	  $end_station = $cttpos->{int04} if(looks_like_number($cttpos->{int04}));
          foreach my $rid (sort { $station_all->{$a}->{int04} <=> $station_all->{$b}->{int04} } keys (%$station_all)){
            push (@_valxx, "$station_all->{$rid}->{int04}:$station_all->{$rid}->{int04} - $station_all->{$rid}->{txt01}");
          }
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},"$prefix",$but->selector_class("$key","eselect2","width:250px;","$end_station",@_valxx));

       }
       elsif($key eq "int04"){
    	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
         print $q->td({-class=>'content1_cms'}, "$prefix", $q->textfield(-class=>'etxt2',-name=>"int04",-default=>"$cttpos->{int04}",-size=>"15",-maxlength=>40)),"\n";
       #End GPS
       }elsif($key eq "txt06"){
         print $q->Tr(),"\n";
         print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	 print $q->td({-class=>'content1_cms'}, "$cttpos->{$key}"),"\n";

       }elsif($key eq "date_time"){
         my ($s_yy,$s_mo,$s_dd,$s_hh,$s_mi) = $lb->split_date($pricing->{start_time});
         my ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi) = $lb->split_date($pricing->{end_time});
         print $q->Tr(),"\n";
         print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
           print $q->td({-class=>'content1_cms'},
           $q->textfield(-id=>'datepicker3',-class=>'etxt',-name=>"start_date",-default=>"$s_dd.$s_mo.$s_yy",-size=>"10",-maxlength=>10),
           $q->textfield(-class=>'etxt',-name=>"s_hh",-default=>"$s_hh", -override=>'1',-size=>"2",-maxlength=>2),":",
           $q->textfield(-class=>'etxt',-name=>"s_mi",-default=>"$s_mi", -override=>'1',-size=>"2",-maxlength=>2)," &rarr; ",
           $q->textfield(-id=>'datepicker4',-class=>'etxt2',-name=>"end_date",-default=>"$e_dd.$e_mo.$e_yy",-size=>"10",-maxlength=>10),
           $q->textfield(-class=>'etxt2',-name=>"e_hh",-default=>"$e_hh", -override=>'1',-size=>"2",-maxlength=>2),":",
           $q->textfield(-class=>'etxt2',-name=>"e_mi",-default=>"$e_mi", -override=>'1',-size=>"2",-maxlength=>2)),"\n";

       }elsif($key =~ /int10/ && "$size" eq "select"){#bike_state
	  my @_lock_valxx = ();
          foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{bike_state} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{bike_state}->{$s_key}" if($s_key =~ /1|2|3|7/);
          }
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect2","",$cttpos->{$key},@_lock_valxx));
 
       }elsif($key =~ /int20/ && "$size" eq "select"){#lock_state locked/unlocked
	  my @_lock_valxx = ();
          foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{lock_state} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{lock_state}->{$s_key}";
          }
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect2","",$cttpos->{$key},@_lock_valxx),"$postdes");

       }elsif($key eq "int26"){
         if($cttpos->{int26}){
       	   print $q->Tr(),"\n";
	   print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
           my $co2saving = "";
              my $co2diff = $pri->co2calc($cttpos);
	      #my $sprit_price = $pri->sprit2calc($cttpos);
              $co2saving .= "$co2diff kg CO&sup2;<br />";
	      #$co2saving .= "$sprit_price EUR<br />" if($sprit_price !~ /-/);
              $cttpos->{int26} =~ s/\./,/;
              $co2saving .= "bei $cttpos->{int26} KM";
            print $q->td({-class=>'content1_cms'},"$co2saving");
         }
        }elsif($key =~ /int03/){
       	  print $q->Tr(),"\n";
	  print $q->td({-class=>'left_italic_cms',-nowrap=>"1"},"$des"),"\n";
          if($cttpos->{int35} && $pricing->{start_time} && $pricing->{end_time}){
            print $q->td({-class=>'content1_cms', -nowrap=>1},"$pricing->{real_clock} $pricing->{freed_time}"),"\n";
          }else{
            print $q->td({-colspan=>'1',-class=>'content1_cms'},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$cttpos->{$key}", -override=>'1',-size=>"$size",-maxlength=>10)),"\n";
          }
        }elsif($key =~ /int02/){
       	  print $q->Tr(),"\n";
	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          if($cttpos->{int35} && $pricing->{start_time} && $pricing->{end_time}){
            print "<td class='content1_cms' nowrap>\n";
            foreach my $fid (sort keys(%{ $rental_feed->{rental_description}->{tarif_elements} })){
              if(ref($rental_feed->{rental_description}->{tarif_elements}->{$fid}) eq "ARRAY"){
                print "$rental_feed->{rental_description}->{tarif_elements}->{$fid}[0]: $rental_feed->{rental_description}->{tarif_elements}->{$fid}[1]<br />\n";
              }
            }
            print "</td>\n";
          }else{
            print $q->td({-class=>'content1_cms',-style=>""},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$cttpos->{$key}", -override=>'1',-size=>"$size",-maxlength=>100),"\n");
          }
        }elsif($key =~ /int07/){
       	  print $q->Tr(),"\n";
	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-style=>"",-nowrap=>'1'},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$cttpos->{$key}", -override=>'1',-size=>"6",-maxlength=>6),$but->selector("int08","40px","$cttpos->{int08}",("0:%","1:€"))),"\n";
        }elsif($key =~ /int01/){
       	 print $q->Tr(),"\n";
	 print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
         print $q->td({-class=>'content1_cms',-nowrap=>"1"},"$gesamt €");
 	}elsif($key =~ /txt20|txt24|txt25|txt29/ && $size eq "readonly"){
	 if($cttpos->{$key} && $users_dms->{int03} >= 1){
	  $cttpos->{$key} = $q->unescapeHTML("$cttpos->{$key}");
          $cttpos->{$key} = $lb->newline($cttpos->{$key},"","");
       	  print $q->Tr(),"\n";
	  print $q->td({-class=>'content1_cms',-colspan=>2},"<span style='font-style:italic;'>$des</span> $cttpos->{$key}"),"\n";
	 }
        }elsif($key =~ /txt/){
       	  print $q->Tr(),"\n";
	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms'},$q->textfield(-class=>'etxt',-name=>"$key",-default=>"$cttpos->{$key}", -override=>'1',-size=>"$size",-maxlength=>100)),"\n";
        }
	#only if invoice isn't booked
	if($key eq "txt23" && $cttpos->{ca_id} && $cttpos->{ct_id} && !$cttpos->{state}){
       	  print $q->Tr(),"\n";
  	  print $q->td({-class=>'left_italic_cms'}, $q->a({-class=>"linknav4",-href=>"/$dbt->{shareedms_conf}->{parent_node}/Waren/Gebühren?c_id4trans=$cttpos->{ct_id}\&owner=$users_dms->{u_id}",-title=>""},"Gebühr hinzufügen")),"\n";
	  print $q->td({-class=>'content1_cms'},"&nbsp;"),"\n";
	}

    }
    #service-config
    elsif($node_meta->{ct_table} eq "contentuser" && $node_meta->{tpl_id} == 199){
     my ($key,$tplkey,$des,$size,$service_interval,$service_type) = split /=/,$_;
     $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
     $ctrel->{$key} = $lb->newline($ctrel->{$key},"",1);

     if($key =~ /ct_name/){
       print $q->Tr(),"\n";
       print $q->td({-class=>'content1_cms',-colspan=>3},$q->b("$ctrel->{ct_name}")),"\n";
     }
     elsif($key =~ /txt\d/ && $tplkey =~ /int\d/){
    	$j++;
     	my $desc_num = "$des $j";
     	$desc_num .= " ($key:$tplkey)" if($users_dms->{u_id} eq $varenv->{superu_id});
	my $desc_key = "";
	my $desc_name = "";
	my $desc_size = "";
	my $desc_intervall = "";
	my $desc_type = "";
	($desc_key,$desc_name,$desc_size,$desc_intervall,$desc_type) = split(/=/,$ctrel->{$key});
       	print $q->Tr(),"\n";
       	print $q->hidden(-name=>"$key",-override=>1,-value=>"$tplkey"),"\n";
       	print $q->td({-class=>'content1_cms'},"$desc_num",$q->textfield(-class=>'etxt', -style=>"width:15em;",-name=>"$key", -default=>"$desc_name", -override=>'1')),"\n";
       	print $q->hidden(-name=>"$key",-override=>1,-value=>"checkbox"),"\n";
       	print $q->td({-class=>'content1_cms'},"Intervall",$q->textfield(-class=>'etxt', -style=>"width:2em;",-name=>"$key", -default=>"$desc_intervall", -override=>'1'),"Tage"),"\n";
       	print $q->td({-class=>'content1_cms'},$but->radiobox2("$key","$desc_type","","weich","hart")),"\n";
     }
    }
    #dms-users
    elsif($node_meta->{ct_table} eq "users" && $node_meta->{tpl_id} == 198){
     my ($key,$des,$size,$postdes) = split /=/,$_;
     $des .= " ($key)" if($users_dms->{u_id} eq $varenv->{superu_id});

     $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
     $ctrel->{$key} = $lb->newline($ctrel->{$key},"",1);
     my $ctadr = { c_id => 0 };
     if($ctrel->{u_id}){
   	  my $adref = {          
            table           => "contentadr",
            fetch           => "one",
            template_id     => "202",
            c_id          => $ctrel->{u_id},
          };
          $ctadr = $dbt->fetch_record($dbh,$adref);
      }

      if($key eq "u_id"){
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>2}, "$ctrel->{$key}"),"\n";
      }elsif($key =~ /^txt\d+/){
       if($size eq "select_multiple"){ #user defined service_tour alias user_tour
	  my $height = scalar(@_service_valxx);
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector2("$key","50px;","$height",$ctrel->{$key},@_service_valxx));
       }
       elsif($size =~ /area/){
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms',-colspan=>1},"$des"),"\n";
        print $q->td({-class=>'content1_cms', -colspan=>'3'},$q->textarea(-class=>"etxt", -style=>'border: 1px solid silver; height:7em; width:25em;', -name=>"txt11", -override=>'1',-default=>"$ctrel->{txt11}")),"\n";
        
       }
       else{
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>2},$q->textfield(-class=>'etxt', -style=>"width:15em;",-name=>"$key", -default=>"$ctadr->{$key}")),"\n";
       }
      }
      elsif($key =~ /int/ && $size =~ /\w\+\w/){
	if($des =~ /Waren/){
 	  print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>1},$q->b("DMS Zugriffsrechte")),"\n";
	}
        my ($a,$b,$c) = split /\+/,$size;
 	print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>1},$but->radiobox2("$key","$ctrel->{$key}","$a","$b","$c")),"\n";
      }
    }
    #defaults to content and contentadr edit
    else{
     my ($key,$des,$size,$postdes) = split /=/,$_;
     my $w = $size . "em";
     $w = "15em" if($size eq "readonly");
     my $seldes = $des;
     $des .= " ($key)" if($users_dms->{u_id} eq $varenv->{superu_id});

     if($key =~ /txt25/ && $node_meta->{tpl_id} == 225 && $size eq "readonly"){ 
	  my %station_filter_hash = ();
	  if($ctrel->{txt25} && $ctrel->{txt25} =~ /\d\s\d/){
     		%station_filter_hash = map { $_ => 1 } split(/\s+/,$ctrel->{txt25});
   	  }elsif($ctrel->{txt25} && $ctrel->{txt25} =~ /(\d+)/){
     		$station_filter_hash{$1} = 1;
   	  }
	  my $station_filter = "";
   	  foreach my $type_id (keys (%station_filter_hash)){
		$station_filter .= "$dbt->{copri_conf}->{type_id}->{$type_id}<br />";
          }

	  print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$station_filter); 
      }
      elsif($key =~ /c_id|ct_name|barcode|txt/ && $size eq "readonly"){
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>2},$q->textfield(-class=>'etxt', -style=>"width:$w;color:grey;",-name=>"$key", -default=>"$ctrel->{$key}", -readonly=>1)),"\n";
      }
      elsif($key eq "ct_name"){
	#blank Teilenummer
	my $ct_name = $ctrel->{$key};
 	if(($node_meta->{ct_table} eq "content") && ($tpl->{tpl_order} =~ /barcode/) && ("$ct_name" eq "$ctrel->{barcode}")){
          $ct_name = "";
	}
	my $oprefix = "";
	$oprefix = "$dbt->{operator}->{$varenv->{dbname}}->{oprefix}-" if($node_meta->{tpl_id} == 224 || $node_meta->{tpl_id} == 228);
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	#CMS Textkey readonly
	if($node_meta->{tpl_id} == 194 && $ctrel->{int11}){
          print $q->td({-class=>'content1_cms',-colspan=>2}, "$oprefix", $q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1',-default=>"$ct_name",-readonly=>1),$postdes),"\n";
	}else{
          print $q->td({-class=>'content1_cms',-colspan=>2}, "$oprefix", $q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1',-default=>"$ct_name"),$postdes),"\n";
	}
      }
      elsif($key eq "barcode"){
	my $oprefix = "";
 	$oprefix = "$dbt->{operator}->{$varenv->{dbname}}->{oprefix}" if($node_meta->{tpl_id} == 205);
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>1}, "$oprefix", $q->textfield(-class=>'etxt',-style=>"width:$w;",-id=>"json_select",-name=>"$key",-value=>"$ctrel->{$key}", -override=>'1',-size=>"25",-maxlength=>50),$q->span({-id=>'log'},"")),"\n";
      }
       elsif($key =~ /int/ && $size =~ /\w\+\w/ && $postdes eq "reverse"){
          my ($a,$b) = split /\+/,$size;
	  print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>1},$but->radiobox2reverse("$key","$ctrel->{$key}","$a","$b")),"\n";
	}
       elsif($key =~ /int/ && $size =~ /\w\+\w/){
 	
	   #Ack digest
	  my $ack_code = "";
	  if($node_meta->{tpl_id} == 202 && $ctrel->{txt34}){
 	   my $ack_digest = $q->unescapeHTML($ctrel->{txt34});
	   if($key eq "int04"){
 	     $ack_code = " ($1)" if($ack_digest =~ /^(.{5})/);
	   }elsif($key eq "int13"){
 	     $ack_code = " ($1)" if($ack_digest =~ /(.{5})$/);
	   }
	  }
          my ($a,$b,$c) = split /\+/,$size;
	  print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>1},$but->radiobox2("$key","$ctrel->{$key}","$a","$b","$c"), $ack_code),"\n";
       }	
       elsif($key =~ /int/ && $size =~ /checkbox/){
	  print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	  print $q->td({-class=>'content1_cms', -colspan=>'1'}, $but->checkbox("1","$key","$ctrel->{$key}"), $postdes),"\n";
 	  print $q->hidden(-name=>"$key",-override=>1,-value=>"null");
       }
       #bike station 
       elsif($key eq "int04" && "$size" eq "select" && $node_meta->{tpl_id} == 205){
          my @_valxx = ();
          foreach my $rid (sort { $station_all->{$a}->{int04} <=> $station_all->{$b}->{int04} } keys (%$station_all)){
            push (@_valxx, "$station_all->{$rid}->{int04}:$station_all->{$rid}->{int04} - $station_all->{$rid}->{txt01}");
          }
	  my $oprefix = "$dbt->{operator}->{$varenv->{dbname}}->{oprefix}";
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},"$oprefix",$but->selector_class("$key","eselect","width:250px;",$ctrel->{int04},@_valxx));

       }
       elsif($key eq "int04"){
	  my $oprefix = "";
 	  $oprefix = "$dbt->{operator}->{$varenv->{dbname}}->{oprefix}" if($node_meta->{tpl_id} == 225 || $node_meta->{tpl_id} == 205);
	  print $q->Tr();
	  print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>1}, "$oprefix", $q->textfield(-class=>'etxt',-style=>"width:$w;",-name=>"$key",-value=>"$ctrel->{$key}", -override=>'1',-size=>"25",-maxlength=>50),"");
       }
       elsif($key =~ /int12/ && "$size" eq "select" && $node_meta->{tpl_id} == 210){#Flotte bike_group (bikenode.main_id)
	  my @_valxx = ("");
     	  foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
      	    push (@_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name} - $dbt->{copri_conf}->{type_id}->{$bike_nodes->{$rid}->{type_id}}");
      	  }	
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector_class("$key","eselect","width:250px;",$ctrel->{$key},@_valxx));
       }

       elsif($key =~ /int21|int22/ && "$size" eq "select" && $node_meta->{tpl_id} == 228){#Bonus Tarif
	  my @_valxx = ("");
     	  foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
      	    push (@_valxx, "$tariff_all->{$rid}->{barcode}:$tariff_all->{$rid}->{barcode} - $tariff_all->{$rid}->{ct_name}");
      	  }	
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector_class("$key","eselect","width:350px;",$ctrel->{$key},@_valxx));
       }
       elsif($key =~ /int10/ && "$size" eq "select" && $node_meta->{tpl_id} == 205){#bike_state
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	if($occupied_style){
          print $q->td({-class=>'content1_cms',-colspan=>'2'},"$dbt->{copri_conf}->{bike_state}->{$cttpos->{$key}}", $q->span({-style=>"$occupied_style"},"Das Rad ist in Benutzung. Der Mietvorgang kann nur über das Mietjournal beendet werden")),"\n";
  	}else{
	  my @_lock_valxx = ();
          foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{bike_state} })) {
           push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{bike_state}->{$s_key}";
          }
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_lock_valxx));
	}
       } 
       elsif($key =~ /int10/ && "$size" eq "select" && $node_meta->{tpl_id} == 225){#station_state
	my @_lock_valxx = ();
        foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{station_state} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{station_state}->{$s_key}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_lock_valxx));
       } 

       elsif($key =~ /int18/ && "$size" eq "select" && $node_meta->{tpl_id} == 210){#sharing_type
	my @_lock_valxx = ();
	#delete hidden-lv if not LV
	delete $dbt->{copri_conf}->{sharing_type}->{4} if($varenv->{syshost} ne "shareedms-lv");
        foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{sharing_type} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{sharing_type}->{$s_key}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_lock_valxx));
       } 
       elsif($key =~ /int20/ && "$size" eq "select" && $node_meta->{tpl_id} == 205){#lock_state locked/unlocked
	my @_lock_valxx = ();
	foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{lock_state} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{lock_state}->{$s_key}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_lock_valxx),"$postdes");
       } 
       elsif($key =~ /int11/ && "$size" eq "select" && $node_meta->{tpl_id} == 205){#lock_system BC Ilockit usw
	my @_lock_valxx = ();
        foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{lock_system} })) {
            push @_lock_valxx, "$s_key:$dbt->{copri_conf}->{lock_system}->{$s_key}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_lock_valxx));
       } 
       elsif($key =~ /int16/ && "$size" eq "select" && $node_meta->{tpl_id} == 229){
	my @_fee_valxx = ();
        foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{fee_type} })) {
            push @_fee_valxx, "$s_key:$dbt->{copri_conf}->{fee_type}->{$s_key}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_fee_valxx));
       } 
       #payment_type by select
       elsif($node_meta->{tpl_id} == 202 && $key eq "int03" && $size eq "select"){
         my @_payment_type = ();
         foreach my $s_key (sort keys (%{ $dbt->{copri_conf}->{payment_type} })) {
           push @_payment_type, "$s_key:$dbt->{copri_conf}->{payment_type}->{$s_key}";
         }
         print $q->Tr();
         print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	 #print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector_class("$key","eselect","",$ctrel->{$key},@_payment_type));
	 print $q->td({-class=>'content1_cms',-colspan=>2},$q->textfield(-class=>'etxt', -style=>"width:$w;color:grey;",-name=>"intbyhidden", -default=>"$dbt->{copri_conf}->{payment_type}->{$ctrel->{$key}}", -readonly=>1)),"\n";
 	  print $q->hidden(-name=>"$key",-override=>1,-value=>"$ctrel->{$key}");
       }

       elsif($key =~ /int/){
	$ctrel->{$key} = "75" if($key eq "int06" && !$ctrel->{$key} && $node_meta->{tpl_id} == 225);
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>2},$q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1', -default=>"$ctrel->{$key}"), $postdes),"\n";
       }
       elsif($key =~ /txt/ && "$size" eq "select" && "$des" =~ /Land/){
        my @_valxx;
        my $selsize="150px";
        my $country = $lb->country_code();
	my $country_all = $lb->country_code_all();
        $ctrel->{$key} = "DE" if(!$ctrel->{$key});
        foreach (sort { $country->{$a} cmp $country->{$b} } keys (%$country)){
          push @_valxx, "$_:$country->{$_}";
        }
	foreach (sort { $country_all->{$a} cmp $country_all->{$b} } keys (%$country_all)){
          push @_valxx, "$_:$country_all->{$_}";
        }
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
        print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector("$key","$selsize",$ctrel->{$key},@_valxx));
       }
       elsif($node_meta->{tpl_id} == 205 && $key =~ /txt23/ && "$size" =~ /select/){#Service-Farbcode
	 my @_valxx = ("red","blue","green");
	 my @service_code = ("red","red","red","red","red","red");
	 @service_code = split(/\s/,$ctrel->{$key}) if($ctrel->{$key} =~ /\w+\s\w+\s\w+\s\w+\s\w+\s\w+/g);
	 print $q->Tr();
         print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
         print "<td class='content1_cms' colspan='1'>\n";
	    foreach(@service_code){
		 print $but->selector_color("$key","color:white;background-color:$_;width:60px;",$_,@_valxx);
	    }
	 print "</td>\n";
       }
       elsif($key =~ /txt/ && "$size" =~ /select/){
  	if($size =~ /_multiple/){
	 if($key =~ /txt07/ && $node_meta->{tpl_id} eq "225"){ # station defined Service Tour 
	  my $height = scalar(@_service_valxx);
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector2("$key","50px;","$height",$ctrel->{$key},@_service_valxx));
         }
         elsif($key =~ /txt24/ && $node_meta->{tpl_id} == 225){ #Station on station_group (bikenode.main_ids)
          my @_valxx = ("");
          foreach my $rid (sort { $bike_nodes->{$a}->{node_name} cmp $bike_nodes->{$b}->{node_name} } keys (%$bike_nodes)){
	    push (@_valxx, "$bike_nodes->{$rid}->{main_id}:$bike_nodes->{$rid}->{node_name}");
          }

	  my $height = scalar(@_valxx);
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector2("$key","250px;","$height",$ctrel->{$key},@_valxx));
         }

		 #sharee user_group Tarif-2.0 (tarif.c_ids)
         elsif($key =~ /txt30/ && $varenv->{dbname} ne $dbt->{primary}->{sharee_primary}->{database}->{dbname}){ 
          my @_valxx = ("");
          foreach my $rid (sort { $tariff_all->{$a}->{barcode} <=> $tariff_all->{$b}->{barcode} } keys (%$tariff_all)){
	    if($tariff_all->{$rid}->{ct_name}){
              push (@_valxx, "$tariff_all->{$rid}->{barcode}:$tariff_all->{$rid}->{barcode} $dbt->{copri_conf}->{sharing_type}->{$tariff_all->{$rid}->{int18}} - $tariff_all->{$rid}->{ct_name} - $bike_nodes->{$tariff_all->{$rid}->{int12}}->{node_name}");
    	    }
          }
	  my $height = scalar(@_valxx);
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-style=>'vertical-align:top;',-colspan=>'1'},"$des");
          print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector2("$key","350px;","$height",$ctrel->{$key},@_valxx));
         }
        }
	#end _multiple select
	else{
         #Operator letzt Auswahl/Zuweisung
         if($key eq "txt19" && $node_meta->{tpl_id} == 202){
	  if($varenv->{dbname} eq $dbt->{primary}->{sharee_primary}->{database}->{dbname}){
	    my @_valxx = ("");
	    foreach my $key (sort keys  (%{ $dbt->{operator} })) {
		push (@_valxx,$key);
	    }
            print $q->Tr();
            print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	    print $q->td({-class=>'content1_cms',-colspan=>'1'},$but->selector("$key","100px;",$ctrel->{$key},@_valxx),$postdes);
 	    print $q->hidden(-name=>"txt17",-override=>1,-value=>"$ctrel->{txt17}");#used operators for loop
	  }
         }
	 else{
          my @_valxx = ("");
	  @_valxx = split(/,/,$varenv->{$seldes}) if($varenv->{$seldes});
          my $selsize = "80px";
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$but->selector("$key","$selsize",$ctrel->{$key},@_valxx));
	 }
	}
       }
       elsif($key =~ /txt/ && $size =~ /area$/){
        $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
        $ctrel->{$key} = $lb->newline($ctrel->{$key},"",1);
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms',-colspan=>1},"$des"),"\n";
	print $q->td({-class=>'content1_cms', -colspan=>'3'},$q->textarea(-class=>"etxt", -style=>'border: 1px solid silver; height:3em; width:25em;', -name=>"$key", -override=>'1',-default=>"$ctrel->{$key}")),"\n";
       }
       elsif($key =~ /txt/ && $size =~ /area(\d+)/){
        $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
        $ctrel->{$key} = $lb->newline($ctrel->{$key},"",1);

	#my $h= $1 if($size =~ /area(\d+)/);
        my $h= "height:" . $1 . "em;";
        my $w = "width:25em;";
        $w= "width:" . $2 . "em;" if($size =~ /area(\d+)-(\d+)/);
	print $q->Tr();
	print $q->td({-class=>'left_italic_cms',-colspan=>1},"$des"),"\n";
	if($key eq "txt28" && $ctrel->{$key} =~ /SEPA-Lastschriftmandat/){
           use URI::Encode qw(uri_encode uri_decode);
	   $ctrel->{txt28} = uri_decode($ctrel->{txt28});
	   $ctrel->{txt28} =~ s/\+/ /g;
	   #print $q->td({-style=>'border:1px solid silver;'},$ctrel->{txt28}),"\n";
	   #}else{
	}
	print $q->td({-class=>'content1_cms', -colspan=>'3'},$q->textarea(-id=>"$key", -class=>"etxt", -style=>"border: 1px solid silver;$h $w", -name=>"$key", -override=>'1',-default=>"$ctrel->{$key}")),"\n";
       }
       elsif($key =~ /txt15/ && $varenv->{dbname} ne $dbt->{primary}->{sharee_primary}->{database}->{dbname} && $node_meta->{tpl_id} == 202){
	  my $bonus_record = { c_id => 0, ct_name => "" };

	  #check after saving to see if matches
	  if($varenv->{dbname} eq "sharee_kn"){
	   $bonus_record->{ct_name} = $lb->grep_filecontent("$dbt->{copri_conf}->{basedir}/$dbt->{operator}->{$varenv->{dbname}}->{dir_app}/ftp/SWK_codes/got_last.csv","$ctrel->{txt15}") if($ctrel->{txt15});
	  }
	  if(!$bonus_record->{ct_name}){
	   my $pref_cc = {
                  table           => "content",
                  fetch           => "one",
                  template_id     => "228",
                  ct_name         => "ilike::$ctrel->{txt15}",
           };
           $bonus_record = $dbt->fetch_record($dbh,$pref_cc) if($ctrel->{txt15});
  	  }
	
 	  my @tariff = ("$ctrel->{txt30}");
          @tariff = split(/\s/,$ctrel->{txt30}) if($ctrel->{txt30} =~ /\s/);
          foreach my $tf_id (@tariff){
            $tf_id =~ s/\s//g;
	    my $sharing_type = "public-bonus";
	    $sharing_type = $dbt->{copri_conf}->{sharing_type}->{$tariff_all->{$tf_id}->{int18}};
            if($bonus_record->{int22} && $tf_id && $bonus_record->{int22} == $tf_id){
	      $postdes .= "*code aktiviert $sharing_type Tarif Nr. $bonus_record->{int22}. ";
	    }
	    if((!$ctrel->{txt15} || $bonus_record->{ct_name} !~ /$ctrel->{txt15}/i) && $tf_id && $tariff_all->{$tf_id}->{int18} != 2){
	      $postdes .= $q->span({-style=>'color:red;padding-left:10px;'},"Achtung, $sharing_type Tarif $tf_id ohne *code aktiviert. ");
    	    }
	  }
	  if($ctrel->{txt15} && $bonus_record->{ct_name} !~ /$ctrel->{txt15}/i){
	    $postdes .= $q->span({-style=>'color:red;padding-left:10px;'},"*code ist nicht vorhanden! ");
    	  }elsif($ctrel->{txt15} && $bonus_record->{ct_name} eq $ctrel->{txt15}){
	    my $sharing_type = "public-bonus";
	    $sharing_type = $dbt->{copri_conf}->{sharing_type}->{$tariff_all->{$bonus_record->{int22}}->{int18}} if($bonus_record->{int22});
	    $postdes .= "Ok, $sharing_type *code vorhanden.";
	  }
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1', -default=>"$ctrel->{$key}"), $postdes),"\n";
       }
       elsif($key =~ /txt/ && $key !~ /txt12|txt15/){
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
          print $q->td({-class=>'content1_cms',-colspan=>'2'},$q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1', -default=>"$ctrel->{$key}"), $postdes),"\n";
       }
       elsif($key =~ /byte/){
          my $K_int = unpack "H*", $ctrel->{$key};
	  #$K_int =~ s/(.)/sprintf( "%x", ord($1))/eg;
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>1},"$des"),"\n";
	  print $q->td({-class=>'content1_cms',-colspan=>'2'},$q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1', -default=>"$K_int"), $postdes),"\n";
       }
       elsif($key =~ /time\d+/){
	  $ctrel->{$key} =~ s/:00$//;
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms',-colspan=>1},"$des"),"\n";
	  print $q->td({-class=>'content1_cms',-colspan=>'2'},$q->textfield(-class=>'etxt', -style=>"width:$w;",-name=>"$key", -override=>'1', -default=>"$ctrel->{$key}"), $postdes),"\n";
	}

       if($key =~ /date_time/){
        my ($s_yy,$s_mo,$s_dd,$s_hh,$s_mi) = $lb->split_date($ctrel->{start_time});
        my ($e_yy,$e_mo,$e_dd,$e_hh,$e_mi) = $lb->split_date($ctrel->{end_time});

	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"Start"),"\n";
	print $q->td({-class=>'content1_cms', -colspan=>'1'},
	$q->textfield(-id=>'datepicker3',-class=>'etxt',-name=>"start_date",-default=>"$s_dd.$s_mo.$s_yy",-override=>'1',-size=>"10",-maxlength=>10),
	$q->textfield(-class=>'etxt',-name=>"s_hh", -override=>'1',-default=>"$s_hh",-size=>"2",-maxlength=>2),":",
	$q->textfield(-class=>'etxt',-name=>"s_mi", -override=>'1',-default=>"$s_mi",-size=>"2",-maxlength=>2)),"\n";

	print $q->Tr();
	print $q->td({-class=>'left_italic_cms'},"Ende"),"\n";
	print $q->td({-class=>'content1_cms', -colspan=>'1'},
	$q->textfield(-id=>'datepicker4',-class=>'etxt',-name=>"end_date",-default=>"$e_dd.$e_mo.$e_yy",-override=>'1',-size=>"10",-maxlength=>10),
	$q->textfield(-class=>'etxt',-name=>"e_hh", -override=>'1',-default=>"$e_hh",-size=>"2",-maxlength=>2),":",
	$q->textfield(-class=>'etxt',-name=>"e_mi", -override=>'1',-default=>"$e_mi",-size=>"2",-maxlength=>2)),"\n";
       }
     }#end else
   }
   print $q->end_table;
   print "</td>\n";
  print "</tr>\n";

  print $q->end_table;
  #2.table ende

  my $legend = "";
  $legend = "<span style='color:#FF5F1F;'>&bull; </span> Orange markierte Angaben bitte prüfen und ggf. korrigieren." if($node_meta->{tpl_id} == 221);
  print $q->div($but->singlesubmit7("$edit","$save_key","$ib{$save_key}","margin:10px 5px;"), "$legend"),"\n";

  if($node_meta->{tpl_id} == 194){
        print $q->div({-style=>'font-size:13px;'},"<b>Text Variablen Legende:</b><br />
		--subject-- --subject-- &rarr; eMail Betreff<br />
		::user_name:: &rarr; Kunden Name<br />
		::app_name:: &rarr; App Name<br />
		::prepaid_id:: &rarr; Prepaid-ID (Verwendungszweck)<br />
		::invoice_nr:: &rarr; Faktura Beleg Nr<br />
		::total_sum:: &rarr; Rechnungsbetrag Summe €<br />
		::invoice_name:: &rarr; Faktura PDF Name<br />
		::txid:: &rarr; Payone TXID<br />
		::bike:: &rarr; Mietrad (PrefixNummer)<br />
		::total_price:: &rarr; Mietpreis €<br />
		::signature:: &rarr; eMail-footer Signature<br />
		::konrad_signature:: &rarr; konrad eMail-footer Signature<br />
		"),"\n";
  }

  my $debug = "";
  $debug = "(ct_table: $node_meta->{ct_table} | main_id: $node_meta->{main_id} | c_id: $ctrel->{c_id} | tpl_id: $tpl->{tpl_id} | rel_id: $ctrel->{rel_id})";
 print $q->div({-style=>'z-index:10;font-size:13px;'},"$debug"),"\n" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});

 print "</div>";
}
1;
