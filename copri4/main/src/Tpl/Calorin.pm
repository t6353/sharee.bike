package Calorin;
#
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use Scalar::Util qw(looks_like_number);
use Calendar::Simple;
use Date::Calc qw(:all);
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::Callib;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $feedb = shift || {};

 my $q = new CGI;
 my $cf = new Config;
 my $lb = new Libenz;
 my $but = new Buttons;
 my $cb = new Callib;
 my $apif = new APIfunc;
 my $dbt = new DBtank;
 my $pri = new Pricing;

 my %varenv = $cf->envonline();
 my %ib = $but->ibuttons();
 my $path = $q->path_info();
 my $table = "contenttrans";
 my $u_name = $q->escapeHTML("$R::u_name");
 my $dbh = "";

 my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
 my @tpl_order = ('c_id=ID=5=Datensatz ID (nur intern)','date_time=Mietzeit','txt08=Name=15','int06=Start Station=5','int04=End Station=5','barcode=Bike Nr.=5','int12=Group=select','int10=Rental State=select','int20=Lock State=select','owner=User=select','txt23=Comment=15');
 push (@tpl_order, "txt24=debug,log,device,HW") if($users_dms->{int03} >= 1);

 my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
 my $users_dms_primary = { u_id => 0 };
 $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"");

  my $ct_users = $dbt->users_map($dbh,"");
  my $channel_map = $dbt->channel_map();

  my @_users = (":user");
  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     push (@_users, "$id:$channel_map->{$id}");
     if($channel_map->{$id} eq $R::s_owner){
        #$searchref->{owner} = $id;
        #$s_u_name = $channel_map->{$id};
     }
  }

  if(1==1){
   foreach my $ctu_id (sort { $ct_users->{$a}->{txt01} cmp $ct_users->{$b}->{txt01} } keys (%$ct_users)){
     push (@_users, "$ct_users->{$ctu_id}->{c_id}:$ct_users->{$ctu_id}->{txt01}");
     if($ct_users->{$ctu_id}->{ct_name} && ($ct_users->{$ctu_id}->{txt01} =~ /$R::s_owner/i)){
        #$searchref->{owner} = $ct_users->{$ctu_id}->{c_id};
        #$s_u_name = $ct_users->{$ctu_id}->{txt01};
     }
   }
  }

  my $bnode = {
        parent_id => $dbt->{shareedms_conf}->{waren},
        fetch  => "all",
        keyfield => "main_id",
	content_id => "null",
        };
  my $part_nodes = $dbt->fetch_rel4tpl4nd($dbh,$bnode);

 my $pref_ct = {
     table => "contenttrans",
     fetch => "all",
     keyfield => "c_id",
     template_id => "IN::(209,218,219)",
 };
 my $ct4rel = { c_id => 0 };
 $ct4rel = $dbt->fetch_record($dbh,$pref_ct);

 my $pref_cc = {
     table => "content",
     fetch => "all",
     keyfield => "c_id",
     template_id => "205",
 };
  my $record_part = { c_id => 0 };
  $record_part = $dbt->fetch_record($dbh,$pref_cc);


 my $pref_st = {
        table           => "content",
        fetch           => "all",
        keyfield        => "int04",
        template_id     => "225",
        };
 my $record_st = { c_id => 0 };
 $record_st = $dbt->fetch_record($dbh,$pref_st);

 my @months = $cb->monthmap();

 my $hh = 0;
 my $mm = 0;
 my $day = strftime "%d", localtime;
 my $day_today = $day;
 my $mon = strftime "%m", localtime;
 my $mon_today = $mon;
 my $year = strftime "%Y", localtime;
 my $year_today = $year;
 ($year,$mon,$day,$hh,$mm) = $lb->split_date($users_dms->{cal_start}) if($users_dms->{cal_start});

 print $q->end_form;
 print $q->start_form();

 print "<div id='Content4cal' style='min-width: 1450px;'>\n";

  my $start_date_time = $R::start_date_time;
  my $end_date_time = $R::end_date_time; 

  if(!$start_date_time){
    my ($nyear,$nmon,$nday) = Add_Delta_YMD($year,$mon,1, 0,0,-10);
    $start_date_time = "$nday.$nmon.$nyear";
  }
  

  my $c_date; my $start_chck=0;my $end_chck=0;my $message;
  if($start_date_time){
    ($start_date_time,$start_chck) = $lb->checkdate($start_date_time) if($start_date_time ne "%");
    print $q->div({-style=>'background-color:white;color:red;'},"&rarr; Datum Eingabefehler: $start_date_time <<<") if($end_chck);
  }
  if($end_date_time){
    ($end_date_time,$end_chck) = $lb->checkdate($end_date_time) if($end_date_time ne "%");
    print $q->div({-style=>'background-color:white;color:red;'},"&rarr; Datum Eingabefehler: $end_date_time <<<") if($end_chck);
  }

  if(!$end_date_time){
    #my $days4month = Days_in_Month($year,$mon);
    #$end_date_time = "$days4month.$mon.$year";
    my ($nyear,$nmon,$nday) = Add_Delta_YMD($year,$mon,1, 0,1,0);
    $end_date_time = "$nday.$nmon.$nyear";
  }


  my $limit = $R::limit || 400;
  my $offset = $R::offset || "0";
  if($R::detail_search && !$R::theft_details){
	$limit = 10000;
	$offset = 0;
  }
  #backward | forward
    if($R::go eq "backward_list"){
        $offset -= $limit if($offset >= $limit);
    }elsif($R::go eq "forward_list"){
        $offset += $limit;
    }

  my $c_id = $R::cttpos_id || $R::c_id || "";
  $c_id = "" if($R::insert_contenttranspos);
  my $cttpos = {};
  $R::ct_ct_name =~ s/\#//;
  my $search = {
		table		=> "contenttrans",
		table_pos	=> "$node_meta->{ct_table}",
		limit		=> $q->escapeHTML($limit),
		offset		=> $q->escapeHTML($offset),
	};

  if($R::ct_id && looks_like_number($R::ct_id)){
     	$search->{ct_id} = $R::ct_id;
  }elsif($R::c_id4trans && looks_like_number($R::c_id4trans)){
     	$search->{ct_id} = $R::c_id4trans;
  }elsif($c_id &&  looks_like_number($c_id)){
	$search->{cttpos_id} = $c_id;
  }else{
     	$search->{start_date_time} = "$start_date_time";
     	$search->{end_date_time} = "$end_date_time";
     	$search->{cttpos_id} = $q->escapeHTML("$R::cttpos_id");
	$search->{txt06} = $q->escapeHTML("$R::txt06");
	$search->{txt08} = $q->escapeHTML("$R::txt08");
	$search->{int04} = $q->escapeHTML("$R::int04");
	$search->{int06} = $q->escapeHTML("$R::int06");
	$search->{int12} = $q->escapeHTML("$R::int12");
	$search->{int13} = $q->escapeHTML("$R::int13");
	$search->{cp_ct_name} = $q->escapeHTML("$R::cp_ct_name");
	$search->{int10} = $q->escapeHTML("$R::int10");
	$search->{int20} = $q->escapeHTML("$R::int20");
	$search->{barcode} = $q->escapeHTML("$R::barcode");
	$search->{ct_txt06} = $q->escapeHTML("$R::ct_txt06");#PLZ
	$search->{ct_ct_name} = $q->escapeHTML("$R::ct_ct_name");
	$search->{owner} = $q->escapeHTML("$R::owner");
	$search->{txt23} = $q->escapeHTML("$R::txt23");
	$search->{txt24} = $q->escapeHTML("$R::txt24");
  }
  
  if(!$start_chck && !$end_chck){
    if($node_meta->{ct_table} eq "contenttranspos"){
      $cttpos = $dbt->collect_transpos($dbh,$search,0);
    }elsif($node_meta->{ct_table} eq "contenttheftpos"){
      #7=get_event alarm
      #8=get_position after validate
      if(!$R::theft_details){
        $search->{int05} = "IN::(7,8)";
	$search->{int03} = ">::$dbt->{copri_conf}->{theftalarm}->{move_count}";#move count
	$search->{int07} = ">::$dbt->{copri_conf}->{theftalarm}->{speed}";#speed
	$search->{int08} = ">::$dbt->{copri_conf}->{theftalarm}->{meter}";#meter
	#$search->{speedORmeter} = "(cp.int07 > $dbt->{copri_conf}->{theftalarm}->{speed} OR cp.int08 > $dbt->{copri_conf}->{theftalarm}->{meter})";
      }
      $cttpos = $dbt->collect_theftpos($dbh,$search);
    }
  }

    print $q->div({-class=>"copri_header",-style=>"background-color:$node_meta->{bg_color};"},"$path", $q->span({-style=>"padding:4px 10px;color:white;"},
  " $months[$mon -1] $year",
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat zurück",-href=>"?cal_delta_start=0:-1:0"}," &larr; "),
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat aktuell",-href=>"?cal_today=1"}," &bull; "), 
    $q->a({-class=>"linknav",-style=>"padding:0 0.5em;",-title=>"Monat vorwärts",-href=>"?cal_delta_start=0:1:0"}," &rarr; "),
    "$message"
    )),"\n";

  my $sort_up = "up";
  my $sort_down = "down";
  $users_dms->{cal_sort_updown} = "down" if(!$users_dms->{cal_sort_updown});
  $sort_up = "<b>$sort_up</b>" if($users_dms->{cal_sort_updown} eq "up");
  $sort_down = "<b>$sort_down</b>" if($users_dms->{cal_sort_updown} eq "down");

  print $q->div({-style=>'background-color:silver;height:10px;'},""),"\n";

  print $q->start_table({ -style=>"width:100%;", -border=>'0',-align=>'left', -cellpadding=>'1', -cellspacing=>'0'});
  print $q->Tr();
  print $q->td({-style=>"background-color:silver;border-right: solid thin gray;border-bottom: solid thin gray;width:30px;padding:1px 10px;"},$but->singlesubmit1("detail_search","search")),"\n";

  
  #1. Search-fields
  my $h=3;
  print "<td class='search_line'>\n";
  #print $q->a({-class=>"sortnav",-href=>"?cal_sort_updown=up\&offset=$offset\&limit=$limit",-title=>'Aufsteigend sortieren'},"$sort_up"),"|",$q->a({-class=>"sortnav",-href=>"?cal_sort_updown=down\&offset=$offset\&limit=$limit",-title=>'Absteigend sortieren'},"$sort_down"),"\n";
  print $but->checkbox("1","theft_details","$R::theft_details","Details") if($node_meta->{ct_table} eq "contenttheftpos" && $users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
  foreach(@tpl_order){
    #$h++;
    my ($key,$des,$size) = split /=/,$_;
    my $valxx = $q->param("$key") || "";
    $valxx = "" if(($R::base_edit eq "save_pos") || ($R::base_edit eq "transpos"));#empty search fields, select only bike number

    if($key =~ /time/){
      $size="10px";
      print $q->textfield(-id=>'datepicker1',-class=>'etxt',-name=>"start_$key",-override=>'1', -default=>"$start_date_time",-size=>"$size",-maxlength=>20), " < ", $q->textfield(-id=>'datepicker2',-class=>'etxt',-name=>"end_$key",-override=>'1',-default=>"$end_date_time",-size=>"$size",-maxlength=>20),"\n";
    }
    elsif($key =~ /owner/){
        print $but->selector_class("$key","eselect","width:80px;",$valxx,@_users),"\n";
    }
    elsif($key =~ /int10/ && "$size" eq "select"){# && $node_meta->{tpl_id} == 205){#bike_state
        my @_lock_valxx = (":$des");
        while (my ($key, $value) = each %{ $dbt->{copri_conf}->{bike_state} }) {
          push @_lock_valxx, "$key:$value";#[1:available] ...
        }
        print $but->selector_class("$key","eselect","",$valxx,@_lock_valxx),"\n";
    }
    elsif($key =~ /int12/ && "$size" eq "select"){
          my @_valxx = (":$des");
          foreach my $rid (sort { $part_nodes->{$a}->{node_name} cmp $part_nodes->{$b}->{node_name} } keys (%$part_nodes)){
            push (@_valxx, "$part_nodes->{$rid}->{main_id}:$part_nodes->{$rid}->{node_name} ($part_nodes->{$rid}->{main_id})") if($part_nodes->{$rid}->{main_id} !~ /300006|300016|300026/);
          }
          print $but->selector_class("$key","eselect","width:100px;",$valxx,@_valxx),"\n";
    }
    elsif($key =~ /int20/ && "$size" eq "select"){# && $node_meta->{tpl_id} == 205){#lock_state locked/unlocked
        my @_lock_valxx = (":$des");
        while (my ($key, $value) = each %{ $dbt->{copri_conf}->{lock_state} }) {
          push @_lock_valxx, "$key:$value";#[2:unlocked]
        }
        print $but->selector_class("$key","eselect","width:100px;",$valxx,@_lock_valxx),"\n";
    }
    else{
      $size="9px" if($key =~ /int/);
      if($key =~ /ct_name|barcode/){
        print $q->textfield(-class=>'stxt2',-name=>"$key",-default=>"$valxx",-size=>"$size",-override=>'1',-maxlength=>40, -placeholder=>"$des",-autofocus=>1),"\n";
      }else{
        print $q->textfield(-class=>'stxt2',-name=>"$key",-default=>"$valxx",-size=>"$size",-override=>'1', -maxlength=>40, -placeholder=>"$des"),"\n";
      }
    }
  }
  print "</td>\n";


  my ($daymarker,$raster_mmpx,$day4month) = $lb->month_line($users_dms);

  print $q->Tr();
  print $q->td({-class=>'element',-style=>"height:1.5em;padding:0;border:0px solid green;",-colspan=>"$h",-nowrap=>"1"},"$day4month"),"\n";
  ###
  
  ###Calendar Content
  my $nx=0;
  my $set_style="";
  my $scale_color = $node_meta->{bg_color};
  my $itime="";
  my $start_time="";
  my $end_time="";
  my ($year_st,$mon_st,$day_st,$hh_st,$mm_st);
  my ($year_en,$mon_en,$day_en,$hh_en,$mm_en);

  my $nr=0; 
  my $i= $offset || 0; 
  my $c_left = "14px;";
  foreach my $pid (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

      $nr++;
      $nx++;
      $i++;
      $c_left = "7px;" if($i >= 10);
      $c_left = "0px;" if($i >= 100);
      $set_style = "background-color:#fcfdfb;";
      $set_style = "background-color:#f4f1ee;" if($nx %= 2);
      $set_style = "background-color:#b6dbe3;" if($R::c_id && $R::c_id == $cttpos->{$pid}->{c_id});

      my $trans_style = "padding:0 5px;border: 2px solid #f7ae37;";
      my $stamm_style = "padding:0 5px;border: 2px solid #98c13b;";
      my $part_style = "padding:0 5px;border: 2px solid #dcd77f;";
   
      my ($ct_name,$ct_txt00,$ct_txt01,$ct_phone,$c_id4trans,$tpl_id4trans,$u_name);
      my $opos = "";
	my $id = $cttpos->{$pid}->{ct_id};
	if($ct4rel->{$id}->{c_id} == $cttpos->{$pid}->{ct_id}){
	  $ct_name = $ct4rel->{$id}->{ct_name};
	  $opos = "<span class='bi bi-record2' style='color:Maroon;'></span>" if($ct4rel->{$id}->{int14});
	  $ct_txt00 = $ct4rel->{$id}->{txt00};
	  $ct_txt01 = $ct4rel->{$id}->{txt01};
	  $ct_phone = $ct4rel->{$id}->{txt07};
	  $c_id4trans = $ct4rel->{$id}->{c_id};
	  $tpl_id4trans = $ct4rel->{$id}->{template_id};
	  $trans_style .= "background-color:#f7ae37;" if($ct4rel->{$id}->{c_id} == $users_dms->{c_id4trans});
	}
      if($cttpos->{$pid}->{start_time}){
        ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$pid}->{start_time});
      }else{
        ($year_st,$mon_st,$day_st,$hh_st,$mm_st) = $lb->split_date($cttpos->{$pid}->{itime});
      }
      if($cttpos->{$pid}->{end_time}){
	$cttpos->{$pid}->{end_time} = $now_dt if($node_meta->{ct_table} eq "contenttranspos" && $cttpos->{$pid}->{int10} == 3 && !$cttpos->{$pid}->{reserv_endtime});
        ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$pid}->{end_time});
      }else{
        ($year_en,$mon_en,$day_en,$hh_en,$mm_en) = $lb->split_date($cttpos->{$pid}->{itime});
      }

      #if($year_st && $mon_st && $day_st && $hh_st && $mm_st && $year_en && $mon_en && $day_en && $hh_en && $mm_en){
      if(1==1){

	#generate px for rent scale
	my $start_nr = $year_st . $mon_st . $day_st . $hh_st . $mm_st;
	my $end_nr = $year_en . $mon_en . $day_en . $hh_en . $mm_en;
	my $day_stpx = 0;
	my $rent_day_px = 0;
	my $time_style="";;
	if($start_nr <= $end_nr){
          ($day_stpx,$rent_day_px) = $lb->rent_scale($users_dms,$year_st,$mon_st,$day_st,$hh_st,$mm_st,$year_en,$mon_en,$day_en,$hh_en,$mm_en);
	}else{
	  $time_style="color:red;";
	}

	$itime = $lb->time4de($cttpos->{$pid}->{itime},1);
	if($cttpos->{$pid}->{start_time}){
	   $start_time = $lb->time4de($cttpos->{$pid}->{start_time},1);
	   $end_time = $lb->time4de($cttpos->{$pid}->{end_time},1);
	}else{
	   $start_time = $lb->time4de($cttpos->{$pid}->{itime},1);
	   $end_time = $lb->time4de($cttpos->{$pid}->{itime},1);
	}

		my $u_name = $cttpos->{$pid}->{owner} || "";
    		my $u_name_end = $cttpos->{$pid}->{owner_end} || "";
     		foreach my $ctu_id (keys (%$ct_users)){
       		  if($u_name && $channel_map->{$u_name}){
        		$u_name = $channel_map->{$u_name};
       		  }elsif($cttpos->{$pid}->{owner} eq $ct_users->{$ctu_id}->{c_id}){
        		$u_name = $ct_users->{$ctu_id}->{txt01};
       		  }
       		  if($u_name_end && $channel_map->{$u_name_end}){
        		$u_name_end = $channel_map->{$u_name_end};
       		  }elsif($cttpos->{$pid}->{owner_end} eq $ct_users->{$ctu_id}->{c_id}){
        		$u_name_end = $ct_users->{$ctu_id}->{txt01};
       		  }
     		 }
		 $u_name_end = "-" if(!$u_name_end);


	#if($ct_name){
	if(1==1){
	  print "<Tr><td class='element' colspan='$h' style='$set_style;0.91em;padding:0.3em 0 0 0em;'>\n";

	    my $pos_hash = $cttpos->{$pid};
	    my $pos_details = "";
	    foreach my $did (sort keys (%{$pos_hash})){
	    	    $pos_details .= $did . " = " . $pos_hash->{$did} . "</br>" if($pos_hash->{$did});
	    }
	    my $pos_id = "ID $cttpos->{$pid}->{c_id}";
	    my $user_agent = "";
	    my $charge = "";
	    $charge .= " &rarr; lock-charge $cttpos->{$pid}->{int14} %" if(looks_like_number($cttpos->{$pid}->{int14}));
	    $charge .= " &rarr; bike-charge $cttpos->{$pid}->{int19} %" if(looks_like_number($cttpos->{$pid}->{int19}) && $record_part->{$cttpos->{$pid}->{cc_id}}->{energy_id});
	    if($users_dms_primary->{u_id} && $dbt->{copri_conf}->{betau_id}->{$users_dms_primary->{u_id}}){
	      $pos_id = $q->div({-class=>"popup",-onclick=>"toggle_box('$pid')"},"$cttpos->{$pid}->{c_id}", $q->span({-class=>"popuptext",-id=>"$pid"},"$pos_details"));
	      $user_agent = "";
	      $user_agent .= " &rarr; endAck" if($cttpos->{$pid}->{int27});
	      my $txt26 = $cttpos->{$pid}->{txt26};
	      $txt26 = substr($cttpos->{$pid}->{txt26},0,25) if(length($cttpos->{$pid}->{txt26}) > 20);
	      $user_agent .= " &rarr; $txt26" if($txt26);
	    }

	    my $bikenr = $cttpos->{$pid}->{barcode} || $cttpos->{$pid}->{ct_name};

	    my $status = "$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}" || "state failure";
	    $status = "<span style='color:#c63e3e;'>$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}</span>" if($cttpos->{$pid}->{int10} == 2 || $cttpos->{$pid}->{int10} == 3);
	    if($cttpos->{$pid}->{int10} ne $record_part->{$cttpos->{$pid}->{cc_id}}->{int10}){
		    $status = "<span style='border: 1px solid #c63e3e;'>$dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}}</span>";
	    }
	    my $lock_state = "";
	    #my $lock_state = "lock failure" if(!$cttpos->{$pid}->{int20});
	    $lock_state = "locked" if($cttpos->{$pid}->{int20} == 1);
	    $lock_state = "<span style='color:#c63e3e;'>unlocked</span>" if($cttpos->{$pid}->{int20} == 2);
	    $lock_state = "<span style='color:#c63e3e;'>locking</span>" if($cttpos->{$pid}->{int20} == 3);
	    $lock_state = "<span style='color:#c63e3e;'>unlocking</span>" if($cttpos->{$pid}->{int20} == 4);
	    if($cttpos->{$pid}->{int20} && $cttpos->{$pid}->{int20} ne $record_part->{$cttpos->{$pid}->{cc_id}}->{int20}){
		    $lock_state = "<span style='border: 1px solid #c63e3e;'>$dbt->{copri_conf}->{lock_state}->{$cttpos->{$pid}->{int20}}</span>";
	    }


	    my $track_info = "";
	    my $co2saving = "";
	    if($cttpos->{$pid}->{int26}){
		$co2saving = $pri->co2calc($cttpos->{$pid});
		$cttpos->{$pid}->{int26} =~ s/\./,/;
	      	$track_info = "&rarr; $co2saving kg CO&sup2; ($cttpos->{$pid}->{int26} km)";
	    }
            	
	    my $edit_pos = $q->a({-class=>"editboo",-href=>"?base_edit=transpos\&c_id=$cttpos->{$pid}->{c_id}\&owner=$users_dms->{u_id}",-title=>"Position bearbeiten ($cttpos->{$pid}->{c_id})"},$q->span({-class=>"bi bi-file-earmark-text-fill", -style=>'font-size:1.6em;'}));
	    my $part_path = "";
	    $part_path = $part_nodes->{$cttpos->{$pid}->{int12}}->{node_name} if($cttpos->{$pid}->{int12});
            $bikenr = $q->a({-class=>"linknav3",-style=>"$stamm_style",-href=>"/DMS/Waren/$part_path?detail_search=1&s_barcode=$cttpos->{$pid}->{barcode}",-title=>"$cttpos->{$pid}->{txt01} | Artikel im Warenstamm"},"$bikenr") if($cttpos->{$pid}->{cc_id});

	    if($node_meta->{ct_table} eq "contenttranspos"){
	    	my $start_station = "$cttpos->{$pid}->{int06}";
	    	my $end_station = "$cttpos->{$pid}->{int04}";
	    	$start_station = $q->a({-class=>"linknav3",-style=>"",-href=>"/DMS/Waren/?detail_search=1&s_int04=$cttpos->{$pid}->{int06}",-title=>"$record_st->{$cttpos->{$pid}->{int06}}->{txt01} | Warenstamm nach Station filtern"},"$cttpos->{$pid}->{int06}") if($record_part->{$cttpos->{$pid}->{cc_id}}->{rel_id});
	    	$end_station = $q->a({-class=>"linknav3",-style=>"",-href=>"/DMS/Waren/?detail_search=1&s_int04=$cttpos->{$pid}->{int04}",-title=>"$record_st->{$cttpos->{$pid}->{int04}}->{txt01} | Warenstamm nach Station filtern"},"$cttpos->{$pid}->{int04}") if($record_part->{$cttpos->{$pid}->{cc_id}}->{rel_id});

  		my $comment = $cttpos->{$pid}->{txt23};
	        $comment = substr($cttpos->{$pid}->{txt23},0,50) . " ..." if(length($cttpos->{$pid}->{txt23}) > 50);
		my $comment_view = "";
		$comment_view = "&rarr; $comment" if($comment); 

		my $debug_log_view = "";
		if($search->{txt24}){
                  my $debug_log = "appapi-log: $cttpos->{$pid}->{txt24} | sigapi-log: $cttpos->{$pid}->{txt25} | user-agent: $cttpos->{$pid}->{txt26} | user-device: $cttpos->{$pid}->{txt21} | app-debug: $cttpos->{$pid}->{txt27} | lock-HW: $cttpos->{$pid}->{txt28} | response-log: $cttpos->{$pid}->{txt29}";
		  my $index = index(lc($debug_log), lc($search->{txt24}));
		  $index -= 20;
                  $debug_log = "... " . substr($debug_log,$index,100) . " ...";
		  $debug_log_view = "&rarr; debug-log: $debug_log" if($debug_log);
		  $debug_log_view =~ s/($search->{txt24})/<span style=\'color:#FF5F1F;\'>$1<\/span>/i;
		}

		my $kunde = "$cttpos->{$pid}->{txt08}";
		if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int02} >= 1){
            	  $kunde = $q->a({-class=>"linknav3",-style=>"$part_style",-href=>"/DMS/Kunden/?detail_search=1&s_c_id=$cttpos->{$pid}->{ca_id}",-title=>"Kunde im Kundenstamm"},"$cttpos->{$pid}->{txt08} ($cttpos->{$pid}->{ca_id})");#2021-05-24 saves kd name
		}


		#Parts and prepaids
		if($cttpos->{$pid}->{template_id} && $cttpos->{$pid}->{template_id} =~ /219|224|229/){
		  $part_path = "Prepaid" if($cttpos->{$pid}->{template_id} == 219);
            	  print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $edit_pos $pos_id &rarr; $start_time &rarr; $part_path Nr. $bikenr &rarr; $kunde &rarr; $u_name/$u_name_end $comment_view"),"\n";
		}else{
		  print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $edit_pos $pos_id &rarr; <span style='$time_style'>$start_time – $end_time</span> &rarr; $kunde &rarr; Start Station $start_station &rarr; End Station $end_station &rarr; Bike $bikenr $status $lock_state &rarr; $u_name/$u_name_end<br /><span style='padding-left:60px;'>$charge $track_info $comment_view $debug_log_view</span>"),"\n";
		}

		if($users_dms_primary->{u_id} && $users_dms->{u_id} && $users_dms->{int03} >= 1){
	    	  print $q->div({-style=>'float:left;margin:0.4em 0 0 1em;'}, "&rarr; Faktura", $q->a({-class=>"linknav3",-style=>"$trans_style",-href=>"/DMS/Faktura?ct_trans=open\&c_id4trans=$c_id4trans\&tpl_id4trans=$tpl_id4trans\&owner=$users_dms->{owner}",-title=>"Faktura Terminal öffnen"},"$opos \#$ct_name")),"\n" if($c_id4trans && $tpl_id4trans); 
		  print $q->div({-style=>'float:left;margin-left:1em;'}, "$user_agent"),"\n"; 
		}

    	   }elsif($node_meta->{ct_table} eq "contenttheftpos"){
		my $speed = 0;
	       	if($cttpos->{$pid}->{int07} && $cttpos->{$pid}->{int07} > 0){
		  $speed = $cttpos->{$pid}->{int07} * 1.852;
		  $speed = $lb->round_half($speed);
		}
		my $event_type = "";
		my $meter = $cttpos->{$pid}->{int08};
		$meter =~ s/\./\,/;
		if($cttpos->{$pid}->{int01} && $cttpos->{$pid}->{int05} == 7){
		  $event_type = "Alarm detected";
		}elsif($cttpos->{$pid}->{int01} && $cttpos->{$pid}->{txt10}){
		  $event_type = "statusCode $cttpos->{$pid}->{txt10}";
		}elsif($cttpos->{$pid}->{int02} && $cttpos->{$pid}->{txt06}){
		  my $gps2card = $q->a({-class=>"linknav3",-href=>"/DMS/Karte?man_gps=$cttpos->{$pid}->{txt06}\&owner=$users_dms->{owner}",-title=>"Karte öffnen"}, "$cttpos->{$pid}->{txt06}");
		  $event_type = "Alarm movement &rarr; GPS $gps2card &rarr; speed $speed km/h &rarr; distance $meter m ";
		}
		my $bike_state = "";
		if($cttpos->{$pid}->{int10}){
		  $bike_state = $dbt->{copri_conf}->{bike_state}->{$cttpos->{$pid}->{int10}};
		}
		if($cttpos->{$pid}->{int20}){
		  $lock_state = $dbt->{copri_conf}->{lock_state}->{$cttpos->{$pid}->{int20}};
		}

            	print $q->div({-style=>"float:left;margin-left:$c_left"}, "$i) $pos_id &rarr; $end_time &rarr; Bike $bikenr $bike_state $lock_state &rarr; $event_type &rarr; $u_name"),"\n";

	   }

          print $q->div({-style=>"position:absolute;margin-left:$daymarker;border-right: solid thin #009899;height:1.5em;"},"&nbsp;"),"\n" if("$mon" eq "$mon_today");
          print $q->div({-style=>"position:static;margin-left:$day_stpx;width:$rent_day_px;height:1.5em;background-color:$scale_color;"},"&nbsp;"),"\n";
	  print "</td></Tr>\n";
	}
      }
     }

  print $q->end_table;
  ###

 print "</div>\n";

   my $offset_nr = $offset + $nr;

   #backward | forward
   print $q->div({-style=>'float:left;padding:0.5em;'}, "Zeile: $offset - $offset_nr"),"\n";
   print "<div style='float:left;padding:0.5em;'>\n";
   print $q->a({-class=>"linknav1",-href=>"?go=backward_list;offset=$offset;limit=$limit",-title=>'backward'}," &larr; ") if($offset >= $limit);
   print $q->a({-class=>"linknav1",-href=>"?go=forward_list;offset=$offset;limit=$limit",-title=>'forward'}," &rarr;") if($nr >= $limit-10);
   print "</div>\n";

 print $q->div({-style=>'clear:both;'}, "&nbsp;"),"\n";
 if(1==2 && $node_meta->{ct_table} eq "contenttheftpos"){
   print $q->div({-style=>'text-align:left;text-decoration:underline;padding:0 0.5em;'}, "Legende"),"\n";
   print $q->div({-style=>'text-align:left;font-style:italic;padding:0.1em 0.5em;'}, $q->span({-style=>'padding:0 0.8em;',-style=>"color:$node_meta->{bg_color};"},"Alarm detected:"), "Es wurde eine Bewegung (Erschütterung) mit abgeschlossenem Rahmenschloss erkannt"),"\n";
   print $q->div({-style=>'text-align:left;font-style:italic;padding:0.1em 0.5em;'}, $q->span({-style=>'padding:0 0.8em;',-style=>"color:$node_meta->{bg_color};"},"Alarm movement:"), "Es wurde eine Fortbewegung mit abgeschlossenem Rahmenschloss erkannt"),"\n";
 }

 print $q->end_form;
}
1;

