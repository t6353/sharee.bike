package TransInvoices;
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
#
use strict;
use warnings;
use POSIX;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use DateTime;
use DateTime::Format::Pg;
use Date::Calc::Object qw(:ALL);
use Scalar::Util qw(looks_like_number);
use Data::Dumper;
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Mod::Pricing;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}

#Template
sub tpl(){
  my $self = shift;
  my $varenv = shift;
  my $node_meta = shift;
  my $users_dms = shift;
  my $ctf = shift;
  my $ctadr = shift;
  my $ctt = shift;

  my $q = new CGI;
  my $cf = new Config;
  my $lb = new Libenz;
  my $dbt = new DBtank;
  my $apif = new APIfunc;
  my $but = new Buttons;
  my $pri = new Pricing;
  my %ib = $but->ibuttons();
  my $today = strftime "%d.%m.%Y",localtime;
  my $now_dt = strftime "%Y-%m-%d %H:%M:%S", localtime;
  my $coo = $q->cookie(-name=>'domcookie');

  my $dbh = "";
  my $line_count2 = 0; 
  my $tc=0; 
  my $vibuchen_mtime = $lb->time4de($ctt->{mtime},1);
  my $txt20 = $R::txt20 || $ctt->{txt20} || "";#Leistungsdatum
  my $int05 = $R::int05 || $ctt->{int05} || "";#manuell
  my $rows = 0;
  my @tpl_order = ();
  my $cttpos = { c_id => 0 };

  my $ctf_operator = { c_id => 0 };
  my $pref_cuop = {
    table => "contentuser",
    fetch => "one",
    c_id  => 2,
  };
  $ctf_operator = $dbt->fetch_tablerecord($dbh,$pref_cuop);

  #take original operator accounting c_id to collect related invoices
  #operator invoice
  if($ctt->{template_id} != 208){#not equal Abrechnung
    my $pref = {
        table           => "contenttrans",
        fetch           => "one",
        barcode         => $ctt->{barcode},
	txt00		=> "Abrechnung",
    };
    my $ctt_accounting = $dbt->fetch_record($dbh,$pref);
    ($cttpos,$rows) = $dbt->collect_contenttrans($dbh,$ctt_accounting->{content_id});
    #int9x are not in db
    @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int93=Entgelt TeilRad GmbH");
  }
  #operator accounting
  else{
    ($cttpos,$rows) = $dbt->collect_contenttrans($dbh,$ctt->{content_id});
    #int9x are not in db
    if($varenv->{dbname} eq "sharee_sx"){
      @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int94=Disagio","int02=Gutschrift");
    }else{
      @tpl_order = ("c_id=ID","int01=Netto Erlös","state=Zahlungsart","int94=Disagio","int95=Transaktion","int96=Zahlungsmeldung","int97=Kreditkarte Zuordnung","int02=Gutschrift");
    }
  }
  my $tplf = $dbt->get_tpl($dbh,201);#Kunden-Faktura Firma
  my @tplf_order = split /,/,$tplf->{tpl_order};

  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'left', -cellpadding=>'3', -cellspacing=>'0'}),"\n";

  print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset"),"\n" if($R::offset);
  print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit"),"\n" if($R::limit);
  print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}"),"\n";
  print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}"),"\n";

  $line_count2++; 
  print $q->Tr(),"\n"; 
  #print $q->th("Pos."),"\n";
  foreach (@tpl_order){
   my ($key,$val) = split /=/,$_;
   $tc++ if($val);
   $val .= " ($key)" if($users_dms->{u_id} eq $varenv->{superu_id});
   print $q->th("$val"),"\n";
  }


  print $q->start_form(-name=>'transposform'),"\n";

  my $sum_operatorcredit=0;
  my $sum_netto=0;
  my $sum_brutto=0;
  my $sum_all=0;
  my $sum_umst19=0;
  my $i=0;
  my $accounting_start = "";
  my $accounting_end = "";

  foreach my $id (sort { $cttpos->{$b}->{c_id} <=> $cttpos->{$a}->{c_id} } keys(%$cttpos)){

   my $oac = $pri->operator_accounting2calc($varenv,$cttpos->{$id},$ctf_operator);
   $sum_operatorcredit += $oac->{int02};#Abrechnung Gutschrift
   $sum_netto += $oac->{int100};#Operator invoice (TeilRad Gebühren netto)
   $sum_all += $oac->{int99};#capture brutto
   $oac->{int01} = sprintf('%.2f', $oac->{int01});#capture netto

   if(1==1){
    $i++;
    $line_count2++;
    if($i==1){
      #pay_time only if payone capture, else invoice_time
      my $pitime = $cttpos->{$id}->{pay_time} || $cttpos->{$id}->{invoice_time};
      if($pitime && $pitime =~ /(\d+)\-(\d+)\-(\d+)/){
	my $year = $1;
	my $month = $2;
	my $days4month = Days_in_Month($year,$month);
        $accounting_start = "01.$month.$year";
    	$accounting_end = "$days4month.$month.$year";
      }
    }

    print $q->Tr(),"\n";
    #print $q->td({-class=>'tdint'},"$i"),"\n";

    foreach (@tpl_order){
      my ($key,$val) = split /=/,$_;     
      $cttpos->{$id}->{$key} = $q->unescapeHTML($cttpos->{$id}->{$key});
      my $txtstyle = "text-align:left;min-width:100px;";
      if($key =~ /int\d+/){
       $txtstyle = "text-align:right;min-width:50px;";
      }

      if(1==1){
        if($key =~ /ct_name/){
	  #print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key} $cttpos->{$id}->{txt01}"),"\n";
	  print $q->td({-class=>'tdtxt'},"\# $cttpos->{$id}->{$key}"),"\n";
        }
        elsif($key =~ /c_id/){
	  print $q->td({-class=>'tdint'},"$cttpos->{$id}->{$key}"),"\n";
        }
	elsif($key =~ /int/){
	  $oac->{$key} =~ s/\./,/;
	  print $q->td({-class=>'tdint',-nowrap=>1},"$oac->{$key} €"),"\n";
	}
	elsif($key =~ /state/){
	  print $q->td({-class=>'tdint',-nowrap=>1},"$cttpos->{$id}->{$key}"),"\n";
	}
	elsif($key =~ /txt/){
          print $q->td({-class=>'tdtxt'},"$cttpos->{$id}->{$key}"),"\n";
	}
      }

     }
    }
   }#foreach end

   #sum int100 netto
   if($sum_netto && $sum_netto != 0){
     $sum_brutto = $sum_netto * 1.19;
     $sum_netto = $pri->round($sum_netto);
     $sum_brutto = $pri->round($sum_brutto);
     $sum_umst19 = $sum_brutto - $sum_netto;
   }
   $sum_operatorcredit = $pri->round($sum_operatorcredit);
   my $total_operatorcredit = $sum_operatorcredit - $sum_brutto;
   my $sum_paid = $sum_brutto;

   $sum_netto = sprintf('%.2f', $sum_netto);
   $sum_netto =~ s/\./,/;
   $sum_brutto = sprintf('%.2f', $sum_brutto);
   $sum_brutto =~ s/\./,/;
   $sum_umst19 = sprintf('%.2f', $sum_umst19);
   $sum_umst19 =~ s/\./,/;

   $sum_operatorcredit = sprintf('%.2f', $sum_operatorcredit);
   $sum_operatorcredit =~ s/\./,/;
   $total_operatorcredit = sprintf('%.2f', $total_operatorcredit);
   $total_operatorcredit =~ s/\./,/;

   $sum_all = sprintf('%.2f', $sum_all);
   $sum_all =~ s/\./,/;


   my $payment_text = "";
    foreach(@tplf_order){
      my ($key,$des,$size) = split /=/,$_;
      if($key =~ /txt5\d/){
        $ctf->{$key} = $q->unescapeHTML("$ctf->{$key}");
        $ctf->{$key} = $lb->newline($ctf->{$key},"","");
        $ctt->{state} =~ s/\(payone.*//;
        if($des =~ /$ctt->{state}/){
	  $payment_text = "($key) " if($users_dms->{u_id} eq $varenv->{superu_id});
          if($sum_netto < 0){
            $payment_text .= "$ctf->{txt58}";
          }else{
            $payment_text .= "$ctf->{$key}";
          }
        }
      }
    }

   my $sc = 2;
   $sc = 1 if($ctt->{template_id} != 208);
   my $pt = $tc - $sc;
   print "<tr>\n";
   print "<td style='border:0px solid black;' colspan='$pt'><div  style='font-size:0.81em;padding:0.3em 0em;'>$payment_text</div></td>\n";

   ###print sum
   print "<td style='border:0px solid black;max-width:200px;' colspan='$sc'>\n";
   print $q->start_table({-class=>'list',-style=>'', -border=>'0', -width=>'100%',-align=>'right', -cellpadding=>'3', -cellspacing=>'0'}),"\n";
   print $q->Tr("\n");
   print $q->td("&nbsp;");

   #Sum operator invoice
   if($ctt->{template_id} != 208){#not equal Abrechnung
     print $q->Tr("\n"); $line_count2++;
     print $q->td({-class=>'tdint',-colspan=>1},"Nettobetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_netto €");

     if($sum_netto != 0){
      print $q->Tr("\n");$line_count2++;
      print $q->td({-class=>'tdint',-colspan=>1,-nowrap=>"1"},"19% UmSt auf $sum_netto €");
      print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_umst19 €");
     }

     print $q->Tr("\n");$line_count2++;
     print $q->td({-class=>'tdsum',-colspan=>1},"Entgelt TeilRad GmbH");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_brutto €");

     #print $q->Tr("\n");$line_count2++;
     #print $q->td({-class=>'tdint',-colspan=>1,-style=>'color:silver;'},"(pay Einzüge brutto)");
     #print $q->td({-class=>'tdint',-nowrap=>"1",-style=>'color:silver;'},"$sum_all €");
 
   }
   #Summe Betreiber Abrechnung (Gutschrift)
   else{
     print $q->Tr("\n");$line_count2++;
     #print $q->td({-class=>'tdint',-colspan=>1,-style=>'color:silver;'},"(Einzüge brutto $sum_all)");
     print $q->td({-class=>'tdsum',-colspan=>1},"Summe Gutschrift");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$sum_operatorcredit €");

     print $q->Tr("\n");$line_count2++;
     print $q->td({-class=>'tdsum',-colspan=>1},"Entgelt TeilRad GmbH");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"- $sum_brutto €");

     print $q->Tr("\n");$line_count2++;
     print $q->td({-class=>'tdsum',-colspan=>1},"Auszahlungsbetrag");
     print $q->td({-class=>'tdint',-nowrap=>"1"},"$total_operatorcredit €");
   }

   print $q->end_table;
   print "</td><td>&nbsp;</td>";
   ###print sum end

   print "</tr>";
   print $q->end_table;

   print $q->hidden(-name=>'owner', -override=>'1', -value=>"$users_dms->{u_id}");
   print $q->hidden(-name=>'c_id4trans', -override=>'1', -value=>"$ctt->{content_id}");
   print $q->hidden(-name=>'tpl_id4trans', -override=>'1', -value=>"$ctt->{template_id}");
   print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset") if($R::offset);
   print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit") if($R::limit);
   print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids") if($R::relids);
 

   if($users_dms->{int03} == 2){
    #only if user is also a primary DMS user with invoice rw
    print $q->hidden(-name=>'printer_id', -value=>"PDF", -override=>'1');
    my $dbh_primary = $dbt->dbconnect_extern($dbt->{primary}->{sharee_primary}->{database}->{dbname});
    my $users_dms_primary = { u_id => 0 };
    $users_dms_primary = $dbt->select_users($dbh_primary,$users_dms->{u_id},"and int03=2");

    if($users_dms_primary->{u_id} && $users_dms_primary->{int03} == 2){
     my @_paymentstate = ("");
     my $kind_of_payment = "";
     my @_payment_valxx = ("");#new
     my $setdefault_payment = "";#new
     my $p_hash = $dbt->{shareedms_conf}->{payment_state2};
     #we need it for both
     print $q->hidden(-name=>'sum_operatorcredit', -override=>'1',-value=>"$sum_operatorcredit");
     print $q->hidden(-name=>'sum_paid', -override=>'1',-value=>"$sum_paid");
     if($ctt->{template_id} != 208){#not equal Abrechnung
       push @_paymentstate, "Entgelt TeilRad GmbH";
       push @_payment_valxx, "9:(9) $p_hash->{9}";
       if($ctt->{int04} && $ctt->{int01}){
        $kind_of_payment = "$ctt->{state}";
	$setdefault_payment = 9;
        $ctt->{int01} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int01} ne $sum_paid);
     	my $opos = "";
     	$opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
     	print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> $vibuchen_mtime \"$p_hash->{$ctt->{int04}}\" $ctt->{int01} €"),"\n";      
       }
     }
     else{
       push @_paymentstate, "Betreiber Abrechnung";
       push @_payment_valxx, "8:(8) $p_hash->{8}";
       if($ctt->{int04} && $ctt->{int02}){
        $kind_of_payment = "$ctt->{state}";
	$setdefault_payment = 8;
        $ctt->{int02} =~ s/\./,/;
     	my $style = "color:red;" if($ctt->{int02} ne $sum_operatorcredit);
     	my $opos = "";
     	$opos = "OPOS" if($ctt->{int14} && $ctt->{int14} > 0);
     	print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"},"<span style='color:red;'>$opos</span> Buchung $vibuchen_mtime \"$p_hash->{$ctt->{int04}}\" $ctt->{int02} €"),"\n";      
       }
     }

     if(!$ctt->{close_time}){
      my $send_invoice_checkbox = 0;
      #$send_invoice_checkbox = 0 if($ctt->{txt30});
       print $q->div({-class=>'element6',-style=>'float:right;'}, $q->b("$ctt->{txt00}"),
       ": PDF drucken ",$but->checkbox("1","print_pdf","1","PDF drucken",""),
       " – eMail senden ",$but->checkbox("1","send_invoice","$send_invoice_checkbox","eMail $ctt->{txt00}",""),
       " – Zahlungsart ",
       #$but->selector("state","250px",$kind_of_payment,@_paymentstate),
       $but->selector_class("int04","","",$setdefault_payment,@_payment_valxx),
       $but->singlesubmit1("set_state","buchen")),
        "\n";
     }
    }
  }

  print $q->end_form;

  my $praefix = "$ctt->{txt00}-$varenv->{praefix}";
  print $q->div({-style=>"clear:both;height:0.1px;"},""),"\n";
  if($ctt->{txt30}){
    print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "$ctt->{txt30}"),"\n";
  }elsif( -f "$varenv->{pdf}/$praefix-$ctt->{ct_name}.pdf" ){
     print $q->start_form(),"\n";
     print $q->hidden(-name=>'offset', -override=>'1', -value=>"$R::offset") if($R::offset);
     print $q->hidden(-name=>'limit', -override=>'1', -value=>"$R::limit") if($R::limit);
     print $q->hidden(-name=>'relids', -override=>'1', -value=>"$R::relids") if($R::relids);
     #print $q->div({-style=>"padding:0.5em;font-size:0.81em;width:98%;text-align:right;"}, "eMail wurde nicht versandt! $ctt->{txt00} eMail ", $but->singlesubmit1("ct_trans","send_invoice_again","send_invoice_again")),"\n" if($users_dms->{u_id} == $dbt->{copri_conf}->{superu_id});
     print $q->end_form;
  }

  if( -f "$varenv->{basedir}/pdfinvoice/$praefix-$ctt->{ct_name}.pdf"){
    print $q->div({-style=>'padding:0.5em;font-size:0.81em;width:98%;text-align:right;'}, "Download: ", $q->a({-href=>"$varenv->{wwwhost}/FileOut?file=$praefix-$ctt->{ct_name}.pdf&sessionid=$coo", -target=>"_blank" , -type=>'application/octet-stream', -style=>'text-decoration:underline;'}), "$praefix-$ctt->{ct_name}.pdf"),"\n";
  }

  if(!$int05 && $accounting_start && $accounting_end){
    my $update_ctt = {
        table   => "contenttrans",
        mtime   => "no_time",
        c_id    => $ctt->{content_id},
    };
    #disable Leistungszeitraum workaround for SubOp
    #UPDATE contenttrans set txt20='01.05.2024 - 31.12.2024',txt29='Oberried' where c_id = 29152;
    #$dbt->update_one($dbh,$update_ctt,"txt20='01.12.2023 - 31.12.2024'");
    $dbt->update_one($dbh,$update_ctt,"txt20='$accounting_start - $accounting_end'");
  }

  return $line_count2;
}
1;
