package APIdialog;
#
#
# SPDX-License-Identifier: AGPL-3.0-or-later
# Copyright (c) Rainer Gümpelein, TeilRad GmbH
#
use strict;
use warnings;
use POSIX;
use CGI::Carp qw(fatalsToBrowser);
use CGI ':standard';
use JSON;
#use Test::JSON;
use Scalar::Util qw(looks_like_number);
use Lib::Config;
use Mod::Buttons;
use Mod::Libenz;
use Mod::DBtank;
use Mod::APIfunc;
use Data::Dumper;

sub new {
 my $class = shift;
 my $self = {};
 bless($self,$class);
 return $self;
}


#Template
sub tpl(){
 my $node_meta = shift;
 my $users_dms = shift;
 my $u_group = shift;
 my $return = shift;

 my $q = new CGI;
 my $json = JSON->new->allow_nonref;
 my $cf = new Config;
 my $lb = new Libenz;
 my $dbt = new DBtank;
 my $apif = new APIfunc;
 my $but = new Buttons;
 my $dbh = "";#$dbt->dbconnect();

 my %varenv = $cf->envonline();
 my %ib = $but->ibuttons();
 my $tpl = $dbt->get_tpl($dbh,$node_meta->{template_id});
 my $tpl_order = $tpl->{tpl_order};
 #$tpl_order =~ s/barcode=Rad/barcode=Rad,txt09=Wartungsarbeiten=area/;
 #$tpl_order =~ s/int04=Station/int04=Station,txt09=Wartungsarbeiten=area/ if($tpl->{tpl_id} == 403);
 $tpl_order = "barcode=Rad,txt09=Wartungsprotokoll=area";
 $tpl_order = "int04=Station,txt09=Wartungsprotokoll=area" if($tpl->{tpl_id} == 403);
 
 my @tpl_order = split /,/,$tpl_order;

 my $edit="rel_edit";
 my $save_key = "service_done";
 my $ctrel = {};
 if(ref($return) ne "SCALAR" && $return =~ /shareejson/){
     my $tj = $json->pretty->decode($return);
     foreach my $obj (keys (%{$tj->{shareejson}})){
       if($obj eq "service_id_done" && looks_like_number($tj->{shareejson}->{$obj})){
        my $c_id = $tj->{shareejson}->{$obj};
          my $fetch = {
                table           => "contentpos",
                fetch           => "one",
                c_id         => "$c_id",
          };
          $ctrel = $dbt->fetch_tablerecord($dbh,$fetch);

       }
     }
 }else{
    print $q->div($return),"\n";
    return;
 }

  my $u_name;
  my $channel_map = $dbt->channel_map();
  my $mapref = {};
  my $ct_users = $dbt->users_map($dbh,$mapref);#get serviceAPP and DMS users from contentadr

  foreach my $id (sort { $channel_map->{$a} cmp $channel_map->{$b} } keys (%$channel_map)){
     if($id == $ctrel->{owner}){
        $u_name = $channel_map->{$id};
     }
  }
  if(!$u_name){
   foreach my $ctu_id (keys (%$ct_users)){
     if($ct_users->{$ctu_id}->{c_id} == $ctrel->{owner}){
        $u_name = $ct_users->{$ctu_id}->{txt01};
     }
   }
  }
  $ctrel->{mtime} = $lb->time4de($ctrel->{mtime},"1") if($ctrel->{mtime});

  print $q->hidden(-name=>'mode', -value=>"manager",-override=>1),"\n";
  print $q->hidden(-name=>'detail_search', -value=>"1",-override=>1),"\n";
  print $q->hidden(-name=>'owner', -value=>"$users_dms->{u_id}",-override=>1),"\n";
  print $q->hidden(-name=>'s_barcode', -value=>"$ctrel->{barcode}",-override=>1),"\n" if($ctrel->{barcode});
  print $q->hidden(-name=>'s_int04', -value=>"$ctrel->{int04}",-override=>1),"\n" if($ctrel->{int04});
  print $q->hidden(-name=>'service_id', -value=>"$ctrel->{c_id}",-override=>1),"\n";
  print $q->hidden(-name=>'main_id', -value=>"$node_meta->{main_id}",-override=>1),"\n";
  print $q->hidden(-name=>'template_id', -value=>"$node_meta->{template_id}",-override=>1),"\n";


  print "<div id='Container_cms'>";

  #1. table submit
  print $q->start_table({-class=>'list', -border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
     print $q->Tr();
     print "<td colspan=2 style='height:25px;background-color:$varenv{contentpos_color};padding:2px;border-bottom: 1px solid silver;'>";
     print $but->singlesubmit7("$edit","$save_key","$ib{$save_key}","margin:0 5px;");
     print $q->span({-style=>'margin-left:200px; font-size:1em;'}, "$u_name / $ctrel->{mtime} ID $ctrel->{c_id}");
     print "</td>\n";
  print $q->end_table;

 print $q->start_table({-border=>'0', -width=>'100%',-align=>'center', -cellpadding=>'3', -cellspacing=>'0'});
 my $required="";
 my $autofocus="";
 foreach (@tpl_order){
   my ($key,$des,$size) = split /=/,$_;
   #$des .= " ($key)" if($users_dms->{u_id} eq $varenv{superu_id});
   my $label_des = $des;
   $ctrel->{$key} = $q->unescapeHTML("$ctrel->{$key}");
   $ctrel->{$key} = $lb->newline($ctrel->{$key},"","1");

   if($key =~ /barcode|int04/){
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms'},"$des"),"\n";
	if(!$ctrel->{$key}){
	  print $q->td({-class=>'content1_cms'},$q->textfield(-class=>'etxt', -style=>"",-name=>"s_$key", -default=>"$ctrel->{$key}", -autofocus=>1, -required=>1)),"\n";
	}else{
          print $q->td({-class=>'content1_cms'},"$ctrel->{$key}"),"\n";
	}
   }elsif($key =~ /int/ && $size eq "checkbox"){
          my $ck_style = $but->checkbox_style($key,"cbox");
          print "$ck_style";
	  my $checked = "";
	  $checked = "checked" if($ctrel->{$key});
          print $q->Tr();
          print $q->td({-class=>'left_italic_cms'},"<label for='sq_cbox$key'>$des</label>"),"\n";
          print $q->td({-class=>'content1_cms'},$q->div("<input type='checkbox' id='sq_cbox$key' name='$key' value='1' $checked>")),"\n";

   }elsif($key =~ /txt/){
        my $h= 20;# if($size =~ /area(\d+)/);
        print $q->Tr();
        print $q->td({-class=>'left_italic_cms',-style=>'vertical-align: top;', -colspan=>1},"$des"),"\n";
        print $q->td({-class=>'content1_cms'},$q->textarea(-class=>"autos", -style=>'border: 1px solid silver;',-rows=>"$h",-cols=>'60;', -name=>"$key", -override=>'1',-default=>"$ctrel->{$key}")),"\n";
   }
  }
  print $q->end_table,"\n";
  print $q->div($but->singlesubmit7("$edit","$save_key","$ib{$save_key}","margin:10px 20px;")),"\n";

 print "</div>";

}
1;
