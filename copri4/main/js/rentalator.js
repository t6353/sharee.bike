
function rental_request(url,bikeID,bookingID) {
 console.log('getURL: ' + url);
 console.log('bike: ' + bikeID);
 console.log('pos_id: ' + bookingID);
 if(bookingID){
   bikeID = bookingID;
   console.log('Take bikeID from requesting bookinID: ' + bikeID);
 }
 const button_reserv = document.querySelector('#button_reserv_' + bikeID);
 const button_cancel = document.querySelector('#button_cancel_' + bikeID);
 const return_state = document.querySelector('#return_state_' + bikeID);
 const reserved_bikes = document.querySelector('#reserved_bikes');

 $.getJSON( url, function( data ) {
  console.log('state=' + data.shareejson.state + '|bike: ' + data.shareejson.bike + '==' + bikeID + '| bookingID: ' + data.shareejson.pos_id + '==' + bookingID); 

  var DID = data.shareejson.bike;
  if(data.shareejson.pos_id && bookingID){
     DID = data.shareejson.pos_id;
     console.log('Take DID from responsing pos_id: ' + DID);
  }

  if(data.shareejson.response_state.match(/Failure/)){
	return_state.style = 'color:red';
  }

  if(data.shareejson.state == 'requested' && DID == bikeID){
    return_state.textContent = data.shareejson.response_text;
    button_reserv.style.display = 'none';
    button_cancel.style.display = 'block';
  }
  else if(data.shareejson.state == 'reserved' && DID == bikeID){
    return_state.textContent = data.shareejson.response_text;
    button_reserv.style.display = 'none';
    button_cancel.style.display = 'block';
  }
  else if(data.shareejson.state == 'available' && DID == bikeID){
    return_state.textContent = data.shareejson.response_text;
    button_cancel.style.display = 'none';
    button_reserv.style.display = 'block';
  }
  else {
    return_state.style = 'color:red';
    return_state.textContent = data.shareejson.response_text;
  }

  if(1 == 2){
   for (const key in data.shareejson.bikes_occupied) {
    console.log(key + ':' + data.shareejson.bikes_occupied[key].bike);
    reserved_bikes.textContent = data.shareejson.bikes_occupied[key].bike;
   }
  }

 });
}
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

var scrollToTopBtn = document.getElementById("scroll-top-wrapper");
var rootElement = document.documentElement;

function scrollToTop() {
  // Scroll to top logic
  rootElement.scrollTo({
    top: 0,
    behavior: "smooth"
  });
}
scrollToTopBtn.addEventListener("click", scrollToTop);


