
function change_fontsize1(){
     document.getElementById('Contenttxt').style.cssText = 'font-size:1em;';
}
function change_fontsize2(){
     document.getElementById('Contenttxt').style.cssText = 'font-size:1.2em;';
}
function change_fontsize3(){
     document.getElementById('Contenttxt').style.cssText = 'font-size:1.4em;';
}

function change_contrast(){
   var element = document.querySelectorAll('*');
   for (var i = 0; i < element.length; i++) {
     //const bgcol = element[i].style.backgroundColor;
     const style = getComputedStyle(element[i]);
     const bgcol = style.backgroundColor;
     console.log(bgcol);
     const fgcol = style.color;
     console.log(fgcol);

     if(bgcol == ('rgb(0, 156, 217)')){ //009cd9 (blue)
        element[i].style.backgroundColor = "rgb(63, 63, 63)";//3f3f3f (black)
        element[i].style.color = "rgb(255, 255, 254)";//fffffe (white)
     }
     if(bgcol == ('rgb(63, 63, 63)')){ //black
        element[i].style.backgroundColor = "rgb(0, 156, 217)";
        element[i].style.color = "rgb(255, 255, 254)";
     }
     //hover active
     if(bgcol == ('rgb(3, 101, 149)')){ //036595 (darkblue)
        element[i].style.backgroundColor = "rgb(255, 255, 254)";
        element[i].style.color = "rgb(63, 63, 63)";
     }
     //hover active
     if(bgcol == ('rgb(255, 255, 254)')){ //fffffe (white)
        element[i].style.backgroundColor = "rgb(3, 101, 149)";
        element[i].style.color = "rgb(255, 255, 254)";
     }

     //if only font ist colored and backround is white
     if(fgcol == ('rgb(0, 156, 217)') && bgcol == ('rgb(255, 255, 255)')){ //009cd9
        element[i].style.color = "rgb(63, 63, 63)";
        element[i].style.backgroundColor = "rgb(255, 255, 255)";
     }
     if(fgcol == ('rgb(63, 63, 63)') && bgcol == ('rgb(255, 255, 254)')){ //black
        element[i].style.color = "rgb(255, 255, 254)";
        element[i].style.backgroundColor = "rgb(3, 101, 149)";
     }
     if(fgcol == ('rgb(63, 63, 63)') && bgcol == ('rgb(255, 255, 255)')){ //black
        element[i].style.color = "rgb(0, 156, 217)";
        element[i].style.backgroundColor = "rgb(255, 255, 255)";
     }


   }
}

function show_passwd() {
  var x = document.getElementById('txt04');
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
  var y = document.getElementById('confirm_txt04');
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}

function WhichButton(event,URL){
if ('which' in event)
  {
  switch (event.which)
    {
    case 1:
	if (event.ctrlKey==1)
    	{
		var mainwindow = window.open(URL,"_self");
      		mainwindow.focus();
    	}	
      //alert ("Left mouse button was pressed");
      break;
    //case 2:
    //  alert ("Middle mouse button was pressed");
    //  break;
    case 3:
      var mainwindow = window.open(URL,"_self");
      mainwindow.focus();
      //alert ("Right mouse button was pressed");
      break;
    }
  }
}

//align element
function align_logo() {
   var w = window.outerWidth;
   walign = w / 100 * 16;
   w -= walign;
   document.getElementById("animate_logo_2").style.left = w + 'px';
}

function toggle_box(id) {
  var popup = document.getElementById(id);
  popup.classList.toggle("show");
}



//check all checkboxes
function CheckAll() {
 for (var i = 0; i < document.searchform.elements.length; i++) {
  if(document.searchform.elements[i].type == 'checkbox' && document.searchform.elements[i].name == 'ck4ex'){
    document.searchform.elements[i].checked = !(document.searchform.elements[i].checked);
  }
 }
}
function CheckListAll() {
 for (var i = 0; i < document.listform.elements.length; i++) {
  if(document.listform.elements[i].type == 'checkbox'){
    document.listform.elements[i].checked = !(document.listform.elements[i].checked);
  }
 }
}
function CheckEditAll() {
 for (var i = 0; i < document.editform.elements.length; i++) {
  if(document.editform.elements[i].type == 'checkbox'){
    document.editform.elements[i].checked = !(document.editform.elements[i].checked);
  }
 }
}

//post submit modal box position
function modal_position(xpos,ypos) { 
     var elx       = document.getElementById('xpos');
     var ely       = document.getElementById('ypos');
     var myDialogX = $("#dialog-form2").offset().left - 4;
     var myDialogY = $("#dialog-form2").offset().top - 44;
     //alert(myDialogX + '-----' + myDialogY);
     elx.value     = myDialogX;
     ely.value     = myDialogY;
}



function go2select (select) {
  var wert = select.options[select.options.selectedIndex].value;
   if (select == "nothing") {
     document.forms[0].reset();
     document.forms[0].elements[0].blur();
     return;
   } else {
     location.href = wert;
     document.forms[0].reset();
     document.forms[0].elements[0].blur();
     focus();
   }
}


