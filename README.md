**sharee.bike COPRI bike-sharing management system**

The web application offers the possibility to manage data and processes over several COPRI operator instances. We call this multi-operator capability, which enables a transparent network of bike-sharing companies. The sharee.bike app is already connected via the REST API.

The **sharee.bike app** can be found open source at https://dev.azure.com/TeilRad/sharee.bike%20App , or in the respective app/play stores.

The following features are currently implemented:

Administration of
- Rental bikes
- Rental bike stations
- Rental bike tariffs
- Rental bike users
- Discount promotions
- Coupons

Besides that
- sharee.bike App API
- Payment API
- Continuous rental bike booking journal
- Calendar booking
- Continuous cash journal
- Email messaging
- SMS messaging
- Location map and user registration can be integrated into websites via iFrame

There is also a wiki for installing copri operator-instances for your own and some API description https://gitlab.com/t6353/sharee.bike/-/wikis/home

Best regards,
Rainer Gümpelein
TeilRad GmbH
https://www.sharee.bike
